CREATE OR REPLACE PROCEDURE add_user (IN pv_id varchar(30)
                                                , IN pv_state varchar(30)
                                                , IN pv_createdDate int(13)
                                                , IN pv_lastLogin int(13)
                                                , IN pv_role varchar(30)
                                                , IN pv_signUpSource varchar(30)
                                                , IN pv_active boolean
                                                )
BEGIN
    INSERT INTO user
                (_id, 
                state, 
                createdDate, 
                lastLogin, 
                role,
                signUpSource,
                active) 
    VALUES      (pv_id, 
                pv_state, 
                pv_createdDate, 
                pv_lastLogin, 
                pv_role
                pv_signUpSource
                pv_active); 
END//

CREATE OR REPLACE PROCEDURE add_brand(IN pv_id varchar(30)
                                    , IN pv_name varchar(30)
                                    , IN pv_barcode varchar(30)
                                    , IN pv_brandCode varchar(30)
                                    , IN pv_category varchar(30)
                                    , IN pv_categoryCode varchar(30)
                                    , IN pv_cpg varchar(30)
                                    , IN pv_topbrand boolean
                                    )
BEGIN
    INSERT INTO brand(
        _id
        , name
        , barcode
        , brandCode
        , category
        , categoryCode
        , cpg
        , topbrand)
    VALUES(
        pv_id
        , pv_name
        , pv_barcode
        , pv_brandCode
        , pv_category
        , pv_categoryCode
        , pv_cpg
        , pv_topbrand
    );
END//

CREATE OR REPLACE PROCEDURE add_receipt(IN pv_id varchar(30)
                                    , IN pv_bonusPointsEarned int(9)
                                    , IN pv_bonusPointsEarnedReason varchar(100)
                                    , IN pv_createDate int(13)
                                    , IN pv_dateScanned int(13)
                                    , IN pv_finishedDate int(13)
                                    , IN pv_modifyDate int(13)
                                    , IN pv_pointsAwardedDate int(13)
                                    , IN pv_pointsEarned varchar(30)
                                    , IN pv_purchaseDate int(13)
                                    , IN pv_purchasedItemCount int(9)
                                    , IN pv_rewardsReceiptStatus varhar(30)
                                    , IN pv_totalSpent varchar(30)
                                    , IN pv_userId varchar(30)
                                    , IN pv_brandCode varchar(30)
                                    )
BEGIN
    INSERT INTO receipt(_id
            , bonusPointsEarned
            , bonusPointsEarnedReason
            , createDate
            , dateScanned
            , finishedDate
            , modifyDate
            , pointsAwardedDate
            , pointsEarned
            , purchaseDate
            , purchasedItemCount
            , rewardsReceiptStatus
            , totalSpent
            , userId
            , brandCode
            )
    VALUES(pv_id
            , pv_bonusPointsEarned
            , pv_bonusPointsEarnedReason
            , pv_createDate
            , pv_dateScanned
            , pv_finishedDate
            , pv_modifyDate
            , pv_pointsAwardedDate
            , pv_pointsEarned
            , pv_purchaseDate
            , pv_purchasedItemCount
            , pv_rewardsReceiptStatus
            , pv_totalSpent
            , pv_userId
            , pv_brandCode
    );
END//

CREATE TABLE IF NOT EXISTS receipt(
    _id varchar(30)
    , bonusPointsEarned int(9)
    , bonusPointsEarnedReason varchar(100)
    , createDate int(13)
    , dateScanned int(13)
    , finishedDate int(13)
    , modifyDate int(13)
    , pointsAwardedDate int(13)
    , pointsEarned varchar(30)
    , purchaseDate int(13)
    , purchasedItemCount int(9)
    , receipt_item_id int(9)
    , rewardsReceiptStatus varchar(30)
    , totalSpent varchar(30)
    , userId varchar(30)
);

CREATE TABLE IF NOT EXISTS receipt_item(
    id int(9) PRIMARY KEY auto_increment
    , barcode varchar(30)
    , brandCode varchar(30)
    , competitiveProduct boolean
    , competitorRewardsGroup varchar(100)
    , deleted boolean
    , description varchar(100)
    , discountedItemPrice varchar(30)
    , finalPrice varchar(30)
    , itemNumber varchar(30)
    , itemPrice varchar(30)
    , metabriteCampaignId varchar(100)
    , needsFetchReview boolean
    , needsFetchReviewReason varchar(100)
    , originalFinalPrice varchar(30)
    , originalMetaBriteBarcode varchar(30)
    , originalMetaBriteDescription varchar(100)
    , originalMetaBriteItemPrice varchar(30)
    , originalMetaBriteQuantityPurchased int(9)
    , originalReceiptItemText varchar(100)
    , partnerItemId varchar(30)
    , pointsEarned varchar(30)
    , pointsNotAwardedReason varchar(100)
    , pointsPayerId varchar(100)
    , preventTargetGapPoints boolean
    , priceAfterCoupon varchar(30)
    , quantityPurchased int(9)
    , rewardsGroup varchar(100)
    , rewardsProductPartnerId varchar(30)
    , targetPrice varchar(30)
    , userFlaggedBarcode varchar(30)
    , userFlaggedDescription varchar(100)
    , userFlaggedNewItem boolean
    , userFlaggedPrice varchar(30)
    , userFlaggedQuantity int(9)
);