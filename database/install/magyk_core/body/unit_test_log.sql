DELIMITER //

#282
CREATE OR REPLACE PROCEDURE get_unit_test_log_count(OUT rv_out INT)
	BEGIN
		SELECT COUNT(1) into rv_out from unit_test_log;
	END//


#283
CREATE OR REPLACE PROCEDURE get_unit_test_logs(IN pv_log_employee_id int(9))
	BEGIN
	SELECT unit_test_log.e_time, unit_test_log.flag
	, unit_test_log.id, unit_test_log.log_employee_id
	,  unit_test_log.passed, unit_test_log.proc_index_id
	, unit_test_log.test_group, unit_test_log.ttable
	FROM unit_test_log;
	END//

#284
CREATE OR REPLACE PROCEDURE add_unit_test_log(IN pv_flag int(3)
	                            ,IN pv_log_employee_id int(10)
	                            ,IN pv_passed int(10)
                                ,IN pv_proc_index_id varchar(100)
                                ,IN pv_table varchar(100)
	                            ,IN pv_test_group int(10)
                                , OUT rv_id int(10))
    BEGIN
        SET rv_id=-1;
        INSERT INTO unit_test_log(proc_index_id, test_group,        passed, flag,    e_time,               log_employee_id,  ttable)
        VALUES(                pv_proc_index_id, pv_test_group, pv_passed, pv_flag, UNIX_TIMESTAMP(NOW()), pv_log_employee_id, pv_table);
        SELECT max(id) from unit_test_log INTO rv_id;
    END//

CREATE OR REPLACE PROCEDURE get_max_test_group(OUT rv_group int(10))
    BEGIN
        SELECT MAX(test_group) from unit_test_log into rv_group;
    END//

CREATE OR REPLACE PROCEDURE get_test_group(OUT rv_test_id int(9)
					)
BEGIN
	SELECT MAX(test_group) into rv_test_id FROM unit_test_log;
END//
