import sys, time, datetime

import random
import string

import user_ut as uu
import company_ut as company


sys.path.append("/home/ec2-user/business/magyk")
import login
import connector as co


def procedure(name, params):
    print(f"procedure: {name}, params: {params}, GET")
    jdat = {"name": name, "params": params}
    items = co.machine(jdat)
    return items


def procedure_add(name, params):
    print(f"procedure: {name}, params: {params}, ADD")
    jdat = {"name": name, "params": params}
    item = co.machine_add(jdat)
    return item


def unindexed_proc_view_count():
    return procedure_add("unindexed_proc_view_count", [0])


def add_proc_log(date, name, params):
    return procedure_add("add_proc_log", [date, name, params, 0])


def delete_proc_log(pv_id):
    return procedure_add("delete_proc_log", [pv_id, 0])


def get_proc_index(user_id):
    return procedure("get_proc_indexes_api", [user_id])


def get_called_procs(user_id):
    return procedure("get_distinct_proc_logs", [user_id])


def get_unit_test_logs(user_id):
    return procedure("get_unit_test_logs", [user_id])


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        t_table = test["table"]
        t_test = test["test"]
        t_result = test["result"]
        t_proc_index_id = test["proc_index_id"]
        print(
            f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
        )


def unindexed_proc_view_tests():
    print("unindexed_proc_view_tests")
    start = unindexed_proc_view_count()
    print(f"start: {start}")
    params = ["string", 9, "number", 0]
    name = randomString()
    lcl_id = add_proc_log(str(datetime.datetime.now()), name, str(params))
    middle = unindexed_proc_view_count()
    print(f"proc log id: {lcl_id}, middle: {middle}")
    status = delete_proc_log(lcl_id)
    end = unindexed_proc_view_count()
    print(f"delete status: {status}, end size: {end}")
    if start < middle:
        if middle > end:
            return {
                "test": "test_view",
                "result": True,
                "table": "unindexed_proc_view_tests",
                "proc_index_id": [666],
            }
    return {
        "test": "test_view",
        "result": False,
        "table": "unindexed_proc_view_tests",
        "proc_index_id": [666],
    }


def get_proc_index_set(user_id):
    proc_index = get_proc_index(user_id)
    name_set = set()
    for p in proc_index:
        name_set.add(p["id"])
        s = f'id: {p["id"]}, name {p["name"]} desc: {p["description"]}'
        # print(s)
    return name_set


def get_tested_proc_ids(user_id):

    tested_set = set()
    lcl_logs = get_unit_test_logs(user_id)
    for log in lcl_logs:
        lcl_id = log["proc_index_id"]
        # print(f"proc_index_id: {lcl_id}")
        start = lcl_id[:1]
        if start == "[":
            lcl_id = lcl_id[1:-1]
            lcl_ids = lcl_id.split(",")
            for ll in lcl_ids:
                lll = ll.strip(" ")
                # print(f"ll: {lll}")
                tested_set.add(int(lll))
        else:
            # input(f"tested id: {lcl_id}")
            tested_set.add(int(lcl_id))
    return tested_set


def get_untested(user_id, display):
    proc_index = get_proc_index_set(user_id)
    proc_index_full = get_proc_index(user_id)
    proc_log = get_called_procs(user_id)
    untested_procs = []
    tested_proc_ids = get_tested_proc_ids(user_id)

    print(f"\nproc index ids: {proc_index}")
    print(f"\ntested_proc_ids: {tested_proc_ids}\n")

    remove = []
    for i in proc_index:
        for l in tested_proc_ids:
            # print(f"i: {i} l:{l}")
            if str(i) == str(l):
                print(f"    MATCH: {i}")
                remove.append(i)
    for b in remove:
        # print(f"remove: {b}")
        proc_index.remove(b)
    tmp_indexes = []
    count = 0
    for f in proc_index_full:
        match = False
        lcl_id = f["id"]
        for r in proc_index:
            # print(f"r: {r}, lcl_id: {lcl_id}")
            if r == lcl_id:
                match = True
        if not match:
            tmp_indexes.append(count)

        count += 1

    for t in reversed(tmp_indexes):
        # print(f"remove: {t}")
        proc_index_full.pop(t)

    cc = 0
    print("\nUntested: \n")
    for u in proc_index_full:
        cc += 1
        if cc < display:
            print("     NAME: {}, ID: {}".format(u["name"], u["id"]))
        if cc == display:
            print(f"\n More than {display} untested procs found \n")

    len_pif = len(proc_index_full)
    print(f"{len_pif} untested api procs")


def machine(cookie, display):
    tests = []
    user_id = cookie["user_id"]
    # tests.append(unindexed_proc_view_tests())
    get_untested(user_id, display)

    return tests


def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(stringLength))


def main():
    display = 50
    cookie = login.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie, display)
    test_printer(tests)


if __name__ == "__main__":
    main()
    # s = randomString()
    # print(s)
