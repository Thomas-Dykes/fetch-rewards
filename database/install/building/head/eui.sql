#calculate_EUI_api
call add_proc_index(
86
, 'checks permissions, then runs calculate_EUI_api'
, 0
, '{"pv_date": "date", "pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_EUI": "int(9)"}'
, '["eui"]'
, 'calculate_EUI_api'
, 1
);

#add_EUI_api
call add_proc_index(
87
, 'checks permissions, then runs add_EUI_api'
, 0
, '{"pv_building_id": "int(9)", "pv_service_month_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_EUI_id": "int(9)"}'
, '["eui"]'
, 'add_EUI_api'
, 1
);

#get_EUI_api
call add_proc_index(
88
, 'checks permissions, then runs get_EUI'
, 0
, '{"pv_building_id": "int(9", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["eui"]'
, 'get_EUI_api'
, 1
);

