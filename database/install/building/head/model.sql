
#add_model_api
call add_proc_index(
124
, 'checks permission, then runs add_model'
, 0
, '{"pv_building_id": "int(9)", "pv_slope_heating": "decimal(14,12)", "pv_slope_cooling": "decimal(14,12)", "pv_y_int_heating": "decimal(14,12)", "pv_y_int_cooling": "decimal(14)"
, "pv_threshold": "decimal(9,6)", "pv_r_squared": "decimal(6,5)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_model_id": "int(9)"}'
, '["model", "model_log"]'
, 'add_model_api'
, 1
);

#get_electric_models_api
call add_proc_index(
125
, 'checks permissions, then runs get_electric_models'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["model", "model_log"]'
, 'get_electric_models_api'
, 1
);


#get_gas_models_api
call add_proc_index(
126
, 'checks permissions, then runs get_gas_models'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["model", "model_log"]'
, 'get_gas_models_api'
, 1
);



#get_models_api
call add_proc_index(
127
, 'checks permissions, then runs get_models'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["model", "model_log"]'
, 'get_models_api'
, 1
);

#get_electric_model_api
call add_proc_index(
128
, 'checks permissions, then runs get_electric_model'
, 0
, '{"pv_model_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["model", "model_log"]'
, 'get_electric_model_api'
, 1
);

#get_gas_model_api
call add_proc_index(
129
, 'checks permissions, then runs get_gas_model'
, 0
, '{"pv_model_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["model", "model_log"]'
, 'get_gas_model_api'
, 1
);



#END