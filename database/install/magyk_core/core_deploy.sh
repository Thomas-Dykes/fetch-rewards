#!/bin/bash
set -exou pipefail

echo "set variables"
DB_USER=$2
HOST=$1
DB=$3
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

echo "begin core deploy"
/home/ec2-user/pbc_portal/database/install/magyk_core/table/table_deploy.sh $HOST $DB_USER $DB
/home/ec2-user/pbc_portal/database/install/magyk_core/body/body_deploy.sh $HOST $DB_USER $DB
/home/ec2-user/pbc_portal/database/install/magyk_core/head/head_deploy.sh $HOST $DB_USER $DB
/home/ec2-user/pbc_portal/database/install/magyk_core/data/data_deploy.sh $HOST $DB_USER $DB
echo "end core deploy"