USE mysql;
CREATE USER IF NOT EXISTS 'test_user'@'localhost' IDENTIFIED BY '';
CREATE USER IF NOT EXISTS 'svc_user'@'localhost' IDENTIFIED BY '';
UPDATE user set authentication_string=password('us@c@r0l1nA') where user = 'test_user';
UPDATE user set authentication_string=password('Cra6gr@55') where user = 'svc_user';

REVOKE ALL ON *.* FROM 'test_user'@'localhost';
GRANT SELECT ON pbc.rrole to 'test_user'@'localhost';

GRANT SELECT ON pbc.* TO 'test_user'@'localhost';
GRANT EXECUTE ON pbc.* TO 'test_user'@'localhost' IDENTIFIED BY 'us@c@r0l1nA' WITH GRANT OPTION;

REVOKE ALL ON *.* FROM 'svc_user'@'localhost';
GRANT SELECT ON pbc.rrole to 'svc_user'@'localhost';

GRANT SELECT ON pbc.* TO 'svc_user'@'localhost';
GRANT EXECUTE ON pbc.* TO 'svc_user'@'localhost' IDENTIFIED BY 'Cra6gr@55' WITH GRANT OPTION;

call pbc.add_company_full('fake_company', 1, '1990/01/01', 'a@a.a', 'First', 0, 'Last', 1, 5
, 'pwd', '1122334455', '2020/01/01', 'default_username', 'fake_city', 1, 'fake_address'
, '11111', 10000, 'fake_name', 1, '1980/01/01', @output);

FLUSH PRIVILEGES;
