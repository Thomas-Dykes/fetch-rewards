
#add_service_comment_api
call add_proc_index(
127
, 'checks permission, then runs add_service_comment'
, 0
, '{"pv_service_id": "int(9)", "pv_customer": "int(2)", "pv_comment": "varchar(100)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_comment_id": "int(9)"}'
, '["service_comment"]'
, 'add_service_comment_api'
, 1
);

#get_service_comments_api
call add_proc_index(
128
, 'checks permissions, then runs get_service_comments'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["service_comment"]'
, 'get_service_comments_api'
, 1
);

#get_services_comment_api
call add_proc_index(
129
, 'checks permissions, then runs get_services_comments'
, 0
, '{"pv_service_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["service_comment"]'
, 'get_service_comment_api'
, 1
);

#get_services_comments_api

call add_proc_index(
130
, 'checks permissions, then runs get_services_comments'
, 0
, '{"pv_service_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["service_comment"]'
, 'get_services_comments_api'
, 1
);

#update_my_service_comment_api

call add_proc_index(
131
, 'checks permission, then runs update_my_service_comment'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_service_comment_id": "int(9)", "pv_service_id": "int(9)", "pv_new_comment": "varchar(100)"}'
, '{}'
, '["service_comment"]'
, 'update_my_service_comment_api'
, 1
);

#delete_my_service_comment_api

call add_proc_index(
139
, 'checks permission, then runs delete_my_service_comment_api'
, 0
, '{"pv_flag": "int(3)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_service_comment_id": "int(9)"}'
, '{"rv_status_code": "int(9)"}'
, '["service_comment"]'
, 'delete_my_service_comment_api'
, 1
);

#END