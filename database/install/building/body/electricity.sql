DELIMITER //
CREATE OR REPLACE PROCEDURE calculate_model_usage_electricity_api(IN pv_electricity_id int(9)
                                                                , IN pv_model_log_id int(9)
                                                                , IN pv_user_id int(9)
                                                                , IN pv_ssession_number int(9)
                                                                , OUT rv_model_usage decimal(5,3)
                                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_model_usage_electricity(pv_electricity_id, pv_model_log_id, rv_model_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_model_usage_electricity(IN pv_electricity_id int(9)
                                            , IN pv_model_log_id int(9)
                                            , OUT rv_model_usage decimal(5,3)
                                            )
BEGIN
    declare lcl_oat int default 0;
    declare lcl_threshold int default 0;
    declare model_slope decimal(9,5) default 0;
    declare model_intercept decimal(6,3) default 0;
    declare lcl_area int default 0;
    declare lcl_service_days int default 0;
    UPDATE electricity set model_log_id = pv_model_log_id;
    SELECT electricity.OAT INTO lcl_oat FROM electricity WHERE electricity.id = pv_electricity_id;
    SELECT model_log.threshold INTO lcl_threshold FROM electricity INNER JOIN model_log ON electricity.model_log_id = model_log.id WHERE electricity.id = pv_electricity_id;
    IF (lcl_oat <= lcl_threshold) THEN
        SELECT model_log.slope_heating 
            INTO   model_slope 
            FROM   electricity INNER JOIN model_log ON electricity.model_log_id = model_log.id WHERE electricity.id = pv_electricity_id;

            SELECT model_log.y_int_heating 
            INTO   model_intercept 
            FROM   electricity INNER JOIN model_log ON electricity.model_log_id = model_log.id WHERE electricity.id = pv_electricity_id; 
    END IF;
    IF (lcl_oat > lcl_threshold) THEN
        SELECT model_log.slope_cooling INTO model_slope FROM electricity INNER JOIN model_log ON electricity.model_log_id = model_log.id WHERE electricity.id = pv_electricity_id;
        SELECT model_log.y_int_cooling INTO model_intercept FROM electricity INNER JOIN model_log ON electricity.model_log_id = model_log.id WHERE electricity.id = pv_electricity_id;
    END IF;
    SELECT lcl_oat*model_slope + model_intercept INTO rv_model_usage;
    -- UPDATE electricity set model_usage = rv_model_usage where id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE calculate_base_usage_electricity_api(IN pv_electricity_id int(9)
                                                            , IN pv_user_id int(9)
                                                            , IN pv_ssession_number int(9)
                                                            , OUT rv_base_usage int(9)
                                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_base_usage_electricity(pv_electricity_id, rv_base_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_base_usage_electricity(IN pv_electricity_id int(9)
                                            , IN pv_model_log_id int(9)
                                            , OUT rv_base_usage int(9))
BEGIN
    declare lcl_oat int(9) default 0;
    declare lcl_area int(9) default 0;
    declare lcl_service_days int(9) default 0;
    declare lcl_threshold int default 0;
    declare model_slope decimal(9,5) default 0;
    declare model_intercept decimal(6,3) default 0;
    SELECT electricity.building_area INTO lcl_area FROM 
    electricity INNER JOIN building ON electricity.building_id = building.id
    where electricity.id = pv_electricity_id;
    SELECT (DATEDIFF(electricity.end, electricity.start)+1) INTO lcl_service_days FROM
    electricity WHERE electricity.id = pv_electricity_id;
    select electricity.oat into lcl_oat FROM electricity where id = pv_electricity_id;
    SELECT model_log.threshold INTO lcl_threshold FROM model_log
    WHERE model_log.id = pv_model_log_id;
    IF (lcl_oat > lcl_threshold) THEN
        SELECT model_log.slope_cooling INTO model_slope
        FROM model_log WHERE id = pv_model_log_id;
        SELECT model_log.y_int_cooling INTO model_intercept
        FROM model_log WHERE id = pv_model_log_id;
    END IF;
    IF (lcl_oat <= lcl_threshold || lcl_threshold = 0) THEN
        SELECT model_log.slope_heating 
        INTO model_slope 
        FROM model_log WHERE id = pv_model_log_id;

        SELECT model_log.y_int_heating 
        INTO model_intercept 
        FROM model_log WHERE id = pv_model_log_id;
    END IF;

    SELECT (((lcl_oat*model_slope + model_intercept)*lcl_area*lcl_service_days*.024)+1) INTO rv_base_usage;
    UPDATE electricity set base_usage = rv_base_usage where id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE calculate_saved_usage_electricity_api(IN pv_electricity_id int(9)
                                                        , IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , OUt rv_saved_usage int(9)
                                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_saved_usage_electricity(pv_electricity_id, rv_saved_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_saved_usage_electricity(IN pv_electricity_id int(9)
                                                , OUT rv_saved_usage int(9)
                                                )
BEGIN
    SELECT electricity.base_usage - electricity.uusage  into rv_saved_usage FROM electricity where id = pv_electricity_id;
    -- UPDATE electricity set saved_usage = rv_saved_usage where id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE calculate_percent_savings_electricity_api(IN pv_electricity_id int(9)
                                                                , IN pv_user_id int(9)
                                                                , IN pv_ssession_number int(9)
                                                                , OUT rv_percent_savings decimal(6,3)
                                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_percent_savings_electricity(pv_electricity_id, rv_percent_savings_electricity);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_percent_savings_electricity(IN pv_electricity_id int(9)
                                                , OUT rv_percent_savings decimal(12,6)
                                                )
BEGIN
    select electricity.saved_usage/electricity.base_usage INTO rv_percent_savings FROM electricity where id = pv_electricity_id;
    -- update electricity set percent_savings = rv_percent_savings where id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE calculate_norm_usage_electricity_api(IN pv_electricity_id int(9)
                                                            , IN pv_user_id int(9)
                                                            , IN pv_ssession_number int(9)
                                                            , OUT rv_norm_usage decimal(5,3)
                                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_norm_usage_electricity(pv_electricity_id, rv_norm_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_norm_usage_electricity(IN pv_electricity_id int(9)
                                                , OUT rv_norm_usage decimal(9,5)
                                                )
BEGIN
    select electricity.uusage*1000/24/electricity.building_area/electricity.service_days into rv_norm_usage
    FROM building INNER JOIN electricity ON building.id = electricity.building_id
    WHERE electricity.id = pv_electricity_id;
    -- UPDATE electricity set norm_usage = rv_norm_usage where id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE get_electricity_ids_api(IN pv_service_month_id_start int(9)
                                                , IN pv_service_month_id_end int(9)
                                                , IN pv_building_id int(9)
                                                , IN pv_user_id int(9)
                                                , IN pv_ssession_number int(9)
                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_electricity_ids(pv_building_id, pv_service_month_id_start, pv_service_month_id_end);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_electricity_ids(IN pv_building_id int(9)
                                                , IN pv_service_month_id_start int(9)
                                                , IN pv_service_month_id_end int(9)
                                                )
BEGIN
    declare lcl_start_date date;
    declare lcl_end_date date;
    select electricity.id
    FROM electricity
    WHERE electricity.service_month_id BETWEEN pv_service_month_id_start AND pv_service_month_id_end
    and electricity.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_previous_electricity_uusage(IN pv_building_id int(9)
                                            , IN pv_service_month_id_begin int(9)
                                            , IN pv_service_month_id_end int(9)
                                            , OUT rv_sum_usage int(9)
                                            )
BEGIN
    declare lcl_present date;
    declare lcl_past date;
    select service_month.month_date into lcl_past from service_month where id = pv_service_month_id_begin; 
    select service_month.month_date into lcl_present from service_month where id = pv_service_month_id_end; 
    SELECT SUM(electricity.uusage)*12/(pv_service_month_id_end-pv_service_month_id_begin) INTO rv_sum_usage FROM electricity where electricity.start between lcl_past and lcl_present AND electricity.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE find_electricity_costs_api(IN pv_user_id int(9)
                                                , IN pv_ssession_number int(9)
                                                , IN pv_building_id int(9)
                                                , IN pv_service_month_id_begin int(9)
                                                , IN pv_service_month_id_end int(9)
                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call find_electricity_costs(pv_building_id, pv_service_month_id_begin, pv_service_month_id_end);
    END IF;
END//

CREATE OR REPLACE PROCEDURE find_electricity_costs(IN pv_building_id int(9)
                                                , IN pv_service_month_id_begin int(9)
                                                , IN pv_service_month_id_end int(9)
                                                )
BEGIN
    select CAST(cost AS CHAR) cost, id from electricity where electricity.service_month_id between (pv_service_month_id_begin) and pv_service_month_id_end and electricity.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE calculate_monthly_savings_electricity_api(IN pv_electricity_id int(9)
                                                                , IN pv_building_id int(9)
                                                                , IN pv_user_id int(9)
                                                                , IN pv_ssession_number int(9)
                                                                , OUT rv_monthly_savings decimal(12,5)
                                                                )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_monthly_savings_electricity(pv_electricity_id, pv_building_id, rv_monthly_savings);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_monthly_savings_electricity(IN pv_electricity_id int(9)
                                                                , IN model_log_id int(9)
                                                                , IN pv_building_id int(9)
                                                                , OUT rv_monthly_savings decimal(12,5)
                                                )
BEGIN
    declare lcl_monthly_kwh decimal(11,5) default 0;
    declare lcl_electricity_cost decimal(7,5) default 0;
    select model_log.model_price INTO lcl_electricity_cost FROM model_log where id = model_log_id;
    select electricity.saved_usage INTO lcl_monthly_kwh FROM electricity WHERE id = pv_electricity_id;
    set rv_monthly_savings = lcl_monthly_kwh*lcl_electricity_cost;
    -- UPDATE electricity SET monthly_savings = rv_monthly_savings where id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE add_cumulative_savings_electricity_api(IN pv_electricity_id int(9)
                                                                , IN pv_cumulative_savings decimal(9,2)
                                                                , IN pv_user_id int(9)
                                                                , IN pv_ssession_number int(9)
                                                                )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_cumulative_savings_electricity(pv_electricity_id, pv_cumulative_savings);
    END IF;
END//

CREATE OR REPLACE PROCEDURE add_cumulative_savings_electricity(IN pv_electricity_id int(9)
                                                , IN pv_cumulative_savings decimal(11,2)
                                            )
BEGIN
    -- UPDATE electricity SET cumulative_savings = pv_cumulative_savings WHERE id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE add_electricity_pre_period_raw_api(IN pv_usage int(9)
                                            , IN pv_adjustment int(9)
                                            , IN pv_building_id int(9)
                                            , IN pv_building_area int(9)
                                            , IN pv_oat int(3)
                                            , IN pv_start date
                                            , IN pv_end date
                                            , IN pv_cost decimal(11,3)
                                            , IN pv_service_month_id int(9)
                                            , IN pv_log_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , OUT rv_electricity_id int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_log_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_electricity_pre_period_raw(pv_usage, pv_adjustment, pv_building_id, pv_building_area, pv_oat, pv_start, pv_end, pv_service_month_id, pv_cost, pv_log_user_id, rv_electricity_id);
    END IF;  
END//

CREATE OR REPLACE PROCEDURE add_electricity_pre_period_raw(IN pv_usage int(9)
                                            , IN pv_adjustment int(9)
                                            , IN pv_building_id int(9)
                                            , IN pv_building_area int(9)
                                            , IN pv_oat int(3)
                                            , IN pv_start date
                                            , IN pv_end date
                                            , IN pv_service_month_id int(9)
                                            , IN pv_cost decimal(11,3)
                                            , IN pv_log_user_id int(9)
                                            , OUT rv_electricity_id int(9)
)
BEGIN
    DECLARE lcl_company_id INT DEFAULT 0; 
    DECLARE lcl_norm_usage DECIMAL(5, 3) DEFAULT 0; 
    DECLARE lcl_model_usage DECIMAL(5, 3) DEFAULT 0;
    DECLARE lcl_electricity_eui int default 0;
    DECLARE lcl_is_existing int default 0;
    declare lcl_output decimal(8,4) default 0;
    declare lcl_min_electricity_id int(9) default 0;
    declare lcl_building_area int(9) default 0;
    declare rv_price decimal(7,4) default 0;
    call get_user_company_id(pv_log_user_id, lcl_company_id);
    set lcl_building_area = pv_building_area;
    IF lcl_building_area = 0 THEN
        select building.area into lcl_building_area FROM building where building.id = pv_building_id;
    END IF;
    INSERT INTO electricity 
                (uusage, 
                model_log_id, 
                norm_usage, 
                model_usage, 
                adjustment, 
                adjusted_usage, 
                building_id, 
                building_area,
                oat, 
                base_usage, 
                start, 
                end, 
                service_days, 
                service_month_id,
                service_id,
                cost, 
                saved_usage, 
                percent_savings, 
                monthly_savings, 
                cumulative_savings, 
                e_time, 
                process_e_time, 
                flag, 
                company_id, 
                log_user_id) 
    VALUES      (pv_usage, 
                0,
                0, 
                0, 
                0, 
                0, 
                pv_building_id,
                lcl_building_area, 
                pv_oat, 
                0, 
                pv_start, 
                pv_end, 
                0, 
                pv_service_month_id,
                0,
                pv_cost, 
                0, 
                0, 
                0, 
                0, 
                Unix_timestamp(Now()), 
                Unix_timestamp(Now()), 
                0, 
                lcl_company_id, 
                pv_log_user_id); 

    SELECT Max(id) 
    INTO   rv_electricity_id 
    FROM   electricity; 

    UPDATE electricity 
    SET    service_days = (Datediff(end, start) + 1 );

    CALL calculate_norm_usage_electricity(rv_electricity_id, lcl_norm_usage); 

    UPDATE electricity 
    SET    norm_usage = lcl_norm_usage 
    WHERE  id = rv_electricity_id; 

    UPDATE electricity 
    SET    adjusted_usage = (pv_usage + pv_adjustment) 
    WHERE  id = rv_electricity_id;
    select max(company.id) into lcl_company_id FROM company;
    UPDATE electricity
    SET company_id = lcl_company_id;

    call calculate_eui_electricity(pv_service_month_id, pv_building_id, lcl_electricity_eui);
    UPDATE electricity set dollar_per_kwh = pv_cost/pv_usage;
    -- call get_min_electricity_id(pv_log_user_id, pv_building_id, lcl_min_electricity_id);
    -- call calculate_baseline_electricity_price(pv_building_id, lcl_min_electricity_id, pv_service_month_id, rv_price)
    call add_EUIs(pv_building_id, lcl_output);
END//

CREATE OR REPLACE PROCEDURE get_electricity_graph_data_api(IN pv_user_id int(10)
                                                    , IN pv_ssession_number int(10)
                                                    , IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_electricity_graph_data(pv_building_id, pv_service_month_start_id, pv_service_month_end_id);
    END IF; 
END//


CREATE OR REPLACE PROCEDURE get_electricity_graph_data(IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_begin_date date;
    declare lcl_end_date date;
    SELECT a.adjusted_usage
        , a.base_usage base_usage
        , a.company_id
        , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from electricity a, service_month b
    where a.service_month_id = b.id
    and a.service_month_id between pv_service_month_start_id and pv_service_month_end_id
    and a.building_id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE get_electricity_price_graph_data_api(IN pv_user_id int(10)
                                                    , IN pv_ssession_number int(10)
                                                    , IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_electricity_price_graph_data(pv_building_id, pv_service_month_start_id, pv_service_month_end_id);
    END IF; 
END//


CREATE OR REPLACE PROCEDURE get_electricity_price_graph_data(IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_begin_date date;
    declare lcl_end_date date;
    SELECT CAST(a.dollar_per_kwh AS CHAR) dollar_per_kwh
        , a.company_id
        , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from electricity a, service_month b
    where a.service_month_id = b.id
    and a.service_month_id between pv_service_month_start_id and pv_service_month_end_id
    and a.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_electricity_usage_cost_graph_data_api(IN pv_user_id int(10)
                                                    , IN pv_ssession_number int(10)
                                                    , IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_electricity_usage_cost_graph_data(pv_building_id, pv_service_month_start_id, pv_service_month_end_id);
    END IF; 
END//


CREATE OR REPLACE PROCEDURE get_electricity_usage_cost_graph_data(IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_begin_date date;
    declare lcl_end_date date;
    SELECT a.uusage
        , CAST(a.cost AS CHAR) cost
        , a.company_id
        , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from electricity a, service_month b
    where a.service_month_id = b.id
    and a.service_month_id between pv_service_month_start_id and pv_service_month_end_id
    and a.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_electricity_norm_usage_oat_graph_data_api(IN pv_user_id int(10)
                                                    , IN pv_ssession_number int(10)
                                                    , IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_electricity_norm_usage_oat_graph_data(pv_building_id, pv_service_month_start_id, pv_service_month_end_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE test()
BEGIN 
    SELECT '';
END//


CREATE OR REPLACE PROCEDURE get_electricity_norm_usage_oat_graph_data(IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_begin_date date;
    declare lcl_end_date date;
    SELECT CAST(a.norm_usage AS CHAR) norm_usage
        , CAST(a.oat AS CHAR) oat
        , a.company_id
        -- , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from electricity a, service_month b
    where a.service_month_id = b.id
    and a.service_month_id between pv_service_month_start_id and pv_service_month_end_id
    and a.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_oats_electricity_between_api(IN pv_building_id int(9)
                                        , IN pv_service_month_id_begin int(9)
                                        , IN pv_service_month_id_end int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_oats_electricity_between(pv_user_id, pv_building_id, pv_service_month_id_begin, pv_service_month_id_end);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_oats_electricity_between(IN pv_user_id int(9)
                                    , IN pv_building_id int(9)
                                    , IN pv_service_month_id_begin int(9)
                                    , IN pv_service_month_id_end int(9)
                                    )
BEGIN
    select electricity.oat from electricity
    where electricity.service_month_id between pv_service_month_id_begin and pv_service_month_id_end
    AND building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_norm_usages_between_electricity_api(IN pv_building_id int(9)
                                        , IN pv_service_month_id_begin int(9)
                                        , IN pv_service_month_id_end int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_norm_usages_electricity_between(pv_user_id, pv_building_id, pv_service_month_id_begin, pv_service_month_id_end);
    END IF;
END//


CREATE OR REPLACE PROCEDURE get_norm_usages_electricity_between(IN pv_user_id int(9)
                                            , IN pv_building_id int(9)
                                            , IN pv_service_month_id_begin int(9)
                                            , IN pv_service_month_id_end int(9)
                                            )
BEGIN
    select electricity.norm_usage from electricity
    where electricity.service_month_id between pv_service_month_id_begin AND pv_service_month_id_end
    AND electricity.building_id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE get_savings_electricity_between_api(IN pv_service_month_id_start int(9)
                                        , IN pv_service_month_id_end int(9)
                                        , IN pv_building_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_savings_electricity_between(pv_service_month_id_begin, pv_service_month_id_end, pv_user_id, pv_building_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_savings_electricity_between(IN pv_service_month_id_begin int(9)
                                    , IN pv_service_month_id_end int(9)
                                    , IN pv_user_id int(9)
                                    , IN pv_building_id int(9)

                                    )
BEGIN
    select electricity.monthly_savings from electricity
    where electricity.service_month_id between pv_service_month_id_begin AND pv_service_month_id_end
    AND electricity.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_service_month_year_electricity_between_api(IN pv_service_month_id_begin int(9)
                                        , IN pv_service_month_end int(9)
                                        , IN pv_building_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_service_month_year_electricity_between(pv_user_id, pv_building_id, pv_service_month_id_begin, pv_service_month_id_end);
    END IF;
END//


CREATE OR REPLACE PROCEDURE get_service_month_year_electricity_between(IN pv_service_month_id_begin int(9)
                                            , IN pv_service_month_id_end int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_building_id int(9)

                                            )
BEGIN
    select service_month.month, service_month.year from service_month INNER JOIN electricity ON service_month.id = electricity.service_month_id
    where service_month.id between pv_service_month_id_begin and pv_service_month_id_end
    AND electricity.building_id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE get_electricity_usage_data_api(IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , IN pv_building_id int(9)
                                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_electricity_usage_data(pv_user_id, pv_building_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_electricity_usage_data(IN pv_user_id int(9)
                                                        , IN pv_building_id int(9)
                                                    )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    
    SELECT CONCAT(service_month.month,'-', service_month.year)                    service_month, 
        electricity.uusage, 
        Cast(electricity.base_usage AS CHAR)  base_usage, 
        Cast(electricity.norm_usage AS CHAR)  norm_usage, 
        Cast(electricity.model_usage AS CHAR) model_usage, 
        CAST(electricity.building_area AS CHAR) area,
        Cast(electricity.oat AS CHAR) oat, 
        electricity.company_id,
        electricity.start,
        electricity.end,
        CAST(electricity.adjustment AS CHAR) adjustment,
        electricity.id
    FROM   electricity 
        INNER JOIN service_month 
                ON electricity.service_month_id = service_month.id 
    WHERE  electricity.building_id = pv_building_id; 

END//

CREATE OR REPLACE PROCEDURE get_electricity_savings_data_api(IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , IN pv_building_id int(9)
                                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_electricity_savings_data(pv_user_id, pv_building_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_electricity_savings_data(IN pv_user_id int(9)
                                                        , IN pv_building_id int(9)
                                                        )
BEGIN 
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    
    SELECT CONCAT(service_month.month,'-', service_month.year) service_month, 
        Cast(electricity.saved_usage AS CHAR)  saved_usage, 
        Cast(electricity.percent_savings AS CHAR)  percent_savings, 
        Cast(electricity.monthly_savings AS CHAR) monthly_savings, 
        Cast(electricity.cumulative_savings AS CHAR) cumulative_savings, 
        electricity.company_id 
    FROM   electricity 
        INNER JOIN service_month 
                ON electricity.service_month_id = service_month.id 
    WHERE  electricity.building_id = pv_building_id; 
END//


CREATE OR REPLACE PROCEDURE get_cumulative_savings_post_period_api(IN pv_building_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_cumulative_savings_post_period(pv_building_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_cumulative_savings_post_period(IN pv_building_id int(9)
                                                                , IN pv_user_id int(9)
                                                                )
BEGIN
    select gas.start, gas.end, service_month.month_date savings_date
    , CAST(ROUND(electricity.cumulative_savings, 2) AS CHAR) electricity_savings
    , CAST(ROUND(gas.cumulative_savings, 2) AS CHAR) gas_savings
    , CAST(ROUND(electricity.monthly_savings+gas.monthly_savings, 2) AS CHAR) monthly_savings
    , CAST(ROUND(electricity.cumulative_savings + gas.cumulative_savings, 2) AS CHAR) cumulative_savings
    FROM electricity INNER JOIN service_month ON electricity.service_month_id = service_month.id
    INNER JOIN gas ON service_month.id = gas.service_month_id
    WHERE electricity.base_usage > 0 AND gas.base_usage > 0 AND electricity.building_id = pv_building_id AND gas.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_cumulative_savings_between_api(IN pv_building_id int(9)
                                            , IN pv_service_month_id_start int(9)
                                            , IN pv_service_month_id_end int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_cumulative_savings_post_period(pv_building_id, pv_service_month_id_start, pv_service_month_id_end, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_cumulative_savings_between(IN pv_building_id int(9)
                                                                , IN pv_service_month_id_start int(9)
                                                                , IN pv_service_month_id_end int(9)
                                                                , IN pv_user_id int(9)
                                                                )
BEGIN
    select gas.start, gas.end, service_month.month_date savings_date
    , CAST(ROUND(electricity.cumulative_savings, 2) AS CHAR) electricity_savings
    , CAST(ROUND(gas.cumulative_savings, 2) AS CHAR) gas_savings
    , CAST(ROUND(electricity.monthly_savings+gas.monthly_savings, 2) AS CHAR) monthly_savings
    , CAST(ROUND(electricity.cumulative_savings + gas.cumulative_savings, 2) AS CHAR) cumulative_savings
    FROM electricity INNER JOIN service_month ON electricity.service_month_id = service_month.id
    INNER JOIN gas ON service_month.id = gas.service_month_id
    WHERE service_month.id BETWEEN pv_service_month_id_start AND pv_service_month_id_end
    AND gas.building_id = pv_building_id and electricity.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_cumulative_savings_electricity(IN pv_building_id int(9)
                                                                , IN pv_service_month_id int(9)
                                                                , OUT rv_cumulative_savings varchar(10)
                                                                )
BEGIN
    SELECT CAST(ROUND(electricity.cumulative_savings, 2) AS CHAR) electricity_savings INTO rv_cumulative_savings FROM electricity
    WHERE electricity.service_month_id = pv_service_month_id and electricity.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_adjusted_usage_electricity_api(IN pv_electricity_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , OUT rv_adjusted_usage int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_adjusted_usage_electricity(pv_electricity_id, rv_adjusted_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_adjusted_usage_electricity(IN pv_electricity_id int(9)
                                                                , OUT rv_adjusted_usage int(9)
                                                                )
BEGIN
    SELECT electricity.adjusted_usage INTO rv_adjusted_usage FROM electricity
    WHERE electricity.id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE get_cumulative_savings_post_period_electricity_api(IN pv_building_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_cumulative_savings_post_period_electricity(pv_building_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_cumulative_savings_post_period_electricity(IN pv_building_id int(9)
                                                                , IN pv_user_id int(9)
                                                                )
BEGIN
    select service_month.month_date savings_date, CAST(ROUND(electricity.cumulative_savings, 2) AS CHAR) savings
    FROM electricity INNER JOIN service_month ON electricity.service_month_id = service_month.id
    WHERE electricity.base_usage > 0 AND electricity.building_id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE get_dollar_per_kwh_electricity_api(IN pv_building_id int(9)
                                            , IN pv_service_month_id_begin int(9)
                                            , IN pv_service_month_id_end int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_dollar_per_kwh_electricity(pv_building_id, pv_service_month_id_begin, pv_service_month_id_end);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_dollar_per_kwh_electricity(IN pv_building_id int(9)
                                                                , IN pv_service_month_id_begin int(9)
                                                                , IN pv_service_month_id_end int(9)
                                                                )
BEGIN
    select service_month.month_date savings_date, CAST(ROUND(electricity.dollar_per_kwh, 3) AS CHAR) price, electricity.id 
    FROM electricity INNER JOIN service_month ON electricity.service_month_id = service_month.id
    WHERE electricity.service_month_id BETWEEN pv_service_month_id_begin AND pv_service_month_id_end AND electricity.building_id = pv_building_id;
END//




CREATE OR REPLACE PROCEDURE get_min_electricity_id(IN pv_user_id int(9)
                                                    , IN pv_building_id int(9)
                                                    , OUT rv_electricity_id int(9)
                                                    )
BEGIN
    select min(id) into rv_electricity_id FROM electricity WHERE log_user_id = pv_user_id and building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE update_electricity_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_electricity_id int(9)
                                            , IN pv_new_service_start varchar(30)
                                            , IN pv_new_service_end varchar(30)
                                            , IN pv_new_usage int(9)
                                            , IN pv_new_adjustment decimal(11,3)
                                            , IN pv_new_service_month_id int(9)
                                            , IN pv_new_oat int(9)
                                            , IN pv_new_cost decimal(11,3)
                                            , IN pv_new_building_area int(9)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call update_electricity(pv_electricity_id, pv_new_service_start, pv_new_service_end, pv_new_usage, pv_new_adjustment, pv_new_service_month_id, pv_new_oat, pv_new_cost, pv_new_building_area);
    END IF;
END//


CREATE OR REPLACE PROCEDURe update_electricity(IN pv_electricity_id int(9)
                                            , IN pv_new_service_start varchar(30)
                                            , IN pv_new_service_end varchar(30)
                                            , IN pv_new_usage int(9)
                                            , IN pv_new_adjustment decimal(11,3)
                                            , IN pv_new_service_month_id int(9)
                                            , IN pv_new_oat int(9)
                                            , IN pv_new_cost decimal(11,3)
                                            , IN pv_new_building_area int(9)
                                            )
BEGIN
    update electricity SET 
    start = pv_new_service_start,
    end = pv_new_service_end,
    uusage = pv_new_usage,
    adjustment = pv_new_adjustment,
    service_month_id = pv_new_service_month_id,
    oat = pv_new_oat,
    cost = pv_new_cost,
    building_area = pv_new_building_area
    WHERE id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE update_electricity_model_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_electricity_id int(9)
                                            , IN pv_norm_usage decimal(5,3)
                                            , IN pv_model_usage decimal(5,3)
                                            , IN pv_base_usage int(10)
                                            , IN pv_saved_usage int(9)
                                            , IN pv_percent_savings decimal(12,6)
                                            , IN pv_monthly_savings decimal(16,10)
                                            , IN pv_cumulative_savings decimal(12,2)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call update_electricity_model(pv_electricity_id, pv_norm_usage, pv_model_usage, pv_base_usage, pv_saved_usage, pv_percent_savings, pv_monthly_savings, pv_cumulative_savings);
    END IF;
END//


CREATE OR REPLACE PROCEDURe update_electricity_model(IN pv_electricity_id int(9)
                                            , IN pv_norm_usage decimal(5,3)
                                            , IN pv_model_usage decimal(5,3)
                                            , IN pv_base_usage int(10)
                                            , IN pv_saved_usage int(9)
                                            , IN pv_percent_savings decimal(12,6)
                                            , IN pv_monthly_savings decimal(16,10)
                                            , IN pv_cumulative_savings decimal(12,2)
                                            )
BEGIN
    update electricity SET 
    norm_usage = pv_norm_usage,
    model_usage = pv_model_usage,
    base_usage = pv_base_usage,
    saved_usage = pv_saved_usage,
    percent_savings = pv_percent_savings,
    monthly_savings = pv_monthly_savings,
    cumulative_savings = pv_cumulative_savings
    WHERE id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE delete_buildings_electricity_data_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_building_id int(9)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call delete_buildings_electricity_data(pv_building_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE delete_buildings_electricity_data(IN pv_building_id int(9))
BEGIN 
    delete from electricity where building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE delete_electricity_api(IN pv_flag int(3)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_electricity_id int(9)
                                            , OUT rv_status_code int(9)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call delete_electricity(pv_flag, pv_electricity_id, rv_status_code);
    END IF;
END//

CREATE OR REPLACE PROCEDURE delete_electricity(IN pv_flag int(3)
                                                , IN pv_electricity_id int(9)
                                                , OUT rv_status_code int(9)
                                                )
BEGIN
    declare lcl_outbound_id int(9) default 0;
	declare rv_electricity_json varchar(500) default '';
	declare lcl_uusage int(9) default 0;
	declare lcl_model_log_id int(9) default 0;
	declare lcl_norm_usage decimal(5,3) default 0;
	declare lcl_model_usage decimal(5,3) default 0;
	declare lcl_adjustment int(9) default 0;
	declare lcl_adjusted_usage int(10) default 0;
    declare lcl_building_id int(9) default 0;
    declare lcl_oat decimal(7,3) default 0;
    declare lcl_base_usage int(9) default 0;
    declare lcl_start varchar(30) default 0;
    declare lcl_end varchar(30) default 0;
    declare lcl_service_days int(9) default 0;
    declare lcl_service_month_id int(9) default 0;
    declare lcl_service_id int(9) default 0;
    declare lcl_cost decimal(11,3) default 0;
    declare lcl_dollar_per_kwh decimal(5,3) default 0;
    declare lcl_post_period_id int(9) default 0;
    declare lcl_saved_usage int(9) default 0;
    declare lcl_percent_savings decimal(6,3) default 0;
    declare lcl_monthly_savings decimal(16,3) default 0;
    declare lcl_cumulative_savings decimal(15,6) default 0;
    declare lcl_e_time int(13) default 0;
    declare lcl_flag int(3) default 0;
    declare lcl_company_id int(9) default 0;
    declare lcl_log_user_id int(9) default 0;

	declare lcl_uusage_string varchar(40) default 0;
	declare lcl_model_log_id_string varchar(40) default 0;
	declare lcl_norm_usage_string varchar(40) default 0;
	declare lcl_model_usage_string varchar(40) default 0;
	declare lcl_adjustment_string varchar(40) default 0;
	declare lcl_adjusted_usage_string varchar(40) default 0;
    declare lcl_building_id_string varchar(40) default 0;
    declare lcl_oat_string varchar(40) default 0;
    declare lcl_base_usage_string varchar(40) default 0;
    declare lcl_start_string varchar(40) default 0;
    declare lcl_end_string varchar(40) default 0;
    declare lcl_service_days_string varchar(40) default 0;
    declare lcl_service_month_id_string varchar(40) default 0;
    declare lcl_service_id_string varchar(40) default 0;
    declare lcl_cost_string varchar(40) default 0;
    declare lcl_dollar_per_kwh_string varchar(40) default 0;
    declare lcl_post_period_id_string varchar(40) default 0;
    declare lcl_saved_usage_string varchar(40) default 0;
    declare lcl_percent_savings_string varchar(40) default 0;
    declare lcl_monthly_savings_string varchar(40) default 0;
    declare lcl_cumulative_savings_string varchar(40) default 0;
    declare lcl_e_time_string varchar(40) default 0;
    declare lcl_flag_string varchar(40) default 0;
    declare lcl_company_id_string varchar(40) default 0;
    declare lcl_log_user_id_string varchar(40) default 0;
	DECLARE EXIT HANDLER FOR 1451
	-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails 
	BEGIN
		select -1 into rv_status_code;
	END;
	select electricity.uusage INTO lcl_uusage from electricity where electricity.id = pv_electricity_id;
	select electricity.model_log_id into lcl_model_log_id from electricity where electricity.id = pv_electricity_id;
	select electricity.norm_usage into lcl_norm_usage from electricity where electricity.id = pv_electricity_id;
	select electricity.model_usage into lcl_model_usage from electricity where electricity.id = pv_electricity_id;
    select electricity.adjustment into lcl_adjustment from electricity where electricity.id = pv_electricity_id;
    select electricity.adjusted_usage into lcl_adjusted_usage from electricity where electricity.id = pv_electricity_id;
    select electricity.building_id into lcl_building_id from electricity where electricity.id = pv_electricity_id;
	select electricity.oat into lcl_oat from electricity where electricity.id = pv_electricity_id;
    select electricity.base_usage into lcl_base_usage from electricity where electricity.id = pv_electricity_id;
    select electricity.start into lcl_start from electricity where electricity.id = pv_electricity_id;
    select electricity.end into lcl_end from electricity where electricity.id = pv_electricity_id;
    select electricity.service_days from electricity where electricity.id = pv_electricity_id;
    select electricity.service_month_id into lcl_service_month_id from electricity where electricity.id = pv_electricity_id;
    select electricity.service_id into lcl_service_id from electricity where electricity.id = pv_electricity_id;
    select electricity.cost into lcl_cost from electricity where electricity.id = pv_electricity_id;
    select electricity.dollar_per_kwh into lcl_dollar_per_kwh from electricity where electricity.id = pv_electricity_id;
    select electricity.post_period_id into lcl_post_period_id from electricity where electricity.id = pv_electricity_id;
    select electricity.saved_usage into lcl_saved_usage from electricity where electricity.id = pv_electricity_id;
    select electricity.percent_savings into lcl_percent_savings from electricity where electricity.id = pv_electricity_id;
    select electricity.cumulative_savings into lcl_cumulative_savings from electricity where electricity.id = pv_electricity_id;
    select electricity.e_time into lcl_e_time from electricity where electricity.id = pv_electricity_id;
    select electricity.flag into lcl_flag from electricity where electricity.id = pv_electricity_id;
    select electricity.company_id into lcl_company_id from electricity where electricity.id = pv_electricity_id;
    select electricity.log_user_id INTO lcl_log_user_id from electricity where electricity.id = pv_electricity_id;

	call jsonify_int(lcl_uusage, 'uusage', lcl_uusage_string);
	call jsonify_int(lcl_model_log_id, 'model_log_id', lcl_model_log_id_string);
	call jsonify_decimal(lcl_norm_usage, 'norm_usage', lcl_norm_usage_string);
	call jsonify_decimal(lcl_model_usage, 'model_usage', lcl_model_usage_string);
	call jsonify_int(lcl_adjustment, 'adjustment', lcl_adjustment_string);
	call jsonify_int(lcl_adjusted_usage, 'adjusted_usage', lcl_adjusted_usage_string);
	call jsonify_int(lcl_building_id, 'building_id', lcl_building_id_string);
	call jsonify_decimal(lcl_oat, 'oat', lcl_oat_string);
	call jsonify_int(lcl_base_usage, 'base_usage', lcl_base_usage_string);
	call jsonify_string('start', lcl_start, lcl_start_string);
	call jsonify_string('end', lcl_end, lcl_end_string);
	call jsonify_int(lcl_service_days, 'service_days', lcl_base_usage_string);
	call jsonify_int(lcl_service_month_id, 'service_month_id', lcl_service_month_id_string);
	call jsonify_int(lcl_service_id, 'service_id', lcl_service_id_string);
    call jsonify_decimal(lcl_cost, 'cost', lcl_cost_string);
    call jsonify_decimal(lcl_dollar_per_kwh, 'dollar_per_kwh', lcl_dollar_per_kwh_string);
	call jsonify_int(lcl_post_period_id, 'post_period_id', lcl_post_period_id_string);
	call jsonify_int(lcl_saved_usage, 'saved_usage', lcl_saved_usage_string);
	call jsonify_decimal(lcl_percent_savings, 'percent_savings', lcl_percent_savings_string);
	call jsonify_decimal(lcl_monthly_savings, 'monthly_savings', lcl_monthly_savings_string);
    call jsonify_decimal(lcl_cumulative_savings, 'cumulative_savings', lcl_cumulative_savings_string);
	call jsonify_int(lcl_e_time, 'e_time', lcl_e_time_string);
	call jsonify_int(lcl_flag, 'flag', lcl_flag_string);
	call jsonify_int(lcl_company_id, 'company_id', lcl_company_id_string);
	call jsonify_int(lcl_log_user_id, 'log_user_id', lcl_log_user_id_string);	
    select concat("{", lcl_uusage_string, ",", lcl_model_log_id_string, ",", lcl_norm_usage_string, ",", lcl_model_usage_string, ",", lcl_adjustment_string
    , ",", lcl_adjusted_usage_string, ",", lcl_building_id_string, ",", ",",lcl_oat_string, ",",lcl_base_usage_string, ",", lcl_start_string
    , ",",lcl_end_string, ",",lcl_service_days_string, ",", lcl_service_month_id_string, ",", lcl_service_id_string, ",", lcl_cost_string
    , ",", lcl_dollar_per_kwh_string, ",", lcl_post_period_id_string, ",", lcl_saved_usage_string, ",", lcl_percent_savings_string, ","
    , lcl_monthly_savings_string, ",", lcl_cumulative_savings_string, lcl_e_time_string, ",", lcl_flag_string, ","
    , lcl_company_id_string, ",", lcl_log_user_id_string, "}'") into rv_electricity_json;
    call add_outbound(pv_flag, pv_electricity_id, rv_electricity_json,  'electricity', lcl_outbound_id);
	DELETE FROM electricity where id = pv_electricity_id AND company_id = lcl_company_id;
    select lcl_outbound_id into rv_status_code;
END//

CREATE OR REPLACE PROCEDURE get_electricity_api(IN pv_electricity_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_building(pv_electricity_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_electricity(IN pv_electricity_id int(9)
                                        , IN pv_user_id int(9)
                                        )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select electricity.id, electricity.uusage, electricity.model_log_id
    , electricity.norm_usage, electricity.model_usage, electricity.adjustment
    , electricity.adjusted_usage, electricity.building_id, electricity.oat
    , electricity.base_usage, electricity.start, electricity.end
    , electricity.service_days, electricity.service_month_id
    , electricity.service_id, electricity.cost, electricity.dollar_per_kwh
    , electricity.post_period_id, electricity.saved_usage
    , electricity.percent_savings, electricity.monthly_savings
    , electricity.cumulative_savings, electricity.e_time
    , electricity.flag, electricity.company_id, electricity.log_user_id
    FROM electricitty
    where electricity.id = pv_electricity_id;
END//

CREATE OR REPLACE PROCEDURE get_electricity_post_period_graph_data_api(IN pv_user_id int(10)
                                                    , IN pv_ssession_number int(10)
                                                    , IN pv_building_id int(9)
                                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_electricity_post_period_graph_data(pv_building_id);
    END IF; 
END//


CREATE OR REPLACE PROCEDURE get_electricity_post_period_graph_data(IN pv_building_id int(9)
                                                    )
BEGIN
    declare lcl_begin_date date;
    declare lcl_end_date date;
    SELECT a.start
        , a.end
        , a.uusage
        , b.month_date
        , CAST(a.oat AS CHAR) oat
        , CAST(c.area AS CHAR) area
        , a.service_days
        , CAST(a.adjusted_usage AS CHAR) adjusted_usage
        , CAST(a.norm_usage AS CHAR) norm_usage
        , CAST(a.model_usage AS CHAR) model_usage
        , CAST(a.base_usage AS CHAR) base_usage
        , CAST(a.saved_usage AS CHAR) saved_usage
        , CAST(a.percent_savings AS CHAR) percent_savings
        , CAST(a.monthly_savings AS CHAR) monthly_savings
        , CAST(a.cumulative_savings AS CHAR) cumulative_savings
        , CAST(a.base_usage AS CHAR) base_usage
        , a.company_id
        , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from electricity a, service_month b, building c
    where a.service_month_id = b.id
    and a.building_id = c.id
    and c.id = pv_building_id
    and a.base_usage > 0;
END//

CREATE OR REPLACE PROCEDURE calculate_baseline_electricity_price_api(IN pv_building_id int(9)
                                                        , IN pv_service_month_id_start int(9)
                                                        , IN pv_service_month_id_end int(9)
                                                        , IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , OUT rv_price decimal(4,3)
                                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_baseline_electricity_price(pv_building_id, pv_service_month_id_start, pv_service_month_id_end, rv_price);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_baseline_electricity_price(IN pv_building_id int(9)
                                                , IN pv_service_month_id_start int(9)
                                                , IN pv_service_month_id_end int(9)
                                                , OUT rv_price decimal(4,3)
                                                )
BEGIN    
    declare lcl_start_date date;
    declare lcl_end_date date;
    declare sum_usage decimal(11,3) default 0;
    declare sum_cost decimal(11,3) default 0;
    SELECT SUM(electricity.uusage) INTO sum_usage FROM electricity where electricity.service_month_id between pv_service_month_id_start and pv_service_month_id_end and building_id = pv_building_id and base_usage = 0;
    SELECT SUM(electricity.cost) INTO sum_cost FROM electricity where electricity.service_month_id between pv_service_month_id_start and pv_service_month_id_end and building_id = pv_building_id and base_usage = 0;
    UPDATE electricity set dollar_per_kwh = sum_cost/sum_usage where building_id = pv_building_id;
    SELECT sum_cost/sum_usage INTO rv_price;
END//

CREATE OR REPLACE PROCEDURE calculate_post_period_electricity_price_api(IN pv_building_id int(9)
                                                        , IN pv_service_month_id_start int(9)
                                                        , IN pv_service_month_id_end int(9)
                                                        , IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , OUT rv_price decimal(4,3)
                                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_post_period_electricity_price(pv_building_id, pv_service_month_id_start, pv_service_month_id_end, rv_price);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_post_period_electricity_price(IN pv_building_id int(9)
                                                , IN pv_service_month_id_start int(9)
                                                , IN pv_service_month_id_end int(9)
                                                , OUT rv_price decimal(4,3)
                                                )
BEGIN    
    declare lcl_start_date date;
    declare lcl_end_date date;
    declare sum_usage decimal(11,3) default 0;
    declare sum_cost decimal(11,3) default 0;
    SELECT SUM(electricity.uusage) INTO sum_usage FROM electricity where electricity.service_month_id between pv_service_month_id_start and pv_service_month_id_end and building_id = pv_building_id and base_usage > 0;
    SELECT SUM(electricity.cost) INTO sum_cost FROM electricity where electricity.service_month_id between pv_service_month_id_start and pv_service_month_id_end and building_id = pv_building_id and base_usage > 0;
    UPDATE electricity set dollar_per_kwh = sum_cost/sum_usage where building_id = pv_building_id;
    SELECT sum_cost/sum_usage INTO rv_price;
END//


CREATE OR REPLACE PROCEDURE update_price_electricity_api(IN pv_price decimal(6,4)
                                            , IN pv_building_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call update_price_electricity(pv_price, pv_building_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE update_price_electricity(IN pv_price decimal(6,4)
                                                , IN pv_building_id int(9)
                                                )
BEGIN
    UPDATE electricity set dollar_per_kwh = pv_price where building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_service_month_id_electricity(IN pv_electricity_id int(9)
                                            , OUT rv_service_month_id int(9)
                                            )
BEGIN
    select electricity.service_month_id INTO rv_service_month_id 
    FROM electricity WHERE id = pv_electricity_id;
END//