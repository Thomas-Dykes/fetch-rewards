DELIMITER //

CREATE OR REPLACE PROCEDURE add_service_month_api(IN pv_month varchar(10)
                                                ,IN pv_year varchar(10)
                                                ,IN pv_user_id int(10)
                                                , IN pv_ssession_number int(9)
                                                , IN pv_date date
                                                , OUT rv_service_month_id int(10)
                                                )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_service_month(pv_month, pv_year, pv_user_id, pv_date, rv_service_month_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE add_service_month(IN pv_month varchar(10)
                                                ,IN pv_year varchar(10)
                                                ,IN pv_user_id int(10)
                                                , IN pv_date date
                                                , OUT rv_service_month_id int(10))
BEGIN 
    declare lcl_count int default 0;

    set rv_service_month_id = -6;

    SELECT count(1) into lcl_count
    FROM service_month 
    where month = pv_month
    and year = pv_year;

    IF lcl_count = 0 THEN
        insert into service_month(month, year,month_date, log_user_id)
        values(pv_month, pv_year,pv_date, pv_user_id);

        SELECT id into rv_service_month_id
        FROM service_month 
        where month = pv_month
        and year = pv_year;
    END IF;

    IF lcl_count > 0 THEN
        set rv_service_month_id= -6;
    END IF;

    END//

CREATE OR REPLACE PROCEDURE get_service_months_api(IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_service_months(pv_user_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_service_months(IN pv_user_id int(9)
                                            )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select service_month.id, service_month.month, service_month.year
    , service_month.e_time
    , service_month.log_user_id
    FROM service_month;
END//

CREATE OR REPLACE PROCEDURE get_service_month_api(IN pv_service_month_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_service_month(pv_service_month_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_service_month(IN pv_service_month_id int(9)
                                            )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select service_month.id, service_month.month, service_month.year
    , service_month.e_time
    , service_month.log_user_id
    FROM service_month where id = pv_service_month_id;
END//

CREATE OR REPLACE PROCEDURE get_service_month_year(IN pv_service_month_id int(9)
                                                , OUT rv_service_month_year varchar(30)
                                                )
BEGIN
    select CONCAT(service_month.month, "-", service_month.year) INTO rv_service_month_year FROM service_month WHERE id = pv_service_month_id;
END//

CREATE OR REPLACE PROCEDURE get_eligible_service_months_electricity_api(IN pv_building_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_eligible_service_months_electricity(pv_building_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_eligible_service_months_electricity(IN pv_building_id int(9))
BEGIN
    declare lcl_company_id int default 0;
    declare min_e_id int default 0;
    declare max_e_id int default 0;
    select min(electricity.service_month_id) into min_e_id from electricity where electricity.building_id = pv_building_id;
    select max(electricity.service_month_id) into max_e_id from electricity where electricity.building_id = pv_building_id;
    IF min_e_id IS NOT NULL and max_e_id IS NOT NULL THEN
    select service_month.id, service_month.month, service_month.year, service_month.month_date date
    FROM service_month where service_month.id between min_e_id and (max_e_id);
    END IF;
    IF min_e_id IS NULL AND max_e_id IS NULL THEN
        select service_month.id, service_month.month, service_month.year, service_month.month_date date FROM service_month;
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_eligible_service_months_gas_api(IN pv_building_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_eligible_service_months_gas(pv_building_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_eligible_service_months_gas(IN pv_building_id int(9))
BEGIN
    declare min_g_id int default 0;
    declare max_g_id int default 0;
    select min(gas.service_month_id) into min_g_id from gas where gas.building_id = pv_building_id;
    select max(gas.service_month_id) into max_g_id from gas where gas.building_id = pv_building_id;
    IF min_g_id IS NOT NULL and max_g_id IS NOT NULL THEN
    select service_month.id, service_month.month, service_month.year, service_month.month_date date
    FROM service_month where service_month.id between min_g_id and (max_g_id);
    END IF;
    IF min_g_id IS NULL AND max_g_id IS NULL THEN
        select service_month.id, service_month.month, service_month.year, service_month.month_date date FROM service_month;
    END IF;
END//


CREATE OR REPLACE PROCEDURE get_eligible_service_months_api(IN pv_building_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_eligible_service_months(pv_building_id, pv_user_id);      
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_eligible_service_months(IN pv_building_id int(9)
                                                        , IN pv_user_id int(9))
BEGIN
    declare lcl_company_id int default 0;
    declare min_e_id int default 0;
    declare min_es_id int default 0;
    declare min_g_id int default 0;
    declare min_gs_id int default 0;
    declare min_s_id int default 0;
    declare max_e_id int default 0;
    declare max_es_id int default 0;
    declare max_g_id int default 0;
    declare max_gs_id int default 0;
    declare max_s_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select min(electricity.id) into min_e_id from electricity where electricity.building_id = pv_building_id;
    select electricity.service_month_id into min_es_id from electricity where electricity.id = min_e_id;
    select min(gas.id) into min_g_id from gas where gas.building_id = pv_building_id;
    select gas.service_month_id into min_gs_id from gas where gas.id = min_g_id;
    select GREATEST(min_es_id, min_gs_id) into min_s_id;
    select max(electricity.id) into max_e_id from electricity where electricity.building_id = pv_building_id;
    select electricity.service_month_id into max_es_id from electricity where electricity.id = max_e_id;
    select max(gas.id) into max_g_id from gas where gas.building_id = pv_building_id and gas.service_month_id IS NOT NULL;
    select gas.service_month_id into max_gs_id from gas where gas.id = max_g_id ;
    select LEAST(max_es_id, max_gs_id) into max_s_id;
    select service_month.id, service_month.month, service_month.year, service_month.month_date date
    FROM service_month where id between min_s_id and max_s_id;
END//


CREATE OR REPLACE PROCEDURE get_eligible_post_period_service_months_api(IN pv_service_month_id int(9)
                                        , IN pv_building_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_eligible_post_period_service_months(pv_service_month_id, pv_building_id, pv_user_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_eligible_post_period_service_months(IN pv_service_month_id int(9)
                                                        , IN pv_building_id int(9)
                                                        , IN pv_user_id int(9))
BEGIN
    declare lcl_company_id int default 0;
    declare min_e_id int default 0;
    declare min_es_id int default 0;
    declare min_g_id int default 0;
    declare min_gs_id int default 0;
    declare min_s_id int default 0;
    declare max_e_id int default 0;
    declare max_es_id int default 0;
    declare max_g_id int default 0;
    declare max_gs_id int default 0;
    declare max_s_id int default 0;
    declare input_date_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select max(electricity.id) into max_e_id from electricity where electricity.building_id = pv_building_id;
    select electricity.service_month_id into max_es_id from electricity where electricity.id = max_e_id;
    select max(gas.id) into max_g_id from gas where gas.building_id = pv_building_id and gas.service_month_id IS NOT NULL;
    select gas.service_month_id into max_gs_id from gas where gas.id = max_g_id;
    select LEAST(max_es_id, max_gs_id) into max_s_id;
    select service_month.id, service_month.month, service_month.year, service_month.month_date date
    , service_month.e_time
    , service_month.log_user_id
    FROM service_month where id between pv_service_month_id and max_s_id;
END//

CREATE OR REPLACE PROCEDURE get_service_month_api(IN pv_service_month_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_service_month(pv_service_month_id, pv_user_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_service_month(IN pv_service_month_id int(9)
                                            , IN pv_user_id int(9)
                                            )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select service_month.id, service_month.month, service_month.year
    , service_month.e_time
    , service_month.log_user_id
    FROM service_month
    where id = pv_service_month_id;
END//

CREATE OR REPLACE PROCEDURE get_service_month_date_api(IN pv_service_month_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        , OUT rv_service_month_date varchar(20)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_service_month_date(pv_service_month_id, rv_service_month_date);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_service_month_date(IN pv_service_month_id int(9)
                                            , OUT rv_service_month_date varchar(20)
                                            )
BEGIN
    select service_month.month_date INTO rv_service_month_date
    FROM service_month
    where id = pv_service_month_id;
END//


-- CREATE OR REPLACE PROCEDURE get_service_month_after_api(IN pv_service_month_id int(9)
--                                         , IN pv_user_id int(9)
--                                         , IN pv_ssession_number int(9)
--                                         )
-- BEGIN
--     declare lcl_auth int default 0;
--     declare lcl_company_id int default 0;
--     CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
--     IF lcl_auth = 1 THEN
--         call get_service_month_after(pv_service_month_id, pv_user_id);
--     END IF; 
-- END//

-- CREATE OR REPLACE PROCEDURE get_service_month_after(IN pv_service_month_id int(9)
--                                             , IN pv_user_id int(9)
--                                             )
-- BEGIN
--     declare lcl_company_id int default 0;
--     call get_user_company_id(pv_user_id, lcl_company_id);
--     select service_month.id, service_month.month, service_month.year
--     , service_month.e_time
--     , service_month.log_user_id
--     FROM service_month
--     where id > pv_service_month_id;
-- END//

CREATE OR REPLACE PROCEDURE get_min_building_service_month_id(IN pv_building_id int(9)
                                                            , OUT rv_service_month_id int(9)
                                                )
BEGIN
    select min(service_month_id) into rv_service_month_id from electricity where building_id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE get_past_data_api(IN pv_service_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_past_data(pv_service_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_past_data(IN pv_service_id int(9)
                    )
BEGIN
    SELECT electricity.uusage, electricity.dollar_kwh
    FROM electricity INNER JOIN sservice ON electricity.service_id = sservice.user_id
    WHERE DATEDIFF(sservice.service_date, electricity.start) < 365;
END//

CREATE OR REPLACE PROCEDURE check_duplicate_api(IN pv_user_id int(9)
                                                , IN pv_ssession_number int(9)
                                                , IN pv_old_service_start varchar(30)
                                                , IN pv_old_service_end varchar(30)
                                                , IN pv_old_service_month_id int(9)
                                                , IN pv_new_service_start varchar(30)
                                                , IN pv_new_service_end varchar(30)
                                                , IN pv_new_service_month_id int(9)
                                                , OUT rv_is_duplicate int(1)
                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call check_duplicate_api(pv_old_service_start, pv_old_service_end, pv_old_service_month_id, pv_new_service_start, pv_new_service_end, pv_new_service_month_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE check_duplicate(IN pv_old_service_start varchar(30)
                                                , IN pv_old_service_end varchar(30)
                                                , IN pv_old_service_month_id int(9)
                                                , IN pv_new_service_start varchar(30)
                                                , IN pv_new_service_end varchar(30)
                                                , IN pv_new_service_month_id int(9)
                                                , OUT rv_is_duplicate int(1)
                                                )
BEGIN 
    declare lcl_old_start date;
    declare lcl_old_end date;
    declare lcl_new_start date;
    declare lcl_new_end date;
    set rv_is_duplicate = 0;
    set lcl_old_start = DATE_FORMAT(pv_old_service_start, "%Y %m %d");
    set lcl_old_end = DATE_FORMAT(pv_old_service_end, "%Y %m %d");
    set lcl_new_start = DATE_FORMAT(pv_new_service_start, "%Y %m %d");
    set lcl_new_end = DATE_FORMAT(pv_new_service_end, "%Y %m %d");
    IF lcl_new_start BETWEEN lcl_old_start AND lcl_old_end THEN
        set rv_is_duplicate = 1;
    END IF;
    IF lcl_new_end BETWEEN lcl_old_start AND lcl_old_end THEN
        set rv_is_duplicate = 1;
    END IF;
    IF pv_old_service_month_id = pv_new_service_month_id THEN
        set rv_is_duplicate = 1;
    END IF;
END//

CREATE OR REPLACE PROCEDURE interpret_service_month(IN pv_month varchar(10)
                                                    , IN pv_year int(10)
                                                    , OUT rv_service_month_id int(9)
                                                    )
BEGIN
    select service_month.id INTO rv_service_month_id from service_month
    WHERE service_month.month = pv_month
    AND service_month.year = pv_year;
END//
