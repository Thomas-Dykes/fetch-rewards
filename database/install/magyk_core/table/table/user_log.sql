
	CREATE TABLE IF NOT EXISTS user_log (
	id int PRIMARY KEY AUTO_INCREMENT
	, first_name varchar(50)
	, last_name varchar(50)
	, email_address varchar(100)
	, dob varchar(30)
	, phone_number varchar(20)
	, pwd varchar(100)
	, tmp_pwd varchar(10)
	, address_id int references address(id)	
	, flag int(3)
	, e_time int(13)
	, company_id int(9)
	, log_user_id int(10)
	, INDEX(id)
	, FOREIGN KEY (address_id) references address(id)
	);