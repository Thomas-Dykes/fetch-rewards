CREATE TABLE IF NOT EXISTS system (
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    name varchar(30),
    system_type_id int(10),
    building_id int(10),
    company_id int(10),
    flag int(9),
    e_time int(13),
    log_user_id int(9)
);