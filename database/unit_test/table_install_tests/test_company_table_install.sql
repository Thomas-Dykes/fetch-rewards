

DELIMITER //


CREATE OR REPLACE PROCEDURE test_company_table_install( IN pv_username INT(10)
														,OUT rv_passed INT(3))
BEGIN
	DECLARE lcl_start INT;
	DECLARE lcl_middle INT;
	DECLARE lcl_end INT;
	DECLARE lcl_response INT;
	DECLARE lcl_company_id INT;
	DECLARE lcl_user_id INT;
	SET rv_passed = -6;
	CALL get_user_id(pv_username, lcl_user_id);
	CALL get_company_count(lcl_start);
	CALL add_company(0, 'name', 2, lcl_company_id);
	CALL get_company_count(lcl_middle);
	CALL delete_company(lcl_company_id,0,lcl_user_id, lcl_response);
	CALL get_company_count(lcl_end);
	IF lcl_start < lcl_middle THEN
		SET rv_passed = -99;
		IF lcl_middle > lcl_end THEN
			SET rv_passed = 1;
		END IF;
	END IF;
END//


call test_company_table_install(1,@rv_passed);

select @rv_passed;

call add_proc_index(
252
, 'tests the company_table_install procedure'
, 0
, '{}'
, '{"rv_passed":"int(9)"}'
, '["company"]'
, 'test_company_table_install'
, 0
);

call get_max_test_group(@ggroup_id);

call add_unit_test_log(
0
, 1
, @rv_passed
, 252
, 'company'
, @ggroup_id
, @out
);
