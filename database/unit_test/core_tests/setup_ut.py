import datetime, sys, random
import delete_ut, user_ut, ssession_ut, address_ut, company_ut, ggroup_rrole_ut, ggroup_ut, rrole_ut, state_ut, user_ggroup_ut, user_log_ut

from random import choice
from string import ascii_uppercase

sys.path.append("/home/ec2-user/pbc_portal/pbc")
import login
import connector as co
import config

def create_jedi_group():
    username = config.login_test_user
    password = config.login_test_password
    user_id = user_ut.get_user_id(username)
    proc_name = "add_ggroup"
    params = ["initial ggroup", 0, user_id, "jedi", 0]
    jdat = {"name": proc_name, "params": params}
    ggroup_id = co.machine_add(jdat)
    return ggroup_id


def hire_yoda(cookie):

    username = config.user_username
    password = config.user_password
    user_id = user_ut.get_user_id(username)
    session_number = cookie["ssession_number"]
    # if user_id > 0:
    #     delete_response = fire(cookie, user_id)
    #     return hire_yoda(cookie)

    if user_id == -7 or user_id == 0:
        return hire(
            cookie,
            "ranger town",
            "1980/01/01",
            "your@mom.me",
            "ron",
            "john",
            config.login_test_password,
            "666-555-7777",
            "2019-01-01",
            1,
            "98 fix street",
            config.login_test_user,
            91203,
        )



def hire(
    cookie,
    city,
    dob,
    email,
    first,
    last,
    pwd,
    phone_number,
    start_date,
    state_id,
    address,
    username,
    zip_code,
):
    log_employee_id = cookie["employee_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "hire_api"
    params = [
            1,
            "ranger town",
            0,
           "1980/01/01",
            "your@mom.me",
            0,
            "ron",
            1,
            "john",
            23,
            config.login_test_password,
            "666-555-7777",
            session_number,
            "2019-01-01",
            1,
            "98 fix street",
            config.login_test_user,
            91203,
            0
    ]
    jdat = {"name": proc_name, "params": params}
    user_id = co.machine_add(jdat)
    print(f"user id: {user_id}")
    return user_id


def create_address(user_id):
    proc_name = "add_address"
    params = [
        "Nazareth",
        0,
        12.23,
        user_id,
        56.66,
        1,
        "1234 rosebud lane",
        35275,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)

def get_min_address_id():
    proc_name = "get_min_address_id"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)

def create_root_address(company_id):
    proc_name = "add_root_address"
    params = [
        "Bethlehem",
        company_id,
        0,
        15.51,
        1,
        65.56,
        1,
        "3333 Forgetmenot Court",
        23232,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)

def get_root_address(company_id):
    proc_name = ""

def create_jedi_ggroup(company_id):
    proc_name = "add_ggroup"
    params = ["base admin group", 0, 1, "jedi", 0]
    jdat = {"name": proc_name, "params": params}
    jedi_id = co.machine_add(jdat)
    return jedi_id

def create_root_user(address_id, admin, company_id, customer, dob, email_address, employee, first_name, last_name, password, phone_number, start_date, username):
    proc_name = "add_root_user"
    start = datetime.datetime(2019, 4, 20)
    params = [address_id, admin, company_id, customer, dob, email_address, employee, first_name, 0, last_name, password, phone_number, start, username, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)

def create_user(address_id, admin, customer, dob, email_address, employee, first_name, last_name, user_id, password, phone_number, start_date, username):
    proc_name = "add_user"
    dob = datetime.datetime(1000, 7, 4)
    params = [address_id, admin, customer, dob, email_address, employee, first_name, 0, last_name, user_id, password, phone_number, start_date, username, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)

def get_yoda(cookie):
    return hire_yoda(cookie)

def initialize_groups_roles(user_id):
    # returns the group id for jedi group
    proc_name = "new_ggroups_and_ggroup_rroles"
    params = [user_id, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def create_user_ggroup(user_id, log_user_id, group_id):
    proc_name = "add_user_ggroup"
    params = [user_id, group_id, log_user_id]
    jdat = {"name": proc_name, "params": params}
    co.machine_add(jdat)

def create_company(name):
    proc_name = "add_company"
    params = [0, name, 1, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)

def get_root_company():
    proc_name = "get_root_company_id"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def update_company(company_id, log_user_id):
    proc_name = "update_company"
    params = [company_id, log_user_id, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)

def initialize_roles():
    proc_name = "new_rroles"
    params = [0]
    try:
        jdat = {"name": proc_name, "params": params}
        co.machine_add(jdat)
    except Exception as e:
        print(f"error: {e}")

def create_login_test_user(user_id, address_id, log_user_id):
    username = config.login_test_user
    password = config.login_test_password
    return create_user(log_user_id, user_id, username, password)


def ex():
    proc_name = "example"
    params = []
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def main(
    username=config.user_username,
    password=config.user_password,
    company=config.test_company,
):
    jedi = create_jedi_group()
    root_password = random.randint(10000, 999999)
    # root_user =f"{company} root".replace(" ","_")
    root_user = "".join(choice(ascii_uppercase) for i in range(12))
    print(f"main: {username}")
    companies = company_ut.get_companys()
    print(f"length of companies: {len(companies)}")
    if len(companies) < 5:
        if len(companies) == 0:
            company_id = create_company(company)
        company_id = get_root_company()
        input(f"company id: {company_id}")
        address_count = address_ut.get_address_count()
        if address_count == 0:
            root_address_id = create_root_address(company_id)
            print(f"root address: {root_address_id}")
        else:
            root_address_id = get_min_address_id()
        dob = datetime.datetime(1990, 10, 21)
        email_address = 'thecynic187@gmail.com'
        admin = 1
        customer = 0
        employee = 0
        first_name = 'Jiminy'
        last_name = 'Cricket'
        phone_number = '1122334455'
        start_date = datetime.datetime(2020, 1, 1)
        username_two = config.user_username


        root_user_id = create_root_user(root_address_id, admin, company_id, customer, dob, email_address, employee, first_name, last_name, password, phone_number, start_date, username)
        print(f"root user id: {root_user_id}")

        update_company(1, root_user_id)

        address_id = create_address(root_user_id)
        user_id = create_user(root_address_id, admin, customer, dob, email_address, employee, first_name, last_name, root_user_id, password, phone_number, start_date, root_user)
        print(f"user_id: {user_id}")
        
        if company == config.test_company:
            lcl_cookie = login.inbound_login(
                config.user_username, config.user_password
            )
            print(f"inbound_cookie: {lcl_cookie}")
            yoda_id = get_yoda(lcl_cookie)

        cookie = ssession_ut.login(config.user_username, config.user_password)
        print(f"cookie: {cookie}")
        ggroup_rroles = ggroup_rrole_ut.get_ggroup_rroles(cookie)

        if len(ggroup_rroles) < 104:
            group_id = initialize_groups_roles(user_id)
        user_ggroups = user_ggroup_ut.get_user_ggroups(cookie)

        if len(user_ggroups) < 2:
            create_user_ggroup(root_user_id, root_user_id, group_id)
            create_user_ggroup(user_id, root_user_id, group_id)

if __name__ == "__main__":
    # delete_ut.main()
    initialize_roles()
    main('leonardo', 'ggg333', 'magyk')
    # main("antleypk", "b1gd0g", "magyk")
    # main("rivasp", "r0g3rR@bb1t", "daygo cbd")
    # main("rivas_p", "funn7h@tt", "honey farm")
    # main("pka_92", "h@ng1n9p1ant", "cooksmart")
