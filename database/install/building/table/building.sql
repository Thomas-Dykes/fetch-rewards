CREATE TABLE IF NOT EXISTS building (
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    area int(10),
    name varchar(30),
    building_category_id int(9),
    electricity_model_id int(9),
    gas_model_id int(9),
    address_id int(9),
    built varchar(30),
    e_time int(13),
    flag int(3),
    company_id int(10),
    log_user_id int(10)
);