import time, decimal, sys
import user_log_ut as pl
import user_ut as pt

import address_ut as address

import rrole_ut as ro
import ggroup_ut as gg
import user_ut
import ssession_ut
import setup_ut

sys.path.append("/home/ec2-user/pbc_portal/pbc")
# sys.path.append("../../../magyk")
import login
import connector as co
import config


def add_role(power, role, table):
    return ro.add_role(power, role, table)


def add_ggroup_rrole(cookie, ggroup_id, rrole_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_ggroup_rrole_api"
    params = ["0", ggroup_id, user_id, rrole_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def delete_ggroup_rrole(cookie, ggroup_id, rrole_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "delete_ggroup_rrole_api"
    params = [0, ggroup_id, user_id, ssession_number, rrole_id, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def get_ggroup_rroles(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_ggroup_rroles_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    ggroup_rroles = co.machine(jdat)
    for ggroup_rrole in ggroup_rroles:
        print(f"ggroup_rrole: {ggroup_rrole}")
    return ggroup_rroles


def get_ggroups_rroles(cookie, ggroup_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_ggroups_rroles_api"
    params = [ggroup_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    ggroup_rroles = co.machine(jdat)
    for ggroup_rrole in ggroup_rroles:
        print(f"ggroup_rrole: {ggroup_rrole}")
    return ggroup_rroles


def get_ggroup_rrole(cookie, ggroup_id, rrole_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_ggroup_rrole_api"
    params = [ggroup_id, user_id, ssession_number, rrole_id]
    jdat = {"name": proc_name, "params": params}
    print(jdat)
    ggroup_rrole = co.machine(jdat)[0]
    print(f"ggroup_rrole: {ggroup_rrole}")
    return ggroup_rrole


def test_add_1(cookie, rrole_id_one, ggroup_id):
    user_id = cookie["user_id"]
    print(f"jedi group {ggroup_id}")

    # input(f"role id: {rrole_id_one}")
    print(f"add test 1, role id: {rrole_id_one}")
    add_ggroup_rrole(cookie, ggroup_id, rrole_id_one)
    time.sleep(0.1)
    #    print(f"id: {id}")
    print(" \n \n ")
    print("get test 1")
    ggroup_rrole = get_ggroup_rrole(cookie, ggroup_id, rrole_id_one)
    lcl_ggroup_id = ggroup_rrole["ggroup_id"]
    lcl_rrole_id = ggroup_rrole["rrole_id"]
    print(f"lcl_ggroup_id: {lcl_ggroup_id}, lcl_rrole_id: {lcl_rrole_id}")
    if lcl_ggroup_id == ggroup_id:
        if lcl_rrole_id == rrole_id_one:
            return {
                "test": "test_add_1",
                "result": True,
                "table": "ggroup_rrole",
                "proc_index_id": [86, 87, 88, 89],
            }

    return {
        "test": "test_add_1",
        "result": False,
        "table": "ggroup_rrole",
        "proc_index_id": [86, 87, 88, 89],
    }


def test_add_2(cookie, rrole_id_two, ggroup_id):
    user_id = cookie["user_id"]

    print("add test 2")
    add_ggroup_rrole(cookie, ggroup_id, rrole_id_two)
    time.sleep(0.1)
    #    print(f"id: {id}")
    print(" \n \n ")
    print("get test 2")
    ggroup_rrole = get_ggroup_rrole(cookie, ggroup_id, rrole_id_two)
    lcl_ggroup_id = ggroup_rrole["ggroup_id"]
    lcl_rrole_id = ggroup_rrole["rrole_id"]
    print(f"lcl_ggroup_id: {lcl_ggroup_id}, lcl_rrole_id: {lcl_rrole_id}")
    if lcl_ggroup_id == ggroup_id:
        if lcl_rrole_id == rrole_id_two:
            return {
                "test": "test_add_1",
                "result": True,
                "table": "ggroup_rrole",
                "proc_index_id": [86, 87, 88, 89],
            }

    return {
        "test": "test_add_1",
        "result": False,
        "table": "ggroup_rrole",
        "proc_index_id": [86, 87, 88, 89],
    }


def test_gets(cookie, rrole_id_one, rrole_id_two, ggroup_id):

    ggroup_rrole_one = {}
    ggroup_rrole_two = {}

    for i in get_ggroup_rroles(cookie):
        if i["ggroup_id"] == ggroup_id:
            if i["rrole_id"] == rrole_id_one:
                ggroup_rrole_one = i
            if i["rrole_id"] == rrole_id_two:
                ggroup_rrole_two = i

    add_1 = get_ggroup_rrole(cookie, ggroup_id, rrole_id_one)
    add_2 = get_ggroup_rrole(cookie, ggroup_id, rrole_id_two)

    if add_1["ggroup_id"] == ggroup_rrole_one["ggroup_id"]:
        print("match ggroup_id_1")
        print("add_1 rrole_id is:")
        print(add_1["rrole_id"])
        print("ggroup_rrole_1_rrole_id is:")
        print(ggroup_rrole_one["rrole_id"])
        if add_1["rrole_id"] == ggroup_rrole_one["rrole_id"]:
            print("match rrole_id_1")
            if add_2["ggroup_id"] == ggroup_rrole_two["ggroup_id"]:
                print("match ggroup_id_2")
                if add_2["rrole_id"] == ggroup_rrole_two["rrole_id"]:
                    print("match rrole_id_2")
                    return {
                        "test": "test_gets",
                        "result": True,
                        "table": "ggroup_rrole",
                        "proc_index_id": [84, 85],
                    }
    return {
        "test": "test_gets",
        "result": False,
        "table": "ggroup_rrole",
        "proc_index_id": [84, 85],
    }


def test_get_ggroups_rroles_api(pv_cookie, rrole_id_1, rrole_id_2, ggroup_id):
    yoda = setup_ut.hire_yoda(pv_cookie)

    match = 0
    for i in get_ggroups_rroles(pv_cookie, ggroup_id):
        lcl_id = i["id"]
        # print(f"\n i: {i} \n \n     i, id: {lcl_id}")
        if lcl_id == rrole_id_1:
            print("match 1")
            match += 1
        if lcl_id == rrole_id_2:
            print("match 2\n ")
            match += 1
    # input(f"rrole_id_1 {rrole_id_1},\n rrole_id2 {rrole_id_2},\n ggroup_id: {ggroup_id},\n match: {match}")
    if match == 2:
        return {
            "test": "test_get_ggroups_rroles_api",
            "result": True,
            "table": "ggroup_rrole",
            "proc_index_id": [441],
        }

    return {
        "test": "test_get_ggroups_rroles_api",
        "result": False,
        "table": "ggroup_rrole",
        "proc_index_id": [441],
    }


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        try:
            t_table = test["table"]
            t_test = test["test"]
            t_result = test["result"]
            t_proc_index_id = test["proc_index_id"]
            print(
                f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
            )
        except TypeError as e:
            sleep = 3
            print(f"    print error: {e} \n")
            print(f"    record: {test} \n")
            print("sleep {}".format(sleep))
            time.sleep(sleep)


def machine(cookie):
    tests = []
    print(" \n \n ")
    user_id = cookie["user_id"]
    # ggroup_id = gg.get_jedi_group_id(user_id)
    ggroup_id = gg.add_ggroup(cookie, "group role test", "tmp group for group role ut")
    print(f"ggroup_id: {ggroup_id}")
    rrole_id_one = ro.get_new_role_id()
    rrole_id_one = ro.add_rrole("demo power", rrole_id_one, "demo role", 0)
    print(f"rrole_id_one: {rrole_id_one}")
    rrole_id_two = ro.get_new_role_id()
    rrole_id_two = ro.add_rrole("demo power2", rrole_id_two, "demo role2", 0)
    print(f"rrole_id_two: {rrole_id_two}")
    result = test_add_1(cookie, rrole_id_one, ggroup_id)
    result2 = test_add_2(cookie, rrole_id_two, ggroup_id)
    tests.append(result)
    tests.append(result2)
    print(" \n \n ")
    tests.append(test_gets(cookie, rrole_id_one, rrole_id_two, ggroup_id))
    tests.append(
        test_get_ggroups_rroles_api(cookie, rrole_id_one, rrole_id_two, ggroup_id)
    )
    delete = delete_ggroup_rrole(cookie, ggroup_id, rrole_id_one)
    delete_two = delete_ggroup_rrole(cookie, ggroup_id, rrole_id_two)
    gg.delete_ggroup(cookie, ggroup_id)
    ro.delete_rrole(rrole_id_one)
    # ro.delete_rrole(rrole_id_two)
    print(f"delete {delete}, delete_two: {delete}")
    return tests


def main():
    cookie = login.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
