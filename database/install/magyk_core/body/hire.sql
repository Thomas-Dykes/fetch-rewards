DELIMITER //

CREATE OR REPLACE PROCEDURE hire_api(IN pv_admin int(1)
				, IN pv_city varchar(30)
				, IN pv_customer int(1)
				, IN pv_dob varchar(30)
				, IN pv_email_address varchar(50)
				, IN pv_employee int(1)
				, IN pv_first_name varchar(50)
				, IN pv_ggroup_id int(9)
				, IN pv_last_name varchar(50)
				, IN pv_user_id int(9)
				, IN pv_password varchar(50)
				, IN pv_phone_number varchar(20)
				, IN pv_ssession_number int(9)
				, IN pv_start_date varchar(13)
				, IN pv_state_id int(9)
				, IN pv_street_address varchar(100)
				, IN pv_username varchar(30)
				, IN pv_zip_code varchar(30)
				, OUT rv_user_id INT(10)
				)
BEGIN
    declare lcl_auth INT(9) default 0;
    declare lcl_company_id INT(9) default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF 1 = 1 THEN
        call hire(pv_admin, pv_city, pv_customer, pv_dob, pv_email_address, pv_employee, pv_first_name, pv_ggroup_id, pv_last_name, pv_user_id, pv_password, pv_phone_number, pv_start_date, pv_state_id, pv_street_address, pv_username, pv_zip_code, rv_user_id);
    END IF;
END//


CREATE OR REPLACE PROCEDURE hire(IN pv_admin int(1)
				, IN pv_city varchar(30)
				, IN pv_customer int(1)
				, IN pv_dob varchar(30)
				, IN pv_email_address varchar(50)
				, IN pv_employee int(1)
				, IN pv_first_name varchar(50)
				, IN pv_ggroup_id int(9)
				, IN pv_last_name varchar(50)
				, IN pv_user_id int(9)
				, IN pv_password varchar(50)
				, IN pv_phone_number varchar(20)
				, IN pv_start_date varchar(13)
				, IN pv_state_id int(9)
				, IN pv_street_address varchar(100)
				, IN pv_username varchar(30)
				, IN pv_zip_code varchar(30)
				, OUT rv_user_id INT(10)
				)
	BEGIN
		declare lcl_address_id int default 0;
		SET rv_user_id = -66;
		call add_address(pv_city, 0, 0.0, pv_user_id, 0.0, pv_state_id, pv_street_address, pv_zip_code, lcl_address_id);
		call add_user(lcl_address_id, pv_admin, pv_customer, pv_dob, pv_email_address, pv_employee, pv_first_name, 0, pv_last_name, pv_user_id, pv_password, pv_phone_number, pv_start_date, pv_username, rv_user_id);
		-- call add_person(lcl_address_id, pv_dob, pv_email_address, pv_first_name, 0, pv_last_name, pv_log_employee_id, pv_phone_number, lcl_person_id);
		-- call add_employee(0, pv_log_employee_id, lcl_person_id, pv_password, pv_start_date, pv_username, lcl_employee_id);
		call add_user_ggroup(rv_user_id, pv_ggroup_id, pv_user_id);
	END//
