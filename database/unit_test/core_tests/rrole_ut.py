import time, decimal, sys

import user_log_ut as pl


sys.path.append("/home/ec2-user/pbc_portal/pbc")
# sys.path.append("../../../magyk")
import login
import connector as co
import config

sleep = 5


def delete_rrole(role_id):
    proc_name = "delete_rrole"
    params = [role_id, 0]
    jdat = {"name": proc_name, "params": params}
    count = co.machine_add(jdat)
    print(f"local count: {count}")
    return count


def get_rrole_count():
    proc_name = "get_rrole_count"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    count = co.machine_add(jdat)
    print(f"local count: {count}")
    return count


def get_new_role_id():
    proc_name = "get_max_rrole_id"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    max_id = co.machine_add(jdat)
    new_id = max_id + 1
    print(f"max id: {max_id}, new id: {new_id}")
    return new_id


def add_rrole(ppower, rrole_id, rrole, ttable):
    proc_name = "add_rrole"
    params = ["0", rrole_id, ppower, rrole, ttable, 0]
    jdat = {"name": proc_name, "params": params}
    print(f"jdat rrole: {jdat}")
    return co.machine_add(jdat)


def get_rroles():
    proc_name = "get_rroles"
    params = []
    jdat = {"name": proc_name, "params": params}
    rroles = co.machine(jdat)
    for rrole in rroles:
        print(f"rrole: {rrole}")
    return rroles


def get_rrole(rrole_id):
    proc_name = "get_rrole"
    params = [rrole_id]
    jdat = {"name": proc_name, "params": params}
    rrole = co.machine(jdat)[0]
    print(f"rrole: {rrole}")
    return rrole


def test_add_1(cookie):
    ppower_one = "q"
    rrole_one = "weirdness"
    ttable_one = "test_table_one"
    rrole_id = get_new_role_id()
    print("add test 1")
    rrole_id = add_rrole(ppower_one, rrole_id, rrole_one, ttable_one)
    time.sleep(0.1)
    print(f"id: {rrole_id}")
    print(" \n \n ")
    print("get test 1")
    rrole = get_rrole(rrole_id)
    lcl_ppower = rrole["ppower"]
    lcl_rrole = rrole["rrole"]
    lcl_ttable = rrole["ttable"]

    print(f"lcl_ppower: {lcl_ppower}")
    print(f"lcl_rrole: {lcl_rrole}")
    print(f"lcl_table: {lcl_ttable}")
    if lcl_ppower == ppower_one:

        if lcl_ttable == ttable_one:
            if lcl_rrole == rrole_one:
                return (
                    rrole_id,
                    {
                        "test": "test_add_1",
                        "result": True,
                        "table": "rrole",
                        "proc_index_id": [126, 128, 129],
                    },
                )
    return (
        rrole_id,
        {
            "test": "test_add_1",
            "result": False,
            "table": "rrole",
            "proc_index_id": [126, 128, 129],
        },
    )


def test_add_2(cookie):
    ppower_two = "s"
    rrole_two = "weirdness_two"
    ttable_two = "test_table_two"
    rrole_id = get_new_role_id()
    print("add test 2")
    rrole_id = add_rrole(ppower_two, rrole_id, rrole_two, ttable_two)
    time.sleep(0.1)
    print(f"id: {id}")
    print(" \n \n ")
    print("get test 2")
    rrole = get_rrole(rrole_id)
    lcl_ppower = rrole["ppower"]
    lcl_rrole = rrole["rrole"]
    lcl_ttable = rrole["ttable"]

    print(f"lcl_ppower: {lcl_ppower}")
    print(f"lcl_rrole: {lcl_rrole}")
    print(f"lcl_table: {lcl_ttable}")
    if lcl_ppower == ppower_two:
        if lcl_ttable == ttable_two:
            if lcl_rrole == rrole_two:
                return (
                    rrole_id,
                    {
                        "test": "test_add_2",
                        "result": True,
                        "table": "rrole",
                        "proc_index_id": [126, 128, 129],
                    },
                )
    return (
        rrole_id,
        {
            "test": "test_add_2",
            "result": False,
            "table": "rrole",
            "proc_index_id": [126, 128, 129],
        },
    )


def test_gets(cookie, a, b):
    rroles = get_rroles()
    rrole_one = get_rrole(a)
    rrole_two = get_rrole(b)
    add_1 = {"rrole": "weirdness", "ppower": "q", "ttable": "test_table_one"}
    add_2 = {"rrole": "weirdness_two", "ppower": "s", "ttable": "test_table_two"}
    print(f"rrole one: {rrole_one}")
    print(f"add one: {add_1}")
    print(f"rrole_two: {rrole_two}")
    print(f"add_two: {add_2}")
    if add_1["rrole"] == rrole_one["rrole"]:
        print("rrole_one_pass")
        if add_1["ppower"] == rrole_one["ppower"]:
            print("ppower_one_pass")
            if add_1["ttable"] == rrole_one["ttable"]:
                print("Table_one_pass")
                if add_2["rrole"] == rrole_two["rrole"]:
                    print("rrole_two_pass")
                    if add_2["ppower"] == rrole_two["ppower"]:
                        print("ppower_two_pass")
                        if add_2["ttable"] == rrole_two["ttable"]:
                            print("ttable_two_pass")
                            if len(rroles) == get_rrole_count():
                                print("len correct")
                                return {
                                    "test": "test_gets",
                                    "result": True,
                                    "table": "rrole",
                                    "proc_index_id": [127],
                                }
    return {
        "test": "test_gets",
        "result": False,
        "table": "rrole",
        "proc_index_id": [127],
    }


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        try:
            t_table = test["table"]
            t_test = test["test"]
            t_result = test["result"]
            t_proc_index_id = test["proc_index_id"]
            print(
                f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
            )
        except TypeError as e:
            sleep = 3
            print(f"    print error: {e} \n")
            print(f"    record: {result} \n")
            print("sleep {}".format(sleep))
            time.sleep(sleep)


def machine(cookie):
    tests = []
    print(" \n \n ")
    lcl_id, result = test_add_1(cookie)
    lcl_id2, result2 = test_add_2(cookie)
    tests.append(result)
    tests.append(result2)
    print(" \n \n ")
    tests.append(test_gets(cookie, lcl_id, lcl_id2))
    delete_rrole(lcl_id)
    delete_rrole(lcl_id2)
    return tests


def main():
    cookie = login.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
