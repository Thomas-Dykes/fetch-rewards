#calculate_model_usage_gas_api
call add_proc_index(
89
, 'checks permissions, then runs calculate_model_usage_gas'
, 0
, '{"pv_gas_id": "int(9)", "pv_model_log_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_model_usage": "decimal(5,3)"}'
, '["gas"]'
, 'calculate_model_usage_gas_api'
, 1
);

#calculate_model_usage_one_slope_gas_api
call add_proc_index(
90
, 'checks permissions, then runs calculate_model_usage_one_slope_gas'
, 0
, '{"pv_gas_id": "int(9)", "pv_model_log_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_model_usage": "decimal(5,3)"}'
, '["gas"]'
, 'calculate_model_usage_one_slope_gas_api'
, 1
);

#calculate_baseline_gas_price_api

call add_proc_index(
91
, 'checks permissions, then runs calculate_baseline_gas_price'
, 0
, '{"pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_price": "decimal(4,3)"}'
, '["gas"]'
, 'calculate_baseline_gas_price_api'
, 1
);

#calculate_base_usage_gas_api
call add_proc_index(
92
, 'checks permissions, then runs calculate_base_usage_gas'
, 0
, '{"pv_gas_id": "int(9)", "pv_model_log_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_base_usage": "decimal(5,3)"}'
, '["gas"]'
, 'calculate_base_usage_gas_api'
, 1
);

#calculate_base_usage_gas_one_slope_api
call add_proc_index(
93
, 'checks permissions, then runs calculate_base_usage_gas_one_slope'
, 0
, '{"pv_gas_id": "int(9)", "pv_model_log_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_base_usage": "decimal(5,3)"}'
, '["gas"]'
, 'calculate_base_usage_gas_one_slope_api'
, 1
);


#calculate_saved_usage_gas_api
call add_proc_index(
94
, 'checks permissions, then runs calculate_saved_usage_gas'
, 0
, '{"pv_gas_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_saved_usage": "decimal(5,3)"}'
, '["gas"]'
, 'calculate_saved_usage_gas_api'
, 1
);

#calculate_percent_savings_gas_api
call add_proc_index(
95
, 'checks permissions, then runs calculate_percent_savings_gas'
, 0
, '{"pv_gas_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_percent_savings": "decimal(5,3)"}'
, '["gas"]'
, 'calculate_percent_savings_gas_api'
, 1
);

#calculate_norm_usage_gas_api
call add_proc_index(
96
, 'checks permissions, then runs calculate_norm_usage_gas'
, 0
, '{"pv_gas_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_norm_usage": "decimal(5,3)"}'
, '["gas"]'
, 'calculate_norm_usage_gas_api'
, 1
);

#get_gas_ids_api
call add_proc_index(
97
, 'checks permissions, then runs get_gas_ids'
, 0
, '{"pv_start_date": "date", "pv_end_date": "date", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_gas_ids_api'
, 1
);

#calculate_monthly_savings_gas_api
call add_proc_index(
98
, 'checks permissions, then runs calculate_monthly_savings_gas'
, 0
, '{"pv_gas_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_cost": "decimal(4,3)"}'
, '["gas"]'
, 'calculate_monthly_savings_gas_api'
, 1
);

#add_cumulative_savings_gas_api
call add_proc_index(
99
, 'checks permissions, then runs add_cumulative_savings_gas'
, 0
, '{"pv_gas_id": "int(9)", "pv_cumulative_savings": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'add_cumulative_savings_gas_api'
, 1
);

#add_gas_pre_period_raw_api
call add_proc_index(
100
, 'checks permissions, then runs add_gas_pre_period_raw'
, 0
, '{"pv_usage": "int(9)", "pv_adjustment": "int(9)", "pv_building_id": "int(9)", "pv_oat": "int(9)", "pv_start": "date", "pv_end": "date", "pv_dollar_kwh": "decimal(6,2)"
, "pv_log_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_gas_id": "int(9)"}'
, '["gas"]'
, 'add_gas_pre_period_raw_api'
, 1
);

#get_gas_graph_data_api
call add_proc_index(
101
, 'checks permission, then runs get_gas_graph_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_gas_graph_data_api'
, 1
);

#get_gas_usage_cost_graph_data_api

call add_proc_index(
102
, 'checks permission, then runs get_gas_usage_cost_graph_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_gas_usage_cost_graph_data_api'
, 1
);

#get_gas_norm_usage_oat_graph_data_api

call add_proc_index(
103
, 'checks permission, then runs get_gas_norm_usage_oat_graph_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_gas_norm_usage_oat_graph_data_api'
, 1
);

#get_gas_price_graph_data_api

call add_proc_index(
104
, 'checks permission, then runs get_gas_price_graph_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_gas_price_graph_data_api'
, 1
);

#get_gas_post_period_graph_data_api

call add_proc_index(
105
, 'checks permission, then runs get_gas_post_period_graph_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_gas_post_period_graph_data_api'
, 1
);

#get_gas_graph_data_between_api
call add_proc_index(
106
, 'checks permission, then runs get_gas_graph_data_between'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_gas_graph_data_between_api'
, 1
);

#get_oats_gas_between_api
call add_proc_index(
107
, 'checks permission, then runs get_oats_gas_between'
, 0
, '{"pv_pre_period_date": "date", "pv_period_length": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_oats_gas_between_api'
, 1
);

#get_norm_usages_gas_between_api
call add_proc_index(
108
, 'checks permission, then runs get_norm_usages_gas_between'
, 0
, '{"pv_pre_period_date": "date", "pv_period_length": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_norm_usages_gas_between_api'
, 1
);

#get_savings_gas_between_api
call add_proc_index(
109
, 'checks permission, then runs get_savings_gas_between'
, 0
, '{"pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)", "pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_savings_pgas_api'
, 1
);

#get_service_month_year_gas_between_api
call add_proc_index(
110
, 'checks permission, then runs get_service_month_year_gas_between'
, 0
, '{"pv_pre_period_date": "date", "pv_period_length": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas", "service_month"]'
, 'get_service_month_year_gas_between_api'
, 1
);

#find_gas_costs_api

call add_proc_index(
111
, 'checks permission, then runs find_gas_costs_api'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, 'find_gas_costs_api'
, 1
);


#get_gas_usage_data_api
call add_proc_index(
112
, 'checks permission, then runs get_gas_usage_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_gas_usage_data_api'
, 1
);


#get_gas_savings_data_api

call add_proc_index(
113
, 'checks permission, then runs get_gas_savings_data_api'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_gas_savings_data_api'
, 1
);

#get_cumulative_savings_gas_api
call add_proc_index(
114
, 'checks permission, then runs get_cumulative_savings_gas'
, 0
, '{"pv_building_id": "int(9)", "pv_service_month_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_cumulative_savings_gas_api'
, 1
);

#get_cumulative_savings_post_period_gas_api

call add_proc_index(
115
, 'checks permission, then runs get_cumulative_savings_post_period_gas'
, 0
, '{"pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_cumulative_savings_post_period_gas_api'
, 1
);

#get_oats_gas_between_api

call add_proc_index(
116
, 'checks permission, then runs get_oats_gas_between'
, 0
, '{"pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_oats_gas_between_api'
, 1
);

#get_norm_usages_between_gas_api

call add_proc_index(
117
, 'checks permission, then runs get_norm_usages_between_gas'
, 0
, '{"pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_norm_usages_gas_between_api'
, 1
);

#get_savings_gas_between_api

call add_proc_index(
118
, 'checks permission, then runs get_savings_elecetricity_beteween'
, 0
, '{"pv_service_month_id": "int(9)", "pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_savings_gas_between_api'
, 1
);

#get_dollar_per_mmbtu_gas_api

call add_proc_index(
119
, 'checks permission, then runs get_dollar_per_mmbtu_gas_api'
, 0
, '{"pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["gas"]'
, 'get_dollar_per_mmbtu_gas_api'
, 1
);

#update_gas_api

call add_proc_index(
120
, 'checks permission, then runs update_gas'
, 0
, ''
, ''
, '["gas"]'
, 'update_gas_api'
, 1
);

#update_gas_model_api

call add_proc_index(
121
, 'checks permission, then runs update_gas_model'
, 0
, ''
, ''
, '["gas"]'
, 1
);

#delete_buildings_gas_data_api

call add_proc_index(
122
, 'checks permission, then runs delete_buildings_gas_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)"}'
, ''
, '["gas"]'
, 1
);

#delete_gas_api

call add_proc_index(
123
, 'checks permission, then runs delete_gas'
, 0
, '{"pv_flag": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_gas_id": "int(9)"}'
, '{"rv_status_code": "int(9)"}'
, '["gas"]'
, 1
);

#END