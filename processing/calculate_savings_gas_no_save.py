import csv
from scipy import optimize
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
import sys
import json
sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config

def get_gas_ids(building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_gas_ids"
    params = [building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    e_ids = co.machine(jdat)
    e_id_return = []
    for e_id in e_ids:
        print(f"e_id: {e_id}")
        e_id_return.append(float(e_id["id"]))
    return e_id_return

def calculate_base_usage(gas_id, model_usage):
    proc_name = "calculate_base_usage_gas"
    params = [gas_id, model_usage, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage

def calculate_base_usage_one_slope(gas_id, model_log_id):
    proc_name = "calculate_base_usage_gas_one_slope"
    params = [gas_id, model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage

def get_model_log_id(model_id):
    proc_name = "get_model_log_id_gas"
    params = [model_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_log_id = co.machine_add(jdat)
    return model_log_id

def delete_model(model_id):
    proc_name = "delete_gas_model"
    params = [model_id]
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat) 

def calculate_monthly_savings(gas_id, model_log_id, building_id):
    proc_name = "calculate_monthly_savings_gas"
    params = [gas_id, model_log_id, building_id, 0]
    jdat = {"name": proc_name, "params": params}
    monthly_savings = co.machine_add(jdat)
    return monthly_savings

def get_service_month_date(service_month_id):    
    proc_name = "get_service_month_date"
    params = [service_month_id, 0]
    jdat = {"name": proc_name, "params": params}
    month_year = co.machine_add(jdat)
    return month_year

def get_cumulative_savings_electricity(building_id, service_month_id):
    proc_name = "get_cumulative_savings_electricity"
    params = [building_id, service_month_id, 0]
    jdat = {"name": proc_name, "params": params}
    gas_savings = co.machine_add(jdat)
    return gas_savings

def get_adjusted_usage(gas_id):
    proc_name = "get_adjusted_usage_gas"
    params = [gas_id, 0]
    jdat = {"name": proc_name, "params": params}
    gas_savings = co.machine_add(jdat)
    return gas_savings

def calculate_eui_gas(building_id, service_month_id):
    proc_name = "calculate_EUI_gas_no_store"
    params = [building_id, service_month_id, 0]
    jdat = {"name": proc_name, "params": params}
    eui = co.machine_add(jdat)
    return eui

def get_eui_electricity(service_month_id, building_id):
    proc_name = "get_EUI_electricity"
    params = [service_month_id, building_id, 0]
    jdat = {"name": proc_name, "params": params}
    eui = co.machine_add(jdat)
    return eui

def get_service_month_id(e_id):
    proc_name = "get_service_month_id_gas"
    params = [e_id, 0]
    jdat = {"name": proc_name, "params": params}
    service_month_id = co.machine_add(jdat)
    return service_month_id

def get_model_price(model_log_id):
    proc_name = "get_model_price"
    params = [model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    price = co.machine_add(jdat)
    return price

def main(user_id, building_id, model_id, post_service_month_id_start, post_service_month_id_end):

    model_log_id = get_model_log_id(model_id)
    
    e_ids = get_gas_ids(building_id, post_service_month_id_start, post_service_month_id_end)
    print(f"e_ids: {e_ids}")
    cumulative_savings = 0
    total_string = '['
    for e_id in e_ids:
        service_month_id = get_service_month_id(e_id)
        adjusted_usage = get_adjusted_usage(e_id)
        base_usage = calculate_base_usage(e_id, model_log_id)
        # electricity_price = get_model_price(model_log_id)
        # electricity_savings = get_cumulative_savings_electricity(building_id, service_month_id)
        month_year = get_service_month_date(service_month_id)
        # saved_usage = base_usage - adjusted_usage
        # percent_savings = saved_usage/base_usage
        # monthly_savings = saved_usage*gas_price
        # gas_savings = float(cumulative_savings) + float(monthly_savings)
        eui_gas = calculate_eui_gas(building_id, service_month_id)
        # eui_gas = get_eui_gas(building_id, service_month_id)
        # eui = eui_electricity + eui_gas        
        total_string = total_string + '{"adjusted_usage": ' + str(int(float(adjusted_usage))) + ', '
        total_string = total_string + '"base_usage": ' + str(int(float(base_usage))) + ', '
        total_string = total_string + '"service_month_id": ' + str(int(float(service_month_id))) + ', '
        total_string = total_string + '"month_year": ' + '"' + str(month_year) + '"' + ', '
        total_string = total_string + '"eui_gas": ' + str(round(float(eui_gas), 2)) + '}' + ", "
    total_string = total_string[:len(total_string)-2] + ']'
    print(f"TOTAL STRING: {total_string}")
    total_json = json.loads(total_string)
    # print(f"TOTAL STRING: {total_string}")
    print(f"TOTAL JSON: {total_json}")
    return total_json



if __name__ == "__main__":
    user_id = 6
    building_id = 20
    # ssession_number = 6
    post_start = 591
    post_end = 604
    model_id = 82

    main(user_id, building_id, model_id, post_start, post_end)