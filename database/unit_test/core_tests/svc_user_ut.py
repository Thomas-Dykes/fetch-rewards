import sys, time

sys.path.append("/home/ec2-user/pbc_portal/pbc")
import login as ll
import api_connector as aco
import connector as co
import config
import pymysql


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        try:
            t_table = test["table"]
            t_test = test["test"]
            t_result = test["result"]
            t_proc_index_id = test["proc_index_id"]

            if t_result:
                print(
                    f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
                )

            if not t_result:
                print(
                    f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
                )
        except Exception as e:
            print(f"\n\n Exception: {e} \n\n Occured")
            print(f"\n\n data: {test} \n\n")
            time.sleep(3)


def get_procs():
    return co.machine({"name": "get_public_procs", "params": {1}})


def test_proc(proc):
    try:
        aco.machine({"name": proc, "params": {1}})
        return True
    except pymysql.InternalError as e:
        number = int(str(e).split(",")[0][1:])
        if number == 1370:
            return False

        print(f"excption: {e}")
        print(f"number: {number}")
        return True

    except Exception as e:
        print(f"exception {e}")
        return True


def test_forbidden():
    if not (test_proc("get_public_procs")):
        print("pass")
        return {
            "test": "svc_forbidden",
            "result": True,
            "table": "proc_index",
            "proc_index_id": 666,
        }

    return {
        "test": "svc_forbidden",
        "result": False,
        "table": "proc_index",
        "proc_index_id": 666,
    }


def test_allowed(procs):
    tests = []
    for a in procs:
        result = test_proc(a["name"])
        print(f"result: {result}")

        test = {
            "test": "svc_allowed name: {}".format(a["name"]),
            "result": result,
            "table": "proc_index",
            "proc_index_id": a["id"],
        }

        print(f"test:{test}")

        tests.append(test)
    return tests


def machine(cookie):
    print(f"cookie: {cookie}")
    procs = get_procs()
    results = []
    for p in procs:
        print(p)
    # results.append(test_forbidden())
    for test in test_allowed(procs):
        results.append(test)
    return results


def main():
    cookie = ll.machine()
    print(f"cookie: {cookie}")
    machine(cookie)
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
