#Iterate through the Json and load it.  Create one loop function for each table that reads data from a json and loads it into a database.  Use SQL to write the database and the tables
import json

import connector as co
import config

users = []

def add_user(id, state, createdDate, lastLogin, role, signUpSource, active):
    proc_name = "add_user"
    params = [id, state, createdDate, lastLogin, role, signUpSource, active]
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

with open("./users.json/users.json", "r") as f:
    for line in f.readlines():
        users.append(json.loads(line))
        id = json.loads(line)["_id"]["$oid"]
        state = json.loads(line)["state"]
        createdDate = json.loads(line)["createdDate"]["$date"]
        try:
            lastLogin = json.loads(line)["lastLogin"]["$date"]
        except KeyError:
            lastLogin = 0
        role = json.loads(line)["role"]
        signUpSource = json.loads(line)["signUpSource"]
        state = json.loads(line)["state"]
        add_user(id, state, createdDate, lastLogin, role, signUpSource, state)
        
        





# for r in users:
#     print(r.keys())
print(users[0:5])