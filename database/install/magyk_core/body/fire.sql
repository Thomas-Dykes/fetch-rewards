

DELIMITER //

#375

CREATE OR REPLACE PROCEDURE fire_api(IN pv_user_id INT(9)
									,IN pv_ssession_number INT(9)
									, OUT rv_status_code INT(9)
				)
	BEGIN
		-- declare lcl_run_add_user int default 0;
		-- declare lcl_run_add_user_ggroup int default 0;
		-- declare lcl_run_hire int default 0;
		-- declare lcl_user_company_id int default 0;
		-- declare lcl_address_id int default 0;
		-- declare lcl_user_id int default 0;
		-- declare lcl_ggroup_id int default 0;
		-- declare lcl_job_id int default 0;

		-- SET rv_status_code = -6;
		-- call get_user_company_id(pv_log_user_id, lcl_user_company_id);
		-- call database_access_add(lcl_run_add_user, lcl_user_company_id
		-- , pv_log_user_id, 'm', pv_ssession_number, 'user');
		-- call database_access_add(lcl_run_add_person, lcl_user_company_id
        --         , pv_log_user_id, 'm', pv_ssession_number, 'person');
		-- call database_access_add(lcl_run_add_person_log, lcl_user_company_id
		-- , pv_log_user_id, 'm', pv_ssession_number, 'person_log');
		-- call database_access_add(lcl_run_add_user_job, lcl_user_company_id
        --         , pv_log_user_id, 'm', pv_ssession_number, 'user_job');
		-- call database_access_add(lcl_run_add_user_ggroup, lcl_user_company_id
        --         , pv_log_user_id, 'm', pv_ssession_number, 'user_ggroup');
		-- set lcl_run_hire = lcl_run_add_user*lcl_run_add_person*lcl_run_add_person_log*lcl_run_add_user_job*lcl_run_add_user_ggroup;
		-- if lcl_run_hire = 1 THEN
		    declare lcl_auth int default 0;
    	declare lcl_company_id int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		SET rv_status_code = -99;
		CALL fire(pv_user_id, rv_status_code);
		END IF;
	END//

#376

CREATE OR REPLACE PROCEDURE fire (IN pv_user_id INT(9)
									, OUT rv_status_code INT(9))
	BEGIN
		DECLARE lcl_person_id INT default 0;
		DECLARE lcl_ej_response INT default 0; 
		DECLARE lcl_eg_response INT default 0;
		DECLARE lcl_tl_response INT default 0;
		DECLARE lcl_p_response INT default 0;
		DECLARE lcl_e_response INT default 0;
		DECLARE lcl_response BIGINT default 0;
		SET rv_status_code = -6;
		CALL delete_users_ggroups(pv_user_id,lcl_eg_response);
		SET rv_status_code = -98;
        CALL delete_user(0, pv_user_id, lcl_e_response);
		select lcl_eg_response, lcl_e_response;
		set lcl_response = lcl_eg_response*lcl_e_response;
		IF lcl_response > 0 THEN
			SET rv_status_code = 1;
		END IF; 
		select rv_status_code;
	END//


-- CALL add_proc_index(
-- 	375
-- 	,'checks authentication, then runs fire'
-- 	,0
-- 	,'{"pv_user_id": "int(9)", "pv_log_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
-- 	,'{"rv_status_code": "int(9)"}'
-- 	,'["user", "person"]'
-- 	,'fire_api'
-- 	,1
-- );

-- CALL add_proc_index(
-- 	376
-- 	,'fire an user, move records to archive, public'
-- 	,0
-- 	,'{"pv_user_id": "int(9)", "pv_log_user_id": "int(9)"}'
-- 	,'{"rv_status_code": "int(9)"}'
-- 	,'["user", "person"]'
-- 	,'fire'
-- 	,0
-- );