import time, decimal, sys, random, string


sys.path.append("/home/ec2-user/pbc_portal/pbc")
# sys.path.append("../../../magyk")
import login
import connector as co
import config


def add_state(abbreviation, country_id, name):
    proc_name = "add_state_api"
    params = [abbreviation, country_id, name, 0]
    jdat = {"name": proc_name, "params": params}
    id = co.machine_add(jdat)
    print(f"state id: {id}")
    return id


def get_states():
    proc_name = "get_states_api"
    params = []
    jdat = {"name": proc_name, "params": params}
    states = co.machine(jdat)
    for state in states:
        print(f"state: {state}")
    return states


def get_state_by_name(name):
    proc_name = "get_state_by_name_api"
    params = [name]
    jdat = {"name": proc_name, "params": params}
    state = co.machine(jdat)
    print(f"state: {state}")
    return state


def get_state(pv_id):
    proc_name = "get_state_api"
    params = [pv_id]
    jdat = {"name": proc_name, "params": params}
    try:
        state = co.machine(jdat)[0]
        print(f"state: {state}")
        return state
    except Exception as e:
        print(f"exception: {e}")
        print(f"params: {params}")
        return {"id": "null", "name": "null"}


def delete_state(pv_id):
    proc_name = "delete_state"
    params = [pv_id, 0]
    jdat = {"name": proc_name, "params": params}
    status = co.machine_add(jdat)
    print(f"status: {status}")
    return status


def get_state_count():
    proc_name = "get_state_count"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    count = co.machine_add(jdat)
    print(f"count: {count}")
    return count


def test_add_1():
    abbreviation = randomString(5)
    country_id = 3
    name = randomString(5)
    id = add_state(abbreviation, country_id, name)
    time.sleep(0.1)
    state = get_state(id)
    lcl_abbreviation = state["abbreviation"]
    lcl_country_id = state["country_id"]
    lcl_name = state["name"]
    print(
        f"lcl_abbreviation: {lcl_abbreviation}, lcl_contry_id: {lcl_country_id}, lcl_name: {lcl_name}"
    )
    if lcl_abbreviation == abbreviation:
        if lcl_country_id == country_id:
            if lcl_name == name:
                delete = delete_state(id)
                if delete == 1:
                    return (
                        id,
                        {
                            "test": "test_add_1",
                            "result": True,
                            "table": "state",
                            "proc_index_id": [322, 323, 388, 389],
                        },
                    )
    return (
        id,
        {
            "test": "test_add_1",
            "result": False,
            "table": "state",
            "proc_index_id": [322, 323, 388, 389],
        },
    )


def test_add_2(name, abbreviation):
    country_id = 3
    id = add_state(abbreviation, country_id, name)
    time.sleep(0.1)
    state = get_state(id)
    lcl_abbreviation = state["abbreviation"]
    lcl_country_id = state["country_id"]
    lcl_name = state["name"]
    print(
        f"lcl_abbreviation: {lcl_abbreviation}, lcl_contry_id: {lcl_country_id}, lcl_name: {lcl_name}"
    )
    if lcl_abbreviation == abbreviation:
        if lcl_country_id == country_id:
            if lcl_name == name:
                return (
                    id,
                    {
                        "test": "test_add_2",
                        "result": True,
                        "table": "state",
                        "proc_index_id": [322, 323, 388],
                    },
                )
    return (
        id,
        {
            "test": "test_add_2",
            "result": False,
            "table": "state",
            "proc_index_id": [322, 323, 388],
        },
    )


def test_get_state_by_name(name1, name2, abbrev_1, abbrev_2, country_1, country_2):
    state_1 = get_state_by_name(name1)[0]
    state_2 = get_state_by_name(name2)[0]
    add_1 = {"abbreviation": abbrev_1, "country_id": country_1}
    add_2 = {"abbreviation": abbrev_2, "country_id": country_2}
    if state_1["abbreviation"] == add_1["abbreviation"]:
        if state_2["abbreviation"] == add_2["abbreviation"]:
            if state_1["country_id"] == add_1["country_id"]:
                if state_2["country_id"] == add_2["country_id"]:
                    return {
                        "test": "test_get_state_by_name",
                        "result": True,
                        "table": "state",
                        "proc_index_id": [401],
                    }
    return {
        "test": "test_get_state_by_name",
        "result": False,
        "table": "state",
        "proc_index_id": [401],
    }


def test_gets(n1, n2, a1, a2):
    states = get_states()
    count = get_state_count()
    index = count - 2
    index1 = count - 1
    state = states[index]
    state1 = states[index1]
    print(f"n1: {n1}, a1: {a1}")
    print(f"state: {state}")
    if state["abbreviation"] == a1:
        if state["name"] == n1:
            if state1["abbreviation"] == a2:
                if state1["name"] == n2:
                    return {
                        "test": "test_gets",
                        "result": True,
                        "table": "state",
                        "proc_index_id": [154, 155],
                    }
    return {
        "test": "test_gets",
        "result": False,
        "table": "state",
        "proc_index_id": [154, 155],
    }


def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(stringLength))


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        t_table = test["table"]
        t_test = test["test"]
        t_result = test["result"]
        t_proc_index_id = test["proc_index_id"]
        print(
            f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
        )


def machine(cookie):
    tests = []
    print(" \n \n ")

    abbreviation = randomString(5)
    name = randomString(5)

    abbreviation1 = randomString(5)
    name1 = randomString(5)

    lcl_id, result = test_add_1()
    tests.append(result)
    lcl_id_2, result_2 = test_add_2(name, abbreviation)
    tests.append(result_2)
    lcl_id_3, result_3 = test_add_2(name1, abbreviation1)

    tests.append(result_3)
    tests.append(test_gets(name, name1, abbreviation, abbreviation1))
    tests.append(test_get_state_by_name("Alabama", "Alaska", "AL", "AK", 1, 1))

    print(" \n \n ")
    return tests


def main():
    cookie = login.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
