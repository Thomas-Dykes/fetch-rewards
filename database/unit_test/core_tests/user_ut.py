import time, decimal, sys

import user_log_ut as ul

import address_ut as address

import ggroup_ut as ggroup

sys.path.append("/home/ec2-user/pbc_portal/pbc")
# sys.path.append("../../../magyk")
import login
import connector as co
import config


def get_user(cookie, user_id):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_user_api"
    params = [user_id, log_user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    #    user = co.machine(jdat)[0]
    try:
        user = co.machine(jdat)[0]
        print(f"user: {user}")
    except IndexError:
        return {"id": -6}
    return user


def get_user_infos(cookie):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_user_infos_api"
    params = [log_user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    user_infos = co.machine(jdat)
    for user_info in user_infos:
        print(f"user_info: {user_info}")
    return user_infos

def delete_user(cookie, user_id):
    user_id = cookie["user_id"]
    proc_name = "delete_user"
    params = [0, user_id, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)



def get_user_count():
    proc_name = "get_user_count"
    params = ["0"]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def get_user_company_count(user_id):
    proc_name = "get_user_company_count"
    params = [user_id, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def get_password(user_id):
    proc_name = "get_password"
    params = [user_id, 0]
    jdat = {"name": proc_name, "params": params}
    password = co.machine_add(jdat)
    return password


def renew_password(cookie, user_id, old_password, new_password):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "renew_password_api"
    params = [
        user_id,
        log_user_id,
        new_password,
        old_password,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    renewed_password = co.machine_add(jdat)
    print(f"renewed password: {renewed_password}")
    return renewed_password


def replace_password(cookie, user_id, new_password):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "replace_password_api"
    params = [user_id, new_password, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    replaced_password = co.machine_add(jdat)
    return replaced_password


def get_random_available_username(cookie):
    log_user_id = cookie["user_id"]
    proc_name = "get_random_available_username"
    params = [log_user_id, 0]
    jdat = {"name": proc_name, "params": params}
    username = co.machine_add(jdat)
    print(f"username:  {username}")
    return username


def get_user_id(username):
    proc_name = "get_user_id"
    params = [username, 0]
    jdat = {"name": proc_name, "params": params}
    user_id = co.machine_add(jdat)
    print(f"user id: {user_id}")
    return user_id

def get_my_info(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_my_info_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    try:
        my_info = co.machine(jdat)[0]
    except IndexError:
        my_info = "get my info failed, user ut"
    print(f"my_info: {my_info}")
    return my_info

def get_user_company(user_id):
    proc_name = "get_user_company_id"
    params = [user_id, 0]
    jdat = {"name": proc_name, "params": params}
    company_id = co.machine_add(jdat)
    print(f"company id: {company_id}")
    return company_id

def get_root_user(company_id):
    proc_name = "get_root_user"
    params = [company_id, 0]
    jdat = {"name": proc_name, "params": params}
    user_id = co.machine_add(jdat)
    print(f"root user: {user_id}")
    return user_id

def get_random_available_username(user_id):
    proc_name = "get_random_available_username"
    params = [user_id, 0]
    jdat = {"name": proc_name, "params": params}
    username = co.machine_add(jdat)
    print(f"username: {username}")
    return username


def get_user_username(cookie, user_id):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_user_username_api"
    params = [user_id, log_user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    user_username = co.machine_add(jdat)
    print(f"user_username: {user_username}")
    return user_username

def add_user(cookie, address_id, admin, customer, dob, email_address, user, first_name, last_name, password, phone_number, start_date, username):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_user_api"
    params = [address_id, admin, customer, dob, email_address, user, first_name, 0, last_name, user_id, password, phone_number, ssession_number, start_date, username, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)

def add_random_user(cookie, address_id, admin, customer, dob, email_address, user, first_name, last_name, password, phone_number, start_date):
    user_id = cookie["user_id"]
    proc_name = "add_random_user"
    params = [address_id, admin, customer, dob, email_address, user, first_name, 0, last_name, user_id, password, phone_number, start_date, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)

def fire(cookie, user_id):
    proc_name = "fire_api"
    ssession_number = cookie["ssession_number"]
    params = [user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    # print(f"delete jdat: {jdat}")
    status = co.machine_add(jdat)
    return status


def hire(
    cookie,
    city,
    dob,
    email,
    first,
    last,
    pwd,
    phone_number,
    start_date,
    state_id,
    address,
    username,
    zip_code,
):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    jedi_ggroup = ggroup.get_jedi_group_id(log_user_id)
    proc_name = "hire_api"
    params = [
            1,
            city,
            0,
            dob,
            email,
            0,
            first,
            jedi_ggroup,
            last,
            log_user_id,
            config.login_test_password,
            phone_number,
            ssession_number,
            start_date,
            state_id,
            address,
            config.login_test_user,
            zip_code,
            0
    ]
    jdat = {"name": proc_name, "params": params}
    user_id = co.machine_add(jdat)
    print(f"user id: {user_id}")
    return user_id


def get_users(cookie):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_users_api"
    params = [log_user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    users = co.machine(jdat)
    for user in users:
        print(f"user: {user}")
    return users



def hire_yoda(cookie):

    username = config.login_test_user
    password = config.login_test_password
    user_id = get_user_id(username)
    session_number = cookie["ssession_number"]
    # if user_id > 0:
    #     delete_response = fire(cookie, user_id)
    #     return hire_yoda(cookie)

    if user_id == -6 or user_id == 0:
        return hire(
            cookie,
            "ranger town",
            "1980/01/01",
            "your@mom.me",
            "ron",
            "john",
            config.login_test_password,
            "666-555-7777",
            "2019-01-01",
            1,
            "98 fix street",
            config.login_test_user,
            91203,
        )

def test_hire(cookie):
    print("TEST HIRE RIGHT HERE")
    username = config.login_test_user
    password = config.login_test_password
    user_id = get_user_id(username)
    # if user_id > 0:
    #     delete_response = fire(cookie, user_id)
    #     return test_hire(cookie)

    if user_id == -6 or user_id == 0:
        rv_user_id = hire_yoda(cookie)
        rv_user_username = get_user_username(cookie, rv_user_id)
        print(f"rv user id: {rv_user_id}")
        print(f"USERNAME: {username}")
        print(f"rv_user_username: {rv_user_username}")
        if username == rv_user_username:
            delete_user(cookie, rv_user_id)
            return (
                rv_user_id,
                {
                    "test": "test_hire",
                    "result": True,
                    "table": "user",
                    "proc_index_id": 90,
                },
            )
    return (
        -77,
        {
            "test": "test_hire",
            "result": False,
            "table": "user",
            "proc_index_id": 90,
        },
    )


def test_fire(cookie, yoda_id):

    fire_status = fire(cookie, yoda_id)
    print(f"fire, status: {fire_status}")
    if fire_status == 1:
        return {
            "test": "test_fire",
            "result": True,
            "table": "user",
            "proc_index_id": [405, 406],
        }
    return {
        "test": "test_fire",
        "result": False,
        "table": "user",
        "proc_index_id": [405, 406],
    }



def test_delete_foreign_key_duplicate_error(pv_user_id):
    print("test delete user needs to have proper return")
    proc_name = "delete_user"
    params = [0, pv_user_id, 0]
    jdat = {"name": proc_name, "params": params}
    status_code = co.machine_add(jdat)
    print(f"status: {status_code}")
    if status_code == -1:
        return {
            "test": "test_foreign_key_duplicate_error",
            "result": True,
            "table": "user",
            "proc_index_id": [355],
        }
    return {
        "test": "test_foreign_key_duplicate_error",
        "result": False,
        "table": "user",
        "proc_index_id": [355],
    }


def test_add(cookie, address_id, admin, customer, dob, email_address, user, first_name, last_name, pwd, phone_number, start_date, username):
    id = add_user(cookie, address_id, admin, customer, dob, email_address, user, first_name, last_name, pwd, phone_number, start_date, username)
    time.sleep(0.1)
    print(f"id: {id}")
    print(" \n \n ")
    user = get_user(cookie, id)
    print(user)
    lcl_user_id = user["id"]

    lcl_start_date = str(user["start_date"]).replace("-", "/")
    # print("lcl start date: {}, type: {}".format(lcl_start_date, type(lcl_start_date)))
    lcl_username = user["username"]

    print(f"lcl_user_id: {lcl_user_id}, lcl_start_date, {lcl_start_date}, lcl_username, {lcl_username}")
    if lcl_user_id == id:
        print("passed user_id")
        print(f"lcl: {lcl_start_date}, start: {start_date}")
        if str(lcl_start_date) == start_date:
            print("passed start date")
            if lcl_username == username:
                print("passed username")
                return (
                    id,
                    {
                        "test": "test_add_1",
                        "result": True,
                        "table": "user",
                        "proc_index_id": [48, 49, 65, 64],
                    },
                )
    return (
        id,
        {
            "test": "test_add_1",
            "result": False,
            "table": "user",
            "proc_index_id": [48, 49, 65, 64],
        },
    )


# cookie, address_id, dob, email_address, first_name, last_name, phone_number


def test_renew_password(cookie, user_id, old_password, new_password):

    new_password_renew = renew_password(cookie, user_id, old_password, new_password)
    cookie = login.inbound_login(config.login_test_user, new_password)
    print(f"cookie: {cookie}")

    if cookie["ssession_number"] > 0:
        return {
            "test": "test_renew_password",
            "result": True,
            "table": "user",
            "proc_index_id": [358],
        }
    return {
        "test": "test_renew_password",
        "result": False,
        "table": "user",
        "proc_index_id": [358],
    }


def test_replace_password(pv_cookie, user_id, new_password):
    response = replace_password(pv_cookie, user_id, new_password)
    print(f"\n      response: {response}")
    cookie = login.inbound_login(config.login_test_user, new_password)
    print(f"MOTHERFUCKING COOKIE: {cookie}")

    if cookie["ssession_number"] > 0:
        return {
            "test": "test_replace_password",
            "result": True,
            "table": "user",
            "proc_index_id": [359],
        }
    input(f"cookie: {cookie}")
    return {
        "test": "test_replace_password",
        "result": False,
        "table": "user",
        "proc_index_id": [359],
    }


# def test_get_user_info(
#     cookie,
#     address_id,
#     dob,
#     email_address,
#     first_name,
#     last_name,
#     user_id,
#     phone_number,
#     pwd,
#     start_date,
#     username,
# ):
#     info_user_id = add_user(cookie, address_id, 1, 0, dob, email_address, 0, first_name, last_name, pwd, phone_number, start_date, u1)
#     print("info_user_id is:")
#     print(info_user_id)
#     user_id = add_user(
#         cookie, address_id, dob, email_address, first_name, last_name, phone_number
#     )
#     info = get_user_info(cookie, info_user_id)
#     lcl_first_name = info["first_name"]
#     lcl_last_name = info["last_name"]
#     lcl_username = info["username"]
#     if lcl_first_name == first_name:
#         if lcl_last_name == last_name:
#             if lcl_username == username:
#                 return {
#                     "test": "test_get_user_info",
#                     "result": True,
#                     "table": "user, user",
#                     "proc_index_id": [52, 53],
#                 }
#     return {
#         "test": "test_get_user_info",
#         "result": False,
#         "table": "user, user",
#         "proc_index_id": [52, 53],
#     }


def test_get_user_username(cookie, user_id):
    user = get_user(cookie, user_id)
    lcl_username = user["username"]
    print(f"lcl_username: {lcl_username}")
    proc_username = get_user_username(cookie, user_id)
    print(f"proc_username: {proc_username}")
    if lcl_username == proc_username:
        return {
            "test": "test_get_user_username",
            "result": True,
            "table": "user",
            "proc_index_id": [59, 60],
        }
    return {
        "test": "test_get_user_username",
        "result": False,
        "table": "user",
        "proc_index_id": [59, 60],
    }


def test_get_my_info(cookie):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    user = get_user(cookie, log_user_id)
    user_id = user["id"]
    user = get_user(cookie, user_id)
    user_log_id = user["user_log_id"]
    user_log = ul.get_user_log(cookie, user_log_id)
    out_first_name = user_log["first_name"]
    out_last_name = user_log["last_name"]
    out_username = user["username"]
    my_info = get_my_info(cookie)
    #    lcl_name = my_info["name"]"user_name_test_3"
    lcl_first_name = my_info["first_name"]
    lcl_last_name = my_info["last_name"]
    lcl_username = my_info["username"]
    print("first_name is")
    print(lcl_first_name)
    print("print first_name is")
    print(out_first_name)
    if lcl_first_name == out_first_name:
        if lcl_last_name == out_last_name:
            if lcl_username == out_username:
                return {
                    "test": "test_get_my_info",
                    "result": True,
                    "table": "user, user, user_log",
                    "proc_index_id": [367, 369, 370],
                }
    return {
        "test": "test_get_my_info",
        "result": False,
        "table": "user, user",
        "proc_index_id": [367, 369, 370],
    }


def test_get_user_infos(cookie):
    user_infos = get_user_infos(cookie)
    count = get_user_company_count(cookie["user_id"])
    user_info_one = user_infos[count - 2]
    user_info_two = user_infos[count - 1]
    add_1 = {"first_name": "Jiminy", "last_name": "Cricket"}
    add_2 = {"first_name": "roger", "last_name": "rabbit"}
    print("THIS IS WHERE user INFO IS PRINTING!")
    print(f"user_info_one: {user_info_one}")
    print(f"add_one: {add_1}")
    print(f"user_info_two: {user_info_two}")
    print(f"add_two: {add_2}")
    if str(add_1["first_name"]) == str(user_info_one["first_name"]):
        if str(add_2["first_name"]) == str(user_info_two["first_name"]):
            if str(add_1["last_name"]) == str(user_info_one["last_name"]):
                if str(add_2["last_name"]) == str(user_info_two["last_name"]):
                    return {
                        "test": "test_get_user_infos",
                        "result": True,
                        "table": "user, user",
                        "proc_index_id": [54, 55],
                    }
    return {
        "test": "test_get_user_infos",
        "result": False,
        "table": "user, user",
        "proc_index_id": [54, 55],
    }


def test_gets(cookie, p1, p2, s1, s2, u1, u2):
    users = get_users(cookie)
    count = get_user_company_count(cookie["user_id"])
    users_count = len(users)
    print(f"[user get] count: {count}, emp count: {users_count}")
    one = count - 2
    two = count - 1
    user_one = users[one]
    user_two = users[two]
    add_1 = {"user_id": p1, "start_date": s1.replace("/", "-"), "username": u1}
    add_2 = {"user_id": p2, "start_date": s2.replace("/", "-"), "username": u2}
    print(f"user one: {user_one}")
    print(f"add_one: {add_1}")
    print(f"user_two: {user_two}")
    print(f"add_two: {add_2}")
    print("start date: {}".format(str(user_two["start_date"])))
    if str(add_1["user_id"]) == str(user_one["user_id"]):
        print("user match")
        if add_1["start_date"] == str(user_one["start_date"]):
            print("start date")
            if add_1["username"] == user_one["username"]:
                print("username")
                if str(add_2["user_id"]) == str(user_two["user_id"]):
                    print("user id")
                    if add_2["start_date"] == str(user_two["start_date"]):
                        print("start date")
                        if add_2["username"] == user_two["username"]:
                            print("username")
                            if count == len(users):
                                print("len correct")
                                return {
                                    "test": "test_gets",
                                    "result": True,
                                    "table": "user",
                                    "proc_index_id": [51, 50],
                                }
    return {
        "test": "test_gets",
        "result": False,
        "table": "user",
        "proc_index_id": [51, 50],
    }


def test_get_user_id(cookie, user_id):
    print("TEST GET USER ID")
    user = get_user(cookie, user_id)
    lcl_id = user["id"]
    print(f"lcl_id: {lcl_id}")
    proc_id = get_user_id(config.login_test_user)
    print(f"user_id: {user_id}")
    if lcl_id == proc_id:
        return {
            "test": "test_get_user_id",
            "result": True,
            "table": "user",
            "proc_index_id": [61, 62],
        }
    return {
        "test": "test_get_user_id",
        "result": False,
        "table": "user",
        "proc_index_id": [61, 62],
    }


def test_delete_user(cookie, e_id):
    response = delete_user(cookie, e_id)
    print(f"delete response:{response}")
    if response > 0:
        return {
            "test": "test_delete_user",
            "result": True,
            "table": "user",
            "proc_index_id": [410],
        }
    return {
        "test": "test_delete_user_id",
        "result": False,
        "table": "user",
        "proc_index_id": [410],
    }


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        t_table = test["table"]
        t_test = test["test"]
        t_result = test["result"]
        t_proc_index_id = test["proc_index_id"]
        print(
            f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
        )


def machine(cookie):
    user_id = cookie["user_id"]
    tests = []
    print(" \n \n ")
    a1 = address.add_address(
        cookie, "chicago", "1.3", "2.44", 1, "596 N Western", 25645
    )
    d1 = "1992/10/10"
    em1 = "whocares@blah.web"
    f1 = "Jiminy"
    l1 = "Cricket"
    u1 = get_random_available_username(user_id)
    pwd = "password"
    # p1 = add_user(
    #     cookie, a1, 1, 0, d1, em1, 0, f1, l1, pwd, "959-050-0000", "2020/01/01", u1
    # )
    ph1 = "18478675309"
    s1 = "2019/01/01"
    u2 = get_random_available_username(user_id)
    u3 = get_random_available_username(user_id)

    f2 = "roger"
    l2 = "rabbit"  # you need to use get random username for this
    a2 = address.add_address(
        cookie, "asheville", "2.3", "4.44", 1, "33 Rock Lane", 56756
    )
    # p2 = add_user(
    #     cookie, a2, 1, 0, d1, em1, 0, f2, l2, pwd, "959-050-4444", "2020/01/01", u2
    # )
    s2 = "2019/04/05"
    e_id, result = test_add(cookie, a1, 1, 0, d1, em1, 0, f1, l1, pwd, "959-050-0000", "2020/01/01", u1)
    print(f"THE RESULT: {result}")
    e_id_2, result_2 = test_add(cookie, a2, 1, 0, d1, em1, 0, f2, l2, pwd, "959-050-4444", "2020/01/01", u2)
    tests.append(result)
    tests.append(result_2)
    tests.append(test_get_user_username(cookie, user_id))

    print(" \n \n ")
    # tests.append(test_gets(cookie, p1, p2, s1, s2, u1, u2))
    tests.append(test_delete_foreign_key_duplicate_error(cookie["user_id"]))
    tests.append(test_get_my_info(cookie))
    tests.append(test_get_user_infos(cookie))
    # tests.append(
    #     test_get_user_info(cookie, a1, d1, em1, f1, l1, e_id, ph1, "pwd", s1, u3)
    # )
    # # HERE
    # tests.append(test_get_user_tasks(cookie, user_id))

    # these all need to happen in order
    emp_id, hire_result = test_hire(cookie)
    print(f"EMP_ID: {emp_id}")
    tests.append(hire_result)
    tests.append(test_get_user_id(cookie, emp_id))
    tests.append(
        test_renew_password(cookie, emp_id, config.login_test_password, "train_trAx")
    )
    # tests.append(test_replace_password(cookie, emp_id, config.login_test_password))
    tests.append(test_fire(cookie, emp_id))
    # tests.append(test_delete_user(cookie, e_id))
    # delete_user(cookie, e_id_2)
    # address.delete_address(cookie, a1)
    # address.delete_address(cookie, a2)
    # delete_user(p1, user_id)
    # delete_user(p2, user_id)
    # delete_user_ggroups()
    return tests


# def test_get_my_info(cookie, address_id, dob, email_address, first_name, last_name, user_id, phone_number, pwd, start_date, username):


def main():
    cookie = login.machine()
    print(f"cookie:￼ {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
