DELIMITER //



#666
CREATE OR REPLACE PROCEDURE get_outbound_count(OUT rv_out INT)
	BEGIN
		SELECT COUNT(1) into rv_out from outbound;
	END//


#666
CREATE OR REPLACE PROCEDURE get_outbounds_api(IN pv_log_user_id int(9)
						, IN pv_ssession_number int(9))
	BEGIN
		declare lcl_run_get_outbounds int default 0;
		call database_access_get(lcl_run_get_outbounds, pv_log_user_id, 'r', pv_ssession_number, 'outbound');
		if lcl_run_get_outbounds = 1 THEN
		call get_outbounds(pv_log_user_id);
		end if;
	END//
#666
CREATE OR REPLACE PROCEDURE get_outbounds(IN pv_log_user_id int(9))
        BEGIN
             	declare lcl_user_company_id int default 0;
                CALL get_user_company_id(pv_log_user_id, lcl_user_company_id);
                SELECT outbound.company_id, outbound.e_time, outbound.flag, outbound.id
				, outboun.json, outbound.log_user_id, outbound.table_name
                FROM outbound
                where company_id = lcl_user_company_id;
        END//


#666
CREATE OR REPLACE PROCEDURE get_my_outbounds_api(IN pv_log_user_id int(9)
                                                , IN pv_ssession_number int(9))
        BEGIN
             	declare lcl_run_get_outbounds int default 0;
                call database_access_get(lcl_run_get_outbounds, pv_log_user_id, 'r', pv_ssession_number, 'outbound');
                if lcl_run_get_outbounds = 1 THEN
                call get_my_outbounds(pv_log_user_id);
                end if;
        END//

#666

CREATE OR REPLACE PROCEDURE get_my_outbounds(IN pv_log_user_id int(9)
					)
	BEGIN
             	declare lcl_user_company_id int default 0;
                CALL get_user_company_id(pv_log_user_id, lcl_user_company_id);
                SELECT outbound.company_id, outbound.e_time, outbound.flag, outbound.id
				, outbound.json, outbound.log_user_id, outbound.table_name
                FROM outbound
		INNER JOIN user_outbound ON outbound.id = user_outbound.outbound_id
                where outbound.company_id = lcl_user_company_id AND user_outbound.user_id = pv_log_user_id;
        END//

#666

CREATE OR REPLACE PROCEDURE get_outbound_api(IN pv_log_user_id int(9)
											, IN pv_outbound_id int(9)
                                            , IN pv_ssession_number int(9)
                                                )
        BEGIN
	declare lcl_run_get_outbound int default 0;
        call database_access_get(lcl_run_get_outbound, pv_log_user_id, 'r', pv_ssession_number, 'outbound');
        IF lcl_run_get_outbound = 1 THEN call get_outbound(pv_log_user_id, pv_outbound_id);
        END IF;
        END//

#666

CREATE OR REPLACE PROCEDURE get_outbound(IN pv_log_user_id int(9)
										, IN pv_outbound_id int(9)
                               )
        BEGIN
		declare lcl_user_company_id int default 0;
        call get_user_company_id(pv_log_user_id, lcl_user_company_id);
        SELECT outbound.company_id, outbound.e_time, outbound.flag, outbound.id
		, outbound.json, outbound.log_user_id, outbound.table_name
        FROM outbound
        where outbound.company_id = lcl_user_company_id
        AND outbound.id = pv_outbound_id;
        END//


#666
CREATE OR REPLACE PROCEDURE get_recent_outbound(IN pv_log_user_id INT(10))
	BEGIN
		declare lcl_user_company_id int default 0;
		call get_user_company_id(pv_log_user_id, lcl_user_company_id);
		SELECT *
		FROM outbound
		WHERE company_id = lcl_user_company_id
		ORDER BY id DESC
		LIMIT 5;

		-- where id = (select max(id)
		-- 		from outbound
		-- 		where company_id = lcl_user_company_id);
	END//

#666
CREATE OR REPLACE PROCEDURE add_outbound(IN pv_flag int(3)
							, IN pv_log_user_id int(9)
							, IN pv_json varchar(500)
							, IN table_name varchar(40)
							, OUT rv_outbound_id int(9)
							)
	BEGIN
		declare lcl_user_company_id int default 0;
		call get_user_company_id(pv_log_user_id, lcl_user_company_id);
		select MAX(id) from outbound where outbound.log_user_id = pv_log_user_id INTO rv_outbound_id;
		IF rv_outbound_id IS NULL THEN
		set rv_outbound_id = -1;
		END IF;
		INSERT INTO outbound(company_id, e_time,flag,json,log_user_id, table_name)
		VALUES (lcl_user_company_id, UNIX_TIMESTAMP(NOW()), pv_flag, pv_json, pv_log_user_id, table_name);
	END//