import time, decimal, sys, unittest
from datetime import date
import pandas as pd

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import login
import connector as co


def add_service_month(cookie, month, year, date):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_service_month_api"
    params = [month, year, user_id, ssession_number, date, 0]
    jdat = {"name": proc_name, "params": params}
    service_month_id = co.machine_add(jdat)
    return service_month_id


def get_eligible_service_months_electricity(cookie, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_eligible_service_months_electricity_api"
    params = [building_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    eligible_service_months_electricity = co.machine_add(jdat)
    return eligible_service_months_electricity


def get_eligible_service_months_gas_api(cookie, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_eligible_service_months_gas_api"
    params = [building_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    eligible_service_month_gas = co.machine_add(jdat)
    return eligible_service_month_gas


def get_eligible_post_period_service_months(cookie, service_month_id, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_eligible_post_period_service_months_api"
    params = [service_month_id, building_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    eligible_post_period_service_months = co.machine_add(jdat)
    return eligible_post_period_service_months


def get_service_months(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_service_months_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    service_months = co.machine(jdat)
    # for service_month in service_months:
    #     print(f"service_month: {service_month}")
    return service_months

# pymysql.err.OperationalError: (1054, "Unknown column 'service_month.flag' in 'field list'")
def get_service_month(cookie, service_month_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_service_month_api"
    params = [service_month_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    service_month = co.machine(jdat)[0]
    print(f"service_month: {service_month}")
    return service_month


class serviceMonthTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.service_month_id_one = None
        cls.service_month_id_two = None

    def test_add_1(self):
        entered_data = {"month": "02", "year": "1009", "date": date(1999, 2, 1)}
        try:
            service_month_id = add_service_month(
                self.cookie,
                entered_data["month"],
                entered_data["year"],
                entered_data["date"],
            )
            time.sleep(0.1)
            print(f"\n\n{service_month_id} Where the fuck i am \n\n")
            service_month_data = get_service_month(self.cookie, service_month_id)
            for key in entered_data:
                self.assertEqual(
                    entered_data[key], service_month_data[key], f"{key} shoule be same"
                )
            self.test_name.append("test_add_1")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_service_month_api", "get_service_month_api"]
            )
            self.__class__.service_month_id_one = service_month_id
        except Exception as e:
            self.test_name.append("test_add_1")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_service_month_api", "get_service_month_api"]
            )
            raise Exception(repr(e))

    def test_add_2(self):
        entered_data = {"month": "01", "year": "1022", "date": date(1989, 2, 1)}
        try:
            service_month_id = add_service_month(
                self.cookie,
                entered_data["month"],
                entered_data["year"],
                entered_data["date"],
            )
            time.sleep(0.1)
            service_month_data = get_service_month(self.cookie, service_month_id)
            for key in entered_data:
                self.assertEqual(
                    entered_data[key], service_month_data[key], f"{key} should be same"
                )
            self.test_name.append("test_add_2")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_service_month_api", "get_service_month_api"]
            )
            self.__class__.service_month_id_two = service_month_id
        except Exception as e:
            self.test_name.append("test_add_2")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_service_month_api", "get_service_month_api"]
            )
            raise Exception(repr(e))

    def test_get_all(self):
        service_month_one = {"month": None, "year": None, "date": None}
        service_month_two = {"month": None, "year": None, "date": None}
        try:
            for item in get_service_months(self.cookie):
                if item["id"] == self.service_month_id_one:
                    service_month_one = item
                if item["id"] == self.service_month_id_two:
                    service_month_two = item

            service_month_1_by_get = get_service_month(
                self.cookie, self.service_month_id_one
            )
            service_month_2_by_get = get_service_month(
                self.cookie, self.service_month_id_two
            )

            for key in service_month_one:
                self.assertEqual(
                    service_month_one[key],
                    service_month_1_by_get[key],
                    f"{key} should be same",
                )
                self.assertEqual(
                    service_month_two[key],
                    service_month_2_by_get[key],
                    f"{key} should be same",
                )
            self.test_name.append("test_gets_all")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["get_service_months_api", "get_service_month_api"]
            )
        except Exception as e:
            self.test_name.append("test_gets_all")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["get_service_months_api", "get_service_month_api"]
            )


if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = serviceMonthTest()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())
