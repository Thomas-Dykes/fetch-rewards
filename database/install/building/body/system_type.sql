DELIMITER //

CREATE OR REPLACE PROCEDURE add_system_type_api(IN pv_name varchar(30)
                                            , IN pv_abv varchar(10)
                                            , IN pv_description varchar(100)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , OUT rv_system_id int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_system_type(pv_name, pv_abv, pv_description, pv_user_id, rv_system_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE add_system_type(IN pv_name varchar(30)
                                            ,IN pv_abv varchar(10)
                                            ,IN pv_description varchar(100)
                                            ,IN pv_user_id int(10)
                                            , OUT rv_system_id int(10))
BEGIN
    SET rv_system_id = -6;
    INSERT INTO system_type(name,abv, description,log_user_id, flag, e_time)
    VALUES(pv_name,pv_abv, pv_description, pv_user_id, 0, UNIX_TIMESTAMP(NOW()));

    SELECT MAX(id)
    INTO rv_system_id
    FROM system_type
    WHERE log_user_id = pv_user_id;
END //

CREATE OR REPLACE PROCEDURE get_system_types_api(IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_system_types(pv_user_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_system_types(IN pv_user_id int(9)
                                            )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select system_type.id, system_type.name, system_type.abv
    , system_type.description, system_type.e_time, system_type.flag
    , system_type.log_user_id
    FROM system_type
    WHERE system_type.company_id = lcl_company_id;
END//

CREATE OR REPLACE PROCEDURE get_system_type_api(IN pv_system_type_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_system_type(pv_system_type_id, pv_user_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_system_type(IN pv_system_type_id int(9)
                                            , IN pv_user_id int(9)
                                            )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select system_type.id, system_type.name, system_type.abv
    , system_type.description, system_type.e_time, system_type.flag
    , system_type.log_user_id
    FROM system_type
    WHERE system_type.company_id = lcl_company_id AND system_type.id = pv_system_type_id;
END//
