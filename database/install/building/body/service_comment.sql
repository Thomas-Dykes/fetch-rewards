DELIMITER //

CREATE OR REPLACE PROCEDURE add_service_comment_api(IN pv_service_id int(10)
                                                , IN pv_comment varchar(100)
                                                , IN pv_user_id int(10)
                                                , IN pv_ssession_number int(9)
                                                , OUT rv_comment_id int(10))
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_service_comment(pv_service_id, pv_comment, pv_user_id, rv_comment_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE add_service_comment(IN pv_service_id int(10)
                                                , IN pv_comment varchar(100)
                                                , IN pv_user_id int(10)
                                                , OUT rv_comment_id int(10))
BEGIN
    DECLARE lcl_company_id int default 0;
    CALL get_user_company_id(pv_user_id, lcl_company_id);

    INSERT INTO service_comment 
                (service_id, 
                comment, 
                company_id, 
                flag, 
                e_time, 
                log_user_id) 
    VALUES     (pv_service_id,
                pv_comment,  
                lcl_company_id, 
                0, 
                Unix_timestamp(( Now() )), 
                pv_user_id); 

    SELECT MAX(id)
    INTO rv_comment_id
    FROM service_comment
    WHERE log_user_id = pv_user_id;
END//


CREATE OR REPLACE PROCEDURE get_service_comments_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_service_comments(pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_service_comments(IN pv_user_id int(9)
                                                )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    SELECT service_comment.id,
        service_comment.service_id, 
       service_comment.comment comment, 
       service_comment.e_time, 
       service_comment.flag, 
       service_comment.log_user_id 
FROM   service_comment 
WHERE  company_id = lcl_company_id; 
END//

CREATE OR REPLACE PROCEDURE get_services_comments_api(IN pv_service_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_services_comments(pv_service_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_services_comments(IN pv_service_id int(9)
                                                , IN pv_user_id int(9)
                                                )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select service_comment.id, service_comment.service_id
    , service_comment.comment comment, service_comment.e_time
    , service_comment.flag, service_comment.log_user_id, user.username
    FROM service_comment INNER JOIN sservice ON sservice.id = service_comment.service_id
    INNER JOIN user ON service_comment.log_user_id = user.id
    where service_comment.company_id = lcl_company_id
    AND sservice.id = pv_service_id;
END//


CREATE OR REPLACE PROCEDURE update_my_service_comment_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_service_comment_id int(9)
                                            , IN pv_new_service_id int(9)
                                            , IN pv_new_comment varchar(100)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    declare lcl_log_user_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    SELECT service_comment.log_user_id INTO lcl_log_user_id FROM service_comment WHERE id = pv_service_comment_id;

    IF lcl_auth = 1 AND pv_user_id = lcl_log_user_id THEN
        call update_my_service_comment(pv_service_comment_id, pv_new_service_id, pv_new_comment);
    END IF;
END//


CREATE OR REPLACE PROCEDURe update_my_service_comment(IN pv_service_comment_id int(9)
                                            , IN pv_new_service_id int(9)
											, IN pv_new_comment varchar(100)
                                            )
BEGIN
    update service_comment SET 
    service_id = pv_new_service_id,
	comment = pv_new_comment
    WHERE id = pv_service_comment_id;
END//

CREATE OR REPLACE PROCEDURE delete_my_service_comment_api(IN pv_flag int(3)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_service_comment_id int(9)
                                            , OUT rv_status_code int(9)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    declare lcl_log_user_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    SELECT service_comment.log_user_id INTO lcl_log_user_id FROM service_comment WHERE id = pv_service_comment_id;

    IF lcl_auth = 1 AND pv_user_id = lcl_log_user_id THEN
        call delete_my_service_comment(pv_service_comment_id, rv_status_code);
    END IF;
END//

CREATE OR REPLACE PROCEDURE delete_my_service_comment(IN pv_flag int(3)
                                        , IN pv_service_comment_id int(9)
										, OUT rv_status_code int(9))
BEGIN
	declare lcl_outbound_id int(9) default 0;
	declare rv_service_comment_json varchar(500) default '';
	declare lcl_service_id int(9) default 0;
	declare lcl_parent_id int(9) default 0;
	declare lcl_comment varchar(100) default 0;
    declare lcl_e_time int(13) default 0;
    declare lcl_flag int(3) default 0;
    declare lcl_company_id int(9) default 0;
    declare lcl_log_user_id int(9) default 0;
	declare lcl_service_id_string varchar(40) default '';
	declare lcl_parent_id_string varchar(40) default '';
	declare lcl_comment_string varchar(40) default '';
	declare lcl_company_id_string varchar(40) default '';
	declare lcl_e_time_string varchar(40) default '';
	declare lcl_flag_string varchar(40) default '';
    declare lcl_log_user_id_string varchar(40) default '';
	DECLARE EXIT HANDLER FOR 1451
	-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails 
	BEGIN
		select -1 into rv_status_code;
	END;
	select service_comment.service_id INTO lcl_service_id FROM service_comment WHERE service_comment.id = pv_service_comment_id;
	select service_comment.parent_id into lcl_parent_id FROM service_comment WHERE service_comment.id = pv_service_comment_id;
	select service_comment.comment into lcl_comment FROM service_comment WHERE service_comment.id = pv_service_comment_id;
    select service_comment.e_time into lcl_e_time FROM service_comment WHERE service_comment.id = pv_service_comment_id;
    select service_comment.flag into lcl_flag FROM service_comment WHERE service_comment.id = pv_service_comment_id;
    select service_comment.company_id into lcl_company_id FROM service_comment WHERE service_comment.id = pv_service_comment_id;
    select service_comment.log_user_id INTO lcl_log_user_id FROM service_comment WHERE service_comment.id = pv_service_comment_id;
	call jsonify_int(lcl_service_id, 'service_id', lcl_service_id_string);
	call jsonify_int(lcl_parent_id, 'parent_id', lcl_parent_id_string);
	call jsonify_string('comment', lcl_comment, lcl_comment_string);
	call jsonify_int(lcl_e_time, 'e_time', lcl_e_time_string);
	call jsonify_int(lcl_flag, 'flag', lcl_flag_string);
	call jsonify_int(lcl_company_id, 'company_id', lcl_company_id_string);
	call jsonify_int(lcl_log_user_id, 'log_user_id', lcl_log_user_id_string);	
    select concat("{", lcl_service_id_string, ",", lcl_parent_id_string, ",", lcl_comment_string, ",", lcl_e_time_string, ",", lcl_flag_string, ",", lcl_company_id_string, ",", lcl_log_user_id_string, "}'") into rv_service_comment_json;
    call add_outbound(pv_flag, pv_service_comment_id, rv_service_comment_json,  'service_comment', lcl_outbound_id);
	DELETE FROM service_comment where id = pv_service_comment_id;
    select lcl_outbound_id into rv_status_code;
END//

