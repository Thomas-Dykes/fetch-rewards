set -exou pipefail

echo "begin head deploy"

echo "set variables"
DB_USER=$2
HOST=$1
DB=$3
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/head/address.sql || echo "address head failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/head/company.sql || echo "company head failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/head/country.sql || echo "country head failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/head/ssession.sql || echo "ssession head failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/head/state.sql || echo "state head failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/head/user.sql || echo "user head failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/head/user_log.sql || echo "user_log head failed"

echo "end head deploy"