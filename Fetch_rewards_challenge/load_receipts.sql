    INSERT INTO receipt(_id
            , bonusPointsEarned
            , bonusPointsEarnedReason
            , createDate
            , dateScanned
            , finishedDate
            , modifyDate
            , pointsAwardedDate
            , pointsEarned
            , purchaseDate
            , purchasedItemCount
            , rewardsReceiptStatus
            , totalSpent
            , userId
            , brandCode
            )
    VALUES(pv_id
            , pv_bonusPointsEarned
            , pv_bonusPointsEarnedReason
            , pv_createDate
            , pv_dateScanned
            , pv_finishedDate
            , pv_modifyDate
            , pv_pointsAwardedDate
            , pv_pointsEarned
            , pv_purchaseDate
            , pv_purchasedItemCount
            , pv_rewardsReceiptStatus
            , pv_totalSpent
            , pv_userId
            , pv_brandCode
    );