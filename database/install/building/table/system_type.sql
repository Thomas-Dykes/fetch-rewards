CREATE TABLE IF NOT EXISTS system_type (
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    name varchar(30),
    abv varchar(10),
    description varchar(100),
    e_time int(13),
    flag int(3),
    log_user_id int(10)
);