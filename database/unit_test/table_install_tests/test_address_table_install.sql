DELIMITER //



CREATE OR REPLACE PROCEDURE test_address_table_install(IN pv_username varchar(30)
                                                      , OUT rv_passed int(1)
								)
BEGIN
	DECLARE lcl_start INT;
	DECLARE lcl_middle INT;
	DECLARE lcl_end INT;
	DECLARE lcl_response INT;
	DECLARE lcl_address_id INT;
	DECLARE lcl_user_id INT;
	SET rv_passed = -6;
	CALL get_user_id(pv_username, lcl_user_id);
	CALL get_address_count(lcl_start);
    CALL add_address('tijuana3', 0, 22.22, lcl_user_id, 33.33, 1, '1234 fairy lane', 22222, lcl_address_id);
	CALL get_address_count(lcl_middle);
    CALL delete_address(lcl_address_id, 0, lcl_user_id, lcl_response); 
	CALL get_address_count(lcl_end);
	IF lcl_start < lcl_middle THEN
		SET rv_passed = -99;
		IF lcl_middle > lcl_end THEN
		SET rv_passed = 1;
		END IF;
	END IF;

END//

call test_address_table_install('leonardo', @rv_passed);

select @rv_passed;

call add_proc_index(
251
, 'tests the address_table_install procedure'
, 0
, '{}'
, '{"rv_passed": "int(9)"}'
, '["address"]'
, 'test_address_table_install'
, 0
);

call add_unit_test_log(
0
, 1
, @rv_passed
, 251
, 'address'
, 0
, @out
);
