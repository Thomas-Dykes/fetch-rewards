DELIMITER //

CREATE OR REPLACE PROCEDURE get_user_ggroup_count(OUT rv_out INT)
	BEGIN
		SELECT COUNT(1) into rv_out from user_ggroup;
	END//

set @rv_passed = 0;

CREATE OR REPLACE PROCEDURE test_user_ggroup_table_install(IN pv_username varchar(30)
																,OUT rv_passed INT(2)
																)
BEGIN
	DECLARE lcl_ggroup_id INT default 0;
	DECLARE lcl_start INT default 0;
	DECLARE lcl_middle INT default 0;
	DECLARE lcl_end INT default 0;
	DECLARE lcl_response INT default 0;
	DECLARE lcl_response2 INT default 0;
	DECLARE lcl_user_id int default 0;

	SET rv_passed = -6;

	CALL get_user_id(pv_username, lcl_user_id);
	
	CALL add_ggroup('test user ggroup', 0, lcl_user_id, 'user group', lcl_ggroup_id);

	CALL get_user_ggroup_count(lcl_start);
	
	CALL add_user_ggroup(lcl_user_id, lcl_ggroup_id, lcl_user_id);

	CALL get_user_ggroup_count(lcl_middle);

	CALL delete_user_ggroup(lcl_user_id, 0, lcl_ggroup_id,  lcl_response);

	CALL delete_ggroup(0, lcl_ggroup_id, lcl_user_id, lcl_response2);

	CALL get_user_ggroup_count(lcl_end);

	IF lcl_start < lcl_middle THEN
		SET rv_passed = -99;
		IF lcl_middle > lcl_end THEN
			SET rv_passed = 1;
		END IF;
	END IF;

END//

call test_user_ggroup_table_install('leonardo',@rv_passed);

select @rv_passed;

call add_proc_index(
253
, 'test user_ggroup_table_install'
, 0
, '{}'
, '{"rv_passed": "int(9)"}'
, '["user_ggroup"]'
, 'test_user_ggroup_table_install'
, 0
);

call add_unit_test_log(
0
, 1
, @rv_passed
, 253
, 'user_ggroup'
, 0
, @out
);
