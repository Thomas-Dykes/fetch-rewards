CREATE TABLE IF NOT EXISTS ssession(
	id int(10) PRIMARY KEY AUTO_INCREMENT
	, ssession_number int(10)
	, user_id int(10)
	, start_time int(13)
	, end_time int(13)
	, flag int(3)
	, e_time int(13)
	, company_id int(10)
	, log_user_id int(10)
	);
