
#add_building_api
call add_proc_index(
41
, 'checks permissions, then runs add_building'
, 0
, '{"pv_area": "int(9)", "pv_name": "varchar(30)", "pv_building_category_id": "int(9)", "pv_electricity_model_id": "int(9)", "pv_gas_model_id": "int(9)"
, "pv_address_id": "int(9)", "pv_ssession_number": "int(9)", "pv_user_id": "int(9)"}'
, '{"rv_building_id": "int(9)"}'
, '["building"]'
, 'add_building_api'
, 1
);

#get_buildings_api
call add_proc_index(
42
, 'checks permissions, then runs get_buildings'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_building_id": "int(9)"}'
, '["building"]'
, 'get_buildings_api'
, 1
);

#get_building_api
call add_proc_index(
43
, 'checks permissions, then runs get_building'
, 0
, '{"pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_building_id": "int(9)"}'
, '["building"]'
, 'get_buildings_api'
, 1
);

#get_building_info_api
call add_proc_index(
44
, 'checks permissions, then runs get_building_info'
, 0
, '{"pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["building"]'
, 'get_building_info_api'
, 1
);

#add_building_with_address_api
call add_proc_index(
45
, 'checks permissions, then runs add_building_with_address'
, 0
, '{"pv_area": "int(9)", "pv_name": "varchar(30)", "pv_building_category_id": "int(9)", "pv_electricity_model_id": "int(9)", "pv_gas_model_id": "int(9)"
, "pv_address_id": "int(9)", "pv_ssession_number": "int(9)", "pv_user_id": "int(9)", "pv_city": "varchar(30)"
, "pv_latitude": "decimal(9,6)", "pv_longitude": "decimal(9,6)", "pv_state_id": "int(9)", "pv_street_address": "int(9)"
, "pv_zip_code": "int(9)"}'
, '{"rv_building_id": "int(9)"}'
, '["building"]'
, 'add_building_with_address_api'
, 1
);

#assign_buildings_category_api
call add_proc_index(
46
, 'checks permission, then runs assign_buildings_category'
, 0
, '{"pv_building_id": "int(9)", "pv_building_category_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_status": "int(9)"}'
, '["building"]'
, 'assign_buildings_category_api'
, 1
);

#assign_buildings_group_api
call add_proc_index(
47
, 'checks permission, then runs assign_buildings_group'
, 0
, '{"pv_building_id": "int(9)", "pv_group_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_status": "int(9)"}'
, '["building"]'
, 'assign_buildings_group_api'
, 1
);

#get_companies_buildings_api

call add_proc_index(
48
, 'checks permission, then runs get_companies_buildings_api'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_company_id": "int(9)"}'
, '{}'
, '["building"]'
, 'get_companies_buildings_api'
, 1
);

#delete_building_api

call add_proc_index(
49
, 'checks permission, then runs delete_building'
, 0
, '{"pv_flag": "int(3)", "pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_status_code": "int(9)"}'
, '["building"]'
, 'delete_building_api'
, 1
);

#update_building_api

call add_proc_index(
50
, 'checks permission, then runs update_building'
, 0
. ''
, ''
, '["building"]'
, 'update_building_api'
, 1
);



#END