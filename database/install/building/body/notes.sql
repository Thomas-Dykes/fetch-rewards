CREATE OR REPLACE PROCEDURE get_electricity_graph_data(IN pv_user_id int(10))
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id); 
    
    SELECT a.adjusted_usage
        , a.base_usage base_usage
        , a.company_id
        , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from electricity a, service_month b
    where a.service_month_id = b.id
    and a.company_id = lcl_company_id
    and a.base_usage > 0;
END//
