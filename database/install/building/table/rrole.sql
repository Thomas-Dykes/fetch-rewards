	CREATE TABLE IF NOT EXISTS rrole (
		id int(10) PRIMARY KEY
		, rrole varchar(100)
        , company_id int(9)
		, flag int(3)
		, e_time int(13)
		, INDEX(id)
		, UNIQUE KEY(rrole)
		);
