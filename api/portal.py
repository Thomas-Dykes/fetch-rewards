import json
import logging
import sys
import time
import traceback
from pathlib import Path
import json
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin

import graph as Graph
import graph_gas as Graph_gas

sys.path.append("../pbc")
import pbc
import login as ll
import api_connector as co

sys.path.append("/home/ec2-user/pbc_portal/processing")
import calculate_savings_electricity_save
import calculate_savings_gas_save
import calculate_savings_electricity_no_save
import calculate_savings_gas_no_save
import calculate_savings_with_data_electricity
import calculate_savings_with_data_gas
import import_picture
import model_new_electricity
import model_new_gas
import model_both
import upload_new_electricity
import upload_new_gas
import update_price_electricity
import update_price_gas
app = Flask(__name__)

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
app.config["CORS_HEADERS"] = "Content-Type"
app.logger.setLevel(logging.WARNING)
log_path = Path("//var/log/pbc/api/api_{}".format(pbc.get_time()))
fh = logging.FileHandler(log_path, delay=False)

fh.setLevel(logging.INFO)
app.logger.addHandler(fh)


def make_response(jdat):
    # print(f"response: {jdat}")
    response = jsonify(jdat)
    response.headers.add("Access-Control-Allow-Headers", "Content-Type,Authorization")
    response.headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
    return response

@app.route("/execute", methods=["POST"])
@cross_origin()
def execute():
    print("\n\n\nexecute\n\nBegin")
    data = request.json

    # print(f"electricitydata {data}")
    name = data["name"]
    params = data["params"]
    print(f"name: {name} \n params: {params}")
    # for p in params:
    #     print("param {}, type: {}".format(p, type(p)))
    return make_response(co.machine(data))


@app.route("/execute_add", methods=["POST"])
@cross_origin()
def execute_add():
    return execute_one()


@app.route("/execute_one", methods=["POST"])
@cross_origin()
def execute_one():
    data = request.json
    print(f'req: {request.data}')
    print(f"execute_add, data: {data}")

    name = data["name"]
    params = data["params"]
    print(f'In execute_one {data}')
    return make_response(co.machine_add(data))

@app.route("/login", methods=["POST"])
@cross_origin()
def login():
    text = request.data
    # print(f"text: {text}")
    data = request.json["p_cookie"]
    # print(f"data {data}")
    password = str(data["password"])
    # print(f"password: '{password}''")
    if password.startswith('"') and password.endswith('"'):
        password = password[1:-1]

    username = str(data["username"])
    print(f"username: '{username}''")
    if username.startswith('"') and username.endswith('"'):
        username = username[1:-1]

    return make_response(ll.inbound_login(username, password))

@app.route("/model", methods=["post"])
@cross_origin()
def model():
    print("model")
    response = json.loads(request.data)['params']
    print(f"this_response: {response}")
    user_id = response[0]
    ssession = response[1]
    building_id = response[2]
    name = response[3]
    model_build_id = response[4]
    apply_start = response[5]
    apply_end = response[6]
    electricity_price = response[7]
    gas_price = response[8]
    # ADD NEW VARIABLE SERVICE_MONTH ID HERE
    ts = str(time.time()).split(".")[0]
    model_id = model_both.main(user_id, ssession, building_id, electricity_price, gas_price, name, model_build_id, apply_start, apply_end)
    total_string_e = calculate_savings_electricity_no_save.main(user_id, building_id, model_id, apply_start, apply_end)
    total_string_g = calculate_savings_gas_no_save.main(user_id, building_id, model_id, apply_start, apply_end)
    return make_response(total_string_g)
    # return adjusted_usage_string_e, adjusted_usage_string_g, base_usage_string_e, base_usage_string_g, month_year_string_e, month_year_string_g, eui_electricity_string, eui_gas_string

@app.route("/model_electricity", methods=["post"])
@cross_origin()
def model_electricity():
    print("model_electricity")
    response = json.loads(request.data)['params']
    print(f"this_response: {response}")
    user_id = response[0]
    ssession = response[1]
    building_id = response[2]
    name = response[3]
    model_build_id = response[4]
    apply_start = response[5]
    apply_end = response[6]
    electricity_price = response[7]
    gas_price = response[8]
    # ADD NEW VARIABLE SERVICE_MONTH ID HERE
    ts = str(time.time()).split(".")[0]
    model_id = model_both.main(user_id, ssession, building_id, electricity_price, gas_price, name, model_build_id, apply_start, apply_end)
    total_string_e = calculate_savings_electricity_no_save.main(user_id, building_id, model_id, apply_start, apply_end)
    # total_string_e = json.loads(total_string_e)
    return make_response(total_string_e)

@app.route("/model_gas", methods=["post"])
@cross_origin()
def model_gas():
    print("model_gas")
    response = json.loads(request.data)['params']
    print(f"this_response: {response}")
    user_id = response[0]
    ssession = response[1]
    building_id = response[2]
    name = response[3]
    model_build_id = response[4]
    apply_start = response[5]
    apply_end = response[6]
    electricity_price = response[7]
    gas_price = response[8]
    # ADD NEW VARIABLE SERVICE_MONTH ID HERE
    ts = str(time.time()).split(".")[0]
    model_id = model_both.main(user_id, ssession, building_id, electricity_price, gas_price, name, model_build_id, apply_start, apply_end)
    total_string_g = calculate_savings_gas_no_save.main(user_id, building_id, model_id, apply_start, apply_end)
    return make_response(total_string_g)

@app.route("/price_remodel_electricity", methods=["get", "post"])
@cross_origin()
def price_remodel_electricity():
    response = json.loads(request.data)['params']
    user_id = response[0]
    building_id = response[1]
    model_id = response[2]
    apply_start = response[3]
    apply_end = response[4]
    price = response[5]

    # print(Graph_gas.get_data(user_id, build_start, model_build_id, apply_start, apply_end, building_id))
    update_price_electricity.main(price, building_id)
    calculate_savings_with_data_electricity.main(user_id, building_id, model_id, apply_start,
                                                 apply_end)
    
    return ''

@app.route("/price_remodel_gas", methods=["get", "post"])
@cross_origin()
def price_remodel_gas():
    response = json.loads(request.data)['params']
    user_id = response[0]
    building_id = response[1]
    model_id = response[2]
    apply_start = response[3]
    apply_end = response[4]
    price = response[5]

    # print(Graph_gas.get_data(user_id, build_start, model_build_id, apply_start, apply_end, building_id))
    update_price_gas.main(price, building_id)
    calculate_savings_with_data_gas.main(user_id, building_id, model_id, apply_start,
                                                 apply_end)
    
    return ''

@app.route("/remodel", methods=["post"])
@cross_origin()
def remodel():
    print("remodel")
    response = json.loads(request.data)['params']
    print(f"this_response: {response}")
    user_id = response[0]
    building_id = response[1]
    apply_start = response[2]
    apply_end = response[3]
    model_id = response[4]
    electricity_price = response[5]
    gas_price = response[6]
    # ADD NEW VARIABLE SERVICE_MONTH ID HERE
    ts = str(time.time()).split(".")[0]
    update_price_electricity.main(electricity_price, model_id)
    update_price_gas.main(gas_price, model_id)
    total_string_e = calculate_savings_electricity_no_save.main(user_id, building_id, model_id, apply_start,
                                                 apply_end)
    total_string_g = calculate_savings_gas_no_save.main(user_id, building_id, model_id, apply_start,
                                                 apply_end)
    return make_response(response)

@app.route("/remodel_save", methods=["post"])
@cross_origin()
def remodel_save():
    print("remodel_save")
    response = json.loads(request.data)['params']
    print(f"this_response: {response}")
    user_id = response[0]
    building_id = response[1]
    apply_start = response[2]
    apply_end = response[3]
    model_id = response[4]
    electricity_price = response[5]
    gas_price = response[6]
    # ADD NEW VARIABLE SERVICE_MONTH ID HERE
    ts = str(time.time()).split(".")[0]
    update_price_electricity.main(electricity_price, model_id)
    update_price_gas.main(gas_price, model_id)
    calculate_savings_electricity_save.main(user_id, building_id, model_id, apply_start,
                                                 apply_end)
    calculate_savings_gas_save.main(user_id, building_id, model_id, apply_start,
                                                 apply_end)
    return make_response(response)

@app.route("/remodel_electricity", methods=["post"])
@cross_origin()
def remodel_electricity():
    print("remodel_electricity")
    response = json.loads(request.data)['params']
    print(f"this_response: {response}")
    user_id = response[0]
    building_id = response[1]
    apply_start = response[2]
    apply_end = response[3]
    model_id = response[4]
    electricity_price = response[5]
    gas_price = response[6]
    # ADD NEW VARIABLE SERVICE_MONTH ID HERE
    ts = str(time.time()).split(".")[0]
    update_price_electricity.main(electricity_price, model_id)
    total_string_e = calculate_savings_electricity_no_save.main(user_id, building_id, model_id, apply_start, apply_end)
    return make_response(total_string_e)


@app.route("/remodel_gas", methods=["post"])
@cross_origin()
def remodel_gas():
    print("remodel-gas")
    response = json.loads(request.data)['params']
    print(f"this_response: {response}")
    user_id = response[0]
    building_id = response[1]
    apply_start = response[2]
    apply_end = response[3]
    model_id = response[4]
    electricity_price = response[5]
    gas_price = response[6]
    # ADD NEW VARIABLE SERVICE_MONTH ID HERE
    ts = str(time.time()).split(".")[0]
    update_price_gas.main(gas_price, model_id)
    total_string = calculate_savings_gas_no_save.main(user_id, building_id, model_id, apply_start,
                                                 apply_end)
    return make_response(total_string)



@app.route("/gas_regression_graph", methods=["get", "post"])
@cross_origin()
def gas_regression_graph():
    response = json.loads(request.data)['params']
    user_id = response[0]
    ssession = response[1]
    building_id = response[2]
    build_begin = response[3]
    model_build_id = response[4]
    apply_start = response[5]
    apply_end = response[6]
    print(Graph_gas.get_data(user_id, (model_build_id-12), model_build_id, apply_start, apply_end, building_id))
    return make_response(Graph_gas.get_data(user_id, (model_build_id-12), model_build_id, apply_start, apply_end, building_id))



@app.route("/regression_graph", methods=["get", "post"])
@cross_origin()
def regression_graph():
    response = json.loads(request.data)['params']
    user_id = response[0]
    ssession = response[1]
    building_id = response[2]
    build_begin = response[3]
    model_build_id = response[4]
    apply_start = response[5]
    apply_end = response[6]
    print(f"this is the regression response: {response}")
    return make_response(Graph.get_data(user_id, (model_build_id-12), model_build_id, apply_start, apply_end, building_id))

@app.route("/test", methods=["POST", "GET"])
@cross_origin()
def test():
    print("test")
    return make_response(1)

@app.route("/test_python", methods=["get", "post"])
@cross_origin
def test_python():
    response = json.loads(request.data['params'])
    data_electricity = response[0]
    data_gas = response[1]
    user_id = response[2]
    building_id = response[3]
    model_build_id = resposne[4]
    apply_start = response[5]
    apply_end = response[6]
    return make_response(test_start.main(data_electricity, data_gas, user_id, building_id, (model_build_id-12), model_build_id, apply_start, apply_end))

# @app.route("/tmp_remodel_gas", methods=["get", "post"])
# @cross_origin()
# def tmp_remodel_gas():
#     response = json.loads(request.data)['params']
#     building_id = response[0]
#     model_service_month_id = response[1]
#     apply_start = response[2]
#     apply_end = response[3]
#     # print(Graph_gas.get_data(user_id, build_start, model_build_id, apply_start, apply_end, building_id))
#     return make_response(tmp_remodel_gas.main(building_id, model_log_id, apply_start, apply_end))

# @app.route("/tmp_remodel_electricity", methods=["get", "post"])
# @cross_origin()
# def tmp_remodel_electricity():
#     response = json.loads(request.data)['params']
#     building_id = response[0]
#     model_service_month_id = response[1]
#     apply_start = response[2]
#     apply_end = response[3]
#     # print(Graph_gas.get_data(user_id, build_start, model_build_id, apply_start, apply_end, building_id))
#     return make_response(tmp_remodel_electricity.main(building_id, model_log_id, apply_start, apply_end))


@app.route("/upload_picture", methods=["post"])
@cross_origin()
def upload_picture():
    print("upload-picture")
    response = json.loads(request.data)['params']
    print(f"this_response: {response}")
    company_id = response[0]
    tab_name = response[1]
    photo_id = response[2]
    file_name = response[3]
    print(f"this_response: {response}")
    # ADD NEW VARIABLE SERVICE_MONTH ID HERE
    # ts = str(time.time()).split(".")[0]
    import_picture.main(company_id, tab_name, photo_id, file_name)
    return ''

@app.route("/upload_electricity_new", methods=["post"])
@cross_origin()
def upload_electricity_new():
    print("upload-electricity-new")
    response = json.loads(request.data)['params']
    print(f"this_response: {response}")
    user_id = response[0]
    data = response[1]
    building_id = response[2]
    print(f"this_response: {response}")
    # ADD NEW VARIABLE SERVICE_MONTH ID HERE
    ts = str(time.time()).split(".")[0]
    upload_new_electricity.main(data, building_id, user_id)
    return ''


@app.route("/upload_gas_new", methods=["post"])
@cross_origin()
def upload_gas_no_process():
    print("upload-gas-new")
    response = json.loads(request.data)['params']
    print(f"this_response: {response}")
    user_id = response[0]
    data = response[1]
    building_id = response[2]
    # ADD NEW VARIABLE SERVICE MONTH ID HERE
    ts = str(time.time()).split(".")[0]
    upload_new_gas.main(data, building_id, user_id)
    return ''


@app.route("/electricity_recent_costs", methods=["GET", "POST"])
@cross_origin()
def electricity_recent_costs():
    """
    Gets the cost of electricity this month, last month, and last year, given a month to start at
    Requires json to be sent of the format {"params": [user_id, ssession_number, building_id, service_month_id]
    The service_month_id should be the most recent month you want data on.
    Returns:
        a response with a dictionary of the format:
            [$current_service_month_id: value, $current_service_month_id-1: value, "year total": value]

    """
    try:
        data = request.json
        params = data['params']
        user_id, ssession_number, building_id, current_service_month_id_str = params
        current_service_month_id = int(current_service_month_id_str)
    except Exception as e:
        return make_response({"Exception": "electricity_recent_costs requires a building_id, service_month_id, user_id, and "
                                           "ssession_number to be sent in the json"})
    try:
        cost_per_month = {}
        for service_month_id in range(current_service_month_id - 11, current_service_month_id + 1):
            data = {"name": 'find_electricity_costs_api',
                    "params": [user_id,
                               ssession_number,building_id, service_month_id - 1,
                               service_month_id]}
            print(f'In electricity_recent: {data}')
            cost_per_month[service_month_id] = co.machine_add(data)*100
        return_data_with_decimal = {current_service_month_id: cost_per_month[current_service_month_id],
                                    current_service_month_id - 1: cost_per_month[current_service_month_id - 1],
                                    "year_total": sum(filter(lambda x: x is not None, cost_per_month.values()))}
        return_data = dict(zip(map(str, return_data_with_decimal.keys()), list(map(str, return_data_with_decimal.values()))))
        return make_response(return_data)
    except Exception as e:
        return make_response({"Error": str(e), "Stack": traceback.format_exc()})


@app.route("/gas_recent_costs", methods=["GET", "POST"])
@cross_origin()
def gas_recent_costs():
    """
    Gets the cost of gas this month, last month, and last year, given a month to start at
    Requires json to be sent of the format {"params": [user_id, ssession_number, building_id, service_month_id]
    The service_month_id should be the most recent month you want data on.
    Returns:
        a response with a dictionary of the format:
            [$current_service_month_id: value, $current_service_month_id-1: value, "year total": value]

    """
    try:
        data = request.json
        params = data['params']
        user_id, ssession_number, building_id, current_service_month_id_str = params
        current_service_month_id = int(current_service_month_id_str)
    except Exception as e:
        return make_response({"Exception": "gas_recent_costs requires a building_id, service_month_id, user_id, and "
                                           "ssession_number to be sent in the json"})
    try:
        cost_per_month = {}
        for service_month_id in range(current_service_month_id - 11, current_service_month_id + 1):
            data = {"name": 'find_gas_costs_api',
                    "params": [user_id,
                               ssession_number,
                               building_id, service_month_id - 1,
                               service_month_id]}
            print(f'In gas_recent: {data}')
            cost_per_month[service_month_id] = co.machine_add(data)*100
        return_data_with_decimal = {current_service_month_id: cost_per_month[current_service_month_id],
                                    current_service_month_id - 1: cost_per_month[current_service_month_id - 1],
                                    "year_total": sum(filter(lambda x: x is not None, cost_per_month.values()))}
        return_data = dict(zip(map(str, return_data_with_decimal.keys()), list(map(str, return_data_with_decimal.values()))))
        return make_response(return_data)
    except Exception as e:
        return make_response({"Error": str(e), "Stack": traceback.format_exc()})

# @app.route("/model_eui", methods=["post"])
# @cross_origin()
# def model_eui():
#     print("model-eui")
#     response = json.loads(request.data)['params']
#     building_id = response[0]
#     service_month_id_start = response[1]
#     service_month_id_end = response[2]

# @app.route("/model_gas", methods=["post"])
# @cross_origin()
# def model_gas():
#     print("model-gas")
#     response = json.loads(request.data)['params']
#     print(f"this_response: {response}")
#     user_id = response[0]
#     ssession = response[1]
#     building_id = response[2]
#     name = response[3]
#     model_build_id = response[4]
#     apply_start = response[5]
#     apply_end = response[6]
#     # ADD NEW VARIABLE SERVICE_MONTH ID HERE
#     ts = str(time.time()).split(".")[0]

#     model_id_gas = model_new_gas.main(user_id, ssession, building_id, name, model_build_id, apply_start, apply_end)
#     print(f"model_log_id: {model_id_gas}")
#     calculate_savings_with_data_gas.main(user_id, building_id, model_id_gas, apply_start,
#                                                  apply_end)

    # return make_response(response)

@app.before_request
def log_request_info():
    app.logger.debug(f"Start of API Info:")
    app.logger.debug(f"Headers: {request.headers}")
    app.logger.debug(f"Json: {request.json}")
    app.logger.debug(f"End")
    app.logger.debug(f"")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=443, debug=True)
