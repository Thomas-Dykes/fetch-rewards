import json, time, os, sys, csv, datetime
import pymysql
import config, save
from typing import List, Dict, Any

import sys


def connect(
    db_name: str, name: str, password: str, rds_host: str
) -> pymysql.Connection:
    """
    Connects to the database
    Args:
        db_name: database name
        name: username
        password: password
        rds_host: ip address of database server

    Returns:
        A connection to the database

    """
    try:
        connection = pymysql.connect(
            rds_host,
            user=name,
            passwd=password,
            db=db_name,
            connect_timeout=5,
            autocommit=True,
        )
        return connection

    except Exception as e:
        print("Unable to connect")
        # return 0
        # exit(10)


def db_connect():
    print("--db_connect \n", flush=True)
    conn = connect(
        db_name=config.db,
        name=config.db_user,
        password=config.db_password,
        rds_host=config.host,
    )
    if conn.open:
        print("Connected! \n")
        return conn

    print("Connection Failed")


def convert_string_and_sql_type_to_python_type(value: str, sql_type: str):
    """
    Converts a string and name of a sql type to a python type (which mypysql will convert to a sql type later)
    TODO: Add more conversions as needed
    Args:
        value: The string to be converted
        sql_type: The sql type the string will eventually be

    Returns:
        The value as the python type that coorisponds to the sql type.

    """
    if "int" in sql_type:
        return int(value)
    elif "varchar" in sql_type:
        return value
    else:
        return value


def execute_proc(cursor, conn, proc_to_run, param_values):
    print("proc to run: {}, param values: {}".format(proc_to_run, param_values))
    # print("the type of param_values: {}, param length {}".format(type(param_values), len(param_values)))
    cursor.callproc(proc_to_run, param_values)

    # commit changes
    if proc_to_run[:3] == "add":
        print("     COMMIT CHANGES \n")
        conn.commit()

    data_list = cursor.fetchall()
    #  print(f"data list: {data_list}")
    data_length = len(data_list)
    #  print("data list length: {}".format(data_length))
    return data_list


def execute_proc_add(cursor, conn, proc_to_run, param_values):
    print("execute proc to run: {}, param values: {}".format(proc_to_run, param_values))
    cursor.callproc(proc_to_run, param_values)

    # commit changes
    if proc_to_run[:3] == "add":
        print("     COMMIT CHANGES \n")
        conn.commit()

    param_l = len(param_values) - 1
    #  print(f"param l {param_l}")
    query = f"SELECT @_{proc_to_run}_{param_l}"
    # print(f"query: {query}")
    cursor.execute(query)
    val = cursor.fetchone()[f"@_{proc_to_run}_{param_l}"]
    return val


def output_printer(jdat, debug):
    results = []
    try:
        for result in jdat:
            #  print(result)
            lcl_keys = result.keys()
            lcl_result = []
            for k in lcl_keys:
                lcl_key = k
                lcl_value = result[k]
                lcl_frame = {}
                lcl_frame["key"] = k
                lcl_frame["value"] = result[k]
                lcl_result.append(lcl_frame)
            results.append(lcl_result)
        print("\n    Results")
        for r in results:
            for attribute in r:
                if not (debug) and attribute["key"] != "epoch_time_modifier":
                    if not (debug) and attribute["key"] != "e_time":
                        if not (debug) and attribute["key"] != "flag":
                            if not (debug) and attribute["key"] != "log_employee_id":
                                if not (debug) and attribute["key"] != "company_id":
                                    lcl_key = attribute["key"]
                                    lcl_value = attribute["value"]
                                    print(" {}: {}".format(lcl_key, lcl_value))
                if debug:
                    print(attribute)
            print("")
        print("     END RESULTS")

    except TypeError:
        pass


def machine(pv_jdat):
    conn = db_connect()
    # lcl_jdat = json.loads(pv_jdat)
    proc_to_run = pv_jdat["name"]
    param_values = pv_jdat["params"]
    save.log(proc_to_run, param_values)
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    data = execute_proc(cursor, conn, proc_to_run, param_values)
    # output_printer(data, False)
    conn.close()
    return data


def machine_add(pv_jdat):
    print(f"machine add '{pv_jdat}'")
    try:
        conn = db_connect()
        # lcl_jdat = json.loads(pv_jdat)
        proc_to_run = pv_jdat["name"]
        param_values = pv_jdat["params"]
        save.log(proc_to_run, param_values)
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        data = execute_proc_add(cursor, conn, proc_to_run, param_values)
        # output_printer(data, False)
        conn.close()
        return data
    except AttributeError as e:
        print("Attribute error, in connector")
        print(f"error: {e}")
        return 0


def main():
    debug = False
    print("--connector.main")

    if len(sys.argv) > 1:
        jdat_in = sys.argv[1]
        print(f"jdat_in {jdat_in}")
        jdat = json.loads(jdat_in)
        data = machine(jdat)
        output_printer(data, False)

    else:
        print("Please pass a valid json to std:in arg one when calling main")


if __name__ == "__main__":
    main()
