DELIMITER //

CREATE OR REPLACE procedure get_user_company_id(in pv_user_id int(9)
						, out rv_company_id int(9))
BEGIN
	declare max_company_id int default 0;
	SELECT user.company_id
	into rv_company_id
	FROM user
	WHERE user.id = pv_user_id;
	if rv_company_id IS NULL THEN
	UPDATE company
	SET company.log_user_id = pv_user_id;
	SELECT max(id) into rv_company_id from company;
	END IF;
END//

#NOT IN PROC INDEX
CREATE OR REPLACE PROCEDURE get_user_company_count(IN pv_user_id INT(10)
                                                    , OUT rv_count INT(10))
	BEGIN
		DECLARE lcl_company_id INT default 0;
		CALL get_user_company_id(pv_user_id, lcl_company_id);
		SELECT COUNT(1) INTO rv_count FROM user WHERE company_id = lcl_company_id;
	END//

CREATE OR REPLACE PROCEDURE get_root_user(IN pv_company_id INT(10)
                                             ,OUT rv_user_id INT(10))
	BEGIN
		SET rv_user_id = -6;
		SELECT MIN(id) INTO rv_user_id FROM user where company_id = pv_company_id;
	END//




#NEEDS FIXED
CREATE OR REPLACE PROCEDURE add_random_user(IN pv_address_id int(10)
							, IN pv_dob varchar(30)
							, IN pv_email_address varchar(40)
							, IN pv_first_name varchar(30)
							, IN pv_flag int(3)
							, IN pv_last_name varchar(100)
							, IN pv_user_id int(6)
							, IN pv_password varchar(100)
							, IN pv_phone_number varchar(20)
							, IN pv_rrole_id int(9)
							, IN pv_start_date date
							, IN pv_company_id int(9)
							, OUT rv_user_id int(9)
							)
	BEGIN
		DECLARE lcl_user_log_id int(9);
		DECLARE lcl_username VARCHAR(40);
		CALL get_random_available_username(pv_user_id, lcl_username);
		CALL get_user_company_id(pv_user_id, lcl_user_company_id);
		INSERT INTO user_log (address_id, company_id, dob, email_address, e_time, first_name, flag, last_name, log_user_id, phone_number, pwd, start_date)
        VALUES (pv_address_id, pv_company_id, pv_dob, pv_email_address, UNIX_TIMESTAMP(NOW()), pv_first_name, pv_flag, pv_last_name, pv_user_id, pv_phone_number, pv_password, pv_start_date);
		select MAX(id) from user_log where user_log.company_id = lcl_user_company_id INTO lcl_user_log_id;
		INSERT INTO user (company_id, e_time, flag, log_user_id, rrole_id, user_log_id, username)
		VALUES (pv_company_id, UNIX_TIMESTAMP(NOW()), pv_flag, pv_user_id, pv_rrole_id, lcl_user_log_id, lcl_username);
		SELECT MAX(id) from user where user.company_id = lcl_user_company_id INTO rv_user_id;
		IF rv_user_id IS NULL THEN
		SET rv_user_id = 0;
		END IF;
	END//

#NOT IN PROC INDEX
CREATE OR REPLACE PROCEDURE get_random_available_username(IN pv_user_id INT(10)
                                                ,OUT rv_username VARCHAR(40))
BEGIN
	DECLARE lcl_number INT DEFAULT NULL;
	DECLARE lcl_company_id INT DEFAULT NULL;
	SELECT ROUND(10000 + 89999*rand()) INTO lcl_number;
	CALL get_user_company_id(pv_user_id, lcl_company_id);
	SELECT CONCAT(lcl_company_id,'-',lcl_number) INTO rv_username;
END//
#NOT IN PROC INDEX
CREATE OR REPLACE PROCEDURE get_password(IN pv_user_id int(9)
											, OUT rv_password varchar(100)
											)
BEGIN
	select user_log.pwd FROM user INNER JOIN user_log ON user.user_log_id = user_log.id where user.id = pv_user_id;
END//

#198

CREATE OR REPLACE PROCEDURE delete_users()
        BEGIN
		DELETE FROM user;
		ALTER TABLE user AUTO_INCREMENT = 1;
        END//

#APPEARS TO WORK
CREATE OR REPLACE PROCEDURE delete_user_api(IN pv_flag int(3)
												, IN pv_cookie_user_id int(9)
                                                , IN pv_ssession_number int(9)
												, IN pv_user_id int(9)
												, OUT rv_status_code int(9)
                                                )
        BEGIN
    	declare lcl_auth int default 0;
    	declare lcl_company_id int default 0;
    	CALL authenticate(pv_cookie_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		call delete_user(pv_flag, pv_user_id, rv_status_code);
        END IF;
        END//


#APPEARS TO WORK
CREATE OR REPLACE PROCEDURE delete_user(IN pv_flag int(3)
										, IN pv_user_id int(9)
										, OUT rv_status_code int(9))
BEGIN
	declare lcl_outbound_id int(9) default 0;
	declare rv_user_json varchar(500) default '';
	declare rv_user_log_json varchar(500) default '';
	declare lcl_address_id int(9) default 0;
	declare lcl_company_id int(9) default 0;
	declare lcl_dob varchar(20) default 0;
	declare lcl_email_address varchar(40) default 0;
	declare lcl_e_time int(9) default 0;
	declare lcl_first_name varchar(40) default 0;
	declare lcl_flag int(3) default 0;
	declare lcl_last_name varchar(40) default '';
	declare lcl_log_user_id int(9) default 0;
	declare lcl_user_log_id int(9) default 0;
	declare lcl_phone_number varchar(40) default '';
	declare lcl_address_id_string varchar(40) default '';
	declare lcl_company_id_string varchar(40) default '';
	declare lcl_dob_string varchar(40) default '';
	declare lcl_email_address_string varchar(40) default '';
	declare lcl_e_time_string varchar(40) default '';
	declare lcl_first_name_string varchar(40) default '';
	declare lcl_flag_string varchar(40) default '';
	declare lcl_last_name_string varchar(40) default '';
	declare lcl_log_user_id_string varchar(40) default '';
	declare lcl_user_string varchar(40) default '';
	declare lcl_user_log_id_string varchar(40) default '';
	declare lcl_phone_number_string varchar(40) default '';
	declare lcl_almost_finished_string varchar(500) default '';
	DECLARE EXIT HANDLER FOR 1451
	-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails 
	BEGIN
		select -1 into rv_status_code;
	END;
	select user_log.address_id INTO lcl_address_id FROM user INNER JOIN user_log ON user.user_log_id = user_log.id WHERE user.id = pv_user_id;
	select user.company_id into lcl_company_id FROM user where user.id = pv_user_id;
	select user_log.dob into lcl_dob FROM user INNER JOIN user_log ON user.user_log_id = user_log.id WHERE user.id = pv_user_id;
	select user_log.email_address into lcl_email_address from user INNER JOIN user_log ON user.user_log_id = user_log.id WHERE user.id = pv_user_id;
	select user.e_time into lcl_e_time from user where user.id = pv_user_id;
	select user_log.first_name into lcl_first_name from user INNER JOIN user_log ON user.user_log_id = user_log.id WHERE user.id = pv_user_id;
	select user.flag into lcl_flag from user where user.id = pv_user_id;
	select user_log.last_name into lcl_last_name from user INNER JOIN user_log ON user.user_log_id = user_log.id WHERE user.id = pv_user_id;
	select user.user_log_id into lcl_user_log_id from user where user.id = pv_user_id;
	select user_log.phone_number into lcl_phone_number from user INNER JOIN user_log ON user.user_log_id = user_log.id where user.id = pv_user_id;
	call jsonify_int(lcl_address_id, 'address_id', lcl_address_id_string);
	call jsonify_int(lcl_company_id, 'company_id', lcl_company_id_string);
	call jsonify_string('dob', lcl_dob, lcl_dob_string);
	call jsonify_string('email_address', lcl_email_address, lcl_email_address_string);
	call jsonify_int(lcl_e_time, 'e_time', lcl_e_time_string);
	call jsonify_string('first_name', lcl_first_name,  lcl_first_name_string);
	call jsonify_int(lcl_flag, 'flag', lcl_flag_string);
	call jsonify_string('last_name', lcl_last_name, lcl_last_name_string);
	call jsonify_int(lcl_user_log_id, 'user_log_id', lcl_user_log_id_string);
	call jsonify_int(pv_user_id, 'user_id', lcl_user_string);
	call jsonify_string('phone_number', lcl_phone_number, lcl_phone_number_string);
	select concat("{", lcl_company_id_string, ",", lcl_e_time_string, ",", lcl_flag_string, ",", lcl_user_log_id_string, ",", lcl_user_string, "}") into rv_user_json;
	select concat("{", lcl_address_id_string, ",", lcl_company_id_string, ",", lcl_dob_string, ",", lcl_email_address_string, ",", lcl_e_time_string, ",", lcl_first_name_string, ",", lcl_flag_string, ",", lcl_last_name_string, ",", lcl_phone_number_string, "}") into rv_user_log_json;
	call add_outbound(pv_flag, pv_user_id, rv_user_json,  'user', lcl_outbound_id);
	call add_outbound(pv_flag, pv_user_id, rv_user_log_json, 'user_log', lcl_outbound_id);
	DELETE FROM user where id = pv_user_id AND company_id = lcl_company_id;
	DELETE FROM user_log where user_log.id = lcl_user_log_id AND user_log.company_id = lcl_company_id;
	select lcl_outbound_id into rv_status_code;
END//

#47

CREATE OR REPLACE PROCEDURE get_user_count(OUT rv_out INT)
	BEGIN
		SELECT COUNT(1) into rv_out FROM user;
	END//


#48
CREATE OR REPLACE PROCEDURE get_user_api(in pv_user_id int(9)
							, IN pv_log_user_id int(9)
							, IN pv_ssession_number int(9)
							, OUT rv_status_code int(9))


	BEGIN
    	declare lcl_auth int default 0;
    	CALL authenticate(pv_log_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		call get_user(pv_user_id, pv_log_user_id, rv_status_code);
		END IF;
		set rv_status_code = 1;
	END//

#49

CREATE OR REPLACE PROCEDURE get_user(IN pv_user_id int(9)
                                        , IN pv_log_user_id int(10)
													, OUT rv_status_code int(9)
                                                                        )
        BEGIN
		DECLARE EXIT HANDLER FOR 1318 
		-- ERROR 1318 (42000): Incorrect number of arguments for PROCEDURE business_transactional_prod.get_user_api;
		BEGIN
			select -1 into rv_status_code;
		END;
		SELECT user.id, user.user_log_id, user.username, user.e_time, user.flag
        , user.company_id, user.start_date, user_log.first_name
		, user_log.last_name, user_log.email_address, user_log.dob
		, user_log.phone_number, user.rrole_id
		FROM user INNER JOIN user_log ON user.user_log_id = user_log.id
        where user.id = pv_user_id;
		select 1 into rv_status_code;
        END//

#50
CREATE OR REPLACE PROCEDURE get_users_api(IN pv_user_id int(9)
								, IN pv_ssession_number int(9))
	BEGIN
    	declare lcl_auth int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		call get_users(pv_user_id);
		END IF;
	END//

#51
CREATE OR REPLACE PROCEDURE get_users(IN pv_user_id int(9))
	BEGIN
		declare lcl_user_company_id int default 0;
		SELECT user.id, user.user_log_id, user.username, user.e_time, user.flag
        , company.name company_name, user.start_date, user_log.first_name
		, user_log.last_name, user_log.email_address, user_log.dob
		, user_log.phone_number, user.rrole_id
		FROM user INNER JOIN user_log ON user.user_log_id = user_log.id
		INNER JOIN company ON user_log.company_id = company.id;
	END//



#59

CREATE OR REPLACE PROCEDURE get_user_username_api(in pv_user_id int(9)
						, IN pv_log_user_id int(9)
						, IN pv_ssession_number int(6)
						, out rv_username_output varchar(30)
						)
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_log_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1
		THEN call get_user_username(pv_user_id, rv_username_output);
	END IF;
END//

#60

CREATE OR REPLACE PROCEDURE get_user_username(in pv_user_id int(10)
					, out rv_username_output varchar(30)
					)
BEGIN
	select user.username into rv_username_output
	FROM user
	WHERE user.id = pv_user_id;
END//


CREATE OR REPLACE PROCEDURE get_user_infos_api(IN pv_log_user_id int(9)
							, IN pv_ssession_number int(9)
							)
	BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_log_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
		call get_user_infos(pv_log_user_id);
	END IF;
	END//

#56
	
CREATE OR REPLACE PROCEDURE get_user_infos(IN pv_user_id int(10))
BEGIN
	SELECT a.id
			, a.username
			, b.first_name
			, b.last_name
			, b.address_id
			, c.name company_name
			
	FROM user a,
		user_log b, 
		company c
	WHERE a.user_log_id = b.id
		and a.company_id = c.id;
END//



#61

CREATE OR REPLACE PROCEDURE get_user_id_api(in pv_username varchar(30)
								, IN pv_user_id int(9)
								, IN pv_ssession_number int(9)
								, out rv_id_output int(9)
							)
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
		SET rv_id_output = -99;
		call get_user_id(pv_username, rv_id_output);
	END IF;
END//


#62

CREATE OR REPLACE PROCEDURE get_user_id(in pv_username varchar(30)
						, out rv_id_output int(9)
							)
BEGIN
	declare lcl_id_output int default 0;
	declare lcl_count int default 0;
	SET rv_id_output = -6;
	select count(1) into lcl_count
	FROM user 
	WHERE username = pv_username;
	
	IF lcl_count = 0 THEN
		SET rv_id_output = -7;
	END IF;

	IF lcl_count = 1 THEN
		SELECT id
		from user 
		WHERE username = pv_username;
	END IF;
END//

#666
CREATE OR REPLACE PROCEDURE add_user_api(IN pv_address_id int(9)
							, IN pv_dob varchar(30)
							, IN pv_email_address varchar(40)
							, IN pv_first_name varchar(30)
							, IN pv_flag int(3)
							, IN pv_last_name varchar(100)
							, IN pv_user_id int(6)
							, IN pv_password varchar(100)
							, IN pv_phone_number varchar(20)
							, IN pv_ssession_number int(9)
							, IN pv_start_date date
							, IN pv_username varchar(30)
							, IN pv_rrole_id int(9)
							, IN pv_company_id int(9)
							, OUT rv_user_id int(9)
							)
	BEGIN
    	declare lcl_auth int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		CALL add_user(pv_address_id, pv_dob, pv_email_address, pv_first_name, pv_flag, pv_last_name, pv_user_id, pv_password, pv_phone_number, pv_start_date, pv_username, pv_rrole_id, pv_company_id, rv_user_id);
		END IF;
		IF rv_user_id IS NULL THEN
		set rv_user_id = -2;
		END IF;
	END//


#NEEDS FIXED
CREATE OR REPLACE PROCEDURE add_user(IN pv_address_id int(9)
							, IN pv_dob varchar(30)
							, IN pv_email_address varchar(40)
							, IN pv_first_name varchar(30)
							, IN pv_flag int(3)
							, IN pv_last_name varchar(30)
							, IN pv_user_id int(6)
							, IN pv_password varchar(30)
							, IN pv_phone_number varchar(20)
							, IN pv_start_date varchar(30)
							, IN pv_username varchar(30)
							, IN pv_rrole_id int(9)
							, IN pv_company_id int(9)
							, OUT rv_user_id int(9)
							)
	BEGIN
		declare lcl_user_log_id int default 0;
		INSERT INTO user_log (address_id, company_id, dob, email_address, e_time, first_name, flag, last_name, log_user_id, phone_number, pwd)
        VALUES (pv_address_id, pv_company_id, pv_dob, pv_email_address, UNIX_TIMESTAMP(NOW()), pv_first_name, pv_flag, pv_last_name, pv_user_id, pv_phone_number, PASSWORD(pv_password));
		select MAX(id) from user_log where user_log.company_id = pv_company_id INTO lcl_user_log_id;
		INSERT INTO user (company_id, e_time, flag, log_user_id, start_date, user_log_id, username, rrole_id)
		VALUES (pv_company_id, UNIX_TIMESTAMP(NOW()), pv_flag, pv_user_id, pv_start_date, lcl_user_log_id, pv_username, pv_rrole_id);
		SELECT MAX(id) from user where user.company_id = pv_company_id INTO rv_user_id;
		IF rv_user_id IS NULL THEN
		SET rv_user_id = 0;
		END IF;
	END//

CREATE OR REPLACE PROCEDURE renew_password_api(IN pv_user_id INT(10)
					, IN pv_new_human_password varchar(100)
					, IN pv_old_password varchar(100)
					, IN pv_ssession_number int(9)
					, OUT rv_status int(9)
					)
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
		call renew_password(pv_user_id, pv_new_human_password, pv_old_password, rv_status);
	END IF;
END//

CREATE OR REPLACE PROCEDURE renew_password(IN pv_user_id int(9)
										, IN pv_new_human_password varchar(100)
										, IN pv_old_password varchar(100)
										, OUT rv_status int(9)
										)
BEGIN
    	declare lcl_company_id int default 0;
		declare replace_password varchar(100) default '';
		declare lcl_password varchar(100) default '';
		declare lcl_user_log_id int default 0;
		declare do_i_replace int default 0;
		SET rv_status = -99;
		SELECT user_log.pwd INTO replace_password FROM user INNER JOIN user_log ON user.user_log_id = user_log.id 
		WHERE user.id = pv_user_id;
		SELECT PASSWORD(pv_old_password) INTO lcl_password;
		IF lcl_password = replace_password THEN
			SET rv_status = -98;
			SET do_i_replace = 1;
		END IF;
	IF do_i_replace = 1 THEN
		SET rv_status = 1;
		SELECT user_log.id INTO lcl_user_log_id FROM user INNER JOIN user_log ON user.user_log_id = user_log.id where user.id = pv_user_id;
		UPDATE user_log set user_log.pwd = PASSWORD(pv_new_human_password) where  user_log.id = lcl_user_log_id;
	END IF;
END//



CREATE OR REPLACE PROCEDURE get_my_info_api(IN pv_user_id int(10)
										,IN pv_ssession_number int(10))
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_my_info(pv_user_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_my_info(IN pv_user_id int(10))
BEGIN
	SELECT a.id
			, a.username
			, b.first_name
			, b.last_name
			, b.address_id
			, c.name company_name
			
	FROM user a,
		user_log b, 
		company c
	WHERE a.user_log_id = b.id
		and a.company_id = c.id
		and a.id = pv_user_id;
END//


#346

CREATE OR REPLACE PROCEDURE replace_password_api(IN pv_user_id INT(9)
					, IN pv_new_human_password varchar(40)
					, IN pv_ssession_number int(9)
					, OUT rv_status varchar(100)
					)
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
		CALL replace_password(pv_user_id, pv_new_human_password, rv_status);
	END IF;

END//

CREATE OR REPLACE PROCEDURE replace_password(pv_user_id INT(10)
											, pv_new_human_password VARCHAR(50)
											, rv_status INT(10)
											)
BEGIN
	declare lcl_count INT default 0;
	declare lcl_user_log_id int default 0;
	set rv_status = -66;
	SELECT user.user_log_id INTO lcl_user_log_id 
	FROM user INNER JOIN user_log ON user.user_log_id = user_log.id 
	WHERE user.id = pv_user_id;
	UPDATE user_log
	set user_log.pwd = PASSWORD(pv_new_human_password) 
	where id = lcl_user_log_id;
	
	SELECT COUNT(1) into lcl_count 
	FROM user INNER JOIN user_log ON user.user_log_id = user_log.id
	where user_log.pwd = PASSWORD(pv_new_human_password)
	AND user.id = pv_user_id;
	IF lcl_count > 0 THEN
		set rv_status = 1;
	END IF;
END//



#NEEDS FIXED

CREATE OR REPLACE PROCEDURE add_root_user(IN pv_address_id int(9)
							, IN pv_company_id int(9)
							, IN pv_dob varchar(30)
							, IN pv_email_address varchar(40)
							, IN pv_first_name varchar(30)
							, IN pv_flag int(3)
							, IN pv_last_name varchar(30)
							, IN pv_password varchar(30)
							, IN pv_phone_number varchar(20)
							, IN pv_start_date varchar(30)
							, IN pv_username varchar(30)
							, OUT rv_user_id int(9)
							)
	BEGIN
		declare lcl_user_log_id int default 0;
		
		INSERT INTO user_log (address_id, company_id, dob, email_address, e_time, first_name, flag, last_name, phone_number, pwd)
        VALUES (pv_address_id, pv_company_id, pv_dob, pv_email_address, UNIX_TIMESTAMP(NOW()), pv_first_name, pv_flag, pv_last_name, pv_phone_number, Password(pv_password));
		
		select MAX(id) from user_log where user_log.company_id = pv_company_id INTO lcl_user_log_id;
		
		INSERT INTO user (company_id,e_time, flag, rrole_id, start_date, user_log_id, username)
		VALUES (pv_company_id, UNIX_TIMESTAMP(NOW()), pv_flag, pv_rrole_id, pv_start_date, lcl_user_log_id, pv_username);
		
		SELECT MAX(id) from user where user.company_id = pv_company_id INTO rv_user_id;
		update user set user.log_user_id = rv_user_id;
	END//

	
#666
CREATE OR REPLACE PROCEDURE add_user_with_address_api(IN pv_dob varchar(30)
							, IN pv_email_address varchar(40)
							, IN pv_first_name varchar(30)
							, IN pv_flag int(3)
							, IN pv_last_name varchar(100)
							, IN pv_user_id int(6)
							, IN pv_password varchar(100)
							, IN pv_phone_number varchar(20)
							, IN pv_ssession_number int(9)
							, IN pv_start_date date
							, IN pv_rrole_id int(9)
							, IN pv_username varchar(30)
							, IN pv_city varchar(30)
							, IN pv_state_id int(10)
							, IN pv_street_address varchar(100)
							, IN pv_zip_code varchar(10)
							, IN pv_company_id int(9)
							, OUT rv_user_id int(9)
							)
	BEGIN
    	declare lcl_auth int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		CALL add_user_with_address(pv_dob, pv_email_address, pv_employee, pv_first_name, pv_flag, pv_last_name, pv_user_id, pv_password, pv_phone_number, pv_start_date, pv_rrole_id, pv_username, pv_city, pv_state_id, pv_street_address, pv_zip_code, pv_company_id, rv_user_id);
		END IF;
		IF rv_user_id IS NULL THEN
		set rv_user_id = -2;
		END IF;
	END//


#NEEDS FIXED
CREATE OR REPLACE PROCEDURE add_user_with_address(IN pv_dob varchar(30)
							, IN pv_email_address varchar(40)
							, IN pv_first_name varchar(30)
							, IN pv_flag int(3)
							, IN pv_last_name varchar(30)
							, IN pv_user_id int(6)
							, IN pv_password varchar(30)
							, IN pv_phone_number varchar(20)
							, IN pv_start_date varchar(30)
							, IN pv_rrole_id int(9)
							, IN pv_username varchar(30)
							, IN pv_city varchar(30)
							, IN pv_state_id int(10)
							, IN pv_street_address varchar(100)
							, IN pv_zip_code varchar(10)
							, IN pv_company_id int(9)
							, OUT rv_user_id int(9)
							)
	BEGIN
		declare lcl_user_log_id int default 0;
		declare lcl_address_id int default 0;
		INSERT INTO address(city, company_id, e_time, flag, log_user_id, state_id, street_address, zip_code)
		VALUES (pv_city, pv_company_id, UNIX_TIMESTAMP(NOW()), pv_flag, pv_user_id, pv_state_id, pv_street_address, pv_zip_code);
		SELECT MAX(id) from address INTO lcl_address_id;
		INSERT INTO user_log (address_id, company_id, dob, email_address, e_time, first_name, flag, last_name, log_user_id, phone_number, pwd)
        VALUES (lcl_address_id, pv_company_id, pv_dob, pv_email_address, UNIX_TIMESTAMP(NOW()), pv_first_name, pv_flag, pv_last_name, pv_user_id, pv_phone_number, PASSWORD(pv_password));
		select MAX(id) from user_log INTO lcl_user_log_id;
		INSERT INTO user (company_id,e_time, flag, log_user_id, start_date, rrole_id, user_log_id, username)
		VALUES (pv_company_id, UNIX_TIMESTAMP(NOW()), pv_flag, pv_user_id, pv_start_date, pv_rrole_id, lcl_user_log_id, pv_username);
		SELECT MAX(id) from user where user.company_id = pv_company_id INTO rv_user_id;
		IF rv_user_id IS NULL THEN
		SET rv_user_id = 0;
		END IF;
	END//


CREATE OR REPLACE PROCEDURE get_user_id(in pv_username varchar(30)
						, out rv_id int(9)
							)
BEGIN
	declare lcl_count int default 0;
	SET rv_id = -6;
	
	select count(1) into lcl_count
	FROM user
	WHERE username=pv_username;
	
	IF lcl_count > 0 THEN
		SELECT id into rv_id
		from user
		WHERE username=pv_username;
	END IF;
END//

CREATE OR REPLACE PROCEDURE update_user_phone_number_api(IN pv_new_phone_number varchar(30)
														, IN pv_user_id int(9)
														, IN pv_ssession_number int(9)
														, OUT rv_status int(9)
														)
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL update_user_phone_number(pv_new_phone_number, pv_user_id, rv_status);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE update_user_phone_number(IN pv_new_phone_number varchar(30)
													, IN pv_user_id int(9)
													, OUT rv_status int(9)
													)
BEGIN
	declare lcl_user_company_id int default 0;
	declare lcl_user_log_id int default 0;
	set rv_status = -99;
	call get_user_company_id(pv_user_id, lcl_user_company_id);
	select user_log.id into lcl_user_log_id FROM user INNER JOIN user_log ON user.user_log_id = user_log.id WHERE user.id = pv_user_id;
	update user_log set phone_number = pv_new_phone_number where id = lcl_user_log_id AND company_id = lcl_user_company_id;
	if lcl_user_log_id > 0 THEN 
	set rv_status = 1;
	END IF;
END//


CREATE OR REPLACE procedure is_admin(IN pv_user_id int(9), OUT rv_response int(3))
BEGIN
	declare lcl_rrole_string varchar(100) default "";
	set rv_response = -1;
	select rrole.rrole INTO lcl_rrole_string
	FROM user INNER JOIN rrole ON user.rrole_id = rrole.id WHERE user.id = pv_user_id;
	IF lcl_rrole_string = "admin" THEN
	set rv_response = 1;
	END IF;
END//

CREATE OR REPLACE PROCEDURE get_companies_users_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_company_id int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_companies_users(pv_company_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_companies_users(IN pv_company_id int(9)
                                        )
BEGIN
SELECT user.id, user.user_log_id, user.username, user.e_time, user.flag
        , user.company_id, user.start_date, user_log.first_name
		, user_log.last_name, user_log.email_address, user_log.dob
		, user_log.phone_number, user.rrole_id
		FROM user INNER JOIN user_log ON user.user_log_id = user_log.id
		where user.company_id = pv_company_id;
END//



CREATE OR REPLACE PROCEDURE update_user_api(IN pv_cookie_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_new_username varchar(30)
                                            , IN pv_new_start_date varchar(30)
											, IN pv_new_rrole_id int(9)
											, IN pv_new_first_name varchar(50)
											, IN pv_new_last_name varchar(50)
											, IN pv_new_email_address varchar(100)
											, IN pv_new_dob varchar(30)
											, IN pv_new_phone_number varchar(20)
											, IN pv_new_pwd varchar(100)
											, IN pv_new_street_address varchar(100)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_cookie_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
    call update_user(pv_user_id, pv_new_username, pv_new_start_date
	, pv_new_rrole_id, pv_new_first_name, pv_new_last_name, pv_new_email_address
	, pv_new_dob, pv_new_phone_number, pv_new_pwd, pv_new_street_address);
    END IF;
END//


CREATE OR REPLACE PROCEDURe update_user(IN pv_user_id int(9)
                                            , IN pv_new_username varchar(30)
                                            , IN pv_new_start_date varchar(30)
											, IN pv_new_rrole_id int(9)
											, IN pv_new_first_name varchar(50)
											, IN pv_new_last_name varchar(50)
											, IN pv_new_email_address varchar(100)
											, IN pv_new_dob varchar(30)
											, IN pv_new_phone_number varchar(20)
											, IN pv_new_pwd varchar(100)
											, IN pv_new_street_address varchar(100)
                                            )
BEGIN
	declare lcl_user_log_id int(9) default 0;
	declare lcl_address_id int(9) default 0;
	select user.user_log_id INTO lcl_user_log_id FROM user where id = pv_user_id;
	select user_log.address_id INTO lcl_address_id FROM user_log where id = lcl_user_log_id;
    update user SET 
    username = pv_new_username,
	start_date = pv_new_start_date,
	rrole_id = pv_new_rrole_id
	WHERE id = pv_user_id;
	UPDATE user_log SET 
	first_name = pv_new_first_name,
	last_name = pv_new_last_name,
	dob = pv_new_dob,
	phone_number = pv_new_phone_number,
	pwd = PASSWORD(pv_new_pwd)
    WHERE id = lcl_user_log_id;
	UPDATE address SET 
	street_address = pv_new_street_address
	WHERE id = lcl_address_id;
END//


-- #NOT IN PROC INDEX

-- CREATE OR REPLACE PROCEDURE get_user_rroles(IN pv_user_id int(9)
-- 												, IN pv_user_log_id int(9))
-- BEGIN
-- 		SELECT a.id, 
-- 			a.rrole, 
-- 			a.ppower 
-- 		FROM   rrole a, 
-- 			ggroup_rrole b, 
-- 			user_ggroup c 
-- 		WHERE  a.id = b.rrole_id 
-- 			AND b.ggroup_id = c.ggroup_id 
-- 			AND c.user_id = pv_user_id 
-- 		GROUP BY a.id, 
-- 				a.rrole, 
-- 				a.ppower; 
-- END// 
-- #416

-- CREATE OR REPLACE PROCEDURE get_user_rroles_api(IN pv_user_id INT(10)
-- 							, IN pv_user_log_id int(10)
-- 							, IN pv_ssession_number int(9)
-- 							)
-- 	BEGIN
-- 		declare lcl_user_company_id int default 0;
-- 		declare lcl_run int default 0;
-- 		declare lcl_user_id int default 0;
-- 		call database_access_add(lcl_run, lcl_user_company_id
-- 		, pv_user_log_id, 'r', pv_ssession_number, 'rrole');
-- 		If lcl_run = 1 THEN
-- 			call get_user_rroles(pv_user_id, pv_user_log_id);
-- 		END IF;
-- 	END//


DELIMITER //

#90

-- CREATE OR REPLACE PROCEDURE hire_api(IN pv_admin int(1)
-- 				, IN pv_city varchar(30)
-- 				, IN pv_customer int(1)
-- 				, IN pv_dob varchar(30)
-- 				, IN pv_email_address varchar(50)
-- 				, IN pv_employee int(1)
-- 				, IN pv_first_name varchar(50)
-- 				, IN pv_ggroup_id int(9)
-- 				, IN pv_last_name varchar(50)
-- 				, IN pv_user_id int(9)
-- 				, IN pv_password varchar(50)
-- 				, IN pv_phone_number varchar(20)
-- 				, IN pv_ssession_number int(9)
-- 				, IN pv_start_date varchar(13)
-- 				, IN pv_state_id int(9)
-- 				, IN pv_street_address varchar(100)
-- 				, IN pv_username varchar(30)
-- 				, IN pv_zip_code varchar(30)
-- 				, OUT rv_user_id INT(10)
-- 				)
-- 	BEGIN
-- 		declare lcl_run_add_address int default 0;
-- 		declare lcl_run_add_user int default 0;
-- 		declare lcl_run_add_user_ggroup int default 0;
-- 		declare lcl_run_hire int default 0;
-- 		declare lcl_user_company_id int default 0;
-- 		declare lcl_dob date;
-- 		declare lcl_start_date date;
-- 		SET rv_user_id = -6;
-- 		select CAST(FROM_UNIXTIME(pv_start_date) AS DATE) AS lcl_start_date;
-- 		select CAST(FROM_UNIXTIME(pv_dob) AS DATE) AS lcl_dob;
-- 		call get_user_company_id(pv_log_user_id, lcl_user_company_id);
-- 		call database_access_add(lcl_run_add_address, lcl_employee_company_id
-- 		, pv_log_user_id, 'w', pv_ssession_number, 'address');
-- 		call database_access_add(lcl_run_add_employee, lcl_employee_company_id
-- 		, pv_log_user_id, 'w', pv_ssession_number, 'employee');
-- 		call database_access_add(lcl_run_add_person, lcl_employee_company_id
--                 , pv_log_user_id, 'w', pv_ssession_number, 'person');
-- 		call database_access_add(lcl_run_add_person_log, lcl_employee_company_id
-- 		, pv_log_user_id, 'w', pv_ssession_number, 'person_log');
-- 		call database_access_add(lcl_run_add_employee_job, lcl_employee_company_id
--                 , pv_log_user_id, 'w', pv_ssession_number, 'employee_job');
-- 		call database_access_add(lcl_run_add_employee_ggroup, lcl_employee_company_id
--                 , pv_log_user_id, 'w', pv_ssession_number, 'employee_ggroup');
-- 		set lcl_run_hire = lcl_run_add_address*lcl_run_add_employee*lcl_run_add_person*lcl_run_add_person_log*lcl_run_add_employee_job*lcl_run_add_employee_ggroup;
-- 		if lcl_run_hire = 1 THEN
-- 		call hire(pv_city, lcl_dob, pv_email_address, pv_first_name, pv_job_id, pv_last_name, pv_log_user_id,
-- 		pv_password, pv_pay, pv_phone_number, lcl_start_date, pv_state_id, pv_street_address, pv_username,
-- 		pv_zip_code, rv_employee_id);
-- 		END IF;
-- 	END//
