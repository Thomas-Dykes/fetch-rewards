DELIMITER //
#275
CREATE OR REPLACE PROCEDURE get_proc_index_count(OUT rv_out int(10))
	BEGIN
		SELECT COUNT(1) into rv_out from proc_index;
	END//

CREATE OR REPLACE PROCEDURE get_proc_index_max(OUT rv_out int(10))
	BEGIN
		SELECT MAX(proc_index.id) INTO rv_out FROM proc_index;
	END//

#281
CREATE OR REPLACE PROCEDURE delete_proc_index(IN pv_log_employee_id INT(9)
					,IN pv_proc_index_id INT(9)
					, OUT rv_status_code INT(9)
					)
	BEGIN
		DECLARE lcl_outbound_id int(9) default 0;
		DECLARE rv_proc_index_json varchar(1000) default '';
		DECLARE lcl_description varchar(100) default '';
		DECLARE lcl_e_time INT default 0;
		DECLARE lcl_flag INT default 0;
		DECLARE lcl_j_inputs varchar(200) default '';
		declare lcl_j_outputs varchar(500) default '';
		declare lcl_j_tables varchar(200) default '';
		DECLARE lcl_log_employee_id int default 0;
		DECLARE lcl_name varchar(40) default '';
		declare lcl_public int(1) default 0;
		declare lcl_description_string varchar(100) default '';
		declare lcl_e_time_string varchar(40) default '';
		declare lcl_flag_string varchar(40) default '';
		declare lcl_j_inputs_string varchar(200) default '';
		declare lcl_j_outputs_string varchar(500) default '';
		declare lcl_j_tables_string varchar(200) default '';
		declare lcl_name_string varchar(40) default '';
		declare lcl_public_string varchar(40) default '';
		DECLARE lcl_almost_finished_string varchar(500) default '';
		DECLARE EXIT HANDLER FOR 1451
			-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraINT fails 
			BEGIN
				select -1 INTo rv_status_code;
			END;
		SELECT proc_index.description INTO lcl_description FROM proc_index WHERE proc_index.id = pv_proc_index_id;
		SELECT proc_index.e_time INTO lcl_e_time FROM proc_index WHERE proc_index.id = pv_proc_index_id;
		SELECT proc_index.flag INTO lcl_flag FROM proc_index WHERE proc_index.id = pv_proc_index_id;
		SELECT proc_index.j_inputs INTO lcl_j_inputs FROM proc_index WHERE proc_index.id = pv_proc_index_id;
		SELECT proc_index.j_outputs INTO lcl_j_outputs FROM proc_index WHERE proc_index.id = pv_proc_index_id;
		SELECT proc_index.j_tables INTO lcl_j_tables FROM proc_index WHERE proc_index.id = pv_proc_index_id;
		SELECT proc_index.name INTO lcl_name FROM proc_index WHERE proc_index.id = pv_proc_index_id;
		SELECT proc_index.public INTO lcl_public FROM proc_index WHERE proc_index.id = pv_proc_index_id;
		CALL JSONIFY_STRING('description', lcl_description, lcl_description_string);
    	CALL JSONIFY_INT(lcl_e_time, 'e_time', lcl_e_time_string);
    	CALL JSONIFY_INT(lcl_flag, 'flag', lcl_flag_string);
		CALL JSONIFY_STRING('j_inputs', lcl_j_inputs, lcl_j_inputs_string);
		CALL JSONIFY_STRING('j_outputs', lcl_j_outputs, lcl_j_outputs_string);
		CALL JSONIFY_STRING('j_tables', lcl_j_tables, lcl_j_tables_string);
		CALL JSONIFY_STRING('name', lcl_name, lcl_name_string);
    	CALL JSONIFY_INT(lcl_public, 'pulic', lcl_public_string);
		select concat("'{", lcl_description_string, ",", lcl_e_time_string, ",", lcl_flag_string, ",", lcl_j_inputs_string, ",", lcl_j_outputs_string, ",", lcl_j_tables_string, ",", lcl_name_string, ",", lcl_public_string, "}") INTO lcl_alMOst_finished_string;
		select concat(lcl_almost_finished_string, "'") INTo rv_proc_index_json;
		CALL add_outbound(0, pv_log_employee_id, rv_proc_index_json, 'proc_index', lcl_outbound_id);
		DELETE FROM proc_index where proc_index.id = pv_proc_index_id;
		select lcl_outbound_id INTO rv_status_code;
	END//
#276
CREATE OR REPLACE PROCEDURE get_proc_indexes(IN pointless_parameter int(1))
	BEGIN
		SELECT  proc_index.description, proc_index.e_time, proc_index.flag, proc_index.id,
		proc_index.j_inputs, proc_index.j_outputs, proc_index.j_tables, proc_index.name
		, proc_index.public
		FROM proc_index
		ORDER BY proc_index.name;
	END//
#277
CREATE OR REPLACE PROCEDURE get_proc_indexes_api(in pointless_parameter int(1))
	BEGIN
		SELECT proc_index.description, proc_index.database_name, proc_index.e_time, proc_index.flag, proc_index.id
		, proc_index.j_inputs, proc_index.j_outputs, proc_index.j_tables, proc_index.name
		, proc_index.public
		FROM proc_index
		WHERE proc_index.public = 1
		ORDER BY proc_index.name;
	END//
#278
CREATE OR REPLACE PROCEDURE get_proc_index_api(IN pv_applied_id int(9))
	BEGIN
		SELECT proc_index.description, proc_index.database_name, proc_index.e_time, proc_index.flag, proc_index.id
                , proc_index.j_inputs, proc_index.j_outputs, proc_index.j_tables, proc_index.name
                , proc_index.public
		FROM proc_index
		where id = pv_applied_id
		AND proc_index.public = 1
		ORDER BY proc_index.name;
	END//

#279
CREATE OR REPLACE PROCEDURE add_proc_index(IN pv_applied_id INT(9)
							, IN pv_description VARCHAR(100)
							, IN pv_flag INT(3)
							, in pv_j_inputs VARCHAR(1000)
							, in pv_j_outputs VARCHAR(200)
							, in pv_j_tables VARCHAR(500)
							, IN pv_name VARCHAR(100)
							, in pv_public INT(1)
							)
	BEGIN
	    DECLARE lcl_count INT DEFAULT -5;
		SELECT COUNT(1) INTO lcl_count FROM proc_index where id = pv_applied_id;
		IF lcl_count = 0 THEN
		INSERT INTO proc_index (description, e_time, flag, id, j_inputs,j_outputs,j_tables,name, public, database_name)
		VALUES (pv_description, UNIX_TIMESTAMP(NOW()), pv_flag, pv_applied_id, pv_j_inputs, pv_j_outputs,pv_j_tables, pv_name, pv_public, 'business_transactional_prod');
		END IF;
	END//
#280
CREATE OR REPLACE PROCEDURE add_proc_index_postgres(IN pv_applied_id INT(9)
							, IN pv_database_name int(9)
							, IN pv_description VARCHAR(100)
							, IN pv_flag INT(3)
							, in pv_j_inputs VARCHAR(500)
							, in pv_j_outputs VARCHAR(200)
							, in pv_j_tables VARCHAR(500)
							, IN pv_name VARCHAR(100)
							, in pv_public INT(1)

							)
	BEGIN
	    DECLARE lcl_count INT DEFAULT -5;
		SELECT COUNT(1) INTO lcl_count FROM proc_index where id = pv_applied_id;
		IF lcl_count = 0 THEN
		INSERT INTO proc_index (description, e_time, flag, id, j_inputs,j_outputs,j_tables,name, public, database_name)
		VALUES (pv_description, UNIX_TIMESTAMP(NOW()), pv_flag, pv_applied_id, pv_j_inputs, pv_j_outputs,pv_j_tables, pv_name, pv_public, pv_database_name);
		END IF;
	END//
#281
CREATE OR REPLACE PROCEDURE get_proc_like(IN pv_name VARCHAR(20))
	BEGIN
		SELECT name, id 
		FROM proc_index 
		WHERE name like CONCAT('%',pv_name,'%')
		ORDER BY name;
	END//
#282
CREATE OR REPLACE PROCEDURE get_public_procs(IN pv_employee_id INT(10))
	BEGIN
		SELECT id, name, description 
		FROM proc_index
		WHERE public = 1;
	END//

