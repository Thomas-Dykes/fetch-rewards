set -exou pipefail

echo "begin data deploy"

echo "set variables"
DB_USER=$2
HOST=$1
DB=$3
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/data/state.sql || echo "state data"

echo "end data deploy"