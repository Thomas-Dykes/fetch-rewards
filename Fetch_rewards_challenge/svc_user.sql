USE mysql;
CREATE USER IF NOT EXISTS 'test_user'@'localhost' IDENTIFIED BY '';
CREATE USER IF NOT EXISTS 'svc_user'@'localhost' IDENTIFIED BY '';
UPDATE user set authentication_string=password('newPass') where user = 'test_user';
UPDATE user set authentication_string=password('chick3en') where user = 'svc_user';

GRANT EXECUTE ON *.* TO 'test_user'@'localhost' IDENTIFIED BY 'newPass' WITH GRANT OPTION;
REVOKE ALL ON *.* FROM 'svc_user'@'localhost';
-- GRANT SELECT ON business_transactional_prod.rrole to 'svc_user'@'localhost';

FLUSH PRIVILEGES;
