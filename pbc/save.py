import csv, datetime
import config


def log(name, params):
    print(f"name: {name}, params: {params}")
    with open(config.log_file, "a", newline="") as csvfile:
        writer = csv.writer(
            csvfile, delimiter=",", quotechar="|", quoting=csv.QUOTE_MINIMAL
        )
        writer.writerow([datetime.datetime.now(), name, params])
