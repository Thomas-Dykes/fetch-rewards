DELIMITER //

CREATE OR REPLACE PROCEDURE new_ggroups_and_ggroup_rroles(IN pv_log_user_id int(9)
							, OUT rv_jedi_id int(1)
                            )
BEGIN
    call add_ggroup('can do customer things',0, pv_log_user_id, 'customer', rv_jedi_id);
	call add_ggroup_rrole(1, rv_jedi_id, pv_log_user_id, 1);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 4);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 7);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 10);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 13);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 16);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 19);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 22);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 25);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 28);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 30);
    call add_ggroup_rrole(4, rv_jedi_id, pv_log_user_id, 32);
END//
call new_ggroups_and_ggroup_rroles(2, @output);


