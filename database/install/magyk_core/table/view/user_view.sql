CREATE OR REPLACE VIEW user_view as
    SELECT a.id, 
        a.username, 
        b.first_name, 
        b.last_name, 
        b.address_id, 
        b.company_id 
    FROM   user a, 
        user_log b 
    WHERE  a.user_log_id = b.id; 