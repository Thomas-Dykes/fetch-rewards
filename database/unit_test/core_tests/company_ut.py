import time, decimal, sys

import user_log_ut as pl


sys.path.append("/home/ec2-user/pbc_portal/pbc")
# sys.path.append("../../../magyk")
import login
import connector as co
import config


def delete_company(cookie, company_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "delete_company_api"
    params = [company_id, 0, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    outbound_id = co.machine_add(jdat)
    print(f"outbound_id: {outbound_id}")
    return outbound_id


def test_delete_company_foreign_key_duplicate_error(pv_company_id):
    print("test delete company needs to have proper return")
    proc_name = "delete_company"
    params = [pv_company_id, 0, 1, 0]
    jdat = {"name": proc_name, "params": params}
    status_code = co.machine_add(jdat)
    print(f"status: {status_code}")
    if status_code == -1:
        return {
            "test": "test_foreign_key_duplicate_error",
            "result": True,
            "table": "company",
            "proc_index_id": [4, 6],
        }
    return {
        "test": "test_foreign_key_duplicate_error",
        "result": False,
        "table": "company",
        "proc_index_id": [4, 6],
    }


def add_company(name, timezone):
    if not (name == "birdhouse inc"):        
        proc_name = "add_company_api"
        params = ["0", name, timezone, 0]
        jdat = {"name": proc_name, "params": params}
        company_id = co.machine_add(jdat)
        print(f"company id: {company_id}")
        return company_id


def add_company_2(user_id, name, timezone):
    proc_name = "add_company_2"
    params = ["0", user_id, name, timezone, 0]
    jdat = {"name": proc_name, "params": params}
    company_id = co.machine_add(jdat)
    print(f"company_id: {company_id}")
    return company_id


def get_company_by_name(name):
    proc_name = "get_company_by_name"
    params = [name, 0]
    jdat = {"name": proc_name, "params": params}
    id = co.machine_add(jdat)
    print(f"company id: {id}")
    return id


def get_company_count():
    proc_name = "get_company_count"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    count = co.machine_add(jdat)
    print(f"local count: {count}")
    return count


def get_companys():
    proc_name = "get_companys_api"
    params = []
    jdat = {"name": proc_name, "params": params}
    companys = co.machine(jdat)
    for company in companys:
        print(f"company: {company}")
    return companys


def get_company(company_id):
    proc_name = "get_company_api"
    params = [company_id]
    jdat = {"name": proc_name, "params": params}
    company = co.machine(jdat)[0]
    print(f"company: {company}")
    return company


def test_add_1():
    timezone = 1
    name = "test name 1"
    print("add test 1")
    id = add_company(name, timezone)
    time.sleep(0.1)
    print(f"id: {id}")
    print(" \n \n ")
    print("get test 1")
    company = get_company(id)
    lcl_name = company["name"]
    lcl_timezone = company["timezone"]
    print(f"lcl_name: {lcl_name}")
    if lcl_name == name:
        if lcl_timezone == timezone:
            return (
                id,
                {
                    "test": "test_add_1",
                    "result": True,
                    "table": "company",
                    "proc_index_id": [20, 19, 18, 17],
                },
            )
    return (
        id,
        {
            "test": "test_add_1",
            "result": False,
            "table": "company",
            "proc_index_id": [20, 19, 18, 17],
        },
    )


def test_add_2():
    timezone = 2
    name = "test name 2"
    print("add test 2")
    id = add_company(name, timezone)
    time.sleep(0.1)
    print(f"id: {id}")
    print(" \n \n ")
    print("get test 2")
    company = get_company(id)
    lcl_name = company["name"]
    lcl_timezone = company["timezone"]
    print(f"lcl_name: {lcl_name}")
    if lcl_name == name:
        if lcl_timezone == timezone:
            return (
                id,
                {
                    "test": "test_add_2",
                    "result": True,
                    "table": "company",
                    "proc_index_id": [20, 19, 18, 17],
                },
            )
    return (
        id,
        {
            "test": "test_add_2",
            "result": False,
            "table": "company",
            "proc_index_id": [20, 19, 18, 17],
        },
    )


def test_gets(a, b):
    #   a/b are valid company ids
    company_one = {}
    company_two = {}
    for i in get_companys():
        if i["id"] == a:
            company_one = i
        if i["id"] == b:
            company_two = i

    add_1 = get_company(a)
    add_2 = get_company(b)

    if add_1["name"] == company_one["name"]:
        if add_2["name"] == company_two["name"]:
            if add_1["timezone"] == company_one["timezone"]:
                if add_2["timezone"] == company_two["timezone"]:

                    return {
                        "test": "test_gets",
                        "result": True,
                        "table": "company",
                        "proc_index_id": [16, 15],
                    }
    return {
        "test": "test_gets",
        "result": False,
        "table": "company",
        "proc_index_id": [16, 15],
    }


# call test_add_company('company_a', 1, @rv_passed);


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        t_table = test["table"]
        t_test = test["test"]
        t_result = test["result"]
        t_proc_index_id = test["proc_index_id"]
        print(
            f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
        )


def machine(cookie):
    tests = []
    print(" \n \n ")
    lcl_id, result = test_add_1()
    user_id = cookie["user_id"]

    tests.append(result)
    lcl_id_two, result_two = test_add_2()
    tests.append(result_two)
    # print(" \n \n ")
    tests.append(test_gets(lcl_id, lcl_id_two))
    # tear down
    # input(f"user_id: {user_id}, lcl_id: {lcl_id}, lcl_Id_2: {lcl_id_two}")
    delete_company(cookie, lcl_id)
    delete_company(cookie, lcl_id_two)

    #    tests.append(test_delete_company_foreign_key_duplicate_error(1))
    return tests


def main():
    cookie = login.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
