import csv
from scipy import optimize
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
# from piecewise import piecewise
# from piecewise import piecewise_plot
import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config




def interpret_service_month(month, year):
    proc_name = "interpret_service_month"
    params = [month, year, 0]
    jdat = {"name": proc_name, "params": params}
    service_month_id = co.machine_add(jdat)
    return service_month_id

def add_electricity_raw(usage, adjustment, building_id, oat, start, end, service_month_id, cost, user_id):
    proc_name = "add_electricity_pre_period_raw"
    params = [usage, adjustment, building_id, 0, oat, start, end, service_month_id, cost, user_id, 0]
    jdat = {"name": proc_name, "params": params}
    electricity_id = co.machine_add(jdat)
    return electricity_id


def main(data, building_id, user_id):
    count = 0
    for row in data:
        print(row)
        if(isinstance(row, str)):
            row = row.split(', ')
        # data_row.add(row)
        # print(f"ROWROWPOST: {row}")
        # print("ROWROWPOSTLEN: {len(")
        # if (0 < count):
        space = row[4][1:len(row[4])].find(" ")
        month = row[4][1:space+1]
        year = row[4][(space+2):len(row[4])]
        print(f"month: {month}")
        print(f"Year: {year}")
        service_month_id = interpret_service_month(month, year)
        if len(row)== 7 :
            electricity_id = add_electricity_raw(int(row[2]), int(row[3]), building_id, round(float(row[5])), row[0], row[1], service_month_id, int(row[6]), user_id)
        if len(row)== 6:
            electricity_id = add_electricity_raw(int(row[2]), int(row[3]), building_id, round(float(row[5])), row[0], row[1], service_month_id, 0, user_id)
        count = count + 1
    #add data
    # for row in data:
    #validate data



if __name__ == "__main__":
    data = []
    user_id = 6
    building_id = 14

    with open('electricity_sample_hospital_NOT_BAYLOR.csv')as f:
        readCSV = csv.reader(f, delimiter=',')
        next(readCSV, None)
        for row in readCSV:
            data.append(row)

    main(data, building_id, user_id)