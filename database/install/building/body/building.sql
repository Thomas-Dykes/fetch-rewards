DELIMITER //

CREATE OR REPLACE PROCEDURE add_building_api(IN pv_area int(10)
                                            , IN pv_name varchar(30)
                                            , IN pv_building_category_id int(9)
                                            , IN pv_electricity_model_id int(9)
                                            , IN pv_gas_model_id int(9)
                                            , IN pv_address_id int(9)
                                            , IN pv_built varchar(30)
                                            , IN pv_company_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , OUT rv_building_id int(9)
)
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_building(pv_area, pv_name, pv_building_category_id, pv_electricity_model_id, pv_gas_model_id, pv_address_id, pv_built, pv_company_id, pv_user_id, rv_building_id);
    END IF;
END//
                                            
CREATE OR REPLACE PROCEDURE add_building(IN pv_area int(10),
                                          IN pv_name varchar(40),
                                          IN pv_building_category_id int(9),
                                          IN pv_electricity_model_id int(9),
                                          IN pv_gas_model_id int(9),
                                          IN pv_address_id int(9),
                                          IN pv_built varchar(30),
                                          IN pv_company_id int(9),
                                          IN pv_user_id int(10),
                                          OUT rv_building_id int(10))
BEGIN
    insert into building(area, name, building_category_id,electricity_model_id, gas_model_id, built, address_id, e_time, flag, company_id, log_user_id)
    values(pv_area, pv_name, pv_building_category_id, pv_electricity_model_id, pv_gas_model_id, pv_built, pv_address_id, UNIX_TIMESTAMP(NOW()), 0, pv_company_id, pv_user_id);
    SELECT MAX(id) into rv_building_id
    from building 
    where area = pv_area
    and log_user_id = pv_user_id;
END//


CREATE OR REPLACE PROCEDURE add_root_building_api(IN pv_area int(10)
                                            , IN pv_name varchar(30)
                                            , IN pv_building_category_id int(9)
                                            , IN pv_electricity_model_id int(9)
                                            , IN pv_gas_model_id int(9)
                                            , IN pv_address_id int(9)
                                            , IN pv_built varchar(30)
                                            , IN pv_company_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , OUT rv_building_id int(9)
)
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_root_building(pv_area, pv_name, pv_building_category_id, pv_electricity_model_id, pv_gas_model_id, pv_address_id, pv_built, pv_company_id, pv_user_id, rv_building_id);
    END IF;
END//
                                            
CREATE OR REPLACE PROCEDURE add_root_building(IN pv_area int(10),
                                          IN pv_name varchar(40),
                                          IN pv_building_category_id int(9),
                                          IN pv_electricity_model_id int(9),
                                          IN pv_gas_model_id int(9),
                                          IN pv_address_id int(9),
                                          IN pv_built varchar(30),
                                          IN pv_company_id int(9),
                                          IN pv_user_id int(10),
                                          OUT rv_building_id int(10))
BEGIN
    IF pv_company_id IS NULL THEN
        set pv_company_id = 1;
    END IF;
    insert into building(area, name, building_category_id,electricity_model_id, gas_model_id, built, address_id, e_time, flag, company_id, log_user_id)
    values(pv_area, pv_name, pv_building_category_id, pv_electricity_model_id, pv_gas_model_id, pv_built, pv_address_id, UNIX_TIMESTAMP(NOW()), 0, pv_company_id, pv_user_id);
    SELECT MAX(id) into rv_building_id
    from building 
    where area = pv_area
    and log_user_id = pv_user_id;
END//



CREATE OR REPLACE PROCEDURE add_building_with_address_api(IN pv_area int(10)
                                            , IN pv_name varchar(30)
                                            , IN pv_building_category_id int(9)
                                            , IN pv_electricity_model_id int(9)
                                            , IN pv_gas_model_id int(9)
                                            , IN pv_built varchar(30)
                                            , IN pv_city varchar(30)
									        , IN pv_flag int(3)
									        , IN pv_state_id int(10)
									        , IN pv_street_address varchar(100)
									        , IN pv_zip_code varchar(10)
                                            , IN pv_company_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , OUT rv_building_id int(9)
)
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_building_with_address(pv_area, pv_name, pv_building_category_id, pv_electricity_model_id, pv_gas_model_id, pv_built, pv_city, pv_flag, pv_state_id, pv_street_address, pv_zip_code, pv_company_id, pv_user_id, rv_building_id);
    END IF;
END//
                                            
CREATE OR REPLACE PROCEDURE add_building_with_address(IN pv_area int(10)
                                        , IN pv_name varchar(40)
                                        , IN pv_building_category_id int(9)
                                        , IN pv_electricity_model_id int(9)
                                        , IN pv_gas_model_id int(9)
                                        , IN pv_built varchar(30)
                                        , IN pv_city varchar(30)
									    , IN pv_flag int(3)
									    , IN pv_state_id int(10)
									    , IN pv_street_address varchar(100)
									    , IN pv_zip_code varchar(10)
                                        , IN pv_user_id int(10)
                                        , IN pv_company_id int(10)
                                        , OUT rv_building_id int(10))
BEGIN
    declare lcl_address_id int default 0;
    INSERT INTO address(city, company_id, e_time, flag, latitude, log_user_id, longitude, state_id, street_address, zip_code)
	VALUES (pv_city, pv_company_id, UNIX_TIMESTAMP(NOW()), pv_flag, pv_latitude, pv_user_id, pv_longitude, pv_state_id, pv_street_address, pv_zip_code);
	SELECT MAX(id) from address INTO lcl_address_id;
    insert into building(area, name, building_category_id, electricity_model_id, gas_model_id, built, address_id, e_time, flag, company_id, log_user_id)
    values(pv_area, pv_name, pv_building_category_id, pv_electricity_model_id, pv_gas_model_id, pv_built, lcl_address_id, UNIX_TIMESTAMP(NOW()), 0, pv_company_id, pv_user_id);
    SELECT MAX(id) into rv_building_id
    from building 
    where area = pv_area
    and log_user_id = pv_user_id;
END//

CREATE OR REPLACE PROCEDURE get_buildings_admin_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_buildings_admin();
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_buildings_admin(
                                        )
BEGIN
    SELECT building.id, 
        building.area, 
        building.name, 
        building_category.name building_category_name,
        building.electricity_model_id, 
        building.gas_model_id, 
        building.built, 
        address.street_address,
        building.e_time, 
        building.flag, 
        building.log_user_id 
    FROM building_category 
    INNER JOIN building ON building_category.id = building.building_category_id
    INNER JOIN address ON building.address_id = address.id;
END//


CREATE OR REPLACE PROCEDURE get_buildings_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_buildings();
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_buildings(
                                        )
BEGIN
    SELECT building.id, 
        building.area, 
        building.name, 
        building_category.name building_category_name,
        building.electricity_model_id, 
        building.gas_model_id, 
        building.built, 
        address.street_address,
        building.e_time, 
        building.flag, 
        building.log_user_id 
    FROM building_category 
    INNER JOIN building ON building_category.id = building.building_category_id
    INNER JOIN address ON building.address_id = address.id;
END//


CREATE OR REPLACE PROCEDURE get_companies_buildings_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_company_id int(9)
                                            )
BEGIN
    declare lcl_auth int default 1;
    -- CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_companies_buildings(pv_company_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_companies_buildings(IN pv_company_id int(9)
                                                , IN pv_user_id int(9)
                                        )
BEGIN
    declare lcl_rrole_id int default 0;
	select user.rrole_id INTO lcl_rrole_id FROM user where id = pv_user_id;
    IF lcl_rrole_id > 1 THEN
    SELECT building.id, 
        building.area, 
        building.name, 
        building_category.name building_category_name,
        building.electricity_model_id, 
        building.gas_model_id, 
        building.built, 
        address.street_address,
        building.e_time, 
        building.flag, 
        building.log_user_id 
    FROM building_category 
    INNER JOIN building ON building_category.id = building.building_category_id
    INNER JOIN address ON building.address_id = address.id
    where building.company_id = pv_company_id;
    END IF;
    IF lcl_rrole_id = 1 THEN 
    call get_users_buildings(pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_building_api(IN pv_building_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_building(pv_building_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_building(IN pv_building_id int(9)
                                        , IN pv_user_id int(9)
                                        )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select building.id, building.area, building.name
    , building_category.name building_category_name
    , building.electricity_model_id, building.gas_model_id
    , building.built, address.street_address, building.e_time
    , building.flag, building.log_user_id
    FROM building_category 
    INNER JOIN building ON building_category.id = building.building_category_id
    INNER JOIN address ON building.address_id = address.id
    where building.id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_building_info_api(IN pv_building_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_building_info(pv_building_id);
    END IF;
END//


CREATE OR REPLACE PROCEDURE get_building_info(IN pv_building_id int(9)
                                        )
BEGIN
    select building.name name, address.street_address street_address, building.area area
    , building.built built, building_category.name building_category_name
    FROM building_category
    INNER JOIN building ON building_category.id= building.building_category_id
    INNER JOIN address ON building.address_id = address.id
    WHERE building.id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE get_building_area(IN pv_building_id int(9)
                                            , OUT rv_area int(9)
                                            )
BEGIN
    select building.area INTO rv_area FROM building
    where building.id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_building_name(IN pv_building_id int(9)
                                            , IN pv_user_id int(9)
                                            , OUT rv_name varchar(100)
                                            )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select building.name INTO rv_name FROM building
    where building.id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE assign_buildings_category_api(IN pv_building_id int(9)
                                                        , IN pv_building_category_id int(9)
                                                        , IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , OUT rv_status int(9)
                                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call assign_buildings_category(pv_building_id, pv_building_category);
    END IF;
END//

CREATE OR REPLACE PROCEDURE assign_buildings_category(IN pv_building_id int(9)
                                            , IN pv_building_category_id int(9)
                                            , OUT rv_status int(9)
                                            )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    update building set building_category_id = pv_building_category_id where id = pv_building_id;
    select count(1) into rv_status from building where id = pv_building_id;
END//


#APPEARS TO WORK
CREATE OR REPLACE PROCEDURE delete_building_api(IN pv_flag int(3)
												, IN pv_building_id int(9)
                                                , IN pv_user_id int(9)
                                                , IN pv_ssession_number int(9)
												, OUT rv_status_code int(9)
                                                )
 BEGIN
	declare lcl_auth int default 0;
	declare lcl_company_id int default 0;
	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
	IF lcl_auth = 1 THEN
	call delete_building(pv_flag, pv_building_id, rv_status_code);
    END IF;
END//


#APPEARS TO WORK
CREATE OR REPLACE PROCEDURE delete_building(IN pv_flag int(3)
										, IN pv_building_id int(9)
										, OUT rv_status_code int(9))
BEGIN
	declare lcl_outbound_id int(9) default 0;
	declare rv_building_json varchar(500) default '';
	declare lcl_area int(9) default 0;
	declare lcl_name varchar(30) default 0;
	declare lcl_building_category_id int(9) default 0;
	declare lcl_electricity_model_id int(9) default 0;
	declare lcl_gas_model_id int(9) default 0;
	declare lcl_address_id int(9) default 0;
	declare lcl_built varchar(30) default 0;
    declare lcl_e_time int(13) default 0;
    declare lcl_flag int(3) default 0;
    declare lcl_company_id int(9) default 0;
    declare lcl_log_user_id int(9) default 0;
	declare lcl_area_string varchar(40) default '';
	declare lcl_name_string varchar(40) default '';
	declare lcl_building_category_id_string varchar(40) default '';
	declare lcl_electricity_model_id_string varchar(40) default '';
    declare lcl_gas_model_id_string varchar(40) default '';
	declare lcl_address_id_string varchar(40) default '';
    declare lcl_built_string varchar(40) default '';
	declare lcl_company_id_string varchar(40) default '';
	declare lcl_e_time_string varchar(40) default '';
	declare lcl_flag_string varchar(40) default '';
    declare lcl_log_user_id_string varchar(40) default '';
	DECLARE EXIT HANDLER FOR 1451
	-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails 
	BEGIN
		select -1 into rv_status_code;
	END;
	select building.area INTO lcl_area FROM building WHERE building.id = pv_building_id;
	select building.name into lcl_name FROM building where building.id = pv_building_id;
	select building.building_category_id into lcl_building_category_id FROM building WHERE building.id = pv_building_id;
	select building.electricity_model_id into lcl_electricity_model_id from building WHERE building.id = pv_building_id;
    select building.gas_model_id into lcl_gas_model_id FROM building where building.id = pv_building_id;
    select building.address_id into lcl_address_id FROM building where building.id = pv_building_id;
    select building.built INTO lcl_built FROM building where building.id = pv_building_id;
	select building.e_time into lcl_e_time from building where building.id = pv_building_id;
    select building.flag into lcl_flag FROM building where building.id = pv_building_id;
    select building.company_id into lcl_company_id FROM building where building.id = pv_building_id;
    select building.log_user_id INTO lcl_log_user_id FROM building WHERE building.id = pv_building_id;

	call jsonify_int(lcl_area, 'area', lcl_area_string);

	call jsonify_string('name', lcl_name, lcl_name_string);
	call jsonify_int(lcl_building_category_id, 'building_category_id', lcl_building_category_id_string);
	call jsonify_int(lcl_electricity_model_id, 'electricity_model_id', lcl_electricity_model_id_string);
	call jsonify_int(lcl_gas_model_id, 'gas_model_id', lcl_gas_model_id_string);
	call jsonify_int(lcl_address_id, 'address_id', lcl_address_id_string);
	call jsonify_string('built', lcl_built, lcl_built_string);
	call jsonify_int(lcl_e_time, 'e_time', lcl_e_time_string);
	call jsonify_int(lcl_flag, 'flag', lcl_flag_string);
	call jsonify_int(lcl_company_id, 'company_id', lcl_company_id_string);
	call jsonify_int(lcl_log_user_id, 'log_user_id', lcl_log_user_id_string);	
    select concat("{", lcl_area_string, ",", lcl_name_string, ",", lcl_building_category_id_string, ",", lcl_electricity_model_id_string, ",", lcl_gas_model_id_string, ",", lcl_address_id_string, ",", lcl_built_string, ",", lcl_e_time_string, ",", lcl_flag_string, ",", lcl_company_id_string, ",", lcl_log_user_id_string, "}'") into rv_building_json;
    call add_outbound(pv_flag, pv_building_id, rv_building_json,  'building', lcl_outbound_id);
	DELETE FROM building where id = pv_building_id AND company_id = lcl_company_id;
    select lcl_outbound_id into rv_status_code;
END//


CREATE OR REPLACE PROCEDURE update_building_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_building_id int(9)
                                            , IN pv_new_name varchar(100)
                                            , IN pv_new_area int(9)
                                            , IN pv_new_built varchar(30)
                                            , IN pv_new_building_category_name varchar(100)
                                            , IN pv_new_street_address varchar(100)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call update_building(pv_building_id, pv_new_name, pv_new_area, pv_new_built, pv_new_building_category_name, pv_new_street_address);
    END IF;
END//


CREATE OR REPLACE PROCEDURe update_building(IN pv_building_id int(9)
                                            , IN pv_new_name varchar(100)
											, IN pv_new_area int(9)
                                            , IN pv_new_built varchar(30)
                                            , IN pv_new_building_category_name varchar(100)
                                            , IN pv_new_street_address varchar(100)
                                            )
BEGIN
    declare lcl_address_id int default 0;
    declare lcl_building_category_id int default 0;
    SELECT building.address_id INTO lcl_address_id FROM building where id = pv_building_id;
    SELECT building.building_category_id INTO lcl_building_category_id FROM building where id = pv_building_id;
    update building SET 
    name = pv_new_name,
	area = pv_new_area,
    built = pv_new_built
    WHERE id = pv_building_id;
    UPDATE address SET 
    street_address = pv_new_street_address
    where id = lcl_address_id;
    UPDATE building_category SET
    name = pv_new_building_category_name
    where id = lcl_building_category_id;
END//