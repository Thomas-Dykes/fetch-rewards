import time, decimal, sys, json

import user_log_ut as pl

import address_ut, company_ut, user_ggroup_ut,  user_ut, ggroup_ut, ggroup_rrole_ut, user_log_ut, proc_index_ut, rrole_ut, setup_ut, ssession_ut, delete_ut


sys.path.append("/home/ec2-user/business/magyk")
# sys.path.append("../../../magyk")
import login
import connector as co
import config


def get_outbounds(cookie):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_outbounds_api"
    params = [log_user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    outbounds = co.machine(jdat)
    for outbound in outbounds:
        print(f"outbound: {outbound}")
    return outbounds


def get_my_outbounds(cookie):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_my_outbounds_api"
    params = [log_user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    my_outbounds = co.machine(jdat)
    for outbound in my_outbounds:
        print(f"outbound: {outbound}")
    return my_outbounds


def get_recent_outbound(cookie):
    log_user_id = cookie["user_id"]
    proc_name = "get_recent_outbound"
    jdat = {"name": proc_name, "params": [log_user_id]}
    return co.machine(jdat)


def get_outbound(cookie, outbound_id):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_outbound_api"
    params = [log_user_id, outbound_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    outbound = co.machine(jdat)
    print(f"outbound: {outbound}")
    return outbound


def add_outbound(cookie, json, table_name):
    log_user_id = cookie["user_id"]
    proc_name = "add_outbound"
    params = ["0", log_user_id, json, table_name, 0]
    jdat = {"name": proc_name, "params": params}
    outbound_id = co.machine_add(jdat)
    print(f"outbound_id: {outbound_id}")
    return outbound_id

def test_delete_address_outbound(cookie):
    address_id = address_ut.add_address(
        cookie, "boise", 12.23, 34.34, 22, "1234 whatever lane", 22222
    )
    response = address_ut.delete_address(cookie, address_id)
    outbound = get_recent_outbound(cookie)[0]
    print(f"address outbound: {outbound}\n\n, delete: {address_id}, status: {response}")

    outbound_table_name = outbound["table_name"]
    outbound_log_user_id = outbound["log_user_id"]
    jdat = json.loads(outbound["json"])
    print(f"jdat: {jdat}")

    rv_id = int(jdat["address_id"])

    if address_id == rv_id:
        if outbound_log_user_id == cookie["user_id"]:
            return {
                "test": "test_delete_address",
                "result": True,
                "table": "address",
                "proc_index_id": [12, 403],
            }
    return {
        "test": "test_delete_address",
        "result": False,
        "table": "address",
        "proc_index_id": [12, 403],
    }


def test_delete_company_outbound(cookie):
    log_user_id = cookie["user_id"]
    company_id = company_ut.add_company_2(log_user_id, "woohoo corp", 5)
    print(f"company_id is: {company_id}")
    response = company_ut.delete_company(cookie, company_id)
    outbound = get_recent_outbound(cookie)[0]

    print(f"company outbound: {outbound}")
    outbound_table_name = outbound["table_name"]
    outbound_log_user_id = outbound["log_user_id"]
    jdat = json.loads(outbound["json"])
    print(f"jdat: {jdat}")
    # try:
    # jdat = json.load(outbound["json"])
    rv_company_id = jdat["company_id"]
    rv_company_id = int(rv_company_id)
    # except AttributeError:

    # rv_company_id = 0

    if company_id == rv_company_id:
        return {
            "test": "test_delete_company",
            "result": True,
            "table": "company",
            "proc_index_id": [21, 404],
        }

    return {
        "test": "test_delete_company",
        "result": False,
        "table": "company",
        "proc_index_id": [21, 404],
    }

def test_delete_user_ggroup_outbound(cookie):
    user_id = cookie["user_id"]
    p1 = user_ut.get_root_user(user_id)
    c1 = setup_ut.get_root_company()
    a1 = address_ut.get_root_address_id(c1)
    username = user_ut.get_random_available_username(user_id)
    e1 = user_ut.add_user(cookie, a1, 1, 0, "1990/10/10", "fake@fake.fake", 0, "James", "jameson", "whatever", "1122334455", "2020/01/01", "james3")
    g1 = ggroup_ut.add_ggroup(cookie, "test_name", "test_description")
    user_ggroup_ut.add_user_ggroup(cookie, e1, g1)
    response = user_ggroup_ut.delete_user_ggroup(cookie, e1, g1)
    print(f"response: {response}")
    outbound = get_recent_outbound(cookie)[0]
    print(f"outbound: {outbound}")

    jdat = json.loads(outbound["json"])
    rv_e = int(jdat["user_id"])
    rv_g = int(jdat["ggroup_id"])

    user_ut.delete_user(cookie, e1)
    ggroup_ut.delete_ggroup(cookie, g1)

    outbound_table_name = outbound["table_name"]
    outbound_log_user_id = outbound["log_user_id"]
    if e1 == rv_e:
        if g1 == rv_g:
            return {
                "test": "test_delete_user_ggroup",
                "result": True,
                "table": "user_ggroup",
                "proc_index_id": [354, 405],
            }
    return {
        "test": "test_delete_user_ggroup",
        "result": False,
        "table": "user_ggroup",
        "proc_index_id": [354, 405],
    }




def test_delete_ggroup_outbound(cookie):
    ggroup_id = ggroup_ut.add_ggroup(cookie, "name", "description")
    response = ggroup_ut.delete_ggroup(cookie, ggroup_id)
    outbound = get_recent_outbound(cookie)[0]
    try:
        rv_ggroup_id = json.loads(outbound["json"])["ggroup_id"]
    except Exception as e:
        print(f"exception: {e}")
        print(f"data: {outbound}")
        rv_ggroup_id = 0
    if ggroup_id == int(rv_ggroup_id):
        return {
            "test": "test_delete_ggroup",
            "result": True,
            "table": "ggroup",
            "proc_index_id": [356, 413],
        }

    return {
        "test": "test_delete_ggroup",
        "result": False,
        "table": "ggroup",
        "proc_index_id": [356, 413],
    }


def test_delete_ggroup_rrole_outbound(cookie):
    # 100000 is used because the rroles are assigned.
    g1 = ggroup_ut.add_ggroup(cookie, "name", "description")
    rrole_ut.add_rrole("t", 100000, "t", "t")
    ggroup_rrole_ut.add_ggroup_rrole(cookie, g1, 100000)
    response = ggroup_rrole_ut.delete_ggroup_rrole(cookie, g1, 100000)
    outbound = get_recent_outbound(cookie)[0]
    jdat = json.loads(outbound["json"])
    rv_g1 = int(jdat["ggroup_id"])
    rv_rrole_id = int(jdat["rrole_id"])

    ggroup_ut.delete_ggroup(cookie, g1)
    rrole_ut.delete_rrole(100000)

    if g1 == rv_g1:
        if rv_rrole_id == 100000:
            return {
                "test": "test_delete_ggroup_rrole",
                "result": True,
                "table": "ggroup_rrole",
                "proc_index_id": [409, 414],
            }
    return {
        "test": "test_delete_ggroup_rrole",
        "result": False,
        "table": "ggroup_rrole",
        "proc_index_id": [409, 414],
    }





def test_delete_user_outbound(cookie):
    address_id = address_ut.add_address(
        cookie, "timbuktu", 54.43, 87.65, 42, "1239 change street", 33333
    )
    user_id = cookie["user_id"]

    u1 = user_ut.get_random_available_username(user_id)

    user_id = user_ut.add_user(cookie, address_id, 1, 0, "1990/10/10", "fake@fake.fake", 0, "James", "jameson", "whatever", "1122334455", "2020/01/01", u1)

    outbound_id = user_ut.delete_user(cookie, user_id)
    outbounds = get_recent_outbound(cookie)
    user = json.loads(outbounds[1]["json"])
    user_log = json.loads(outbounds[0]["json"])
    try:
        rv_user_id = int(user["user_id"])
    except KeyError:
        print("key error")
        print("Sql missing pk")
        rv_user_id = 0

    address_ut.delete_address(cookie, address_id)
    if user_id == rv_user_id:
        if address_id == int(user_log["address_id"]):
            return {
                "test": "test_delete_user",
                "result": True,
                "table": "user",
                "proc_index_id": [418, 419],
            }
    return {
        "test": "test_delete_user",
        "result": False,
        "table": "user",
        "proc_index_id": [418, 419],
    }



def test_delete_ssession_outbound(cookie):
    # id, ssession_number = ssession_ut.login("christjh1", "gyg123")
    pv_number = 12345
    ssession_id = ssession_ut.add_ssession(cookie, 1, pv_number, 100000, time.time())
    ssession_ut.delete_ssession(cookie, ssession_id)
    outbound = get_recent_outbound(cookie)[0]
    jdat = json.loads(outbound["json"])
    try:
        rv_number = int(jdat["ssession_number"])
        rv_id = int(jdat["ssession_id"])
    except KeyError:
        print("key error")
        print(f"jdat: {jdat}")
        print("id + ssession number")
        rv_number = 0
        rv_id = 0

    if pv_number == rv_number:
        if ssession_id == rv_id:
            return {
                "test": "test_delete_ssession",
                "result": True,
                "table": "ssession",
                "proc_index_id": [402, 420],
            }
    return {
        "test": "test_delete_ssession",
        "result": False,
        "table": "ssession",
        "proc_index_id": [402, 420],
    }


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        try:
            t_table = test["table"]
            t_test = test["test"]
            t_result = test["result"]
            t_proc_index_id = test["proc_index_id"]
            print(
                f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
            )
        except Exception as e:
            print(f"error: {e}\n\n")
            print(f"data: {test}")


def machine(cookie):
    user_id = cookie["user_id"]
    g1 = ggroup_ut.get_jedi_group_id(user_id)
    tests = []
    tests.append(test_delete_address_outbound(cookie))

    tests.append(test_delete_company_outbound(cookie))

    # tests.append(test_delete_user_ggroup_outbound(cookie))

    tests.append(test_delete_user_outbound(cookie))

    tests.append(test_delete_ggroup_outbound(cookie))

    tests.append(test_delete_ggroup_rrole_outbound(cookie))

    tests.append(test_delete_ssession_outbound(cookie))

    return tests


def main():
    cookie = login.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
