DELIMITER //

CREATE OR REPLACE PROCEDURE add_sservice_api(IN pv_system_id int(10)
    , IN pv_deficiency_description varchar(100)
    , IN pv_building_id int(10)
    , IN pv_record_date date
    , IN pv_complete_date date
    , IN pv_recommended_action varchar(100)
    , IN pv_actor varchar(20)
    , IN pv_priority_status_id int(20)
    , IN pv_user_id int(9)
    , OUT rv_service_id int(9)
    )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_sservice(pv_system_id, pv_deficiency_description, pv_building_id, pv_record_date, pv_complete_date, pv_recommended_action, pv_actor, pv_priority_status, pv_user_id, rv_service_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE add_sservice(IN pv_system_id int(10)
    , IN pv_deficiency_description varchar(100)
    , IN pv_building_id int(10)
    , IN pv_record_date date
    , IN pv_complete_date date
    , IN pv_recommended_action varchar(100)
    , IN pv_actor varchar(20)
    , IN pv_priority_status_id int(20)
    , IN pv_user_id int(9)
    , OUT rv_service_id int(9)
    )
BEGIN
    declare lcl_company_id int default 0;
    CALL get_user_company_id(pv_user_id, lcl_company_id);
 
    INSERT INTO sservice 
                (system_id, 
                deficiency_description, 
                building_id, 
                record_date, 
                complete_date, 
                recommended_action, 
                requested_by, 
                priority_status, 
                comments, 
                client_notes, 
                company_id, 
                flag, 
                e_time, 
                user_id) 
    VALUES     (pv_system, 
                pv_deficiency_description, 
                pv_building_id, 
                pv_record_date, 
                pv_complete_date, 
                pv_recommended_action, 
                pv_requested_by, 
                pv_priority_status, 
                pv_comments, 
                pv_client_notes, 
                lcl_company_id, 
                0, 
                Unix_timestamp(Now()), 
                pv_user_id); 

    SELECT MAX(id) into rv_service_id
    from sservice
    where user_id = pv_user_id;
END//

CREATE OR REPLACE PROCEDURE get_sservices_api(IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_sservices(pv_user_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_sservices(IN pv_user_id int(9)
                                        )

BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);

    SELECT distinct sservice.system_id, 
        sservice.deficiency_description, 
        sservice.building_id, 
        sservice.record_date, 
        sservice.complete_date, 
        sservice.recommended_action, 
        sservice.actor, 
        sservice.priority_status_id, 
        sservice.e_time, 
        sservice.flag, 
        sservice.log_user_id,
        d.name priority_status,
        a.name system_name,
        b.name system_type
    FROM   sservice, system a, system_type b, priority_status d 
    WHERE  sservice.company_id = lcl_company_id
    and sservice.priority_status_id = d.id 
    and sservice.system_id = a.id
    and a.system_type_id = b.id; 
END//

CREATE OR REPLACE PROCEDURE get_building_sservices_api(IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        , IN pv_building_id int(9))
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_building_sservices(pv_user_id, pv_building_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_building_sservices(IN pv_user_id int(9)
                                                , IN pv_building_id int(9)
                                        )

BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    SELECT distinct sservice.id, 
        sservice.system_id, 
        sservice.deficiency_description, 
        sservice.building_id, 
        sservice.record_date, 
        sservice.complete_date, 
        sservice.recommended_action, 
        sservice.actor, 
        sservice.priority_status_id, 
        -- service_comment.comment,
        sservice.e_time, 
        sservice.flag, 
        sservice.log_user_id,
        d.name priority_status,
        a.name system_name,
        b.name system_type
    FROM   sservice, service_comment, system a, system_type b, priority_status d 
    WHERE  sservice.company_id = lcl_company_id
    and sservice.priority_status_id = d.id 
    and sservice.system_id = a.id
    and a.system_type_id = b.id
    and service_comment.service_id = sservice.id
    and sservice.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE update_status_api(IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        , IN pv_service_id int(9)
                                        , IN pv_system_id int(9)
                                        , IN pv_deficiency_description varchar(1000)
                                        , IN pv_record_date varchar(30)
                                        , IN pv_complete_date varchar(30)
                                        , IN pv_recommended_action varchar(100)
                                        , IN pv_actor varchar(20)
                                        , IN pv_priority_status_id int(9)
                                        , IN pv_comment varchar(1000)
                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call update_status(pv_service_id, pv_system_id, pv_deficiency_description, pv_record_date, pv_complete_date, pv_recommended_action, pv_actor, pv_priority_status_id, pv_comment);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE update_status(IN pv_service_id int(9)
                                        , IN pv_system_id int(9)
                                        , IN pv_deficiency_description varchar(1000)
                                        , IN pv_record_date varchar(30)
                                        , IN pv_complete_date varchar(30)
                                        , IN pv_recommended_action varchar(100)
                                        , IN pv_actor varchar(20)
                                        , IN pv_new_priority_status_id int(9)
                                        , IN pv_comment varchar(1000)
                                        )
BEGIN
    UPDATE sservice set system_id = pv_system_id, deficiency_description = pv_deficiency_description
    , record_date = pv_record_date, complete_date = pv_complete_date, recommended_action = pv_recommended_action
    , actor = pv_actor, priority_status_id = pv_new_priority_status_id where id = pv_service_id;
    update service_comment set service_comment.comment = pv_comment where service_comment.service_id = pv_service_id;
END//

CREATE OR REPLACE PROCEDURE delete_sservice_api(IN pv_flag int(3)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_sservice_id int(9)
                                            , OUT rv_status_code int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call delete_sservice(pv_flag, pv_user_id, pv_sservice_id, rv_status_code);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE delete_sservice(IN pv_flag int(3)
                                        , IN pv_user_id int(9)
                                        , IN pv_sservice_id int(9)
                                        , OUT rv_status_code int(9)
                                        )
BEGIN
	declare lcl_outbound_id int(9) default 0;
	declare rv_sservice_json varchar(500) default '';
	declare lcl_system_id int(9) default 0;
	declare lcl_deficiency_description varchar(1000) default 0;
	declare lcl_building_id int(9) default 0;
	declare lcl_record_date varchar(30) default '';
	declare lcl_complete_date varchar(30) default 0;
	declare lcl_recommended_action varchar(100) default 0;
	declare lcl_actor varchar(20) default 0;
    declare lcl_priority_status_id int(9) default 0;
    declare lcl_e_time int(13) default 0;
    declare lcl_flag int(3) default 0;
    declare lcl_company_id int(9) default 0;
    declare lcl_log_user_id int(9) default 0;
	declare lcl_system_id_string varchar(40) default '';
    declare lcl_deficiency_description_string varchar(1000) default '';
	declare lcl_building_id_string varchar(40) default '';
	declare lcl_record_date_string varchar(40) default '';
    declare lcl_complete_date_string varchar(40) default '';
    declare lcl_recommended_action_string varchar(100) default '';
	declare lcl_actor_string varchar(40) default '';
    declare lcl_priority_status_id_string varchar(40) default '';
	declare lcl_company_id_string varchar(40) default '';
	declare lcl_e_time_string varchar(40) default '';
	declare lcl_flag_string varchar(40) default '';
    declare lcl_log_user_id_string varchar(40) default '';
	DECLARE EXIT HANDLER FOR 1451
	-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails 
	BEGIN
		select -1 into rv_status_code;
	END;
	select sservice.system_id INTO lcl_system_id FROM sservice WHERE sservice.id = pv_sservice_id;
	select sservice.deficiency_description into lcl_deficiency_description FROM sservice WHERE sservice.id = pv_sservice_id;
	select sservice.building_id into lcl_building_id FROM sservice WHERE sservice.id = pv_sservice_id;
    select sservice.record_date into lcl_record_date FROM sservice WHERE sservice.id = pv_sservice_id;
    select sservice.complete_date into lcl_complete_date FROM sservice WHERE sservice.id = pv_sservice_id;
    select sservice.recommended_action INTO lcl_recommended_action FROM sservice WHERE sservice.id = pv_sservice_id;
    select sservice.actor INTO lcl_actor FROM sservice WHERE sservice.id = pv_sservice_id;
    select sservice.priority_status_id into lcl_priority_status_id from sservice where id = pv_sservice_id;
	select sservice.e_time into lcl_e_time from sservice where sservice.id = pv_sservice_id;
    select sservice.flag into lcl_flag FROM sservice where sservice.id = pv_sservice_id;
    select sservice.company_id into lcl_company_id FROM sservice where sservice.id = pv_sservice_id;
    select sservice.log_user_id INTO lcl_log_user_id FROM sservice WHERE sservice.id = pv_sservice_id;
	call jsonify_int(lcl_system_id, 'system_id', lcl_system_id_string);
	call jsonify_string('deficiency_description', lcl_deficiency_description, lcl_deficiency_description_string);
	call jsonify_int(lcl_building_id, 'building_id', lcl_building_id_string);
	call jsonify_string('record_date', lcl_record_date, lcl_record_date_string);
	call jsonify_string('complete_date', lcl_complete_date, lcl_complete_date_string);
	call jsonify_string('recommended_action', lcl_recommended_action, lcl_recommended_action_string);
	call jsonify_string('actor', lcl_actor, lcl_actor_string);
	call jsonify_int(lcl_priority_status_id, 'priority_status_id', lcl_priority_status_id_string);
	call jsonify_int(lcl_e_time, 'e_time', lcl_e_time_string);
	call jsonify_int(lcl_flag, 'flag', lcl_flag_string);
	call jsonify_int(lcl_company_id, 'company_id', lcl_company_id_string);
	call jsonify_int(lcl_log_user_id, 'log_user_id', lcl_log_user_id_string);	
    select concat("{", lcl_system_id_string, ",", lcl_deficiency_description_string, ",", lcl_building_id_string, ",", lcl_record_date_string, ",", lcl_complete_date_string, ",", lcl_recommended_action_string, ",", lcl_actor_string, ",", lcl_priority_status_id_string, ",", lcl_e_time_string, ",", lcl_flag_string, ",", lcl_company_id_string, ",", lcl_log_user_id_string, "}'") into rv_sservice_json;
    call add_outbound(pv_flag, pv_sservice_id, rv_sservice_json, 'sservice', lcl_outbound_id);
	DELETE FROM sservice where id = pv_sservice_id AND company_id = lcl_company_id;
    select lcl_outbound_id into rv_status_code;
END//

CREATE OR REPLACE PROCEDURE get_sservice_api(IN pv_sservice_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        , OUT rv_sservice_id int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_sservice(pv_sservice_id, pv_user_id, rv_sservice_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_sservice(IN pv_sservice_id int(9)
                                        , IN pv_user_id int(9)
                                        , OUT rv_sservice_id int(9)
                                        )

BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    SELECT sservice.system_id, 
        sservice.deficiency_description, 
        sservice.building_id, 
        sservice.record_date, 
        sservice.complete_date, 
        sservice.recommended_action, 
        sservice.actor, 
        sservice.priority_status_id, 
        sservice.e_time, 
        sservice.flag, 
        sservice.log_user_id 
    FROM   sservice 
    WHERE  company_id = lcl_company_id 
        AND id = pv_sservice_id; 
    SELECT MAX(id) into rv_sservice_id;

END//


CREATE OR REPLACE PROCEDURE get_sservice_complete_date_api(in pv_sservice_id int(9)
                                                        , IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , OUT rv_sservice_date date
                                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_sservice_complete_date(pv_sservice_id, rv_sservice_date);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_sservice_complete_date(IN pv_sservice_id int(9)
                                                    , OUT rv_sservice_date date
                                                    )
BEGIN
    SELECT sservice.complete_date INTO rv_sservice_date FROM sservice where id = pv_sservice_id;
END//

CREATE OR REPLACE PROCEDURE get_building_data_api(IN pv_building_id INT(10)
                                                    , IN pv_user_id INT(10)
                                                    , IN pv_ssession_number INT(10)
                                                    )
BEGIN
    DECLARE lcl_run int default 0;
    call authenitcate(pv_user_id, pv_ssession_number, lcl_run);
    IF lcl_run = 1 THEN
        call get_builiding_data(pv_building_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_building_data(IN pv_building_id INT(10)
                                                , IN pv_user_id INT(10))
BEGIN
    SELECT  a.id 
    , a.system_id 
    , a.deficiency_description 
    , a.building_id 
    , a.record_date 
    , a.complete_date 
    , a.recommended_action 
    , a.actor 
    , a.priority_status_id 
    from sservice a
    where sservice.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE add_sservice_a_api(IN pv_system_id int(10)
    , IN pv_deficiency_description varchar(100)
    , IN pv_building_id int(10)
    , IN pv_recommended_action varchar(100)
    , IN pv_actor varchar(20)
    , IN pv_priority_status_id int(20)
    , IN pv_comment varchar(500)
    , IN pv_user_id int(9)
    , IN pv_ssession_number int(9)
    , OUT rv_service_id int(9)
    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_sservice_a(pv_system_id, pv_deficiency_description, pv_building_id, pv_recommended_action, pv_actor, pv_priority_status_id, pv_comment, pv_user_id, rv_service_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE add_sservice_a(IN pv_system_id int(10)
    , IN pv_deficiency_description varchar(100)
    , IN pv_building_id int(10)
    , IN pv_recommended_action varchar(100)
    , IN pv_actor varchar(20)
    , IN pv_priority_status_id int(20)
    , IN pv_comment varchar(500)
    , IN pv_user_id int(9)
    , OUT rv_service_id int(9)
    )
BEGIN
    declare lcl_company_id int default 0;
    CALL get_user_company_id(pv_user_id, lcl_company_id);
 
    INSERT INTO sservice 
                (system_id, 
                deficiency_description, 
                building_id, 
                record_date,
                recommended_action, 
                actor, 
                priority_status_id, 
                company_id, 
                flag, 
                e_time, 
                log_user_id) 
    VALUES     (pv_system_id, 
                pv_deficiency_description, 
                pv_building_id, 
                CURRENT_TIMESTAMP, 
                pv_recommended_action, 
                pv_actor, 
                pv_priority_status_id,               
                lcl_company_id, 
                0, 
                Unix_timestamp(Now()), 
                pv_user_id); 

    SELECT MAX(id) into rv_service_id
    from sservice
    where log_user_id = pv_user_id;
    INSERT INTO service_comment
                (service_id
                , parent_id
                , comment
                , flag
                , e_time
                , log_user_id)
    VALUES      (rv_service_id
                , 1
                , pv_comment
                , 0
                , UNIX_TIMESTAMP(NOW())
                , pv_user_id);
END//

CREATE OR REPLACE PROCEDURE get_sservice_priority_data_api (IN pv_user_id int(9)
                                                , IN pv_ssession_number int(9)
                                                , IN pv_building_id int(9)
                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_sservice_priority_data(pv_building_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_sservice_priority_data(IN pv_building_id int(9))
BEGIN
    select priority_status.name name, priority_status.description description, count(sservice.priority_status_id) count
    FROM sservice INNER JOIN priority_status ON sservice.priority_status_id = priority_status.id
    where sservice.building_id = pv_building_id
    GROUP BY priority_status.id;
END//