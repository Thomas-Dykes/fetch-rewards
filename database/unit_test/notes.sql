select a.adjusted_usage
    , a.baseline service_month_id
    , a.company_id
    , a.service_month_id
    , CONCAT(b.month,"-",b.year) service_month
from electricity a, service_month b
where a.service_month_id = b.id
and company_id = 1;


call get_electricity_graph_data_api(16, 92293);


select id, uusage, service_month_id from electricity;


SELECT a.id, a.uusage, a.start, a.end, TIMESTAMPDIFF(MONTH,b.month_date, '18-04-1') month, TIMESTAMPDIFF(DAY,b.month_date, '18-04-24') day
from electricity a, service_month b
where a.service_month_id = b.id
and TIMESTAMPDIFF(DAY,b.month_date, '18-04-1') < 365 
and TIMESTAMPDIFF(DAY,b.month_date, '18-04-1') > -8;

SELECT electricity.uusage
FRO