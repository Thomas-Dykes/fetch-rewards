CREATE TABLE IF NOT EXISTS sservice (
    id int(10) PRIMARY KEY AUTO_INCREMENT
    , system_id int(9)
    , deficiency_description varchar(100)
    , building_id int(10)
    , record_date varchar(30)
    , complete_date varchar(30)
    , recommended_action varchar(100)
    , actor varchar(20)
    , priority_status_id int(20)
    , company_id int(10)
    , flag int(9)
    , e_time int(13)
    , log_user_id int(10)
);