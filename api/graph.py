import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config
import numpy as np
sys.path.append("/home/ec2-user/pbc_portal/processing/")
from regressor import piecewise
# from regressor import piecewise_plot


def get_oat_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_oats_electricity_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine(jdat)
    for oat in oats:
        print(f"oat: {oat}")
    return oats


def get_norm_usage_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_norm_usages_electricity_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine(jdat)
    for norm_usage in norm_usages:
        print(f"norm_usage: {norm_usage}")
    return norm_usages

def get_service_month_year_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_service_month_year_electricity_between"
    params = [service_month_id_start, service_month_id_end, user_id, building_id]
    jdat = {"name": proc_name, "params": params}
    month_years = co.machine(jdat)
    for month_year in month_years:
        print(f"month_year: {month_year}")
    return month_years


def get_savings_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_savings_electricity_between"
    params = [service_month_id_start, service_month_id_end, user_id, building_id]
    jdat = {"name": proc_name, "params": params}
    savings = co.machine(jdat)
    for saving in savings:
        print(f"savings: {savings}")
    return savings

def piece_wise(pv_x, pv_y):
    model = piecewise(pv_x, pv_y)
    return model


def process_model(model):
    segments = model.segments
    count = 0
    rv_model = {}
    for segment in segments:
        count += 1
        start = segment[0]
        end = segment[1]
        coef = segment[2]
        y_int = coef[0]
        slope = coef[1]
        rv_model[count] = {
            "id": count,
            "start": start,
            "end": end,
            "y_int": y_int,
            "slope": slope,
        }
    return rv_model


def get_equation(pv_x, pv_y):
    return process_model(piece_wise(pv_x, pv_y))



def line(equation):
    """
    seg 1
        x=0, y=xint
        x=end, y=mx+b
    """
    start = equation["start"]
    stop = equation["end"]
    y_int = equation["y_int"]
    slope = equation["slope"]

    x0 = start
    y0 = (slope * x0) + y_int

    x1 = stop
    y1 = (slope * x1) + y_int

    rv_x = [x0, x1]
    rv_y = [y0, y1]
    return rv_x, rv_y

# def get_data():
#     pass
#     '''
#     x = np.array([75,82,87,84,80,70,62,50,45,50,63,63], dtype=float)
#     y = np.array([3.69040322687416,4.07187783900577,4.2764372227762,4.30958834794551,3.81079612277365,3.62055782814526,3.53744850963754,3.75184065742284,4.43914058852103,3.9694133857861,3.59005459617158,3.48321125002545])
#     y1 = np.array([3.76,4.07,4.29,4.16,3.98,3.53,3.53,4.02,4.22,4.02,3.49,3.49])

#     first: [45.0, 66.21757618977752] [4.215667549260701, 3.365989063008459] 
#     second: [66.21757618977752, 87.0], [3.3659890630084592, 4.288957509994855]
#     '''


#     dot_a = [{'x':75, 'y':3.69040322687416, 'm':'May-2017'},{'x':82, 'y':4.07187783900577, 'm':'June-2017'}, {'x':87, 'y':4.2764372227762, 'm':'July-2017'}, {'x':84, 'y':4.30958834794551, 'm':'August-2017'}, {'x':80, 'y':3.81079612277365, 'm':'September-2017'}, {'x':70, 'y':3.62055782814526, 'm':'October-2017'},{'x': 62, 'y': 3.53744850963754, 'm': 'November-2017'}, {'x': 50, 'y': 3.75184065742284, 'm': 'December-2017'}, {'x': 45, 'y': 4.43914058852103, 'm': 'January-2018'}, {'x': 50, 'y': 3.9694133857861, 'm': 'February-2018'}, {'x': 63, 'y': 3.59005459617158, 'm':'March-2018'}, {'x': 63, 'y': 3.48321125002545, 'm':'April-2018'}] 
#     dot_b = [{'x':80, 'y':3.44365141598696, 'c':10906.752, 'm': 'May-2018'}, {'x':87, 'y':3.83038537276358, 'c':8924.10, 'm': 'June-2018'}, {'x': 90, 'y': 4.08787082863646, 'c': 6427.31, 'm': 'July-2018'}, {'x': 87, 'y': 3.90037580316291, 'c': 6023.33, 'm': 'August-2018'},{'x': 80, 'y': 3.82639444116005, 'c': 2235.73, 'm': 'September-2018'}, {'x': 68, 'y': 3.43659908869639, 'c': 97.79, 'm': 'October-2018'},{'x': 54, 'y': 3.29273161196869, 'c': 5545.47, 'm': 'November-2019'}, {'x': 50, 'y': 3.32045509413986, 'c': 69193.72, 'm': 'December-2019'},{'x': 47, 'y': 3.41027752370783, 'c': 5530.29, 'm': 'January-2020'}, {'x': 52, 'y': 3.38181034156784, 'c': 2740.67, 'm': 'February-2020'},{'x': 54, 'y': 3.17085971213132, 'c': 2334.27, 'm': "March-2020"}, {'x':64.77, 'y': 3.41027752370783, 'c': 632.215, 'm': "April-2020"}]

#     line_a = [{'x': 45.0, 'y': 4.215667549260701}, {'x': 66.21757618977752, 'y': 3.365989063008459}]

#     line_b = [{'x': 66.21757618977752, 'y': 3.365989063008459}, { 'x':87.0, 'y': 4.288957509994855}]
#     return {'dot_a': dot_a, 'dot_b': dot_b, 'line_a': line_a, 'line_b': line_b}


def get_data(user_id, service_month_id_build_start, service_month_id_build_end, service_month_id_apply_start, service_month_id_apply_end, building_id):
    x_list_pre = get_oat_between(user_id, building_id, service_month_id_build_start, service_month_id_build_end)
    lcl_x1_return = []
    for row in x_list_pre:
        lcl_x1_return.append(float(row["oat"]))

    x_list_post = get_oat_between(user_id, building_id, service_month_id_apply_start, service_month_id_apply_end)
    lcl_x2_return = []
    for row2 in x_list_post:
        lcl_x2_return.append(float(row2["oat"]))
        
    y_list_pre = get_norm_usage_between(user_id, building_id, service_month_id_build_start, service_month_id_build_end)
    lcl_y1_return = []
    for row3 in y_list_pre:
        lcl_y1_return.append(float(row3["norm_usage"]))
    
    y_list_post = get_norm_usage_between(user_id, building_id, service_month_id_apply_start, service_month_id_apply_end)
    lcl_y2_return = []
    for row4 in y_list_post:
        lcl_y2_return.append(float(row4["norm_usage"]))

    service_month_year_pre = get_service_month_year_between(user_id, building_id, service_month_id_build_start, service_month_id_build_end)
    lcl_s1_return = []
    for row5 in service_month_year_pre:
        lcl_s1_return.append(row5["month"] + "-" + row5["year"])
    service_month_year_post = get_service_month_year_between(user_id, building_id, service_month_id_apply_start, service_month_id_apply_end)
    lcl_s2_return = []
    for row6 in service_month_year_post:
        lcl_s2_return.append(row6["month"] + "-" + row6["year"])

    savings_post = get_savings_between(user_id, building_id, service_month_id_apply_start, service_month_id_apply_end)
    lcl_save_return = []
    for row7 in savings_post:
        lcl_save_return.append(float(row7["monthly_savings"]))

    print(f"lcl_x1_return: {lcl_x1_return}")
    print(len(lcl_x1_return))
    print(f"lcl_x2_return: {lcl_x2_return}")
    print(len(lcl_x2_return))
    print(f"lcl_y1_return: {lcl_y1_return}")
    print(len(lcl_y1_return))
    print(f"lcl_y2_return: {lcl_y2_return}")
    print(len(lcl_y2_return))
    print(f"lcl_s1_return: {lcl_s1_return}")
    print(len(lcl_s1_return))
    print(f"lcl_s2_return: {lcl_s2_return}")
    print(lcl_s2_return)
    print(f"lcl_save_return: {lcl_save_return}")
    print(len(lcl_save_return))
    dot_a = []
    for i in range(len(lcl_x1_return)):
        pre_base_dict = {'x': 0, 'y': 0, 'm': ''}
        pre_base_dict['x'] = lcl_x1_return[i]
        pre_base_dict['y'] = lcl_y1_return[i]
        pre_base_dict['m'] = lcl_s1_return[i]
        dot_a.append(pre_base_dict)
    

    dot_b = []
    for j in range(len(lcl_x2_return)):
        post_base_dict = {'x': 0, 'y': 0, 'c': 0, 'm': ''}
        post_base_dict['x'] = lcl_x2_return[j]
        post_base_dict['y'] = lcl_y2_return[j]
        post_base_dict['m'] = lcl_s2_return[j]
        post_base_dict['c'] = lcl_save_return[j]
        dot_b.append(post_base_dict)


    model = get_equation(lcl_x1_return, lcl_y1_return)

    first = model[1]
    second = model[2]

    midpoint = (first["y_int"] - second["y_int"]) / (second["slope"] - first["slope"])

    # if (midpoint > second["end"]):
        # second[]

    second["start"] = midpoint
    print(f"midpoint: {midpoint}")

    print(f"first: {first}")
    print(f"second: {second}")

    # second["start"] = first["end"]

    first_line = line(first)
    second_line = line(second)

    line_a = []
    line_b = []
    for k in range(2):
        first_line_dict = {'x': 0, 'y': 0}
        first_line_dict['x'] = first_line[0][k]
        first_line_dict['y'] = first_line[1][k]
        line_a.append(first_line_dict)
        second_line_dict = {'x': 0, 'y': 0}
        second_line_dict['x'] = second_line[0][k]
        second_line_dict['y'] = second_line[1][k]
        line_b.append(second_line_dict)

    print(f"dot_a: {dot_a}")
    print(f"dot_b: {dot_b}")
    print(f"line_a: {line_a}")
    print(f"line_b: {line_b}")
    if(midpoint > second["end"]):
        line_b[0]['x'] = line_a[1]['x']
        line_b[0]['y'] = line_a[1]['y']

    return {'dot_a': dot_a, 'dot_b': dot_b, 'line_a': line_a, 'line_b': line_b}

# service_month_id_service = 220
# user_id = 6
# building_id = 15
if __name__ == "__main__":
    data = []
    user_id = 1
    building_id = 15
    # ssession_number = 6
    # period_start = '2017/04/01'
    # period_end = '2019/10/01'
    # model_id = 55

    get_data(user_id, 567, 578, 579, 596, building_id)

