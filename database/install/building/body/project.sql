DELIMITER //

CREATE OR REPLACE PROCEDURE add_project_api(IN pv_name varchar(30)
                                                , IN pv_description varchar(100)
                                                , IN pv_pre_period_end_id int(9)
                                                , IN pv_post_period_start_id int(9)
                                                , In pv_post_period_end_id int(9)
                                                , IN pv_user_id int(10)
                                                , IN pv_ssession_number int(9)
                                                , OUT rv_project_id int(10)
                                                )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_project(pv_name, pv_description, pv_pre_period_end_id, pv_post_period_start_id, pv_post_period_end_id, pv_user_id, rv_project_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE add_project(IN pv_name varchar(30)
                                                , IN pv_description varchar(100)
                                                , IN pv_pre_period_end_id int(9)
                                                , IN pv_post_period_start_id int(9)
                                                , IN pv_post_period_end_id int(9)
                                                , IN pv_user_id int(10)
                                                , OUT rv_project_id int(10)
                                                )
BEGIN
    set rv_project_id = -6;
    INSERT INTO project 
                (name, 
                description, 
                pre_period_end_id,
                post_period_start_id,
                post_period_end_id,
                flag, 
                e_time, 
                log_user_id) 
    VALUES      (pv_name, 
                pv_description, 
                pv_pre_period_end_id,
                pv_post_period_start_id,
                pv_post_period_end_id,
                0, 
                Unix_timestamp(Now()), 
                pv_user_id); 

    SELECT MAX(id)
    FROM project INTO rv_project_id;
END//

CREATE OR REPLACE PROCEDURE update_project_api(IN pv_name varchar(30)
                                                , IN pv_description varchar(100)
                                                , IN pv_pre_period_end_id int(9)
                                                , IN pv_post_period_start_id int(9)
                                                , IN pv_post_period_end_id int(9)
                                                , IN pv_user_id int(10)
                                                , IN pv_ssession_number int(9)
                                                , IN pv_project_id int(10)
                                                )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call update_project(pv_name, pv_description, pv_pre_period_end_id, pv_post_period_start_id, pv_post_period_end_id, pv_project_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE update_project(IN pv_name varchar(30)
                                                , IN pv_description varchar(100)
                                                , IN pv_pre_period_end_id int(9)
                                                , IN pv_post_period_start_id int(9)
                                                , IN pv_post_period_end_id int(9)
                                                , IN pv_project_id int(10)
                                                )
BEGIN
    update project SET 
    name = pv_name,
    description = pv_description,
    pre_period_end_id = pv_pre_period_end_id,
    post_period_start_id = pv_post_period_start_id,
    post_period_end_id = pv_post_period_end_id
    WHERE id = pv_project_id;
END//

CREATE OR REPLACE PROCEDURE get_projects_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_projects(pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_projects(IN pv_user_id int(9)
                                                )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    SELECT project.id, project.name, project.description
    , project.pre_period_end_id, project.post_period_start_id, project.post_period_end_id
    , project.e_time, project.flag, project.log_user_id
    FROM project;
END//

CREATE OR REPLACE PROCEDURE get_project_api(IN pv_project_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_project(pv_project_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_project(IN pv_project_id int(9)
                                                , IN pv_user_id int(9)
                                                )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    SELECT project.id, project.name, project.description
    , project.pre_period_start_id, project.post_period_start_id, project.post_period_end_id
    , project.e_time, project.flag, project.log_user_id
    FROM project where company_id = lcl_company_id AND id = pv_project_id;
END//