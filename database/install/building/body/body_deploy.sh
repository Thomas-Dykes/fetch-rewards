set -exou pipefail

echo "begin body deploy"

echo "set variables"
DB_USER=$2
HOST=$1
DB=$3
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/building.sql || echo "building install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/building_category.sql || echo "building_category install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/rrole.sql || echo "rrole install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/electricity.sql || echo "electricity install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/gas.sql || echo "gas install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/model.sql || echo "model install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/priority_status.sql || echo "priority_status install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/service_comment.sql || echo "service_comment install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/service_month.sql || echo "service_month install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/sservice.sql || echo "sservice install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/system_type.sql || echo "system_type install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/system.sql || echo "system install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/body/eui.sql || echo "eui install failed"