DELIMITER //
#309
CREATE OR REPLACE PROCEDURE jsonify_string(IN pv_name varchar(100)
					, IN pv_string varchar(100)
					, OUT rv_string varchar(300)
					)
BEGIN
	SELECT CONCAT("\"", pv_name,"\"",":","\"",pv_string, "\"") INTO rv_string;
END//

#308
CREATE OR REPLACE PROCEDURE jsonify_int(IN pv_int int(9)
					, IN pv_name varchar(100)
					, OUT rv_string varchar(300)
					)
BEGIN
	SELECT CONCAT("\"", pv_name,"\"",":","\"",pv_int, "\"") INTO rv_string;
END//

CREATE OR REPLACE PROCEDURE jsonify_decimal(IN pv_decimal decimal(20,3)
					, IN pv_name varchar(100)
					, OUT rv_string varchar(300)
					)
BEGIN 
	SELECT CONCAT("\"", pv_name,"\"",":","\"",pv_decimal, "\"") INTO rv_string;
END//

