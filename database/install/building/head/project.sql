
#add_project_api
call add_proc_index(
133
, 'checks permission, then runs add_project'
, 0
, '{"pv_name": "varchar(30)", "pv_description": "varchar(30)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_priority_id": "int(9)"}'
, '["project"]'
, 'add_project_api'
, 1
);

#get_projects_api
call add_proc_index(
134
, 'checks permissions, then runs get_projectes'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["project"]'
, 'get_projects_api'
, 1
);

#get_project_api
call add_proc_index(
135
, 'checks permissions, then runs get_project'
, 0
, '{"pv_project_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["project"]'
, 'get_project_api'
, 1
);

#update_project_api
call add_proc_index(
136
, 'checks permission, then runs update_project'
, 0
, '{"pv_name": "int(9)", "pv_description": "int(9)", "pv_pre_period_end_id": "int(9)", "pv_post_period_start_id": "int(9)", "pv_post_period_end_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["project"]'
, 'update_project_api'
, 1
);
#END