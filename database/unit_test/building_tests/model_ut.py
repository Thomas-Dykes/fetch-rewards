import time, decimal, sys, unittest

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import login
import connector as co
import pandas as pd
import config


def add_model(
    cookie,
    building_id,
    name,
    build_service_month_id_end,
    apply_service_month_id_start,
    apply_service_month_id_end,
    slope_heating_e,
    slope_cooling_e,
    y_int_heating_e,
    y_int_cooling_e,
    threshold_e,
    model_price_e,
    slope_heating_g,
    slope_cooling_g,
    y_int_cooling_g,
    y_int_heating_g,
    threshold_g,
    model_price_g,
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_model_api"
    params = [
        building_id,
        name,
        build_service_month_id_end,
        apply_service_month_id_start,
        apply_service_month_id_end,
        slope_heating_e,
        slope_cooling_e,
        y_int_heating_e,
        y_int_cooling_e,
        threshold_e,
        model_price_e,
        slope_heating_g,
        slope_cooling_g,
        y_int_heating_g,
        y_int_cooling_g,
        threshold_g,
        model_price_g,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    model_id = co.machine_add(jdat)
    return model_id


def get_electric_model(cookie, model_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_electric_model_api"
    params = [model_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    model = co.machine(jdat)[0]
    print(f"model : {model}")
    return model


def get_gas_model(cookie, model_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_gas_model_api"
    params = [model_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    model = co.machine(jdat)[0]
    print(f"model : {model}")
    return model


def get_gas_models(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_gas_models_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    models = co.machine(jdat)
    return models


def get_electric_models(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_electric_models_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    models = co.machine(jdat)
    return models


# Old Procddures

# def get_models(cookie):
#     user_id = cookie["user_id"]
#     ssession_number = cookie["ssession_number"]
#     proc_name = "get_models_api"
#     params = [user_id, ssession_number]
#     jdat = {"name": proc_name, "params": params}
#     models = co.machine(jdat)
#     for model in models:
#         print(f"model: {model}")
#     return models


# def get_model(cookie, model_id):
#     user_id = cookie["user_id"]
#     ssession_number = cookie["ssession_number"]
#     proc_name = "get_model_api"
#     params = [model_id, user_id, ssession_number]
#     jdat = {"name": proc_name, "params": params}
#     model = co.machine(jdat)[0]
#     print(f"model: {model}")
#     return model


class model_test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.model_id_one = None
        cls.model_id_two = None

    def test_add_1(self):
        entered_data = {
            "building_id": 1,
            "name": "Test1",
            "build_service_month_id_end": 1,
            "apply_service_month_id_start": 1,
            "apply_service_month_id_end": 1,
            "slope_heating_e": 1,
            "slope_cooling_e": 1,
            "y_int_heating_e": 1,
            "y_int_cooling_e": 1,
            "threshold_e": 1,
            "model_price_e": 1,
            "slope_heating_g": 2,
            "slope_cooling_g": 2,
            "y_int_heating_g": 2,
            "y_int_cooling_g": 2,
            "threshold_g": 2,
            "model_price_g": 2,
        }
        try:
            model_id = add_model(
                self.cookie,
                entered_data["building_id"],
                entered_data["name"],
                entered_data["build_service_month_id_end"],
                entered_data["apply_service_month_id_start"],
                entered_data["apply_service_month_id_end"],
                entered_data["slope_heating_e"],
                entered_data["slope_cooling_e"],
                entered_data["y_int_heating_e"],
                entered_data["y_int_cooling_e"],
                entered_data["threshold_e"],
                entered_data["model_price_e"],
                entered_data["slope_heating_g"],
                entered_data["slope_cooling_g"],
                entered_data["y_int_heating_g"],
                entered_data["y_int_cooling_g"],
                entered_data["threshold_g"],
                entered_data["model_price_g"],
            )
            electricity_keys = [
                ("building_id", "building_id"),
                ("name", "name"),
                ("build_service_month_id_end", "build_service_month_id_end"),
                ("apply_service_month_id_start", "apply_service_month_id_start"),
                ("apply_service_month_id_end", "apply_service_month_id_end"),
                ("slope_heating_e", "slope_heating"),
                ("slope_cooling_e", "slope_cooling"),
                ("y_int_heating_e", "y_int_heating"),
                ("y_int_cooling_e", "y_int_cooling"),
                ("threshold_e", "threshold"),
                ("model_price_e", "dollar_per_kwh"),
            ]
            time.sleep(0.1)
            gas_keys = [
                ("building_id", "building_id"),
                ("name", "name"),
                ("build_service_month_id_end", "build_service_month_id_end"),
                ("apply_service_month_id_start", "apply_service_month_id_start"),
                ("apply_service_month_id_end", "apply_service_month_id_end"),
                ("slope_heating_g", "slope_heating"),
                ("slope_cooling_g", "slope_cooling"),
                ("y_int_heating_g", "y_int_heating"),
                ("y_int_cooling_g", "y_int_cooling"),
                ("threshold_g", "threshold"),
                ("model_price_g", "dollar_per_mmbtu"),
            ]

            model_data = get_gas_model(self.cookie, model_id)
            for key in gas_keys:
                self.assertEqual(
                    entered_data[key[0]], model_data[key[1]], f"{key} Should be same"
                )

            model_data = get_electric_model(self.cookie, model_id)
            for key in electricity_keys:
                self.assertEqual(
                    entered_data[key[0]], model_data[key[1]], f"{key} Should be same"
                )

            self.test_name.append("test_add_1")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_model_api", "get_electricity_model_api,get_gas_model_api"]
            )
            self.__class__.model_id_one = model_id
        except Exception as e:
            self.test_name.append("test_add_1")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_model_api", "get_electricity_model_api", "get_gas_model_api"]
            )
            raise Exception(repr(e))

    def test_add_2(self):
        entered_data = {
            "building_id": 1,
            "name": "Test1",
            "build_service_month_id_end": 1,
            "apply_service_month_id_start": 1,
            "apply_service_month_id_end": 1,
            "slope_heating_e": 3,
            "slope_cooling_e": 3,
            "y_int_heating_e": 3,
            "y_int_cooling_e": 3,
            "threshold_e": 3,
            "model_price_e": 3,
            "slope_heating_g": 4,
            "slope_cooling_g": 4,
            "y_int_heating_g": 4,
            "y_int_cooling_g": 4,
            "threshold_g": 4,
            "model_price_g": 4,
        }

        try:
            model_id = add_model(
                self.cookie,
                entered_data["building_id"],
                entered_data["name"],
                entered_data["build_service_month_id_end"],
                entered_data["apply_service_month_id_start"],
                entered_data["apply_service_month_id_end"],
                entered_data["slope_heating_e"],
                entered_data["slope_cooling_e"],
                entered_data["y_int_heating_e"],
                entered_data["y_int_cooling_e"],
                entered_data["threshold_e"],
                entered_data["model_price_e"],
                entered_data["slope_heating_g"],
                entered_data["slope_cooling_g"],
                entered_data["y_int_heating_g"],
                entered_data["y_int_cooling_g"],
                entered_data["threshold_g"],
                entered_data["model_price_g"],
            )
            electricity_keys = [
                ("building_id", "building_id"),
                ("name", "name"),
                ("build_service_month_id_end", "build_service_month_id_end"),
                ("apply_service_month_id_start", "apply_service_month_id_start"),
                ("apply_service_month_id_end", "apply_service_month_id_end"),
                ("slope_heating_e", "slope_heating"),
                ("slope_cooling_e", "slope_cooling"),
                ("y_int_heating_e", "y_int_heating"),
                ("y_int_cooling_e", "y_int_cooling"),
                ("threshold_e", "threshold"),
                ("model_price_e", "dollar_per_kwh"),
            ]
            time.sleep(0.1)
            gas_keys = [
                ("building_id", "building_id"),
                ("name", "name"),
                ("build_service_month_id_end", "build_service_month_id_end"),
                ("apply_service_month_id_start", "apply_service_month_id_start"),
                ("apply_service_month_id_end", "apply_service_month_id_end"),
                ("slope_heating_g", "slope_heating"),
                ("slope_cooling_g", "slope_cooling"),
                ("y_int_heating_g", "y_int_heating"),
                ("y_int_cooling_g", "y_int_cooling"),
                ("threshold_g", "threshold"),
                ("model_price_g", "dollar_per_mmbtu"),
            ]

            model_data = get_gas_model(self.cookie, model_id)
            for key in gas_keys:
                self.assertEqual(
                    entered_data[key[0]], model_data[key[1]], f"{key} Should be same"
                )

            model_data = get_electric_model(self.cookie, model_id)
            for key in electricity_keys:
                self.assertEqual(
                    entered_data[key[0]], model_data[key[1]], f"{key} Should be same"
                )

            self.test_name.append("test_add_2")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_model_api", "get_electricity_model_api,get_gas_model_api"]
            )
            self.__class__.model_id_two = model_id
        except Exception as e:
            self.test_name.append("test_add_2")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_model_api", "get_electricity_model_api", "get_gas_model_api"]
            )
            raise Exception(repr(e))

    def test_get_all(self):
        gas_keys = [
            "building_id",
            "name",
            "build_service_month_id_end",
            "apply_service_month_id_start",
            "apply_service_month_id_end",
            "slope_heating",
            "slope_cooling",
            "y_int_heating",
            "y_int_cooling",
            "threshold",
            "dollar_per_mmbtu",
        ]
        electric_keys = [
            "building_id",
            "name",
            "build_service_month_id_end",
            "apply_service_month_id_start",
            "apply_service_month_id_end",
            "slope_heating",
            "slope_cooling",
            "y_int_heating",
            "y_int_cooling",
            "threshold",
            "dollar_per_kwh",
        ]
        try:
            for item in get_gas_models(self.cookie):
                if item["id"] == self.model_id_one:
                    gas_model_one = item
                if item["id"] == self.model_id_two:
                    gas_model_two = item

            for item in get_electric_models(self.cookie):
                if item["id"] == self.model_id_one:
                    electric_model_one = item
                if item["id"] == self.model_id_two:
                    electric_model_two = item

            gas_model_1_by_get = get_gas_model(self.cookie, self.model_id_one)
            gas_model_2_by_get = get_gas_model(self.cookie, self.model_id_two)

            electric_model_1_by_get = get_electric_model(self.cookie, self.model_id_one)
            electric_model_2_by_get = get_electric_model(self.cookie, self.model_id_two)

            for key in gas_keys:
                self.assertEqual(
                    str(gas_model_one[key]),
                    str(gas_model_1_by_get[key]),
                    f"{key} Should be same",
                )
                self.assertEqual(
                    str(gas_model_two[key]),
                    str(gas_model_2_by_get[key]),
                    f"{key} should be same",
                )

            for key in electric_keys:
                self.assertEqual(
                    str(electric_model_one[key]),
                    str(electric_model_1_by_get[key]),
                    f"{key} Should be same",
                )
                self.assertEqual(
                    str(electric_model_two[key]),
                    str(electric_model_2_by_get[key]),
                    f"{key} should be same",
                )

            self.test_name.append("test_gets")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                [
                    "get_electric_models_api",
                    "get_electric_model_api",
                    "get_gas_models_api",
                    "get_gas_model_api",
                ]
            )
        except Exception as e:
            self.test_name.append("test_gets")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                [
                    "get_electric_models_api",
                    "get_electric_model_api",
                    "get_gas_models_api",
                    "get_gas_model_api",
                ]
            )
            raise Exception(repr(e))


if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = model_test()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())