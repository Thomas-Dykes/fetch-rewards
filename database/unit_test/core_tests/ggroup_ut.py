import time, decimal, sys
import user_log_ut as pl
import user_ut as pt

import address_ut as address

import user_ut as ee

sys.path.append("/home/ec2-user/pbc_portal/pbc")
# sys.path.append("../../../magyk")
import login
import connector as co
import config


def add_ggroup(cookie, name, description):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_ggroup_api"
    params = [description, "0", name, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    ggroup_id = co.machine_add(jdat)
    print(f"ggroup id: {ggroup_id}")
    return ggroup_id


def get_jedi_group_id(user_id):
    name = "jedi"
    proc_name = "get_ggroup_id"
    params = [user_id, name, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def delete_ggroup(cookie, ggroup_id):
    proc_name = "delete_ggroup_api"
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    params = [0, ggroup_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    # print(f"delete jdat: {jdat}")
    status = co.machine_add(jdat)
    return status


def get_ggroup_count():
    proc_name = "get_ggroup_count"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    g_count = co.machine_add(jdat)
    print(f"ggroup count: {g_count}")
    return g_count


def get_company_ggroup_count(company_id):
    proc_name = "get_company_ggroup_count"
    params = [company_id, 0]
    jdat = {"name": proc_name, "params": params}
    g_count = co.machine_add(jdat)
    print(f"company ggroup count: {g_count}")
    return g_count


def get_ggroups(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_ggroups_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    ggroups = co.machine(jdat)
    for ggroup in ggroups:
        print(f"ggroup: {ggroup}")
    return ggroups


def get_ggroup(cookie, ggroup_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_ggroup_api"
    params = [ggroup_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    ggroup = co.machine(jdat)[0]
    print(f"ggroup: {ggroup}")
    return ggroup


def test_add_1(cookie):
    name = "test name 1"
    description = "test description 1"
    print("add test 1")
    id = add_ggroup(cookie, name, description)
    time.sleep(0.1)
    print(f"id: {id}")
    print(" \n \n ")
    print("get test 1")
    ggroup = get_ggroup(cookie, id)
    lcl_name = ggroup["name"]
    lcl_description = ggroup["description"]
    print(f"lcl_name: {lcl_name}")
    if lcl_name == name:
        if lcl_description == description:
            return (
                id,
                {
                    "test": "test_add_1",
                    "result": True,
                    "table": "ggroup",
                    "proc_index_id": [79, 80, 81, 82],
                },
            )
    return (
        id,
        {
            "test": "test_add_1",
            "result": False,
            "table": "ggroup",
            "proc_index_id": [79, 80, 81, 82],
        },
    )


def test_add_2(cookie):
    name = "test name 2"
    description = "test description 2"
    print("add test 2")
    id = add_ggroup(cookie, name, description)
    time.sleep(0.1)
    print(f"id: {id}")
    print(" \n \n ")
    print("get test 2")
    ggroup = get_ggroup(cookie, id)
    lcl_name = ggroup["name"]
    lcl_description = ggroup["description"]
    print(f"lcl_name: {lcl_name}, lcl_description: {lcl_description}")
    if lcl_name == name:
        if lcl_description == description:
            return (
                id,
                {
                    "test": "test_add_2",
                    "result": True,
                    "table": "ggroup",
                    "proc_index_id": [79, 80, 81, 82],
                },
            )
    return (
        id,
        {
            "test": "test_add_2",
            "result": False,
            "table": "ggroup",
            "proc_index_id": [79, 80, 81, 82],
        },
    )


def get_company_id(user_id):
    return ee.get_user_company(user_id)


def test_gets(cookie):
    user_id = cookie["user_id"]
    company_id = get_company_id(user_id)
    count = get_company_ggroup_count(company_id)
    ggroups = get_ggroups(cookie)
    groups_count = len(ggroups)
    print(f"count: {count}, groups: {groups_count}")
    ggroup_one = ggroups[count - 2]
    ggroup_two = ggroups[count - 1]
    add_1 = {"name": "test name 1", "description": "test description 1"}
    add_2 = {"name": "test name 2", "description": "test description 2"}
    print(f"ggroup one: {ggroup_one}")
    print(f"add one: {add_1}")
    print(f"ggroup_two: {ggroup_two}")
    print(f"add_two: {add_2}")
    if add_1["name"] == ggroup_one["name"]:
        # print('name match')
        if add_1["description"] == ggroup_one["description"]:
            # print('desc match')
            if add_2["name"] == ggroup_two["name"]:
                # print("n2")
                if add_2["description"] == ggroup_two["description"]:
                    # print("d2")
                    if len(ggroups) == count:
                        # print("len correct")
                        return {
                            "test": "test_gets",
                            "result": True,
                            "table": "ggroup",
                            "proc_index_id": [77, 79],
                        }
    return {
        "test": "test_gets",
        "result": False,
        "table": "ggroup",
        "proc_index_id": [77, 79],
    }


def test_delete_foreign_key_duplicate_error(cookie, pv_ggroup_id):
    user_id = cookie["user_id"]
    print("test delete ggroup needs to have proper return")
    proc_name = "delete_ggroup"
    params = [0, pv_ggroup_id, user_id, 0]
    jdat = {"name": proc_name, "params": params}
    status_code = co.machine_add(jdat)
    print(f"status: {status_code}")
    if status_code == -1:
        return {
            "test": "test_foreign_key_duplicate_error",
            "result": True,
            "table": "ggroup",
            "proc_index_id": [356],
        }
    return {
        "test": "test_foreign_key_duplicate_error",
        "result": False,
        "table": "ggroup",
        "proc_index_id": [356],
    }


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        t_table = test["table"]
        t_test = test["test"]
        t_result = test["result"]
        t_proc_index_id = test["proc_index_id"]
        print(
            f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
        )


def machine(cookie):
    tests = []
    print(" \n \n ")
    lcl_id, result = test_add_1(cookie)
    lcl_id2, result2 = test_add_2(cookie)
    tests.append(result)
    tests.append(result2)
    print(" \n \n ")
    tests.append(test_gets(cookie))
    delete_ggroup(cookie, lcl_id)
    delete_ggroup(cookie, lcl_id2)
    #    tests.append(test_delete_foreign_key_duplicate_error(cookie, 6))
    return tests


def main():
    cookie = login.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
