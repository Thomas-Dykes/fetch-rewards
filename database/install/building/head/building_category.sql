
#add_building_category_api
call add_proc_index(
38
, 'checks permissions, then runs add_building_category'
, 0
, '{"pv_name": "int(9)", "pv_description": "varchar(30)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_building_category_id": "int(9)"}'
, '["building_category"]'
, 'add_building_category_api'
, 1
);

#get_building_categories_api
call add_proc_index(
39
, 'checks permissions, then runs get_building_categories'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["building_category"]'
, 'get_building_categories_api'
, 1
);

#get_building_category_api
call add_proc_index(
40
, 'checks permissions, then runs get_building_category'
, 0
, '{"pv_building_category_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["building_category"]'
, 'get_building_category_api'
, 1
);

#END