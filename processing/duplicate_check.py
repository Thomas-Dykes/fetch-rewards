import csv
from scipy import optimize
import numpy as np
import sys
from datetime import datetime
from decimal import *
sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config

def get_electricity_ids(building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_electricity_ids"
    params = [building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    e_ids = co.machine(jdat)
    e_id_return = []
    for e_id in e_ids:
        print(f"e_id: {e_id}")
        e_id_return.append(float(e_id["id"]))
    return e_id_return

def get_duplicate_check(building_id, service_month_id, user_id):
    proc_name = "get_duplicate_check"
    params = [building_id, service_month_id, user_id]
    jdat = {"name": proc_name, "params": params}
    duplicates = co.machine(jdat)
    print(f"duplicates: {duplicates}")
    for duplicate in duplicates:
        duplicate = str(duplicate)
        print(f"duplicate: {duplicate}")
        return duplicate

def main(data, building_id, user_id, ssession_number, service_month_id_start, service_month_id_end):
    count = 0
    match_count = 0

    for row in data:
        for service_month_id in range(service_month_id_start, service_month_id_end):
            print(match_count)
            duplicate = get_duplicate_check(building_id, service_month_id, user_id)
            if(isinstance(duplicate, str)):
                duplicate = duplicate.split(', ')
                duplicate[0] = duplicate[0][1:len(duplicate[0])]
                duplicate[len(duplicate)-1] = duplicate[len(duplicate)-1][1:(len(duplicate[len(duplicate)-1])-1)]
            # print(f"Inside duplicate: {duplicate}")
            # print(duplicate[0])
            # print(duplicate[0][(len(duplicate[0])-11):len(duplicate[0])])
            # print(datetime.strptime(duplicate[0][(len(duplicate[0])-11):(len(duplicate[0])-1)], "%Y-%m-%d"))
            # print(datetime.strptime(row[0], "%Y/%m/%d"))
            # print(f"row_0: {row[0]}")
            if datetime.strptime(row[0], "%Y/%m/%d") == datetime.strptime(duplicate[0][(len(duplicate[0])-11):(len(duplicate[0])-1)], "%Y-%m-%d"):
                print("START MATCH")
                count = count + 1
            if datetime.strptime(row[1], " %Y/%m/%d") == datetime.strptime(duplicate[1][(len(duplicate[1])-11):(len(duplicate[1])-1)], "%Y-%m-%d"):
                print("END MATCH")
                count = count + 1
            # print(row[2][1:len(row[2])])
            # print(type(row[2]))
            # print(type(duplicate[2]))
            # print(duplicate[2][(len(duplicate[2])-8):(len(duplicate[2])-1)])
            if float(row[2][1:len(row[2])]) ==  float(duplicate[2][(len(duplicate[2])-8):(len(duplicate[2])-1)]):
                print("USAGE MATCH")
                count = count + 1
            # print(float(row[3][1:len(row[3])]))
            # print(duplicate[3][(len(duplicate[3])-6):(len(duplicate[3])-1)])
            if float(row[3][1:len(row[3])]) == float(duplicate[3][(len(duplicate[3])-6):(len(duplicate[3])-1)]):
                print("ADJUSTMENT MATCH")
                count = count + 1
            print(row)
            # print(int(round(float(row[5][1:len(row[5])]))))
            # print(int(float(duplicate[4][(len(duplicate[4])-7):(len(duplicate[4])-1)])))
            if int(round(float(row[5][1:len(row[5])]))) == int(float(duplicate[4][(len(duplicate[4])-7):(len(duplicate[4])-1)])):
                count = count + 1
                print("OAT MATCH")
            # print(match_count)
            if count == 5:
                match_count = match_count + 1
                print(match_count)
            count = 0
            print(f"MATCH COUNT START: {row[0]}")
            print(match_count)
    print(match_count)


if __name__ == "__main__":
    data = []
    user_id = 6
    building_id = 15
    ssession_number = 6
    service_month_id_start = 209
    service_month_id_end = 238
    service_month_id_service = 221

    with open('electricity_sample_hospital.csv')as f:
        readCSV = csv.reader(f, delimiter=',')
        next(readCSV, None)
        for row in readCSV:
            data.append(row)

    main(data, building_id, user_id, ssession_number, service_month_id_start, service_month_id_end)