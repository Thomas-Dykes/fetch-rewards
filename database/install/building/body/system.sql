DELIMITER //

CREATE OR REPLACE PROCEDURE add_system_api(IN pv_name varchar(30),
                                    IN pv_system_type_id int(9),
                                    IN pv_building_id int(9),
                                    IN pv_user_id int(9),
                                    IN pv_ssession_number int(9),
                                    OUT rv_system_id int(9)
                                    )
BEGIN 
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_system(pv_name, pv_system_type_id, pv_building_id, pv_user_id, rv_system_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE add_system(IN pv_name varchar(30)
                                    , IN pv_system_type_id int(9)
                                    , IN pv_building_id int(9)
                                    , IN pv_user_id int(9)
                                    , OUT rv_system_id int(9)
                                    )
BEGIN
    declare lcl_company_id int default 0;
    CALL get_user_company_id(pv_user_id, lcl_company_id);
    INSERT INTO system(name, system_type_id, building_id, e_time, flag, company_id, log_user_id)
    VALUES(pv_name, pv_system_type_id, pv_building_id, UNIX_TIMESTAMP(NOW()), 0, lcl_company_id, pv_user_id);
    SELECT MAX(id) into rv_system_id
    from system
    where log_user_id = pv_user_id;
END//


CREATE OR REPLACE PROCEDURE get_systems_api(IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_systems(pv_user_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_systems(IN pv_user_id int(9)
                                            )
BEGIN 
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    SELECT system.id, 
        system.name, 
        system.system_type_id, 
        system.building_id, 
        system.e_time, 
        system.flag, 
        system.log_user_id,
        b.name system_type_name 
    FROM   system, system_type b 
    WHERE  company_id = lcl_company_id 
        AND b.id = system.system_type_id; 
END//

CREATE OR REPLACE PROCEDURE get_system_api(IN pv_system_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_system(pv_system_id, pv_user_id);
    END IF; 
END//

CREATE OR REPLACE PROCEDURE get_system(IN pv_system_id int(9)
                                        , IN pv_user_id int(9)
                                            )
BEGIN 
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);

    SELECT system.id, 
        system.NAME, 
        system.system_type_id, 
        system.building_id, 
        system.e_time, 
        system.flag, 
        system.log_user_id,
        b.name system_type_name 
    FROM   system, system_type b 
    WHERE  company_id = lcl_company_id 
        AND id = pv_system_id
        AND b.id = system.system_type_id; 

END//


