import time, decimal, sys, unittest

sys.path.append("/home/ec2-user/pbc_portal/pbc")

# add_service_comment_api
# get_services_comments_api

import login
import pandas as pd
import connector as co


def add_service_comment(cookie, service_id, comment):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_service_comment_api"
    params = [service_id, comment, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    comment_id = co.machine_add(jdat)
    return comment_id


def get_service_comments(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_service_comments_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    service_comments = co.machine(jdat)
    # for service_comment in service_comments:
    #     print(f"service_comment: {service_comment}")
    return service_comments


def get_services_comments(cookie, service_id, comment_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_services_comments"
    params = [service_id, user_id]
    jdat = {"name": proc_name, "params": params}
    service_comment = co.machine(jdat)
    service_comment = list(filter(lambda x: (x["id"] == comment_id), service_comment))
    service_comment = service_comment[0]
    # print(f"service_comment: {service_comment}")
    return service_comment


class serviceCommentTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.comment_id_one = None
        cls.service_id_one = None
        cls.comment_id_two = None
        cls.service_id_two = None

    def test_add_1(self):
        entered_data = {
            "service_id": 131,
            "comment": "Testing service comment new 1",
        }
        try:
            comment_id = add_service_comment(
                self.cookie,
                entered_data["service_id"],
                entered_data["comment"],
            )
            time.sleep(0.1)
            service_comment_data = get_services_comments(
                self.cookie, entered_data["service_id"], comment_id
            )
            for key in entered_data:
                self.assertEqual(
                    entered_data[key],
                    service_comment_data[key],
                    f"{key} should be same",
                )

            self.__class__.comment_id_one = comment_id
            self.__class__.service_id_one = entered_data["service_id"]
            self.test_name.append("test_add_1")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_service_comment_api", "get_service_comments_api"]
            )
        except Exception as e:
            self.test_name.append("test_add_1")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_service_comment_api", "get_services_comments_api"]
            )
            raise Exception(repr(e))

    def test_add_2(self):
        entered_data = {
            "service_id": 132,
            "comment": "Testing service comment 1",
        }
        try:

            comment_id = add_service_comment(
                self.cookie,
                entered_data["service_id"],
                entered_data["comment"],
            )
            time.sleep(0.1)
            service_comment_data = get_services_comments(
                self.cookie, entered_data["service_id"], comment_id
            )
            print(f"\n\n{service_comment_data}\n\n")
            for key in entered_data:
                self.assertEqual(
                    entered_data[key],
                    service_comment_data[key],
                    f"{key} Should be same",
                )
            self.__class__.comment_id_two = comment_id
            self.__class__.service_id_two = entered_data["service_id"]
            self.test_name.append("test_add_2")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_service_comment_api", "get_services_comments_api"]
            )
        except Exception as e:
            self.test_name.append("test_add_2")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_service_comment_api", "get_services_comments_api"]
            )
            raise Exception(repr(e))

    def test_get_all(self):
        service_comment_one = {"service_id": None, "comment": None}
        service_comment_two = {"service_id": None, "comment": None}
        try:
            for item in get_service_comments(self.cookie):
                if item["id"] == self.comment_id_one:
                    service_comment_one = item
                if item["id"] == self.comment_id_two:
                    service_comment_two = item

            service_comment_1_by_get = get_services_comments(
                self.cookie, self.service_id_one, self.comment_id_one
            )
            service_comment_2_by_get = get_services_comments(
                self.cookie, self.service_id_two, self.comment_id_two
            )

            for key in service_comment_one:
                self.assertEqual(
                    service_comment_one[key],
                    service_comment_1_by_get[key],
                    f"{key} Should be same",
                )
                self.assertEqual(
                    service_comment_two[key],
                    service_comment_2_by_get[key],
                    f"{key} should be same",
                )
            self.test_name.append("test_gets")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["get_services_comments_api", "get_service_comments_api"]
            )
        except Exception as e:
            self.test_name.append("test_gets")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["get_services_comments_api", "get_service_comments_api"]
            )


if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = serviceCommentTest()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())