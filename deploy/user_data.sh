#!/bin/bash

#path stuff: https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install-linux.html
#pip3 install --upgrade --force-reinstall setuptools after path. 

set -exou pipefail

echo "set variables"
DB_USER="root"
HOST="localhost"
DB="pbc"
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

#file structure 
sudo mkdir //var/log/pbc/ || echo "business exists"
sudo mkdir //var/log/pbc/api/ || echo "api exists"
sudo mkdir //var/log/pbc/proc_log/ || echo "proc log exists"
sudo chmod 777 //var/log/pbc/ -R || echo "chmod 777 log/pbc/ failed"

#package installs
sudo yum install python3.x86_64 -y || echo "python3.x86_64 failed to install"
sudo yum install python3-devel.x86_64 -y || echo "python3 dev tools failed to install"
sudo yum install gcc-c++ -y || echo "gcc-c++ failed to install" #maybe
sudo yum install git -y || echo "git failed to install"
sudo yum install tmux -y || echo "tmux failed to install"
sudo yum install iotop -y || echo "iotop failed to install"
sudo yum install mailx -y || echo "mailx failed"
sudo pip3 install pipenv || echo "pipenv failed to install"
pip3 install wheel || echo "wheel failed to install"
pip3 install python-dotenv || echo "python-dotenv failed to install"
pip3 install flask || echo "flask failed to install"
pip3 install flask-cors || echo "flask-cors failed to install"
pip3 install mysql-connector==2.1.7 || echo "mysql-connector failed to install"
pip3 install pymysql || echo "pymysql failed to install"
pip3 install boto3 || echo "boto3 failed to install"
pip3 install gunicorn || echo "gunicorn failed to install"
pip3 install piecewise || echo "piecewise failed"
pip3 install numpy || echo "numpy failed"
pip3 install matplotlib || echo "matplotlib failed"
pip3 install scipy || echo "scipy failed"
pip3 install slack || echo "slack failed"
pip3 install slackclient || echo "slack client install failed"
pip3 install requests || echo "request install failed"
sudo amazon-linux-extras install lamp-mariadb10.2-php7.2 php7.2 -y || echo "mariadb failed to install"
sudo yum install httpd mariadb-server -y || echo "mariadb_server failed to install"

sudo systemctl start mariadb || echo "mariadb failed to start"
sudo systemctl enable mariadb || echo "mariadb could not be registeristed to start on start up"

#echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 
mysql -h $HOST -u $DB_USER -e "CREATE DATABASE IF NOT EXISTS "$DB";"

/home/ec2-user/pbc_portal/database/install/magyk_core/core_deploy.sh $HOST $DB_USER $DB
/home/ec2-user/pbc_portal/database/install/building/building_deploy.sh $HOST $DB_USER $DB
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/deploy/svc_user.sql

#install table -> body -> head for section
#install building and maygyk core


#curl http://18.223.188.150:8000/test
