select * from user limit 5;

ALTER TABLE user_log ADD COLUMN tmp_pwd varchar(100);  
ADD pwd_reset INT(5);

select id, name from proc_index order by id DESC LIMIT 10;

test table user_log_kern:


INSERT INTO user_log_kern
(first_name, last_name, email_address, pwd, id)
VALUES ("Kern", "Elder", "kelel@gmail.com", 55555, 15); 

INSERT INTO user_kern
(username,id, user_log_id)
VALUES ("kelder", 10, 15);

SELECT first_name, email_address, id
FROM user_log_kern
ORDER BY id DESC
LIMIT 10;

UPDATE user_log_kern
SET tmp_pwd = Password(round(9999 + 99999*rand())) 
    WHERE first_name="Kern";
UPDATE user_log_kern
SET pwd = tmp_pwd, tmp_pwd = NULL;

test tables are:
user_log_kern
user_kern

select * from user_kern where username='kelder';
select * from user_log_kern where first_name='kelder';

select a.username, b.first_name, b.email_address
from user_kern a, user_log_kern b 
where a.user_log_id = b.id;

call set_tmp_pwd('kelder', 'kern', 'kelel@gmail.com', @out);

select username, first_name from user INNER JOIN user_log
ON user.user_log_id = user_log.id
where username="leonardo";