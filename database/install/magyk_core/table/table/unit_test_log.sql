	CREATE TABLE IF NOT EXISTS unit_test_log (
	id int(10) primary key auto_increment
	, proc_index_id varchar(100)
	, ttable varchar(100)
	, test_group int(10)
	, passed int(10)
	, flag int(3)
	, e_time int(13)
	, log_employee_id int(10)
	);
