#!/bin/bash
set -exou pipefail

echo "set variables"
DB_USER=$2
HOST=$1
DB=$3
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

echo "begin building deploy"
/home/ec2-user/pbc_portal/database/install/building/table/table_deploy.sh $HOST $DB_USER $DB
/home/ec2-user/pbc_portal/database/install/building/body/body_deploy.sh $HOST $DB_USER $DB