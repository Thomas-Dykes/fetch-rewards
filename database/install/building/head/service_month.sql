
#add_service_month_api
call add_proc_index(
140
, 'checks permission, then runs add_service_month'
, 0
, '{"pv_month": "varchar(10)", "pv_year": "varchar(10)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_date": "date"}'
, '{"rv_service_month_id": "int(9)"}'
, '["service_month"]'
, 'add_service_month_api'
, 1
);

#get_past_data_api
call add_proc_index(
141
, 'checks permission, then runs get_past_data'
, 0
, '{"pv_service_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["service_month"]'
, 'get_past_data_api'
, 1
);

#get_service_months_api
call add_proc_index(
142
, 'checks permissions, then runs get_service_months'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["service_month"]'
, 'get_service_months_api'
, 1
);

#get_service_month_api
call add_proc_index(
143
, 'checks permissions, then runs get_service_month'
, 0
, '{"pv_service_month_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["service_month"]'
, 'get_service_month_api'
, 1
);

#get_eligible_service_months_electricity_api

call add_proc_index(
144
, 'checks permissions, then runs get_eligible_service_months_electricity'
, 0
, '{"pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["service_month"]'
, 'get_eligible_service_months_electricity_api'
, 1
);

#get_eligible_service_months_gas_api

call add_proc_index(
145
, 'checks permissions, then runs get_eligible_service_months_gas'
, 0
, '{"pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["service_month"]'
, 'get_eligible_service_months_gas_api'
, 1
);

#get_eligible_service_months_api

call add_proc_index(
146
, 'checks permissions, then runs get_eligible_service_months'
, 0
, '{"pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["service_month"]'
, 'get_eligible_service_months_api'
, 1
);

#get_eligible_post_period_service_months_api

call add_proc_index(
147
, 'checks permissions, then runs get_eligible_post_period_service_months'
, 0
, '{"pv_service_month_id": "int(9)", "pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["service_month"]'
, 'get_eligible_service_months_api'
, 1
);

#get_service_month_date_api

call add_proc_index(
135
, 'checks permissions, then runs get_service_month_date'
, 0
, '{"pv_service_month_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_service_month_date": "varchar(20)"}'
, '{}'
, '["service_month"]'
, 'get_service_month_date_api'
, 1
);

#check_duplicate_api

call add_proc_index(
136
, 'checks permission, then runs check_duplicate'
, 0
, ''
, ''
, '["service_month"]'
, 'check_duplicate_api'
, 1
);



#END