#get_states_api

call add_proc_index(
20
, 'checks authentication, then runs get_states'
, 0
, '{}'
, '{}'
, '["state"]'
, 'get_states_api'
, 1
);

#add_state_api

call add_proc_index(
21
, 'adds a row to the state table'
, 0
, '{"pv_abbreviation":"varchar(2)", "pv_country_id":"int(9)", "pv_name":"varchar(40)"}'
, '[]'
, '["state"]'
, 'add_state_api'
, 1
);


#get_state_api

call add_proc_index(
22
, 'gets a row to the state table'
, 0
, '{"pv_state_id":"int(10)"}'
, '[]'
, '["state"]'
, 'get_state_api'
, 1
);

#get_state_by_name_api

CALL add_proc_index(
23
, 'returns a list of states that has exact same name'
, 0
, '{}'
,'{}'
, '["state"]'
, 'get_state_by_name_api'
, 1
);
