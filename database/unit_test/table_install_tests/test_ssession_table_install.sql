
DELIMITER //

/* get_ssession_count(OUT rv_out INT) */

set @rv_passed = 0;

CREATE OR REPLACE PROCEDURE test_ssession_table_install(IN pv_username varchar(30)
	                                                    ,OUT rv_passed INT(3))
BEGIN
	DECLARE lcl_ssession_id INT default 0;
	DECLARE lcl_start INT default 0;
	DECLARE lcl_middle INT default 0;
	DECLARE lcl_end INT default 0;
	DECLARE lcl_response INT default 0;
	DECLARE lcl_user_id INT default 0;
	SET rv_passed = -6;
	call get_user_id(pv_username, lcl_user_id);
	CALL get_ssession_count(lcl_start);
	CALL add_ssession(0, lcl_user_id, 1800, 11242, 10000000, lcl_user_id, lcl_ssession_id);
	CALL get_ssession_count(lcl_middle);
	CALL delete_ssession(0, lcl_user_id,lcl_ssession_id, lcl_response);
	CALL get_ssession_count(lcl_end);
	IF lcl_start < lcl_middle THEN
		SET rv_passed = -99;
		IF lcl_middle > lcl_end THEN
			SET rv_passed = 1;
		END IF;
	END IF;

END//

call test_ssession_table_install(1, @rv_passed);

select @rv_passed;

call add_proc_index(
265
, 'tests the ssession_table_install procedure'
, 0
, '{}'
, '{"rv_passed": "int(9)"}'
, '["ssession"]'
, 'test_ssession_table_install'
, 0
);

call add_unit_test_log(
0
, 1
, @rv_passed
, 265
, 'ssession'
, 0
, @out
);
