	CREATE TABLE IF NOT EXISTS outbound(
	id int(10) PRIMARY KEY AUTO_INCREMENT
	, table_name varchar(40)
	, json varchar(1000)
	, e_time int(13)
	, flag int(3)
	, company_id int(9)
	, log_user_id int(9)
	, INDEX(id)
	);
