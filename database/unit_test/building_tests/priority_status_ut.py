import time, decimal, sys, unittest
import pandas as pd

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import login
import connector as co
import config


def add_priority_status(cookie, name, description):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_priority_status_api"
    params = [name, description, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    priority_status_id = co.machine_add(jdat)
    return priority_status_id


def get_priority_statuses(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_priority_statuses_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    priority_statuss = co.machine(jdat)
    for priority_status in priority_statuss:
        print(f"priority_status: {priority_status}")
    return priority_statuss


def get_priority_status(cookie, priority_status_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_priority_status_api"
    params = [priority_status_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    priority_status = co.machine(jdat)[0]
    print(f"priority_status: {priority_status}")
    return priority_status


class priorityStatusTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.priority_status_id_one = None
        cls.priority_status_id_two = None

    def test_add_1(self):
        entered_data = {"name": "Testing 1", "description": "Testing priority 1"}
        try:
            priority_status_id = add_priority_status(
                self.cookie, entered_data["name"], entered_data["description"]
            )
            time.sleep(0.1)
            priority_status = get_priority_status(self.cookie, priority_status_id)
            for key in entered_data:
                self.assertEqual(
                    entered_data[key], priority_status[key], f"{key} should be same"
                )
            self.test_name.append("test_add_1")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_priority_status_api", "get_priority_status_api"]
            )
            self.__class__.priority_status_id_one = priority_status_id
        except Exception as e:
            self.test_name.append("test_add_1")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_priority_status_api", "get_priority_status_api"]
            )
            raise Exception(repr(e))

    def test_add_2(self):
        entered_data = {"name": "Testing 2", "description": "Testing priority 2"}
        try:
            priority_status_id = add_priority_status(
                self.cookie, entered_data["name"], entered_data["description"]
            )
            time.sleep(0.1)
            priority_status = get_priority_status(self.cookie, priority_status_id)
            for key in entered_data:
                self.assertEqual(
                    entered_data[key], priority_status[key], f"{key} should be same"
                )
            self.test_name.append("test_add_2")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_priority_status_api", "get_priority_status_api"]
            )
            self.__class__.priority_status_id_two = priority_status_id
        except Exception as e:
            self.test_name.append("test_add_2")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_priority_status_api", "get_priority_status_api"]
            )
            raise Exception(repr(e))

    def test_get_all(self):
        priority_status_one = {"name": None, "description": None}
        priority_status_two = {"name": None, "description": None}
        try:
            for item in get_priority_statuses(self.cookie):
                if item["id"] == self.priority_status_id_one:
                    priority_status_one = item
                if item["id"] == self.priority_status_id_two:
                    priority_status_two = item

            priority_status_1_by_get = get_priority_status(
                self.cookie, self.priority_status_id_one
            )
            priority_status_2_by_get = get_priority_status(
                self.cookie, self.priority_status_id_two
            )

            for key in priority_status_one:
                self.assertEqual(
                    priority_status_one[key],
                    priority_status_1_by_get[key],
                    f"{key} should be same",
                )
                self.assertEqual(
                    priority_status_two[key],
                    priority_status_2_by_get[key],
                    f"{key} should be same",
                )
            self.test_name.append("test_gets")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(["get_priority_statuses_api", "get_priority_status_api"])
        except Exception as e:
            self.test_name.append("test_gets")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(["get_priority_statuses_api", "get_priority_status_api"])
            

if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = priorityStatusTest()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())
