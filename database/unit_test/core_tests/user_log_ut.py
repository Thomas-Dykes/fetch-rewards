import time, decimal, sys

import user_log_ut as pl

import address_ut as a

from random import choice
from string import ascii_uppercase


sys.path.append("/home/ec2-user/pbc_portal/pbc")
# sys.path.append("../../../magyk")
import login
import connector as co
import config

def get_user_log_count():
    proc_name = "get_user_log_count"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    count = co.machine_add(jdat)
    print(f"local count: {count}")
    return count

def get_user_logs(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_user_logs_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    user_logs = co.machine(jdat)
    for user_log in user_logs:
        print(f"user_log: {user_log}")
    return user_logs

def get_user_log(cookie, user_log_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_user_log_api"
    params = [user_id, user_log_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    user_log = co.machine(jdat)[0]
    print(f"user_log: {user_log}")
    return user_log

def add_user_log(cookie, address_id, dob, email_address, first_name, last_name, password, phone_number, start_date):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_user_log_api"
    params = [address_id, dob, email_address, first_name, 0, last_name, user_id, password, phone_number, ssession_number, start_date, 0]
    jdat = {"name": proc_name, "params": params}
    user_log_id = co.machine_add(jdat)
    print(f"user log id: {user_log_id}")
    return user_log_id

def delete_user_log(cookie, user_log_id):
    user_id = cookie["user_id"]
    proc_name = "delete_user_log"
    params = [user_log_id, user_id, 0, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)



def get_user_log_max():
    proc_name = "get_user_log_max"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    pl_max = co.machine_add(jdat)
    return pl_max


def update_user_phone_number(cookie, user_id, phone_number):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "update_user_phone_number_api"
    input(type(phone_number))
    params = [log_user_id, user_id, phone_number, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    co.machine_add(jdat)
    return phone_number


def test_add_1(cookie):
    address_id = a.add_address(
        cookie, "test_town", 22.22, 45.67, 3, "1234 test lane", 12345
    )
    dob = "1990/10/11"
    email_address = "blah@blah.blah"
    first_name = "John"
    last_name = "Doe"
    phone_number = "18889997766"
    password = 'abc123'
    start_date = '2020/01/01'
    print("add test 1")
    id = add_user_log(
        cookie, address_id, dob, email_address, first_name, last_name, password, phone_number, start_date
    )

    time.sleep(0.1)
    print(f"id: {id}")
    print(" \n \n ")
    print("get test 1")
    user_log = get_user_log(cookie, id)
    lcl_address_id = user_log["address_id"]
    lcl_dob = user_log["dob"]
    lcl_email_address = user_log["email_address"]
    lcl_first_name = user_log["first_name"]
    lcl_last_name = user_log["last_name"]
    lcl_phone_number = user_log["phone_number"]
    print(
        f"lcl_address_id: {lcl_address_id}, lcl_dob: {lcl_dob}, lcl_email_addresss: {lcl_email_address}, lcl_first_name: {lcl_first_name}, lcl_last_name: {lcl_last_name}, lcl_phone_number: {lcl_phone_number}"
    )
    if lcl_address_id == address_id:
        if lcl_dob == dob:
            if lcl_email_address == email_address:
                if lcl_first_name == first_name:
                    if lcl_last_name == last_name:
                        if lcl_phone_number == phone_number:
                            return (
                                id,
                                {
                                    "test": "test_add_1",
                                    "result": True,
                                    "table": "user_log",
                                    "proc_index_id": [114, 115, 116, 117],
                                },
                            )
    return (
        id,
        {
            "test": "test_add_1",
            "result": False,
            "table": "user_log",
            "proc_index_id": [114, 115, 116, 117],
        },
    )


def test_add_2(cookie):
    address_id = a.add_address(
        cookie, "test_burgh", 88.80, 76.32, 5, "8766 test lane", 23232
    )
    dob = "1990/10/11"
    email_address = "meh@meh.meh"
    first_name = "Jane"
    last_name = "Green"
    phone_number = "18887776655"
    password = 'abc123'
    start_date = '2020/01/01'
    print("add test 1")
    id = add_user_log(
        cookie, address_id, dob, email_address, first_name, last_name, password, phone_number, start_date
    )
    time.sleep(0.1)
    print(f"id: {id}")
    print(" \n \n ")
    print("get test 1")
    user_log = get_user_log(cookie, id)
    lcl_address_id = user_log["address_id"]
    lcl_dob = user_log["dob"]
    lcl_email_address = user_log["email_address"]
    lcl_first_name = user_log["first_name"]
    lcl_last_name = user_log["last_name"]
    lcl_phone_number = user_log["phone_number"]
    print(
        f"lcl_address_id: {lcl_address_id}, lcl_dob: {lcl_dob}, lcl_email_addresss: {lcl_email_address}, lcl_first_name: {lcl_first_name}, lcl_last_name: {lcl_last_name}, lcl_phone_number: {lcl_phone_number}"
    )
    if lcl_address_id == address_id:
        if lcl_dob == dob:
            if lcl_email_address == email_address:
                if lcl_first_name == first_name:
                    if lcl_last_name == last_name:
                        if lcl_phone_number == phone_number:
                            return (
                                id,
                                {
                                    "test": "test_add_2",
                                    "result": True,
                                    "table": "user_log",
                                    "proc_index_id": [114, 115, 116, 117],
                                },
                            )
    return (
        id,
        {
            "test": "test_add_2",
            "result": False,
            "table": "user_log",
            "proc_index_id": [114, 115, 116, 117],
        },
    )


def test_gets(cookie, a, b):
    # a, b, are two valid ids
    max = get_user_log_max()
    print(max)
    user_logs = get_user_logs(cookie)
    user_log_one = {}
    user_log_two = {}
    for p in get_user_logs(cookie):
        if p["id"] == a:
            user_log_one = p
        if p["id"] == b:
            user_log_two = p

    add_1 = get_user_log(cookie, a)
    add_2 = get_user_log(cookie, b)

    print(f"user_log one: {user_log_one}")
    print(f"add one: {add_1}")
    print(f"user_log_two: {user_log_two}")
    print(f"add_two: {add_2}")
    if add_1["address_id"] == user_log_one["address_id"]:
        # print("match name 1")
        if add_1["dob"] == user_log_one["dob"]:
            # print("match desc 1")
            if add_1["email_address"] == user_log_one["email_address"]:
                if add_1["first_name"] == user_log_one["first_name"]:
                    if add_1["last_name"] == user_log_one["last_name"]:
                        if add_1["phone_number"] == user_log_one["phone_number"]:
                            if add_2["address_id"] == user_log_two["address_id"]:
                                if add_2["dob"] == user_log_two["dob"]:
                                    if (
                                        add_2["email_address"]
                                        == user_log_two["email_address"]
                                    ):
                                        if (
                                            add_2["first_name"]
                                            == user_log_two["first_name"]
                                        ):
                                            if (
                                                add_2["last_name"]
                                                == user_log_two["last_name"]
                                            ):
                                                if (
                                                    add_2["phone_number"]
                                                    == user_log_two["phone_number"]
                                                ):
                                                    #                                                    if len(user_logs) == 2:
                                                    #                                                        print("len correct")
                                                    return {
                                                        "test": "test_gets",
                                                        "result": True,
                                                        "table": "user_log",
                                                        "proc_index_id": [112, 113],
                                                    }
    return {
        "test": "test_gets",
        "result": False,
        "table": "user_log",
        "proc_index_id": [112, 113],
    }


def test_delete(cookie, user_log_id):
    status = delete_user_log(cookie, user_log_id)
    if status == 1:
        return {
            "test": "test_gets",
            "result": True,
            "table": "user_log",
            "proc_index_id": [423],
        }
    return {
        "test": "test_gets",
        "result": False,
        "table": "user_log",
        "proc_index_id": [423],
    }


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        t_table = test["table"]
        t_test = test["test"]
        t_result = test["result"]
        t_proc_index_id = test["proc_index_id"]
        print(
            f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
        )


def machine(cookie):
    tests = []
    print(" \n \n ")
    lcl_id, result = test_add_1(cookie)
    lcl_id2, result2 = test_add_2(cookie)
    tests.append(result)
    tests.append(result2)
    print(" \n \n ")
    tests.append(test_gets(cookie, lcl_id, lcl_id2))
    tests.append(test_delete(cookie, lcl_id))

    delete_user_log(cookie, lcl_id2)

    return tests


def main():
    cookie = login.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
