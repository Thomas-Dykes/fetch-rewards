#this gets the count of whatever table youre working on.
DELIMITER //


CREATE OR REPLACE PROCEDURE test_ggroup_rrole_table_install(IN pv_username varchar(30)
                                                           ,OUT rv_passed int(3))
BEGIN
	DECLARE lcl_rrole_id INT default 0;
	DECLARE lcl_ggroup_id INT default 0;
	DECLARE lcl_start INT default 0;
	DECLARE lcl_middle INT default 0;
	DECLARE lcl_end INT default 0;
	DECLARE lcl_response INT default 0;
	DECLARE lcl_response2 INT default 0;
	DECLARE lcl_user_id INT default 0;
	SELECT MIN(id) INTO lcl_rrole_id FROM rrole;
	CALL get_user_id(pv_username, lcl_user_id);
	CALL add_ggroup('test ggroup rrole', 0, lcl_user_id, 'group rrole', lcl_ggroup_id);
	CALL get_ggroup_rrole_count(lcl_start);
	CALL add_ggroup_rrole(0, lcl_ggroup_id, lcl_user_id, lcl_rrole_id); 
	CALL get_ggroup_rrole_count(lcl_middle);
	CALL delete_ggroup_rrole(0, lcl_ggroup_id, lcl_user_id, lcl_rrole_id, lcl_response);
	CALL get_ggroup_rrole_count(lcl_end);
	SET rv_passed = -6;
	IF lcl_start < lcl_middle THEN
		SET rv_passed = -99;
		IF lcl_middle > lcl_end THEN
			SET rv_passed = 1;
		END IF;
	END IF;

END//

call test_ggroup_rrole_table_install('leonardo',@rv_pass);

select @rv_pass;

call add_proc_index(
257
, 'test ggroup_rrole_table_install'
, 0
, '{}'
, '{"rv_passed": "int(9)"}'
, '["ggroup_rrole"]'
, 'test_ggroup_rrole_table_install'
, 0
);

call add_unit_test_log(
0
, 1
, @rv_pass
, 257
, 'ggroup_rrole'
, 0
, @out
);
