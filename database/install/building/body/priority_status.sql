DELIMITER //

CREATE OR REPLACE PROCEDURE add_priority_status_api(IN pv_name varchar(30)
                                                , IN pv_description varchar(100)
                                                , IN pv_user_id int(10)
                                                , IN pv_ssession_number int(9)
                                                , OUT rv_priority_id int(10)
                                                )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_priority_status(pv_name, pv_description, pv_user_id, rv_priority_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE add_priority_status (IN pv_name varchar(30)
                                                ,IN pv_description varchar(100)
                                                ,IN pv_user_id int(10)
                                                ,OUT rv_priority_id int(10)
                                                )
BEGIN
    set rv_priority_id = -6;
    INSERT INTO priority_status 
                (NAME, 
                description, 
                flag, 
                e_time, 
                log_user_id) 
    VALUES      (pv_name, 
                pv_description, 
                0, 
                Unix_timestamp(Now()), 
                pv_user_id); 

    SELECT MAX(id)
    FROM priority_status INTO rv_priority_id;
END//

CREATE OR REPLACE PROCEDURE get_priority_statuses_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_priority_statuses(pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_priority_statuses(IN pv_user_id int(9)
                                                )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    SELECT priority_status.id, priority_status.name, priority_status.description
    , priority_status.e_time, priority_status.flag, priority_status.log_user_id
    FROM priority_status;
END//

CREATE OR REPLACE PROCEDURE get_priority_status_api(IN pv_priority_status_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_priority_status(pv_priority_status_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_priority_status(IN pv_priority_status_id int(9)
                                                , IN pv_user_id int(9)
                                                )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    SELECT priority_status.id, priority_status.name, priority_status.description
    , priority_status.e_time, priority_status.flag, priority_status.log_user_id
    FROM priority_status where company_id = lcl_company_id AND id = pv_priority_status_id;
END//