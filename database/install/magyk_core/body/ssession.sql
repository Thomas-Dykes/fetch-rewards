DELIMITER //

DELIMITER //
#192
create or replace procedure authenticate_api(IN pv_log_employee_id int(10)
							, IN pv_session_number int(10)
							, OUT rv_authenticated int(1)
							)
	BEGIN
		call authenticate(pv_log_employee_id, pv_session_number, rv_authenticated);
	END//


#301
create or replace procedure authenticate(IN pv_user_id int(10)
							, IN pv_ssession_number int(10)
							, OUT rv_authenticated int(1)
							)
	BEGIN
		declare lcl_endtime int default 0;
		declare lcl_id int default 0;
		SELECT max(id) INTO lcl_id
		FROM ssession
		WHERE ssession_number = pv_ssession_number
		AND log_user_id = pv_user_id;
		SELECT ssession.end_time into lcl_endtime
		FROM ssession
		WHERE ssession.id = lcl_id;
		SELECT IF(lcl_endtime > UNIX_TIMESTAMP(NOW()), 1, 0) into rv_authenticated;
		UPDATE ssession SET ssession.end_time = UNIX_TIMESTAMP(NOW())+10800 where id = lcl_id;
	END//


CREATE OR REPLACE PROCEDURE get_company_ssession_count( IN pv_company_id INT(10)
														,OUT rv_out INT(10))
	BEGIN
		SELECT COUNT(1) into rv_out from ssession where company_id = pv_company_id;
	END//


CREATE OR REPLACE PROCEDURE delete_ssessions()
    BEGIN
        DELETE FROM ssession where ssession.id;
        ALTER TABLE ssession AUTO_INCREMENT=1;
	END//


#336
CREATE OR REPLACE PROCEDURE delete_ssession_api(IN pv_flag int(3)
							, IN pv_user_id int(9)
							, IN pv_ssession_id int(9)
							, IN pv_ssession_number int(9)
							, OUT rv_status_code varchar(500)
						)
	BEGIN
    	declare lcl_auth int default 0;
    	declare lcl_company_id int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		call delete_ssession(pv_flag, pv_user_id, pv_ssession_id, rv_status_code);
		END IF;
	END//


#335
CREATE OR REPLACE PROCEDURE delete_ssession(IN pv_flag int(3)
										, IN pv_user_id INT(10)
										, IN pv_ssession_id int(9)
										, OUT rv_status_code int(9))
	BEGIN
		declare lcl_outbound_id int(9) default 0;
		declare rv_ssession_json varchar(500) default 0;
		declare lcl_company_id int(9) default 0;
		declare lcl_e_time int(13) default 0;
		declare lcl_end_time int(13) default 0;
		declare lcl_flag int(9) default 0;
		declare lcl_user_id int(9) default 0;
		declare lcl_ssession_number int(9) default 0;
		declare lcl_start_time int(13) default 0;
		declare lcl_company_id_string varchar(40) default '';
		declare lcl_e_time_string varchar(40) default '';
		declare lcl_end_time_string varchar(40) default '';
		declare lcl_flag_string varchar(40) default '';
		declare lcl_user_id_string varchar(40) default '';
		declare lcl_ssession_number_string varchar(40) default '';
		declare lcl_ssession_string varchar(40) default '';
		declare lcl_start_time_string varchar(40) default '';
		declare lcl_almost_finished_string varchar(500) default '';

	DECLARE EXIT HANDLER FOR 1451
	-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails 
		BEGIN
			select -1 into rv_status_code;
		END;
		select ssession.company_id into lcl_company_id from ssession where ssession.id = pv_ssession_id;
		select ssession.e_time into lcl_e_time from ssession where ssession.id = pv_ssession_id;
		select ssession.end_time into lcl_end_time from ssession where ssession.id = pv_ssession_id;
		select ssession.flag into lcl_flag from ssession where ssession.id = pv_ssession_id;
		select ssession.user_id into lcl_user_id from ssession where ssession.id = pv_ssession_id;
		select ssession.ssession_number into lcl_ssession_number from ssession where ssession.id = pv_ssession_id;
		select ssession.start_time into lcl_start_time_string from ssession where ssession.id = pv_ssession_id;
		call jsonify_int(lcl_company_id, 'company_id', lcl_company_id_string);
		call jsonify_int(lcl_e_time, 'e_time', lcl_e_time_string);
		call jsonify_int(lcl_end_time, 'end_time', lcl_end_time_string);
		call jsonify_int(lcl_flag, 'flag', lcl_flag_string);
		call jsonify_int(lcl_user_id, 'user_id', lcl_user_id_string);
		call jsonify_int(pv_ssession_id, 'ssession_id', lcl_ssession_string);
		call jsonify_int(lcl_ssession_number, 'ssession_number', lcl_ssession_number_string);
		call jsonify_int(lcl_start_time, 'start_time', lcl_start_time_string);
		select concat("{", lcl_company_id_string, ",", lcl_e_time_string, ",", lcl_end_time_string, ",", lcl_flag_string,",", lcl_user_id_string, ",", lcl_ssession_string, ",", lcl_ssession_number_string, ",", lcl_start_time_string, "}") into rv_ssession_json;
		call add_outbound(pv_flag, pv_user_id, rv_ssession_json, 'ssession', lcl_outbound_id);
		delete from ssession where ssession.id = pv_ssession_id;
		select lcl_outbound_id into rv_status_code;
	END//

#143
CREATE OR REPLACE PROCEDURE get_ssession_count(OUT rv_out INT)
	BEGIN
		SELECT COUNT(1) into rv_out from ssession;
	END//
#144
CREATE OR REPLACE PROCEDURE get_ssessions_api(IN pv_user_id int(9)
							, IN pv_ssession_number int(9)
						)
	BEGIN
    	declare lcl_auth int default 0;
    	declare lcl_company_id int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		call get_ssessions(pv_user_id);
		END IF;
	END//
#145
CREATE OR REPLACE PROCEDURE get_ssessions(pv_user_id int(9))
	BEGIN
		declare lcl_user_company_id int default 0;
		call get_user_company_id(pv_user_id, lcl_user_company_id);
		SELECT 
			    id
			   , user_id
		       , ssession_number
			   , start_time
			   , end_time
			   , company_id
			   , user_id
			   , e_time
			   , flag
		FROM ssession
		where company_id = lcl_user_company_id;
	END//

#146
CREATE OR REPLACE PROCEDURE get_ssession_api(IN pv_user_id int(9)
							, IN pv_ssession_id int(9)
							, IN pv_ssession_number int(9)
						)
	BEGIN
    	declare lcl_auth int default 0;
    	declare lcl_company_id int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		call get_ssession(pv_user_id, pv_ssession_id);
		END IF;
	END//
#147
CREATE OR REPLACE PROCEDURE get_ssession(pv_user_id int(9)
										, pv_ssession_id int(9)
										)
	BEGIN
		declare lcl_user_company_id int default 0;
		call get_user_company_id(pv_user_id, lcl_user_company_id);
	    SELECT id
			   , user_id
		       , ssession_number
			   , start_time
			   , end_time
			   , company_id
			   , user_id
			   , e_time
			   , flag
		FROM ssession
		where company_id = lcl_user_company_id
		AND id = pv_ssession_id;
	END//
	
 

#149
CREATE OR REPLACE PROCEDURE add_ssession(IN pv_flag int(3)
							, IN pv_user_id int(9)
							, IN pv_session_length int(10)
							, IN pv_ssession_number int(10)
							, IN pv_start_time int(13)
							, IN pv_log_user_id int(10)
							, OUT rv_ssession_id int(9)
							)
	BEGIN
		declare lcl_user_company_id int default 0;
		call get_user_company_id(pv_user_id, lcl_user_company_id);
		
		INSERT INTO ssession (company_id,user_id, e_time, end_time, flag,log_user_id,ssession_number, start_time)
		VALUES (lcl_user_company_id, pv_user_id, UNIX_TIMESTAMP(NOW()), pv_start_time + 10800, 0, pv_log_user_id, pv_ssession_number, pv_start_time);
		
		call get_ssession_count(@after_add_ssessions);
		
		select MAX(id) from ssession where ssession.user_id = pv_user_id INTO rv_ssession_id;
		IF rv_ssession_id IS NULL THEN
		set rv_ssession_id = 0;
		END IF;
	END//


#151
CREATE OR REPLACE PROCEDURE get_ssession_number(in pv_pwd varchar(40)
												, in pv_username varchar(40)
												, out rv_ssession_number int(10))
BEGIN
	declare lcl_id_check int default NULL;
	set rv_ssession_number = -6;
	
	SELECT a.id 
	INTO   lcl_id_check 
	FROM   user a, 
		user_log b 
	WHERE  a.user_log_id = b.id 
		AND a.username = pv_username 
		AND b.pwd = Password(pv_pwd)
		OR b.tmp_pwd = Password(pv_pwd); 

	IF lcl_id_check > 0 then
		SELECT round(10000 + 89999*rand()) INTO rv_ssession_number;
	END IF;
END//


--  Write procedure to give a person a temp password
--  Call procedure to give person the temp password
--  Then try to log in with the temp password
-- make login work with temp pwd
CREATE OR REPLACE PROCEDURE reset_tmp_pwd(IN pv_username varchar(30)
				, IN pv_first_name varchar(50)
				, IN pv_email_address varchar(100)
				, OUT rv_tmp_pwd varchar(10)
				)
	BEGIN		
		declare lcl_id int default 0;
		SET rv_tmp_pwd = -6;		
		SELECT a.user_log_id
		INTO lcl_id
		FROM user a, user_log b
		WHERE a.user_log_id = b.id
			AND a.username = pv_username
			AND b.first_name = pv_first_name
			AND b.email_address = pv_email_address;

		IF lcl_id > 0 THEN
			SET rv_tmp_pwd = round(9999 + 99999*rand());     	

			UPDATE user_log a
			SET tmp_pwd = Password(rv_tmp_pwd)
			WHERE a.id = lcl_id;

		END IF;	
	END //
#152
CREATE OR REPLACE PROCEDURE login(IN pv_username varchar(30)
					, IN pv_pwd varchar(30)
					, OUT rv_user_id int(9)
					, OUT rv_ssession_number int(5)
					)
	BEGIN
		declare lcl_ssession_id int default 0;
		set rv_user_id = -6;
		set rv_ssession_number = -66;
		call get_user_id(pv_username, rv_user_id);
		call get_ssession_number(pv_pwd, pv_username, rv_ssession_number);
		IF rv_ssession_number > 0 THEN
			call add_ssession(0
							, rv_user_id
							, 3600
							, rv_ssession_number
							, UNIX_TIMESTAMP(NOW())
							, rv_user_id
							, lcl_ssession_id);
		END IF;
	END//

#153
CREATE OR REPLACE PROCEDURE get_ssession_id(IN pv_user_id INT(10),
											IN pv_ssession_number INT(10), 
											OUT rv_ssession_id INT(10))
	BEGIN
		SET rv_ssession_id = -6;
		
		SELECT MAX(id) INTO rv_ssession_id FROM ssession 
		WHERE ssession_number = pv_ssession_number AND user_id = pv_user_id;
	END//

	
#337
CREATE OR REPLACE PROCEDURE logout_api(IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                )
        BEGIN
    		declare lcl_auth int default 0;
    		declare lcl_company_id int default 0;
    		CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    		IF lcl_auth = 1 THEN
                call logout(pv_user_id, pv_ssession_number, rv_status);
            END IF;
        END//


#338
CREATE OR REPLACE PROCEDURE logout (IN pv_user_id INT(10)
									, IN pv_ssession_number INT(10)
									, OUT rv_status INT(10))
	BEGIN
		DECLARE lcl_auth INT default 0;
		DECLARE lcl_sess INT default 0;
		DECLARE lcl_delete INT default 0;
		SET rv_status = -6; 
		CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
		IF lcl_auth = 1 THEN
			SET rv_status = -99;
			CALL get_ssession_id(pv_ssession_number, pv_user_id, lcl_sess);
			CALL delete_ssession(0, pv_user_id, lcl_sess, lcl_delete);
			IF lcl_delete > 0 THEN
				SET rv_status = 1;
			END IF;
		END IF;
	END//

