#get_user_logs_api

call add_proc_index(
24
, 'checks authentication, then runs get_user_logs'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["user_log"]'
, 'get_user_logs_api'
, 1
);

#get_user_log_api

call add_proc_index(
25
, 'checks authentication, then runs get_user_log_api'
, 0
, '{"pv_user_id": "int(9)", "pv_user_log_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["user_log"]'
, 'get_user_log_api'
, 1
);

#add_user_log_api

call add_proc_index(
26
, 'returns all values from user_log_table'
, 0
, '{"pv_address_id": "int(10)", "pv_company_id": "int(10)", "pv_dob": "varchar(30), yyyy/mm/dd", "pv_email_address": "varchar(50)", "pv_first_name": "varchar(50)", "pv_flag": "int(3)", "pv_last_name": "varchar(50)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_user_log_id": "int(9)"}'
, '["user_log"]'
, 'add_user_log_api'
, 1
);

#update_name_api

call add_proc_index(
27
, 'updates the name of the user'
, 0
, '{"pv_first_name": "varchar(30)", "pv_last_name": "varchar(30)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["user_log"]'
, 'update_name_api'
, 1
);
