#get_companies_api

call add_proc_index(
7
, 'checks authentication, then runs get_companies'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '[]'
, '["company"]'
, 'get_companys_api'
, 1
);

#get_company_api

call add_proc_index(
8
, 'checks authentication, then calls get_company'
, 0
, '{"pv_company_id":"int(9)", "pv_user_id":"int(9)", "pv_ssession_number":"int(9)"}'
, '[]'
, '["company"]'
, 'get_company_api'
, 1
);

#add_company_api

call add_proc_index(
9
, 'checks authentication, then runs add_company'
, 0
, '{"pv_flag": "int(3)", "pv_user_id": "int(9)", "pv_name": "varchar(30)", "pv_ssession_number": "int(9)", "pv_timezone": "int(2)"}'
, '{"rv_company_id": "int(5)"}'
, '["company"]'
, 'add_company_api'
, 1
);

#delete_company_api

call add_proc_index(
10
, 'checks_permissions, then runs delete_company'
, 0
, '{"pv_company_id": "int(9)", "pv_flag": "int(3)", "pv_user_id": "int(9)"}'
, '{"rv_status_code": "int(9)"}'
, '["company"]'
, 'delete_company_api'
, 1
);

#get_my_company_api

call add_proc_index(
11
, 'checks permission, then runs get_my_company, which gets the users company'
, 0
, '{"pv_user_id":"int(9)", "pv_ssession_number":"int(9)"}'
, '{}'
, '["company"]'
, 'get_my_company_api'
, 1
);

#update_company_api

call add_proc_index(
12
, 'checks permissions, then updates the companys information'
, 0
, '{"pv_user_id":"int(9)", "pv_ssession_number":"int(9)", "pv_company_id": "int(9)", "pv_new_name": "varchar(30)", "pv_new_timezone": "int(2)"}'
, '{}'
, '["company"]'
, 'update_company_api'
, 1
);

#add_company_full_api

call add_proc_index(
13
, 'checks permission, then adds a company, a building, and a user'
, '{}'
, '{}'
, '["company", "building", "user"]'
, 'add_company_full_api'
, 1
);
