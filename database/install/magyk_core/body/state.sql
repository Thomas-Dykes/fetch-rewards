DELIMITER //

#287
CREATE OR REPLACE PROCEDURE delete_state(IN pv_state_id INT(10)
										,OUT rv_status_id INT(10))
BEGIN
	DECLARE lcl_present INT;
	DECLARE lcl_start INT;
	DECLARE lcl_middle INT;
	DECLARE lcl_end INT;
	SET rv_status_id = -6;
	SELECT COUNT(1) INTO lcl_present FROM state WHERE id = pv_state_id;

	DELETE FROM state where id = pv_state_id;
	SELECT COUNT(1) INTO lcl_end FROM state WHERE id = pv_state_id;
	IF lcl_present > lcl_end THEN
		SET rv_status_id =1;
	END IF;
END//

#153
CREATE OR REPLACE PROCEDURE get_state_count(OUT rv_out int(10))
BEGIN
     	SELECT COUNT(1) INTO rv_out FROM state;
END//

CREATE OR REPLACE PROCEDURE delete_states()
BEGIN
	DELETE FROM state;
	ALTER TABLE state auto_increment =1;
END//
#285
CREATE OR REPLACE PROCEDURE add_state_api(IN pv_abbreviation VARCHAR(6)
					, IN pv_country_id INT(9)
					, IN pv_name VARCHAR(40)
					, OUT rv_state_id INT(10)
					)
BEGIN
	DECLARE lcl_count INT default 0;
	SET rv_state_id = -6;

	SELECT Count(1) 
		INTO lcl_count 
	FROM   state 
	WHERE  abbreviation = pv_abbreviation 
        AND country_id = pv_country_id;

	IF lcl_count = 0 THEN
		INSERT INTO state 
					(abbreviation, 
					country_id, 
					e_time, 
					NAME) 
		VALUES     (pv_abbreviation, 
					pv_country_id, 
					Unix_timestamp(Now()), 
					pv_name); 

		SELECT Max(id) 
		INTO   rv_state_id 
		FROM   state; 
	END IF;
END//

#154
CREATE OR REPLACE PROCEDURE get_states_api(
			)
BEGIN
	call get_states();
END//

CREATE OR REPLACE PROCEDURE get_states()
BEGIN
	SELECT state.id, state.abbreviation, state.country_id, state.name
	, state.e_time
	FROM state;
END//

CREATE OR REPLACE PROCEDURE get_state_by_name_api(IN pv_name varchar(40)
			)
BEGIN
	select state.id, state.abbreviation, state.country_id, state.e_time, state.name
	FROM state
	WHERE state.name = pv_name;
END//


CREATE OR REPLACE PROCEDURE get_state_api(IN pv_state_id varchar(40)
			)
BEGIN
	select id, abbreviation, country_id, e_time, name
	FROM state
	WHERE state.id = pv_state_id;
END//

