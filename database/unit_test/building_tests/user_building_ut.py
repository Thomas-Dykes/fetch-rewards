import time, decimal, sys, unittest

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import login
import connector as co
import config
import pandas as pd


def add_user_building(cookie, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_user_building_api"
    params = [building_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    user_building_id = co.machine_add(jdat)
    return user_building_id


def get_user_buildings(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_user_buildings_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    user_buildings = co.machine(jdat)
    for user_building in user_buildings:
        print(f"user_building: {user_building}")
    return user_buildings


def get_user_building(cookie, user_building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_user_building_api"
    params = [user_building_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    user_building = co.machine(jdat)[0]
    print(f"user_building: {user_building}")
    return user_building


class userBuildingTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.user_building_id_one = None
        cls.user_building_id_two = None

    def test_add_1(self):
        entered_data = {"building_id": 1}
        try:
            user_building_id = add_user_building(
                self.cookie, entered_data["building_id"]
            )
            time.sleep(0.1)

            user_building_data = get_user_building(self.cookie, user_building_id)
            for key in entered_data:
                self.assertEqual(
                    entered_data[key], user_building_data[key], f"{key} should be same"
                )
            self.__class__.user_building_id_one = user_building_id
            self.test_name.append("test_add_1")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_user_building_api", "get_user_building_api"]
            )
        except Exception as e:
            self.test_name.append("test_add_1")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_user_building_api", "get_user_building_api"]
            )
            raise Exception(repr(e))

    def test_add_2(self):
        entered_data = {"building_id": 2}
        try:
            user_building_id = add_user_building(
                self.cookie, entered_data["building_id"]
            )
            time.sleep(0.1)

            user_building_data = get_user_building(self.cookie, user_building_id)
            for key in entered_data:
                self.assertEqual(
                    entered_data[key], user_building_data[key], f"{key} should be same"
                )
            self.__class__.user_building_id_two = user_building_id
            self.test_name.append("test_add_2")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_user_building_api", "get_user_building_api"]
            )
        except Exception as e:
            self.test_name.append("test_add_2")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_user_building_api", "get_user_building_api"]
            )
            raise Exception(repr(e))

    def test_gets(self):
        user_building_one = {"building_id": None}
        user_building_two = {"building_id": None}

        try:
            for item in get_user_buildings(self.cookie):
                if item["id"] == self.user_building_id_one:
                    user_building_one = item
                if item["id"] == self.user_building_id_two:
                    user_building_two = item

            user_building_1_by_get = get_user_building(
                self.cookie, self.user_building_id_one
            )
            user_building_2_by_get = get_user_building(
                self.cookie, self.user_building_id_two
            )

            for key in user_building_one:
                self.assertEqual(
                    user_building_one[key],
                    user_building_1_by_get[key],
                    f"{key} should be same",
                )

                self.assertEqual(
                    user_building_two[key],
                    user_building_2_by_get[key],
                    f"{key} should be same",
                )

            self.test_name.append("test_gets")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["get_user_buildings_api", "get_user_building_api"]
            )
        except Exception as e:
            self.test_name.append("test_gets")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["get_user_buildings_api", "get_user_building_api"]
            )


if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = userBuildingTest()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())
