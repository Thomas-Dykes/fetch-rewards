import time, decimal, sys, unittest

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import login
import connector as co
import config
import pandas as pd


def add_system_type(cookie, name, abv, description):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_system_type_api"
    params = [name, abv, description, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def get_system_types(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_system_types_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    system_types = co.machine(jdat)
    for system_type in system_types:
        print(f"system_type: {system_type}")
    return system_types


def get_system_type(cookie, system_type_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_system_type_api"
    params = [system_type_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    system_type = co.machine(jdat)[0]
    print(f"system_type: {system_type}")
    return system_type


class systemTypeTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.system_id_one = None
        cls.system_id_two = None

    def test_add_1(self):
        entered_data = {
            "name": "Testing",
            "abv": "TST",
            "description": "Testing system type",
        }
        try:
            system_id = add_system_type(
                self.cookie,
                entered_data["name"],
                entered_data["abv"],
                entered_data["description"],
            )
            time.sleep(0.1)
            system_data = get_system_type(self.cookie, system_id)
            for key in entered_data:
                self.assertEqual(
                    entered_data[key], system_data[key], f"{key} should be same"
                )
            self.__class__.system_id_one = system_id
            self.test_name.append("test_add_1")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_system_type_api", "get_system_type_api"]
            )
        except Exception as e:
            self.test_name.append("test_add_1")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_system_type_api", "get_system_type_api"]
            )
            raise Exception(repr(e))

    def test_add_2(self):
        entered_data = {
            "name": "Testing 2",
            "abv": "TST-2",
            "description": "Testing system type 2",
        }
        try:
            system_id = add_system_type(
                self.cookie,
                entered_data["name"],
                entered_data["abv"],
                entered_data["description"],
            )
            time.sleep(0.1)
            system_data = get_system_type(self.cookie, system_id)
            for key in entered_data:
                self.assertEqual(
                    entered_data[key], system_data[key], f"{key} should be same"
                )
            self.__class__.system_id_two = system_id
            self.test_name.append("test_add_2")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_system_type_api", "get_system_type_api"]
            )
        except Exception as e:
            self.test_name.append("test_add_2")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_system_type_api", "get_system_type_api"]
            )
            raise Exception(repr(e))

    def test_get_all(self):
        system_type_one = {"name": None, "abv": None, "description": None}
        system_type_two = {"name": None, "abv": None, "description": None}
        try:
            for item in get_system_types(self.cookie):
                if item["id"] == self.system_id_one:
                    system_type_one = item
                if item["id"] == self.system_id_two:
                    system_type_two = item
            system_1_by_get = get_system_type(self.cookie, self.system_id_one)
            system_2_by_get = get_system_type(self.cookie, self.system_id_two)
            for key in system_type_one:
                self.assertEqual(
                    system_type_one[key], system_1_by_get[key], f"{key} should be same"
                )
                self.assertEqual(
                    system_type_two[key], system_2_by_get[key], f"{key} should be same"
                )
            self.test_name.append("test_gets")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["get_system_types_api", "get_system_type_api"]
            )
        except Exception as e:
            self.test_name.append("test_gets")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["get_system_types_api", "get_system_type_api"]
            )


if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = systemTypeTest()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())