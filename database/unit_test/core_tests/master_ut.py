import time, json, sys
import state_ut, table_install_ut
import address_ut, svc_user_ut, account_ut, business_unit_ut, company_ut, employee_ggroup_ut, employee_job_ut, employee_ut, ggroup_ut, ggroup_rrole_ut, ggroup_job_ut, job_ut, job_task_ut, journal_ut, journal_item_ut, outbound_ut, person_ut, person_log_ut, proc_index_ut, rrole_ut, schedule_ut, setup_ut, wage_log_ut, wage_ut, ssession_ut, delete_ut, project_ut, employee_project_ut

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import login
import config


def run_tests(cookie):
    results = []
    print(f"cookie {cookie}")
    results.append(setup_ut.machine(cookie))
    results.append(address_ut.machine(cookie))
    results.append(company_ut.machine(cookie))
    results.append(ggroup_ut.machine(cookie))
    results.append(ggroup_rrole_ut.machine(cookie))
    # results.append(outbound_ut.machine(cookie))
    results.append(proc_index_ut.machine(cookie))
    results.append(rrole_ut.machine(cookie))
    results.append(state_ut.machine(cookie))
    results.append(svc_user_ut.machine(cookie))
    results.append(ssession_ut.machine(cookie))
    results.append(table_install_ut.machine(cookie))
    results.append(user_ggroup_ut.machine(cookie))
    results.append(user_ut.machine(cookie))
    results.append(user_log_ut.machine(cookie))
    # results.append(view_ut.machine(cookie))
    results.append(delete_ut.machine(cookie))
    return results


def save(results, group, employee_id):
    print(" \n      Results: \n")
    lcl_results = []
    errors = []
    for r in results:
        for result in r:
            try:
                print(f"result: {result}")
                table = result["table"]
                test = result["test"]
                lcl_result = result["result"]
                proc_index_id = result["proc_index_id"]
                rr = f"Table: {table}, Proc ID: {proc_index_id} Test: {test}: Result: {lcl_result}, Group: {group}, Employee ID: {employee_id} \n"
                lcl_results.append(result)
                if lcl_result != True:
                    # input(f"result, error, anykey to continue: {result}")
                    errors.append(result)
                rv_id = add_unit_log(table, proc_index_id, lcl_result, group, employee_id)
            except TypeError:
                print("Type Error")
                print(f"result: {result}")
                input("error")

    return results, errors


def printer(results, group_id, employee_id):
    for r in results:
        for result in r:
            try:
                # print(result)
                table = result["table"]
                test = result["test"]
                lcl_result = result["result"]
                proc_index_id = result["proc_index_id"]
                print(
                    f"Table: {table}, Proc ID: {proc_index_id} Test: {test}: Result: {lcl_result}, Group: {group_id}, Employee ID: {employee_id} \n"
                )

            except TypeError as e:
                sleep = 3
                print(f"    print error: {e} \n")
                print(f"    record: {result} \n")
                print("sleep {}".format(sleep))
                time.sleep(sleep)


def add_unit_log(table, proc_index_id, result, group_id, employee_id):
    proc_name = "add_unit_test_log"
    params = [0, employee_id, result, str(proc_index_id), table, group_id, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def get_max_group():
    proc_name = "get_max_test_group"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def get_group(cookie):
    print("get most recent group")
    max = get_max_group()
    try:
        new_group = max + 1
    except TypeError:
        new_group = 1
    return new_group


def main():

    cookie = login.machine()
    employee_id = cookie["employee_id"]
    group = get_group(cookie)
    results = run_tests(cookie)
    print(f"THE E_ID IS: {employee_id}")
    results, errors = save(results, group, employee_id)
    print("PASSED \n")
    printer(results, group, employee_id)
    if len(errors) > 0:
        # input("errors: {}".format(errors))
        print("ERRORS \n")
        # printer(errors, group, employee_id)
        error_cnt = len(errors)
        for e in errors:
            # for ee in e:
            print(f"error: {e}")
        print(f" \n {error_cnt} ERRORS PRESENT \n \n")

    print("\n     END")


if __name__ == "__main__":
    main()
