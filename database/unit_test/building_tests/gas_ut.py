from datetime import date
import time, sys, unittest
import pandas as pd

sys.path.append("/home/ec2-user/pbc_portal/pbc")
import login
import connector as co
import config


def calculate_model_usage_gas(cookie, gas_id, model_log_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_model_usage_gas_api"
    params = [gas_id, model_log_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    model_usage = co.machine_add(jdat)
    return model_usage


def calculate_base_usage(cookie, gas_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_base_usage_gas_api"
    params = [gas_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage


def edit_base_usage(cookie, gas_id, modle_log_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssesion_number"]
    proc_name = "edit_base_usage_gas_api"
    params = [gas_id, modle_log_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage


def calculate_saved_usage(cookie, gas_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_saved_usage_gas_api"
    params = [gas_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    saved_usage = co.machine_add(jdat)
    return saved_usage


# SQL ERROR : There is no "OUT" variable present in the sql file
def get_gas_savings_data(cookie, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_gas_savings_data_api"
    params = [user_id, ssession_number, building_id, 0]
    jdat = {"name": proc_name, "params": params}
    savings_data = co.machine_add(jdat)
    return savings_data


# SQL ERROR : There is no "OUT" variable present in the sql file
def get_savings_gas_between(cookie, building_id, start, end):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_savings_gas_between_api"
    params = [start, end, building_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    savings = co.machine_add(jdat)
    return savings


def calculate_percent_savings(cookie, gas_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_percent_savings_gas_api"
    params = [gas_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    percent_savings = co.machine_add(jdat)
    return percent_savings


def calculate_norm_usage(cookie, gas_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_norm_usage_gas_api"
    params = [gas_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    norm_usage = co.machine_add(jdat)
    return norm_usage


# SQL ERROR : The sql procedure doesn't have any OUT variable.
def get_service_month_year_gas_between(cookie, start, end, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_service_month_year_gas_between_api"
    params = [start, end, building_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    service_month = co.machine_add(jdat)
    return service_month


# SQL ERROR : The sql procedure doesn't have any OUT variable.
def get_gas_ids(cookie, start, end, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_gas_ids_api"
    params = [start, end, building_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    ids = co.machine(jdat)
    for id in ids:
        print(f"id: {id}")
    return ids


# SQL ERROR : The sql procedure doesn't have any OUT variable.
def find_gas_costs(cookie, building_id, start, end):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "find_gas_costs_api"
    params = [user_id, ssession_number, building_id, start, end, 0]
    jdat = {"name": proc_name, "params": params}
    gas_costs = co.machine_add(jdat)
    return gas_costs


def print_gas_cost_one_variable(cookie, building_id, service_month_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "print_gas_cost_one_variable_api"
    params = [building_id, service_month_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    gas_cost = co.machine_add(jdat)
    return gas_cost


def gas_cost_one_variable_monthly(cookie, building_id, service_month_id):
    user_id = cookie["user_id"]
    ssesion_number = cookie["ssession_number"]
    proc_name = "print_gas_cost_one_variable_monthly_api"
    params = [building_id, service_month_id, user_id, ssesion_number, 0]
    jdat = {"name": proc_name, "params": params}
    cost = co.machine_add(jdat)
    return cost


def print_now_and_before_gas_cost_one_variable_monthly(
    cookie, building_id, service_month_id
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "print_now_and_before_gas_cost_one_variable_monthly"
    params = [building_id, service_month_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    costs = co.machine(jdat)
    return costs


def print_now_and_before_gas_cost_one_variable_monthly(
    cookie, building_id, service_month_id
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "print_now_and_before_gas_cost_one_variable_monthly_api"
    params = [building_id, service_month_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    now_and_before_cost = co.machine_add(jdat)
    return now_and_before_cost


# SQL Procedure : The procedure is commented in the sql file
def calculate_model_average_gas(cookie, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_model_average_gas_api"
    params = [user_id, ssession_number, building_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_average = co.machine_add(jdat)
    return model_average


def calculate_gas_cost(cookie, building_id, start, end, user_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_gas_cost_api"
    params = [building_id, start, end, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    gas_cost = co.machine_add(jdat)
    return gas_cost


def calculate_monthly_savings(cookie, gas_id, building_id, start, end):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_monthly_savings_gas_api"
    params = [gas_id, building_id, start, end, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    monthly_savings = co.machine_add(jdat)
    return monthly_savings


# SQL ERROR : The sql procedure doesn't have any `OUT` variable
def add_cumulative_savings(cookie, gas_id, cumulative_savings):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_cumulative_savings_gas_api"
    params = [gas_id, cumulative_savings, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    monthly_savings = co.machine_add(jdat)
    return monthly_savings


def add_gas_pre_period(
    cookie,
    usage,
    model_log_id,
    adjustment,
    building_id,
    oat,
    start,
    end,
    dollar_kwh,
    service_id,
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_gas_pre_period_api"
    params = [
        usage,
        model_log_id,
        adjustment,
        building_id,
        oat,
        start,
        end,
        dollar_kwh,
        service_id,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    gas_id = co.machine_add(jdat)
    return gas_id


#  SQL ERROR : The sql procedure doesn't have any "OUT" variable
def get_gas_norm_usage_oat_graph_data(cookie, building_id, start, end):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_gas_norm_usage_oat_graph_data_api"
    params = [user_id, ssession_number, building_id, start, end, 0]
    jdat = {"name": proc_name, "params": params}
    graph_data = co.machine_add(jdat)
    return graph_data


#  SQL ERROR : The sql procedure doesn't have any "OUT" variable
def get_gas_usage_cost_graph_data(cookie, building_id, start, end):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_gas_usage_cost_graph_data_api"
    params = [user_id, ssession_number, building_id, start, end, 0]
    jdat = {"name": proc_name, "params": params}
    usage_cost_graph_data = co.machind_add(jdat)
    return usage_cost_graph_data


# SQL ERROR : The sql procedure doesn't have any "OUT" variable
def get_gas_price_graph_data(cookie, building_id, start, end):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_gas_price_graph_data_api"
    params = [user_id, ssession_number, building_id, start, end, 0]
    jdat = {"name": proc_name, "params": params}
    graph_data = co.machine_add(jdat)
    return graph_data


# SQL ERROR : The sql procedure doesn't have any `OUT` variable
def get_gas_graph_data(cookie, building_id, start, end):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_gas_graph_data_api"
    params = [user_id, ssession_number, building_id, start, end, 0]
    jdat = {"name": proc_name, "params": params}
    graph_data = co.machine(jdat)
    for graph_point in graph_data:
        print(f"graph_point: {graph_point}")
    return graph_data


def calculate_gas_cost(cookie, gas_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_gas_cost_api"
    params = [gas_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    gas_cost = co.machine_add(jdat)
    return gas_cost


def get_model_usages(cookie, service_month_id, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_model_usages_gas_api"
    params = [service_month_id, building_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    model_usages = co.machine_add(jdat)
    return model_usages


# SQL Error : There is no sql procedure with following name in the sql file
def get_norm_usages(cookie, period_date, period_length):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_norm_usages_gas_api"
    params = [period_date, period_length, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine_add(jdat)
    return norm_usages


# SQL Error : There is no sql procedure with following name in the sql file
def get_oats(cookie, period_date, period_length):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_oats_gas_api"
    params = [period_date, period_length, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine_add(jdat)
    return oats


# SQL ERROR : The sql procedure doesn't have any `OUT` variable
def get_oats_gas_between(cookie, building_id, start, end):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_oats_gas_between_api"
    params = [building_id, start, end, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    oats_between = co.machine_add(jdat)
    return oats_between


# SQL ERROR : There is no OUT variable present in the sql file
def get_gas_usage_data(cookie, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_gas_usage_data_api"
    params = [user_id, ssession_number, building_id, 0]
    jdat = {"name": proc_name, "params": params}
    usage_data = co.machine(jdat)
    for usage_point in usage_data:
        print(f"usage_point: {usage_point}")
    return usage_data


class gasTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.gas_id_one = None
        cls.gas_id_two = None
        cls.model_log_id = None

    def test_add_1(self):
        entered_data = {
            "usage": 1,
            "model_log_id": 1,
            "adjustment": 1,
            "building_id": 100,
            "oat": 1,
            "start": date(2010, 1, 1),
            "end": date(2010, 2, 1),
            "dollar_kwh": 1.0,
            "service_id": 1,
        }

        gas_id = add_gas_pre_period(
            self.cookie,
            entered_data["usage"],
            entered_data["model_log_id"],
            entered_data["adjustment"],
            entered_data["building_id"],
            entered_data["oat"],
            entered_data["start"],
            entered_data["end"],
            entered_data["dollar_kwh"],
            entered_data["service_id"],
        )

        self.__class__.gas_id_one = gas_id

    def test_add_2(self):
        entered_data = {
            "usage": 2,
            "model_log_id": 2,
            "adjustment": 2,
            "building_id": 100,
            "oat": 2,
            "start": date(2011, 3, 1),
            "end": date(2011, 5, 2),
            "dollar_kwh": 2.0,
            "service_id": 2,
        }
        gas_id = add_gas_pre_period(
            self.cookie,
            entered_data["usage"],
            entered_data["model_log_id"],
            entered_data["adjustment"],
            entered_data["building_id"],
            entered_data["oat"],
            entered_data["start"],
            entered_data["end"],
            entered_data["dollar_kwh"],
            entered_data["service_id"],
        )
        self.__class__.gas_id_two = gas_id

    def test_model_usage_1(self):
        pass

    def test_model_usage_2(self):
        pass


if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = gasTest()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())