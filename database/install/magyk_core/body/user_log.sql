DELIMITER //
#333
CREATE OR REPLACE PROCEDURE delete_user_log(IN pv_user_log_id INT(9)
											  ,IN pv_user_id INT(9)
											  ,IN pv_flag INT(5)
											  ,OUT rv_status INT(9))
	BEGIN
		DECLARE lcl_company_id INT default 0;
		DECLARE lcl_start INT default 0;
		DECLARE lcl_end INT default 0;
		SET rv_status = -6;
		CALL get_user_company_id(pv_user_id, lcl_company_id);
		SELECT COUNT(1) INTO lcl_start FROM user_log WHERE company_id = lcl_company_id and id = pv_user_log_id;
		DELETE FROM user_log WHERE company_id = lcl_company_id and id = pv_user_log_id;
		SELECT COUNT(1) INTO lcl_end FROM user_log WHERE company_id = lcl_company_id and id = pv_user_log_id;
		IF lcl_start > lcl_end THEN
			SET rv_status = 1;
		END IF;
	END//

#249
CREATE OR REPLACE PROCEDURE delete_user_logs()
        BEGIN
        DELETE FROM user_log;
		ALTER TABLE user_log AUTO_INCREMENT = 1;
        END//

#111
CREATE OR REPLACE PROCEDURE get_user_log_count(OUT rv_out INT)
	BEGIN
		SELECT COUNT(1) into rv_out from user_log;
	END//

CREATE OR REPLACE PROCEDURE get_user_log_max(OUT rv_out INT)
	BEGIN
		SELECT MAX(id) INTO rv_out FROM user_log;
	END//


#112
CREATE OR REPLACE PROCEDURE get_user_logs_api(IN pv_user_id int(9)
							, IN pv_ssession_number int(9))
	BEGIN 
    	declare lcl_auth int default 0;
    	declare lcl_company_id int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		CALL get_user_logs(pv_user_id);
		END IF;
	END//

#NEEDS FIXED
CREATE OR REPLACE PROCEDURE get_user_logs(IN pv_user_id int(9))
	BEGIN 
		declare lcl_user_company_id int default 0;
		CALL get_user_company_id(pv_user_id, lcl_user_company_id);
		SELECT user_log.id, user_log.address_id
		, user_log.company_id, user_log.dob
		, user_log.email_address, user_log.first_name
		, user_log.flag, user_log.last_name,
		user_log.phone_number
		FROM user_log
		where user_log.company_id = lcl_user_company_id;
	END//
#114

CREATE OR REPLACE PROCEDURE get_user_log_api(IN pv_user_id int(9)
                                                , IN pv_user_log_id int(9)
												, IN pv_ssession_number int(9)
                                                )
    BEGIN
    	declare lcl_auth int default 0;
    	declare lcl_company_id int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN call get_user_log(pv_user_id, pv_user_log_id);
        END IF;
    END//
#NEEDS FIXED
CREATE OR REPLACE PROCEDURE get_user_log(IN pv_user_id int(9)
                                            , IN pv_user_log_id int(10)
											)
        BEGIN
	declare lcl_user_company_id int default 0;
        call get_user_company_id(pv_user_id, lcl_user_company_id);
        SELECT user_log.id, user_log.address_id
        , user_log.company_id, user_log.dob
		, user_log.email_address, user_log.first_name
		, user_log.flag, user_log.last_name, user_log.phone_number
        FROM user_log
        where user_log.company_id = lcl_user_company_id
        AND user_log.id = pv_user_log_id;
        END//
#116
CREATE OR REPLACE PROCEDURE add_user_log_api(IN pv_address_id int(10)
							, IN pv_dob varchar(30)
							, IN pv_email_address varchar(50)
							, IN pv_first_name varchar(50)
							, IN pv_flag int(3) 
 							, IN pv_last_name varchar(50)
							, IN pv_user_id int(6)
							, IN pv_password varchar(30)
							, IN pv_phone_number varchar(20)
							, IN pv_ssession_number int(9)
							, IN pv_start_date varchar(30)
							, OUT rv_user_log_id int(9)
							)
	BEGIN
        declare lcl_user_company_id int default 0;
		declare lcl_run_add_user_log int default 0;
    	declare lcl_auth int default 0;
    	declare lcl_company_id int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		CALL add_user_log(pv_address_id, pv_dob
		, pv_email_address, pv_first_name
		, pv_flag, pv_last_name, pv_user_id
		, pv_password, pv_phone_number, pv_start_date, rv_user_log_id);
		END IF;
		IF rv_user_log_id IS NULL THEN
		set rv_user_log_id = -2;
		END IF;
	END//


CREATE OR REPLACE PROCEDURE add_user_log(IN pv_address_id int(10)
							, IN pv_dob varchar(30)
							, IN pv_email_address varchar(50)
							, IN pv_first_name varchar(50)
							, IN pv_flag int(3)
							, IN pv_last_name varchar(50)
							, IN pv_user_id int(6)
							, IN pv_password varchar(30)
							, IN pv_phone_number varchar(20)
							, IN pv_start_date varchar(30)
							, OUT rv_user_log_id int(9)
							)
	BEGIN
		declare lcl_user_company_id int default 0;
		CALL get_user_company_id(pv_user_id, lcl_user_company_id);
		INSERT INTO user_log(address_id, company_id, dob, email_address, e_time, first_name, flag, last_name, pwd, phone_number, start_date)
		VALUES (pv_address_id, lcl_user_company_id, pv_dob, pv_email_address, UNIX_TIMESTAMP(NOW()), pv_first_name, pv_flag, pv_last_name, PASSWORD(pv_password), pv_phone_number, pv_start_date);
		select MAX(id) from user_log where user_log.company_id = lcl_user_company_id INTO rv_user_log_id;
		IF rv_user_log_id IS NULL THEN
			set rv_user_log_id = 0;
		END IF;
	END//


