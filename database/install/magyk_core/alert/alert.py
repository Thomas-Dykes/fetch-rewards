import json, sys, os, time, string, smtplib, ssl
import boto3
from botocore.exceptions import ClientError
import random  
from random import randint

#  Seed random number generator 
random.seed(a=None, version=2)
 
"""
1. Emails must be verified before they can send/recieve
   from https://console.aws.amazon.com/ses/home?region=us-east-1#verified-senders-email:
2. Sends from gmail, but fails so send from yahoo(may or may not be an issue),
3. What is company email? Found this:  support@magyk.cloud
"""
# 
SENDER='kernel8803@gmail.com'
RECIEVER='kelel1@yahoo.com'
# Check about NoneType
NoneType = type(None)

# Function to send employee password reset
def send_pwd_reset(): 
  return randint(9999, 99999)

# Generate temporary password
temp_pwd = send_pwd_reset()


# Establish db connection
try:
  sys.path.append("/home/ec2-user/pbc_portal/database/install/magyk_core")
  import connector as co 
  """
    co is used to connect with the database
    notice we have the name of the stored procedure as first argument in json
    we have the parameters that are feed to the sproc as the second item in json
  """
except ModuleNotFoundError:
  print("connector not found; unable to load to db")

key_id = None
access_key = None

with open("/home/ec2-user/pbc_portal/database/install/magyk_core/alert/key.key", 'r') as f:
  jdat = json.loads(f.readlines()[0])
  key_id = jdat['aws_access_key_id']
  access_key = jdat['aws_secret_access_key']


# Creat SES client
client = boto3.client(
  'ses',
  aws_access_key_id=key_id,
  aws_secret_access_key=access_key,
  region_name="us-east-1"
)

client.send_email(
  Source=f'{SENDER}',
  Destination={
    'ToAddresses': [
      f'{RECIEVER}'
    ]
  },
  Message={
    'Subject': {
      'Data': 'Test',
      'Charset': 'UTF-8'
    },
    'Body': {
      'Text': {
        'Data': f'Your temporary password is {temp_pwd}',
        'Charset': 'UTF-8'
      },
    }
  }
)
