import connector as co
import config


def update_price(price, model_id):
    proc_name = "update_gas_model_price"
    params = [price, model_id]
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

def main(price, model_id):

    update_price(price, model_id)

