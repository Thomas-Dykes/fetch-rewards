DELIMITER //
CREATE OR REPLACE PROCEDURE add_api_test(IN pv_response INT(10)
									, IN pv_name varchar(50)
                                    , IN pv_server_id int(10)
                                    , OUT rv_id int(10)
                                    )
	BEGIN
        set rv_id = 0;

        INSERT INTO api_test (name, response, test_time, server_id)
        VALUES(pv_name, pv_response, CURRENT_TIMESTAMP, pv_server_id );

        select max(id) into rv_id
        from api_test;

	END//