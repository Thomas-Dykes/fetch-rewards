import time, decimal, sys
import user_ut as ee
import company_ut as company

sys.path.append("/home/ec2-user/business/magyk")
# sys.path.append("../../../magyk")
import login
import connector as co
import config


def get_tests():
    return [
        [251, "test_address_table_install", "address"],
        [252, "test_company_table_install", "company"],
        [253, "test_user_ggroup_table_install", "user_ggroup"],
        [255, "test_user_table_install", "user"],
        [257, "test_ggroup_rrole_table_install", "ggoup_rrole"],
        [258, "test_ggroup_table_install", "ggroup"],
        [261, "test_user_log_table_install", "user_log"],
        [263, "test_rrole_table_install", "rrole"],
        [265, "test_ssession_table_install", "ssession"]
    ]


def test(test, user_id):
    proc_name = test
    params = [user_id, 0]
    jdat = {"name": proc_name, "params": params}
    print(f"data: {jdat}")
    result = co.machine_add(jdat)
    print(f"result: {result}")
    return result


def int_to_bool(pv_int):
    if pv_int == 1:
        return True
    return False

def tester(tests, user_id):
    results = []
    for t in tests:
        lcl_id = t[0]
        name = t[1]
        table = t[2]
        r = test(name, user_id)
        result = int_to_bool(r)
        rdat = {"proc_index_id": lcl_id, "test": name, "table": table, "result": result}
        results.append(rdat)
        print(f"id: {lcl_id}, test: {name}, table: {table}, result: {result}")
    return results


def get_root_user(company_id):
    return ee.get_root_user(company_id)


def get_company_id(name):
    return company.get_company_by_name(name)


def get_user():
    company_name = config.test_company
    print(f"company_name: {company_name}")
    company_id = get_company_id(company_name)
    print(f"company_id for user: {company_id}")
    user_id = get_root_user(company_id)
    print(f"user_id: {user_id}")
    return user_id


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        try:
            t_table = test["table"]
            t_test = test["test"]
            t_result = test["result"]
            t_proc_index_id = test["proc_index_id"]
            print(
                f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
            )
        except Exception as e:
            print(f"exception: {e}\n\n data: {test}")
            time.sleep(1)


def machine():
    tests = []
    user_id = get_user()
    print(f"user_id WOO!: {user_id}")
    # print(f"user_id: {user_id}")
    if isinstance(user_id, int):
        tests = get_tests()
        return tester(tests, user_id)

    return {"user_id not found, table install_Ut"}


def main():
    tests = machine()
    test_printer(tests)


if __name__ == "__main__":
    main()
