import json, sys, os, requests, time, string
import boto3
from os import walk
from botocore.exceptions import ClientError


# Replace sender@example.com with your "From" address.
# This address must be verified with Amazon SES.
SENDER = "support@facilitykpi.com"

# Replace recipient@example.com with a "To" address. If your account 
# is still in the sandbox, this address must be verified.
RECIPIENT = "kernel8803@gmail.com"

NoneType = type(None)
key_id = None
access_key = None

with open("/home/ec2-user/pbc_portal/notification/key.key", 'r') as f:
  jdat = json.loads(f.readlines()[0])
  key_id = jdat['aws_access_key_id']
  access_key = jdat['aws_secret_access_key']


# If necessary, replace us-west-2 with the AWS Region you're using for Amazon SES.
AWS_REGION = "us-east-2"

# The subject line for the email.
SUBJECT = "Amazon SES Test (SDK for Python)"

# The email body for recipients with non-HTML email clients.
BODY_TEXT = ("Amazon SES Test (Python)\r\n"
             "This email was sent with Amazon SES using the "
             "AWS SDK for Python (Boto)."
            )
            
# The HTML body of the email.
BODY_HTML = """<html>
<head></head>
<body>
  <h1>Amazon SES Test (SDK for Python)</h1>
  <p>This email was sent with
    <a href='https://aws.amazon.com/ses/'>Amazon SES</a> using the
    <a href='https://aws.amazon.com/sdk-for-python/'>
      AWS SDK for Python (Boto)</a>.</p>
</body>
</html>
            """            

# The character encoding for the email.
CHARSET = "UTF-8"

# Create a new SES resource and specify a region.
client = boto3.client(
        'ses',
        aws_access_key_id=key_id,
        aws_secret_access_key=access_key,
        region_name=AWS_REGION
)

# Try to send the email.
try:
    #Provide the contents of the email.
    response = client.send_email(
        Destination={
            'ToAddresses': [
                RECIPIENT,
            ],
        },
        Message={
            'Body': {
                
                'Text': {
                    'Charset': CHARSET,
                    'Data': 'Hello World',
                },
            },
            'Subject': {
                'Charset': CHARSET,
                'Data': SUBJECT,
            },
        },
        Source=SENDER,
        # If you are not using a configuration set, comment or delete the
        # following line
    )
# Display an error if something goes wrong.	
except ClientError as e:
    print(e.response['Error']['Message'])
else:
    print("Email sent! Message ID:"),
    print(response['MessageId'])