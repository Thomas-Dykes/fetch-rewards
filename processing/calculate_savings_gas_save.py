import csv
from scipy import optimize
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config

def calculate_model_usage(gas_id, model_log_id):
    proc_name = "calculate_model_usage_gas_one_slope"
    params = [gas_id, model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_usage = co.machine_add(jdat)
    return model_usage

def calculate_norm_usage(gas_id):
    proc_name = "calculate_norm_usage_gas"
    params = [gas_id, 0]
    jdat = {"name": proc_name, "params": params}
    norm_usage = co.machine_add(jdat)
    return norm_usage

def get_gas_ids(building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_gas_ids"
    params = [building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    e_ids = co.machine(jdat)
    e_id_return = []
    for e_id in e_ids:
        print(f"e_id: {e_id}")
        e_id_return.append(float(e_id["id"]))
    return e_id_return

def calculate_base_usage(gas_id, model_log_id):
    proc_name = "calculate_base_usage_gas"
    params = [gas_id, model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage


def calculate_base_usage_one_slope(gas_id, model_log_id):
    proc_name = "calculate_base_usage_gas_one_slope"
    params = [gas_id, model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage

def calculate_saved_usage(gas_id):
    proc_name = "calculate_saved_usage_gas"
    params = [gas_id, 0]
    jdat = {"name": proc_name, "params": params}    
    saved_usage = co.machine_add(jdat)
    return saved_usage

def calculate_percent_savings(gas_id):
    proc_name = "calculate_percent_savings_gas"
    params = [gas_id, 0]
    jdat = {"name": proc_name, "params": params}
    percent_savings = co.machine_add(jdat)
    return percent_savings

def calculate_monthly_savings(gas_id, model_log_id, building_id):
    proc_name = "calculate_monthly_savings_gas"
    params = [gas_id, model_log_id, building_id, 0]
    jdat = {"name": proc_name, "params": params}
    monthly_savings = co.machine_add(jdat)
    return monthly_savings

def add_savings(gas_id, cumulative_savings):
    proc_name = "add_cumulative_savings_gas"
    params = [gas_id, cumulative_savings]
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat) 

def get_model_log_id(model_id):
    proc_name = "get_model_log_id_gas"
    params = [model_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_log_id = co.machine_add(jdat)
    return model_log_id

def update_gas_model(gas_id, norm_usage, model_usage, base_usage, saved_usage, percent_savings, monthly_savings, cumulative_savings):
    proc_name = "update_gas_model"
    params = [gas_id, norm_usage, model_usage, base_usage, saved_usage, percent_savings, monthly_savings, cumulative_savings]
    jdat = {"name": proc_name, "params": params} 
    co.machine(jdat)  

def get_adjusted_usage(gas_id):
    proc_name = "get_adjusted_usage_gas"
    params = [gas_id, 0]
    jdat = {"name": proc_name, "params": params}
    gas_savings = co.machine_add(jdat)
    return gas_savings  

def get_model_price(model_log_id):
    proc_name = "get_model_price"
    params = [model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_price = co.machine_add(jdat)
    return model_price  

def main(user_id, building_id, model_id,  post_service_month_id_start, post_service_month_id_end):

    model_log_id = get_model_log_id(model_id)
    
    e_ids = get_gas_ids(building_id, post_service_month_id_start, post_service_month_id_end)
    print(f"e_ids: {e_ids}") 
    cumulative_savings = 0
    for e_id in e_ids:
        adjusted_usage = get_adjusted_usage(e_id)
        norm_usage = float(calculate_norm_usage(e_id))
        model_usage = calculate_model_usage(e_id, model_log_id)
        base_usage = calculate_base_usage_one_slope(e_id, model_log_id)
        saved_usage = base_usage - adjusted_usage
        percent_savings = saved_usage/base_usage
        electricity_price = get_model_price(model_log_id)
        monthly_savings = saved_usage*electricity_price
        cumulative_savings = float(cumulative_savings) + float(monthly_savings)
        update_gas_model(e_id, norm_usage, float(model_usage), float(base_usage), float(saved_usage), float(percent_savings), float(monthly_savings), cumulative_savings)

if __name__ == "__main__":
    user_id = 6
    building_id = 20
    # ssession_number = 6
    post_start = 591
    post_end = 604
    model_id = 82

    main(user_id, building_id, model_id, post_start, post_end)