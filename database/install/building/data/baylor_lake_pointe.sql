call add_building(268851,'Baylor Lake Point', 0, 0, 0, 1, '9999/99/99', 2, @out);
-- electricity pre period
call add_electricity_pre_period_raw(679487, 252480, @out, 66, '2017/10/11', '2017/11/08', .07, 2, @output);
call add_electricity_pre_period_raw(685154, 229665, @out, 59, '2017/11/09', '2017/12/08', .07, 2, @output);
call add_electricity_pre_period_raw(700355, 230977, @out, 45, '2017/12/09', '2018/01/10', .07, 2, @output);
call add_electricity_pre_period_raw(620296, 229918, @out, 46, '2018/01/11', '2018/02/08', .07, 2, @output);
call add_electricity_pre_period_raw(873763, 208734, @out, 56, '2018/02/09', '2018/03/18', .07, 2, @output);
call add_electricity_pre_period_raw(493446, 236707, @out, 62, '2018/03/19', '2018/04/10', .07, 2, @output);
call add_electricity_pre_period_raw(650615, 241407, @out, 69, '2018/04/11', '2018/05/08', .07, 2, @output);
call add_electricity_pre_period_raw(826389, 259184, @out, 83, '2018/05/09', '2018/06/07', .07, 2, @output);
call add_electricity_pre_period_raw(955271, 279521, @out, 87, '2018/06/08', '2018/07/10', .07, 2, @output);
call add_electricity_pre_period_raw(961354, 303268, @out, 90, '2018/07/11', '2018/08/09', .07, 2, @output);
-- gas pre period
call add_gas_pre_period_raw(1576, 329, @out, 84, '2018/08/01', '2018/08/31', 5, 2, @output);
call add_gas_pre_period_raw(1598, 349, @out, 80, '2018/09/01', '2018/09/30', 5, 2, @output);
call add_gas_pre_period_raw(1821, 425, @out, 70, '2018/10/01', '2018/10/31', 5, 2, @output);
call add_gas_pre_period_raw(1775, 536, @out, 62, '2018/11/01', '2018/11/30', 5, 2, @output);
call add_gas_pre_period_raw(1959, 681, @out, 49, '2018/12/01', '2018/12/31', 5, 2, @output);
call add_gas_pre_period_raw(2160, 672, @out, 45, '2019/01/01', '2019/01/31', 5, 2, @output);
call add_gas_pre_period_raw(1910, 575, @out, 50, '2019/02/01', '2018/02/28', 5, 2, @output);
call add_gas_pre_period_raw(1897, 608, @out, 63, '2019/03/01', '2019/03/31', 5, 2, @output);
call add_gas_pre_period_raw(1754, 410, @out, 63, '2019/04/01', '2019/04/30', 5, 2, @output);
call add_gas_pre_period_raw(1564, 378, @out, 80, '2018/05/01', '2018/05/30', 5, 2, @output);
call add_gas_pre_period_raw(1317, 324, @out, 87, '2018/06/01', '2018/06/30', 5, 2, @output);
call add_gas_pre_period_raw(1334, 325, @out, 90, '2018/07/01', '2018/07/31', 5, 2, @output);
-- post period electricity
call add_electricity_pre_period_raw(842538, 0, @out, 70, '2019/04/09', '2019/05/08', .07, 2, @output);
call add_electricity_pre_period_raw(1036505, 0, @out, 77, '2018/05/09', '2018/06/09', .07, 2, @output);
call add_electricity_pre_period_raw(1042841, 0, @out, 82, '2019/06/10', '2019/07/10', .07, 2, @output);
call add_electricity_pre_period_raw(1035606, 0, @out, 86, '2019/07/11', '2019/08/07', .07, 2, @output);
call add_electricity_pre_period_raw(1210707, 0, @out, 89, '2019/08/08', '2019/09/09', .07, 2, @output);
call add_electricity_pre_period_raw(1047886, 0, @out, 84, '2019/09/10', '2019/10/09', .07, 2, @output);
call add_electricity_pre_period_raw(762913, 0, @out, 84, '2019/10/10', '2019/11/07', .07, 2, @output);
call add_electricity_pre_period_raw(786984, 0, @out, 54, '2019/11/08', '2019/12/09', .07, 2, @output);
call add_electricity_pre_period_raw(737961, 0, @out, 51, '2019/12/10', '2020/01/10', .07, 2, @output);
call add_electricity_pre_period_raw(736370, 0, @out, 51, '2020/01/11', '2020/02/09', .07, 2, @output);
call add_electricity_pre_period_raw(641905, 0, @out, 54, '2020/02/10', '2020/03/09', .07, 2, @output);
call add_electricity_pre_period_raw(709679, 0, @out, 64, '2020/03/10', '2020/04/07', .07, 2, @output);
call add_electricity_pre_period_raw(752184, 0, @out, 69, '2020/04/08', '2020/05/07', .07, 2, @output);
call add_electricity_pre_period_raw(946991, 0, @out, 77, '2020/05/08', '2020/06/08', .07, 2, @output);
--post period gas
call add_gas_pre_period_raw(2345, 0, @out, 67, '2019/04/01', '2019/04/30', 5, 2, @output);
call add_gas_pre_period_raw(2237, 0, @out, 75, '2019/05/01', '2019/05/31', 5, 2, @output);
call add_gas_pre_period_raw(1923, 0, @out, 81, '2019/06/01', '2019/06/30', 5, 2, @output);
call add_gas_pre_period_raw(1926, 0, @out, 86, '2019/07/01', '2019/07/31', 5, 2, @output);
call add_gas_pre_period_raw(1858, 0, @out, 88, '2019/08/01', '2019/08/31', 5, 2, @output);
call add_gas_pre_period_raw(1843, 0, @out, 86, '2019/09/01', '2019/09/30', 5, 2, @output);
call add_gas_pre_period_raw(2226, 0, @out, 67, '2019/10/01', '2019/10/31', 5, 2, @output);
call add_gas_pre_period_raw(2583, 0, @out, 55, '2019/11/01', '2019/11/30', 5, 2, @output);
call add_gas_pre_period_raw(2749, 0, @out, 52, '2019/12/01', '2019/12/31', 5, 2, @output);
call add_gas_pre_period_raw(2635, 0, @out, 52, '2020/01/01', '2020/01/31', 5, 2, @output);
call add_gas_pre_period_raw(2426, 0, @out, 51, '2020/02/01', '2020/02/29', 5, 2, @output);
call add_gas_pre_period_raw(2185, 0, @out, 64, '2020/03/01', '2020/03/31', 5, 2, @output);
call add_gas_pre_period_raw(1979, 0, @out, 66, '2020/04/01', '2020/04/30', 5, 2, @output);



