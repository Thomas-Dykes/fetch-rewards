DELIMITER //
#307
CREATE OR REPLACE PROCEDURE  new_rroles(OUT rv_rrole_count int(9))
BEGIN
    DECLARE lcl_recent_id INT default 0;
	DECLARE rrole_count INT default 0;
	ALTER TABLE rrole AUTO_INCREMENT = 1;
	SELECT count(1) into rrole_count FROM rrole;
	if rrole_count = 0 THEN
	call add_rrole(0, 1,'r', 'read_address', 'address', lcl_recent_id);
	call add_rrole(0, 2, 'w', 'write_address', 'address', lcl_recent_id);
	call add_rrole(0, 3, 'm', 'manage_address', 'address', lcl_recent_id);
	call add_rrole(0, 4, 'r', 'read_company', 'company', lcl_recent_id);
	call add_rrole(0, 5, 'w', 'write_company', 'company', lcl_recent_id);
	call add_rrole(0, 6, 'm', 'manage_company', 'company', lcl_recent_id);
	call add_rrole(0, 7, 'r', 'read_user', 'user', lcl_recent_id);
	call add_rrole(0, 8, 'w', 'write_user', 'user', lcl_recent_id);
	call add_rrole(0, 9, 'm', 'manage_user', 'user', lcl_recent_id);
	call add_rrole(0, 10, 'r', 'read_user_ggroup', 'user_ggroup', lcl_recent_id);
	call add_rrole(0, 11, 'w', 'write_user_ggroup', 'user_ggroup', lcl_recent_id);
	call add_rrole(0, 12, 'm', 'manage_user_ggroup', 'user_ggroup', lcl_recent_id);
	call add_rrole(0, 13, 'r', 'read_ggroup', 'ggroup', lcl_recent_id);
	call add_rrole(0, 14, 'w', 'write_ggroup', 'ggroup', lcl_recent_id);
	call add_rrole(0, 15, 'm' ,'manage_ggroup', 'ggroup', lcl_recent_id);
	call add_rrole(0, 16, 'r', 'read_ggroup_rrole', 'ggroup_rrole', lcl_recent_id);
	call add_rrole(0, 17, 'w', 'write_ggroup_rrole', 'ggroup_rrole', lcl_recent_id);
	call add_rrole(0, 18, 'm', 'manage_ggroup_rrole', 'ggroup_rrole', lcl_recent_id);
	call add_rrole(0, 19, 'r', 'read_user_log', 'user_log', lcl_recent_id);
	call add_rrole(0, 20, 'w', 'write_user_log', 'user_log', lcl_recent_id);
	call add_rrole(0, 21, 'm', 'manage_user_log', 'user_log', lcl_recent_id);
	call add_rrole(0, 22, 'r', 'read_rrole', 'rrole', lcl_recent_id);
	call add_rrole(0, 23, 'w', 'write_rrole', 'rrole', lcl_recent_id);
	call add_rrole(0, 24, 'm', 'manage_rrole', 'rrole', lcl_recent_id);
	call add_rrole(0, 25, 'r', 'read_ssession', 'ssession', lcl_recent_id);
	call add_rrole(0, 26, 'w', 'write_ssession', 'ssession', lcl_recent_id);
	call add_rrole(0, 27, 'm', 'manage_ssession', 'ssession', lcl_recent_id);
	call add_rrole(0, 28, 'r', 'read_outbound', 'outbound', lcl_recent_id);
	call add_rrole(0, 29, 'w', 'write_outbound', 'outbound', lcl_recent_id);
    call add_rrole(0, 30, 'r', 'read_proc_index', 'proc_index', lcl_recent_id);
    call add_rrole(0, 31, 'w', 'write_proc_index', 'proc_index', lcl_recent_id);
    call add_rrole(0, 32, 'r', 'unit_log_test', 'read_unit_log_test', lcl_recent_id);
    call add_rrole(0, 33, 'w', 'unit_log_test', 'write_unit_log_test', lcl_recent_id);

	END IF;
	SELECT COUNT(1) FROM rrole into rv_rrole_count;
    END//

	call new_rroles(@out);

