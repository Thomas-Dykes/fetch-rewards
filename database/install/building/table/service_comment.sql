CREATE TABLE IF NOT EXISTS service_comment (
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    service_id int(10),
    parent_id int(10), 
    comment varchar(100),
    flag int(9),
    log_user_id int(10),
    company_id int(10),
    e_time int(9)
);