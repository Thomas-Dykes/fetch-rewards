#delete_user_api

call add_proc_index(
28
, 'deletes a user from the user table'
, 0
, '{"pv_flag": "int(3)", "pv_user_id": "int(9)", "pv_user_log_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_status_code": "int(9)"}'
, '["user", "user_log"]'
, 'delete_user_api'
, 1
);

#get_user_api

call add_proc_index(
29
, 'runs permissions, then calls get_user'
, 0
, '{"pv_user_id": "int(9)", "pv_log_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_status_code": "int(9)"}'
, '["user"]'
, 'get_user_api'
, 1
);

#get_users_api
call add_proc_index(
30
, 'runs permissions, then calls get_users'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["user"]'
, 'get_users_api'
, 1
);

#add_user_username_api
call add_proc_index(
31
, 'runs permissions, then calls add_user_username'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_username_output": "int(9)"}'
, '["user"]'
, 'add_user_username_api'
, 1
);

#get_user_id_api
call add_proc_index(
83
, 'runs permissions, then calls get_user_id'
, 0
, '{"pv_username": "varchar(30)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_id_output": "int(9)"}'
, '["user"]'
, 'get_user_id_api'
, 1
);

#add_user_with_address_api
call add_proc_index(
32
, 'runs permissions, then calls add_user_with_address'
, 0
, '{"pv_admin": "int(9)", "pv_customer": "int(9)", "pv_dob": "varchar(30)", "pv_email_address": "varchar(40)", "pv_employee": "int(1)"
, "pv_first_name": "varchar(30)", "pv_flag": "int(3)", "pv_last_name": "varchar(100)", "pv_user_id": "int(9)", "pv_phone_number": "varchar(20)", "pv_ssession_number": "int(9)"
, "pv_username": "varchar(30)", "pv_city": "varchar(30)", "pv_latitude": "decimal(9,6)", "pv_longitude": "decimal(9,6)", "pv_state_id": "int(9)"
, "pv_street_address": "int(9)", "pv_zip_code": "int(9)"}'
, '{"rv_user_id": "int(9)"}'
, '["user"]'
, 'add_user_with_address_api'
, 1
);

#renew_password_api
call add_proc_index(
33
, 'runs permissions, then calls renew_password'
, 0
, '{"pv_user_id": "int(9)", "pv_new_human_password": "varchar(100)", "pv_old_password": "varchar(100)", "pv_ssession_number": "int(9)"}'
, '{"rv_status": "int(9)"}'
, '["user", "user_log"]'
, 'renew_password_api'
, 1
);

#replace_password_api
call add_proc_index(
34
, 'runs permissions, then calls replace_password'
, 0
, '{"pv_user_id": "int(9)", "pv_new_human_password": "varchar(40)", "pv_ssession_number": "int(9)"}'
, '{"rv_status": "int(9)"}'
, '["user", "user_log"]'
, 'replace_password_api'
, 1
);

#get_my_info_api
call add_proc_index(
35
, 'runs permissions, then calls get_my_info'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["user", "user_log"]'
, 'get_my_info_api'
, 1
);

#update_user_phone_number_api
call add_proc_index(
36
, 'runs permissions, then calls update_user_phone_number'
, 0
, '{"pv_new_phone_number": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["user"]'
, 'update_user_phone_number_api'
, 1
);

#add_user_api
call add_proc_index(
37
, 'runs permissions, then calls add_user'
, 0
, '{"pv_address_id": "int(9)", "pv_admin": "int(9)", "pv_customer": "int(9)", "pv_dob": "varchar(30)", "pv_email_address": "varchar(40)", "pv_employee": "int(1)"
, "pv_first_name": "varchar(30)", "pv_flag": "int(3)", "pv_last_name": "varchar(100)", "pv_user_id": "int(9)", "pv_phone_number": "varchar(20)", "pv_ssession_number": "int(9)"
, "pv_username": "varchar(30)"}'
, '{"rv_user_id": "int(9)"}'
, '["user"]'
, 'add_user_api'
, 1
);