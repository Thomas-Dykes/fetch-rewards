CREATE TABLE IF NOT EXISTS building_category (
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    name varchar(40),
    description varchar(200),
    e_time int(13),
    flag int(3),
    log_user_id int(10)
);