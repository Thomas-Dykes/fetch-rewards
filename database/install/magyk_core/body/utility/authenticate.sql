DELIMITER //

create or replace procedure authenticate(IN pv_user_id int(10)
							, IN pv_ssession_number int(10)
							, OUT rv_authenticated int(1)
							)
	BEGIN
		declare lcl_endtime int default 0;
		declare lcl_id int default 0;
		SELECT max(id) INTO lcl_id
		FROM ssession
		WHERE ssession_number = pv_ssession_number
		AND user_id = pv_user_id;
		SELECT ssession.end_time into lcl_endtime
		FROM ssession
		WHERE ssession.id = lcl_id;
		SELECT IF(lcl_endtime > UNIX_TIMESTAMP(NOW()), 1, 0) into rv_authenticated;
	END//

DELIMITER //

create or replace procedure authenticate_api(IN pv_user_id int(10)
							, IN pv_session_number int(10)
							, OUT rv_authenticated int(1)
							)
	BEGIN
		call authenticate(pv_user_id, pv_session_number, rv_authenticated);
	END//
