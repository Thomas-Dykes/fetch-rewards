import csv
from scipy import optimize
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
# from piecewise import piecewise
#from piecewise import piecewise_plot
import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config

# def add_building_raw(area, name):
#     proc_name = "add_building"
#     params = [area, name, 0, 0, 0, 0, 0, 0]
#     jdat = {"name": proc_name, "params": params}
#     building_id = co.machine_add(jdat)
#     return building_id

def add_gas_raw(usage, adjustment, building_id, oat, start, end, dollar_kwh, user_id):
    proc_name = "add_gas_pre_period_raw"
    params = [usage, adjustment, building_id, oat, start, end, dollar_kwh, user_id, 0]
    jdat = {"name": proc_name, "params": params}
    gas_id = co.machine_add(jdat)
    return gas_id


def get_oat_pre(user_id, building_id, service_month_id):
    proc_name = "get_oats_gas_pre"
    params = [user_id, building_id, service_month_id]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine(jdat)
    for oat in oats:
        print(f"oat: {oat}")
    return oats

def get_oat_post(user_id, building_id, service_month_id):
    proc_name = "get_oats_gas_post"
    params = [user_id, building_id, service_month_id]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine(jdat)
    for oat in oats:
        print(f"oat: {oat}")
    return oats

def get_norm_usage_pre(user_id, building_id, service_month_id):
    proc_name = "get_norm_usages_gas_pre"
    params = [user_id, building_id, service_month_id]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine(jdat)
    for norm_usage in norm_usages:
        print(f"norm_usage: {norm_usage}")
    return norm_usages


def get_norm_usage_post(user_id, building_id, service_month_id):
    proc_name = "get_norm_usages_gas_post"
    params = [user_id, building_id, service_month_id]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine(jdat)
    for norm_usage in norm_usages:
        print(f"norm_usage: {norm_usage}")
    return norm_usages

def calculate_model_usage(gas_id, model_log_id):
    proc_name = "calculate_model_usage_gas_one_slope"
    params = [gas_id, model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_usage = co.machine_add(jdat)
    return model_usage

def get_gas_ids(building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_gas_ids"
    params = [building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    e_ids = co.machine(jdat)
    e_id_return = []
    for e_id in e_ids:
        print(f"e_id: {e_id}")
        e_id_return.append(float(e_id["id"]))
    return e_id_return

def calculate_base_usage(gas_id, model_usage):
    proc_name = "calculate_base_usage_gas"
    params = [gas_id, model_usage, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage


def calculate_base_usage_one_slope(gas_id):
    proc_name = "calculate_base_usage_gas_one_slope"
    params = [gas_id, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage

def calculate_saved_usage(gas_id):
    proc_name = "calculate_saved_usage_gas"
    params = [gas_id, 0]
    jdat = {"name": proc_name, "params": params}    
    saved_usage = co.machine_add(jdat)
    return saved_usage

def calculate_percent_savings(gas_id):
    proc_name = "calculate_percent_savings_gas"
    params = [gas_id, 0]
    jdat = {"name": proc_name, "params": params}
    percent_savings = co.machine_add(jdat)
    return percent_savings

def calculate_monthly_savings(gas_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "calculate_monthly_savings_gas"
    params = [gas_id, building_id, service_month_id_start, service_month_id_end, 0]
    jdat = {"name": proc_name, "params": params}
    monthly_savings = co.machine_add(jdat)
    return monthly_savings

def add_savings(gas_id, cumulative_savings):
    proc_name = "add_cumulative_savings_gas"
    params = [gas_id, cumulative_savings]
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat) 

def get_model_log_id(model_id):
    proc_name = "get_model_log_id_gas"
    params = [model_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_log_id = co.machine_add(jdat)
    return model_log_id


def build_plot(x_list_1, y_list_1, x_list_2, y_list_2, line):
    print(
        f"\n  line: {line[0]} {line[1]}"
    )
    plt.plot(x_list_1, y_list_1, "o", label="Pre")
    # print(f"x list: {x_list}, y1: {y1} ")
    print(f"x_list_2: {x_list_2}")
    print(f"y_list_2: {y_list_2}")
    plt.plot(x_list_2, y_list_2, "x", label="Post", color="orange")
    plt.title("Magyk Office")
    plt.xlabel("Monthly Avg. Mean OAT (deg F)")
    plt.ylabel("Watt/sqft")
    print(f"LINE ZERO: {line[0]}")
    print(f"LINE ONE: {line[1]}")
    plt.plot(line[0], line[1], color="green", label="Estimate")
    #  plt.plot('x series', 'yseries', label='yoour label')
    plt.legend()
    
    return plt


def build_model_post_period(pv_x_list_1, pv_y_list_1, pv_x_list_2, pv_y_list_2):
    lcl_x1_return = []
    for row in pv_x_list_1:
        lcl_x1_return.append(float(row["oat"]))
    x1_list = np.array(lcl_x1_return, dtype=float)
    lcl_y1_return = []
    for row2 in pv_y_list_1:
        lcl_y1_return.append(float(row2["norm_usage"]))
    y1_list = np.array(lcl_y1_return, dtype=float)
    lcl_x2_return = []
    for row3 in pv_x_list_2:
        lcl_x2_return.append(float(row3["oat"]))
    x2_list = np.array(lcl_x2_return, dtype=float)
    lcl_y2_return = []
    for row4 in pv_y_list_2:
        lcl_y2_return.append(float(row4["norm_usage"]))
    y2_list = np.array(lcl_y2_return, dtype=float)

    path = "./graph.png"


    m = (len(x1_list) * np.sum(x1_list*y1_list) - np.sum(x1_list) * np.sum(y1_list)) / (len(x1_list)*np.sum(x1_list*x1_list) - np.sum(x1_list) ** 2)

    b = (np.sum(y1_list) - m *np.sum(x1_list))/len(x1_list)

    line = []

    blank_list_1 = []
    blank_list_2 = []
    blank_list_1.append(min(x1_list))
    blank_list_1.append(max(x1_list))
    blank_list_2.append(m*min(x1_list) + b)
    blank_list_2.append(m*max(x1_list) + b)
    line.append(blank_list_1)
    line.append(blank_list_2)


    plot = build_plot(x1_list, y1_list, x2_list, y2_list, line)
    plot.savefig(path)



def main(user_id, building_id, model_id,  post_service_month_id_start, post_service_month_id_end):

    model_log_id = get_model_log_id(model_id)

    x_list_1 = get_oat_pre(user_id, building_id, post_service_month_id_start)
    y_list_1 = get_norm_usage_pre(user_id, building_id, post_service_month_id_start)
    x_list_2 = get_oat_post(user_id, building_id, post_service_month_id_start)
    y_list_2 = get_norm_usage_post(user_id, building_id, post_service_month_id_start)

    build_model_post_period(x_list_1, y_list_1, x_list_2, y_list_2)
    
    e_ids = get_gas_ids(building_id, post_service_month_id_start, post_service_month_id_end)
    print(f"e_ids: {e_ids}") 
    cumulative_savings = 0
    for e_id in e_ids:
        model_usage = float(calculate_model_usage(e_id, model_log_id))
        base_usage = calculate_base_usage_one_slope(e_id)
        saved_usage = calculate_saved_usage(e_id)
        percent_savings = calculate_percent_savings(e_id)
        monthly_savings = calculate_monthly_savings(e_id, building_id, post_service_month_id_start, post_service_month_id_start)
        cumulative_savings = float(cumulative_savings) + float(monthly_savings)
        add_savings(e_id, cumulative_savings)
        
if __name__ == "__main__":
    user_id = 6
    building_id = 23
    # ssession_number = 6
    post_start = 591
    post_end = 604
    model_id = 130
    # with open('gas_post_period.csv')as f:
    #     readCSV = csv.reader(f, delimiter=',')
    #     next(readCSV, None)
    #     for row in readCSV:
    #         data.append(row)

    main(user_id, building_id, model_id, post_start, post_end)