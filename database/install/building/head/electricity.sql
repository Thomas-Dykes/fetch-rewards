#calculate_model_usage_electricity_api
call add_proc_index(
51
, 'checks permissions, then runs calculate_model_usage_electricity'
, 0
, '{"pv_electricity_id": "int(9)", "pv_model_log_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_model_usage": "decimal(5,3)"}'
, '["electricity"]'
, 'calculate_model_usage_electricity_api'
, 1
);

#calculate_base_usage_electricity_api
call add_proc_index(
52
, 'checks permissions, then runs calculate_base_usage_electricity'
, 0
, '{"pv_electricity_id": "int(9)", "pv_model_log_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_base_usage": "decimal(5,3)"}'
, '["electricity"]'
, 'calculate_base_usage_electricity_api'
, 1
);

#calculate_saved_usage_electricity_api
call add_proc_index(
53
, 'checks permissions, then runs calculate_saved_usage_electricity'
, 0
, '{"pv_electricity_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_saved_usage": "decimal(5,3)"}'
, '["electricity"]'
, 'calculate_saved_usage_electricity_api'
, 1
);

#calculate_percent_savings_electricity_api
call add_proc_index(
54
, 'checks permissions, then runs calculate_percent_savings_electricity'
, 0
, '{"pv_electricity_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_percent_savings": "decimal(5,3)"}'
, '["electricity"]'
, 'calculate_percent_savings_electricity_api'
, 1
);

#calculate_norm_usage_electricity_api
call add_proc_index(
55
, 'checks permissions, then runs calculate_norm_usage_electricity'
, 0
, '{"pv_electricity_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_norm_usage": "decimal(5,3)"}'
, '["electricity"]'
, 'calculate_norm_usage_electricity_api'
, 1
);

#get_electricity_ids_api
call add_proc_index(
56
, 'checks permissions, then runs get_electricity_ids'
, 0
, '{"pv_start_date": "date", "pv_end_date": "date", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["electricity"]'
, 'get_electricity_ids_api'
, 1
);

#find_electricity_costs_api

call add_proc_index(
57
, 'checks permissions, then runs find_electricity_costs'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, '["electricity"]'
, 'find_electricity_costs_api'
, 1
)


#calculate_monthly_savings_electricity_api
call add_proc_index(
58
, 'checks permissions, then runs calculate_monthly_savings_electricity'
, 0
, '{"pv_electricity_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_cost": "decimal(4,3)"}'
, '["electricity"]'
, 'calculate_monthly_savings_electricity_api'
, 1
);

#add_cumulative_savings_electricity_api
call add_proc_index(
59
, 'checks permissions, then runs add_cumulative_savings_electricity'
, 0
, '{"pv_electricity_id": "int(9)", "pv_cumulative_savings": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["electricity"]'
, 'add_cumulative_savings_electricity_api'
, 1
);

#add_electricity_pre_period_raw_api
call add_proc_index(
60
, 'checks permissions, then runs add_electricity_pre_period_raw'
, 0
, '{"pv_usage": "int(9)", "pv_adjustment": "int(9)", "pv_building_id": "int(9)", "pv_oat": "int(9)", "pv_start": "date", "pv_end": "date", "pv_dollar_kwh": "decimal(6,2)"
, "pv_log_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_electricity_id": "int(9)"}'
, '["electricity"]'
, 'add_electricity_pre_period_raw_api'
, 1
);

#get_electricity_graph_data_api
call add_proc_index(
61
, 'checks permission, then runs get_electricity_graph_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["electricity"]'
, 'get_electricity_graph_data_api'
, 1
);

#get_electricity_price_graph_data_api

call add_proc_index(
62
, 'checks permission, then runs get_electricity_price_graph_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, '["electricity"]'
, "get_electricity_price_graph_data_api"
, 1
);

#get_electricity_usage_cost_graph_data_api

call add_proc_index(
63
, 'checks permission, then runs get_electricity_usage_cost_graph_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, '["electricity"]'
, "get_electricity_usage_cost_graph_data_api"
, 1
);

#get_electricity_norm_usage_oat_graph_data_api

call add_proc_index(
64
, 'checks permission, then runs get_electricity_norm_usage_oat_graph_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, '["electricity"]'
, "get_electricity_norm_usage_oat_graph_data_api"
, 1
);

#get_oats_electricity_between_api
call add_proc_index(
65
, 'checks permission, then runs get_oats_electricity_between'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, '["electricity"]'
, 'get_oats_electricity_between_api'
, 1
);

#get_norm_usages_electricity_between_api
call add_proc_index(
66
, 'checks permission, then runs get_norm_usages_electricity_between'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, '["electricity"]'
, 'get_norm_usages_electricity_between_api'
, 1
);

#get_service_month_year_electricity_between_api

call add_proc_index(
67
, 'checks permission, then runs get_service_month_year_electricity_between'
, 0
, '{"pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)", "pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["electricity"]'
, "get_service_month_year_electricity_between_api"
, 1
);

#get_electricity_usage_data_api
call add_proc_index(
68
, 'checks permission, then runs get_electricity_usage_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["electricity"]'
, 'get_electricity_usage_data_api'
, 1
);

#get_electricity_savings_data_api

call add_proc_index(
69
, 'checks permission, then runs get_electricity_savings_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)"}'
, '{}'
, '["electricity"]'
, 'get_electricity_savings_data_api'
, 1
);

#get_cumulative_savings_post_period_api

call add_proc_index(
70
, 'checks permission, then runs get_cumulative_savings_post_period'
, 0
,  '{"pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
 , '{}'
 , '["electricity"]'
 , 'get_cumulative_savings_post_period_api'
 , 1
);

#get_cumulative_savings_between_api

call add_proc_index(
71
, 'checks permission, then runs get_cumulative_savings_between'
, 0
, '{"pv_building_id": "int(9)", "pv_service_month_id_start": "int(9)", "pv_service_month_id_end": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["electricity"]'
, 'get_cumulative_savings_between_api'
, 1
);

#get_cumulative_savings_api
call add_proc_index(
72
, 'checks permission, then runs get_cumulative_savings'
, 0
, '{"pv_service_date": "date", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["electricity"]'
, 'get_cumulative_savings_api'
, 1
);

#get_savings_electricity_between_api

call add_proc_index(
73
, 'checks permission, then runs get_savings_elecetricity_beteween'
, 0
, '{"pv_service_month_id": "int(9)", "pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["electricity"]'
, 'get_savings_electricity_between_api'
, 1
);

#get_adjusted_usage_electricity_api

call add_proc_index(
74
, 'checks permission, then runs get_adjusted_usage_electricity'
, 0
, '{"pv_electricity_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_adjusted_usage": "int(9)"}'
, '["electricity"]'
, 'get_adjusted_usage_electricity_api'
, 1
);

#get_dollar_per_kwh_electricity_api

call add_proc_index(
75
, 'checks permission, then runs get_dollar_per_kwh_electricity'
, 0
, '{"pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '["electricity"]'
, 'get_dollar_per_kwh_electricity_api'
, 1
);

#update_electricity_api

call add_proc_index(
76
, 'checks permission, then runs update_electricity'
, 0
, ''
, ''
, 'update_electricity_api'
, 1
);

#update_electricity_model_api

call add_proc_index(
77
, 'checks permission, then runs update_electricity_model'
, 0
, ''
, ''
, 'update_electricity_model_api'
, 1
);

#delete_buildings_electricity_data_api

call add_proc_index(
78
, 'checks permission, then runs delete_buildings_electricity_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)"}'
, '{}'
, 'delete_buildings_electricity_data_api'
, 1
);

#delete_electricity_api

call add_proc_index(
79
, 'checks permission, then runs delete_electricity'
, 0
, '{"pv_flag": "int(3)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_electricity_id": "int(9)"}'
, '{"rv_status_code": "int(9)"}'
, 'delete_electricity_api'
, 1
);

#get_electricity_api

call add_proc_index(
80
, 'checks permission, then runs get_electricity'
, 0
, '{"pv_electricity_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, 'get_electricity_api'
, 1
);

#get_electricity_post_period_graph_data_api

call add_proc_index(
81
, 'checks permission, then runs get_electricity_post_period_graph_data'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)"}'
, '{}'
, 'get_electricity_post_period_graph_data_api'
, 1
);

#get_electricity_graph_data_between_api

call add_proc_index(
82
, 'checks permission, then runs get_electricity_graph_data_between'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)"}'
, '{}'
, 'get_electricity_graph_data_between_api'
, 1
);

#calculate_baseline_electricity_price_api

call add_proc_index(
83
, 'checks permission, then runs calculate_baseline_electricity_price_api'
, 0
, '{"pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_price": "decimal(4,3)"}'
, 'calculate_baseline_electricity_price_api'
, 1
);

#calculate_post_period_electricity_price_api

call add_proc_index(
84
, 'checks permission, then runs calculate_post_period_electricity_price'
, 0
, '{"pv_building_id": "int(9)", "pv_service_month_id_begin": "int(9)", "pv_service_month_id_end": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_price": "decimal(4,3)"}'
, 'calculate_post_period_electricity_price_api'
, 1
);

#update_price_electricity_api

call add_proc_index(
85
, 'checks permission, then runs update_price_electricity'
, 0
, '{"pv_price": "decimal(6,4)", "pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, 'update_price_electricity_api'
, 1
);

#END