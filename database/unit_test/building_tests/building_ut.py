from logging import log
import time, decimal, sys, unittest
import pandas as pd

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import login
import connector as co


def add_building(
    cookie,
    area,
    name,
    building_category_id,
    electricity_model_id,
    gas_model_id,
    address_id,
    built,
    company_id,
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_building_api"
    params = [
        area,
        name,
        building_category_id,
        electricity_model_id,
        gas_model_id,
        address_id,
        built,
        company_id,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    building_id = co.machine_add(jdat)
    return building_id


def get_buildings(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_buildings_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    buildings = co.machine(jdat)
    for building in buildings:
        print(f"building: {building}")
    return buildings


def get_building_category_name(cookie, building_category_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_building_category_api"
    params = [building_category_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    building_category = co.machine(jdat)[0]
    print(f"building_category: {building_category}")
    print(building_category["name"])
    return building_category["name"]


def get_street_address(cookie, address_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_address_api"
    params = [address_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    address = co.machine(jdat)[0]
    print(f"address: {address}")
    print(address["street_address"])
    return address["street_address"]


def get_building(cookie, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_building_api"
    params = [building_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    building = co.machine(jdat)[0]
    print(f"building: {building}")
    return building


class buildingTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.building_id_one = None
        cls.building_id_two = None

    def test_add_1(self):
        entered_data = {
            "area": 1,
            "name": "Testing 1",
            "building_category_id": 1,
            "electricity_model_id": 1,
            "gas_model_id": 1,
            "address_id": 1,
            "built": "test",
            "company_id": 1,
        }
        try:
            building_id = add_building(
                self.cookie,
                entered_data["area"],
                entered_data["name"],
                entered_data["building_category_id"],
                entered_data["electricity_model_id"],
                entered_data["gas_model_id"],
                entered_data["address_id"],
                entered_data["built"],
                entered_data["company_id"],
            )

            time.sleep(0.1)
            building_data = get_building(self.cookie, building_id)

            buiding_category_name = get_building_category_name(
                self.cookie, entered_data["building_category_id"]
            )
            street_address = get_street_address(self.cookie, entered_data["address_id"])

            entered_data.pop("building_category_id")
            entered_data.pop("address_id")
            entered_data.pop("company_id")
            entered_data["building_category_name"] = buiding_category_name
            entered_data["street_address"] = street_address

            for key in entered_data:
                self.assertEqual(
                    entered_data[key], building_data[key], f"{key} should be same"
                )
            self.test_name.append("test_add_1")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                [
                    "add_building_api",
                    "get_building_api",
                    "get_address_api",
                    "get_building_category_api",
                ]
            )
            self.__class__.building_id_one = building_id

        except Exception as e:
            self.test_name.append("test_add_1")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                [
                    "add_building_api",
                    "get_building_api",
                    "get_address_api",
                    "get_building_category_api",
                ]
            )
            raise Exception(repr(e))

    def test_add_2(self):
        entered_data = {
            "area": 2,
            "name": "Testing 2",
            "building_category_id": 2,
            "electricity_model_id": 2,
            "gas_model_id": 2,
            "address_id": 2,
            "built": "test 2",
            "company_id": 2,
        }
        try:
            building_id = add_building(
                self.cookie,
                entered_data["area"],
                entered_data["name"],
                entered_data["building_category_id"],
                entered_data["electricity_model_id"],
                entered_data["gas_model_id"],
                entered_data["address_id"],
                entered_data["built"],
                entered_data["company_id"],
            )

            time.sleep(0.1)
            building_data = get_building(self.cookie, building_id)

            buiding_category_name = get_building_category_name(
                self.cookie, entered_data["building_category_id"]
            )
            street_address = get_street_address(self.cookie, entered_data["address_id"])

            entered_data.pop("building_category_id")
            entered_data.pop("address_id")
            entered_data.pop("company_id")
            entered_data["building_category_name"] = buiding_category_name
            entered_data["street_address"] = street_address

            for key in entered_data:
                self.assertEqual(
                    entered_data[key], building_data[key], f"{key} should be same"
                )
            self.test_name.append("test_add_2")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                [
                    "add_building_api",
                    "get_building_api",
                    "get_address_api",
                    "get_building_category_api",
                ]
            )
            self.__class__.building_id_two = building_id

        except Exception as e:
            self.test_name.append("test_add_2")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                [
                    "add_building_api",
                    "get_building_api",
                    "get_address_api",
                    "get_building_category_api",
                ]
            )
            raise Exception(repr(e))

    def test_gets(self):

        building_one = {
            "id": -1,
            "area": None,
            "name": None,
            "building_category_id": None,
            "electricity_model_id": None,
            "gas_model_id": None,
            "address_id": None,
            "built": None,
            "company_id": None,
        }
        building_two = {
            "id": -1,
            "area": None,
            "name": None,
            "building_category_id": None,
            "electricity_model_id": None,
            "gas_model_id": None,
            "address_id": None,
            "built": None,
            "company_id": None,
        }
        try:
            for item in get_buildings(self.cookie):
                if item["id"] == self.building_id_one:
                    building_one = item
                if item["id"] == self.building_id_two:
                    building_two = item

            building_1_by_get = get_building(self.cookie, self.building_id_one)
            building_2_by_get = get_building(self.cookie, self.building_id_two)

            for key in building_one:
                self.assertEqual(
                    building_one[key], building_1_by_get[key], f"{key} should be same"
                )
                self.assertEqual(
                    building_two[key], building_2_by_get[key], f"{key} should be same"
                )
            self.test_name.append("test_gets")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(["get_building_api", "get_buildings_api"])
        except Exception as e:
            self.test_name.append("test_gets")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(["get_buildings_api", "get_building_api"])
            raise Exception(repr(e))


if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = buildingTest()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())
