DELIMITER //

CREATE OR REPLACE PROCEDURE add_building_category_api(IN pv_name varchar(40)
                                                , IN pv_description varchar(200)
                                                , IN pv_user_id int(9)
                                                , IN pv_ssession_number int(9)
                                                , OUT rv_building_category_id int(9)
                                                )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_building_category(pv_name, pv_description, pv_user_id, rv_building_category_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE add_building_category(IN pv_name varchar(40),
                                          IN pv_description varchar(200),
                                          IN pv_user_id int(10),
                                          OUT rv_building_category_id int(10))
BEGIN
    insert into building_category(name, description, e_time, flag, log_user_id)
    values(pv_name, pv_description, UNIX_TIMESTAMP(NOW()), 0, pv_user_id);
    SELECT MAX(id) into rv_building_category_id
    from building_category
    where log_user_id = pv_user_id;
END//


CREATE OR REPLACE PROCEDURE get_building_categories_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_building_categories(pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_building_categories(IN pv_user_id int(9)
                                                )
BEGIN
    SELECT building_category.id, building_category.name
    , building_category.description
    , building_category.e_time, building_category.flag
    , building_category.log_user_id
    FROM building_category;
END//

CREATE OR REPLACE PROCEDURE get_building_category_api(IN pv_building_category_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_building_category(pv_building_category_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_building_category(IN pv_building_category_id int(9)
                                                , IN pv_user_id int(9)
                                                )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    SELECT building_category.id, building_category.name
    , building_category.description
    , building_category.e_time, building_category.flag
    , building_category.log_user_id
    FROM building_category
    where building_category.id = pv_building_category_id;
END//
