CREATE TABLE IF NOT EXISTS eui (
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    building_id int(10),
    service_month_id int(10),
    eui_electricity decimal(13,2),
    eui_gas decimal(13,2),
    eui_total decimal(13,2),
    is_post_period int(1)
);

