DELIMITER //
# NOT IN PROC INDEX
CREATE OR REPLACE PROCEDURE get_company_by_name(IN pv_name VARCHAR(30)
                                                , OUT rv_company_id INT(10))
	BEGIN
	DECLARE lcl_count INT(3) default 0;
	SET rv_company_id = -6;
	SELECT COUNT(1) INTO lcl_count FROM company WHERE name LIKE pv_name;
	IF lcl_count = 1 THEN
		SELECT id INTO rv_company_id FROM company WHERE name like pv_name; 
	END IF;
	END//

CREATE OR REPLACE PROCEDURE delete_companys()
        BEGIN
             	DELETE FROM company;
                ALTER TABLE company AUTO_INCREMENT=1;
        END//




CREATE OR REPLACE PROCEDURE get_my_company_api(IN pv_user_id int(10),
											IN pv_ssession_number int(10))
BEGIN
	declare lcl_auth int default 0;
	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
	IF lcl_auth = 1 THEN
		CALL get_my_company(pv_user_id);
	END IF;
END//


CREATE OR REPLACE PROCEDURE get_my_company(IN pv_user_id INT(10))
BEGIN
	declare lcl_company_id int default 0;
	CALL get_user_company_id(pv_user_id, lcl_company_id);

	SELECT id, name
	from company
	where id = lcl_company_id;
END//


CREATE OR REPLACE PROCEDURE delete_company_api(IN pv_company_id int(9)
				, IN pv_flag int(3)
				, IN pv_user_id int(9)
				, IN pv_ssession_number int(9)
				, OUT rv_status_code int(9)
                                )
BEGIN
		declare lcl_auth int default 0;
		CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
		IF lcl_auth = 1 THEN
			CALL delete_company(pv_company_id, pv_flag, pv_user_id, rv_status_code);
		END IF;
END//


CREATE OR REPLACE PROCEDURE delete_company(in pv_company_id int(9)
										, IN pv_flag int(3)
										, IN pv_user_id int(9)
										, OUT rv_status_code int(9)
										)
BEGIN
	declare lcl_outbound_id int default 0;
	declare rv_company_json varchar(500) default '';
	declare lcl_e_time int(13) default 0;
	declare lcl_flag int(3) default 0;
	declare lcl_log_user_id int default 0;
	declare lcl_name varchar(40) default '';
	declare lcl_timezone int(2) default 0;
	declare lcl_company_string varchar(40) default '';
	declare lcl_e_time_string varchar(40) default '';
	declare lcl_flag_string varchar(40) default '';
	declare lcl_log_user_id_string varchar(40) default '';
	declare lcl_name_string varchar(40) default '';
	declare lcl_timezone_string varchar(40) default '';
	declare lcl_almost_finished_string varchar(500) default '';
	DECLARE EXIT HANDLER FOR 1451
	-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails 
		BEGIN
			select -1 into rv_status_code;
		END;
			DECLARE EXIT HANDLER FOR 1318
	--  ERROR 1318 (42000): Incorrect number of arguments
		BEGIN
			select -2 into rv_status_code;
		END;
	select company.e_time into lcl_e_time from company where company.id = pv_company_id;
	select company.flag into lcl_flag from company where company.id = pv_company_id;
	select company.log_user_id into lcl_log_user_id from company where company.id = pv_company_id;
	select company.name into lcl_name from company where company.id = pv_company_id;
	select company.timezone into lcl_timezone from company where company.id = pv_company_id;
	call jsonify_int(pv_company_id, 'company_id', lcl_company_string);
	call jsonify_int(lcl_e_time, 'e_time', lcl_e_time_string);
	call jsonify_int(lcl_flag, 'flag', lcl_flag_string);
	call jsonify_int(lcl_log_user_id, 'log_user_id', lcl_log_user_id_string);
	call jsonify_string('name', lcl_name, lcl_name_string);
	call jsonify_int(lcl_timezone, 'timezone', lcl_timezone_string);
	select concat("{", lcl_company_string, ",", lcl_e_time_string, ",", lcl_flag_string, ",", lcl_log_user_id_string, ",", lcl_name_string,",", lcl_timezone_string, "}") into rv_company_json;
	-- select concat(lcl_almost_finished_string, "'") into rv_company_json;	
	call add_outbound(pv_flag, pv_user_id, rv_company_json, 'company', lcl_outbound_id);
	DELETE FROM company where company.id = pv_company_id;
	select lcl_outbound_id into rv_status_code;
END//

#13

CREATE OR REPLACE PROCEDURE get_company_count(OUT rv_out INT)
	BEGIN
		SELECT COUNT(1) into rv_out from company;
	END//


#15

CREATE OR REPLACE PROCEDURE get_companys_api(
				)
BEGIN
	call get_companys();
END//

#16

CREATE OR REPLACE PROCEDURE get_companys()
	BEGIN
		SELECT company.e_time, 
			company.flag, 
			company.id, 
			company.log_user_id, 
			company.name, 
			company.timezone 
		FROM   company; 
	END//

#17

CREATE OR REPLACE PROCEDURE get_company_api(IN pv_company_id int(9)
                                )
BEGIN
        call get_company(pv_company_id);
END//

#18

CREATE OR REPLACE PROCEDURE get_company(IN pv_company_id int(9))
    BEGIN
		SELECT company.e_time, 
			company.flag, 
			company.id, 
			company.log_user_id, 
			company.name, 
			company.timezone 
		FROM   company 
		WHERE  company.id = pv_company_id; 
    END//

#19

CREATE OR REPLACE PROCEDURE add_company_api(IN pv_flag INT(3)
							, IN pv_name VARCHAR(100)
							, IN pv_timezone INT(2)
							, OUT rv_company_id INT(5)
							)
	BEGIN
		DECLARE lcl_run_add_company INT default 0;
		DECLARE lcl_user_company_id INT default 0;
		DECLARE lcl_duplicate INT default 0;
		SET rv_company_id = -6;
		CALL add_company(pv_flag, pv_name, pv_timezone, rv_company_id);
		IF rv_company_id IS NULL THEN
		SET rv_company_id = -3;
		END IF;
	END//


#20
CREATE OR REPLACE PROCEDURE add_company(IN pv_flag int(3)
				, IN pv_name varchar(100)
				, IN pv_timezone int(2)
				, OUT rv_company_id int(9)
)
	BEGIN
		INSERT INTO company(e_time, flag, name, timezone)
		VALUES(UNIX_TIMESTAMP(NOW()), pv_flag, pv_name, pv_timezone);
		select MAX(id) from company into rv_company_id;
		IF rv_company_id IS NULL THEN
		set rv_company_id = -4;
		END IF;
	END//
	
#NOT IN PROC_INDEX
CREATE OR REPLACE PROCEDURE add_company_2(IN pv_flag int(3)
				, IN pv_user_id int(9)
				, IN pv_name varchar(30)
				, IN pv_timezone int(2)
				, OUT rv_company_id int(9)
)
	BEGIN
		INSERT INTO company(e_time, flag, log_user_id, name, timezone)
		VALUES(UNIX_TIMESTAMP(NOW()), pv_flag, pv_user_id, pv_name, pv_timezone);
		select MAX(id) from company into rv_company_id ;
		IF rv_company_id IS NULL THEN
		set rv_company_id = -4;
		END IF;
	END//

CREATE OR REPLACE PROCEDURE get_root_company_id(OUT rv_company_id int(9)
											)
BEGIN
	SELECT min(id) FROM company INTO rv_company_id;
END//

CREATE OR REPLACE PROCEDURE add_company_full_api(IN pv_company_name varchar(30)
							, IN pv_timezone int(2)
							, IN pv_dob varchar(30)
							, IN pv_email_address varchar(40)
							, IN pv_first_name varchar(30)
							, IN pv_flag int(3)
							, IN pv_last_name varchar(100)
							, IN pv_rrole_id int(1)
							, IN pv_user_id int(6)
							, IN pv_ssession_number int(9)
							, IN pv_password varchar(100)
							, IN pv_phone_number varchar(20)
							, IN pv_start_date varchar(30)
							, IN pv_username varchar(30)
							, IN pv_city varchar(30)
							, IN pv_state_id int(10)
							, IN pv_street_address varchar(100)
							, IN pv_zip_code varchar(10)
							, IN pv_area int(10)
                            , IN pv_building_name varchar(40)
                            , IN pv_building_category_id int(9)
                            , IN pv_built varchar(30)
							, OUT rv_company_id int(9)
							)
BEGIN
	declare lcl_auth int default 0;
    declare lcl_company_id int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
		call add_company_full(pv_company_name, pv_timezone, pv_dob, pv_email_address, pv_first_name
		, pv_flag, pv_last_name, pv_rrole_id, pv_user_id, pv_password, pv_phone_number, pv_start_date, pv_username
		, pv_city, pv_state_id, pv_street_address, pv_zip_code, pv_area, pv_building_name
		, pv_building_category_id, pv_built, rv_company_id);
	END IF;
END//

CREATE OR REPLACE PROCEDURE add_company_full(IN pv_company_name varchar(30)
							, IN pv_timezone int(2)
							, IN pv_dob varchar(30)
							, IN pv_email_address varchar(40)
							, IN pv_first_name varchar(30)
							, IN pv_flag int(3)
							, IN pv_last_name varchar(100)
							, IN pv_rrole_id int(1)
							, IN pv_user_id int(6)
							, IN pv_password varchar(100)
							, IN pv_phone_number varchar(20)
							, IN pv_start_date varchar(30)
							, IN pv_username varchar(30)
							, IN pv_city varchar(30)
							, IN pv_state_id int(10)
							, IN pv_street_address varchar(100)
							, IN pv_zip_code varchar(10)
							, IN pv_area int(10)
                            , IN pv_building_name varchar(40)
                            , IN pv_building_category_id int(9)
                            , IN pv_built varchar(30)
							, OUT rv_company_id int(9)
)
BEGIN
	declare lcl_user_id int default 0;
	declare lcl_address_id int default 0;
	declare lcl_building_id int default 0;
	declare max_company_id int default 0;
	call add_company(pv_flag, pv_company_name, pv_timezone, rv_company_id);
	call add_user_with_address(pv_dob, pv_email_address, pv_first_name, pv_flag
	, pv_last_name, pv_user_id, pv_password, pv_phone_number, pv_start_date, pv_rrole_id
	, pv_username, pv_city, pv_state_id, pv_street_address, pv_zip_code, rv_company_id, lcl_user_id);
	-- update user set company_id = rv_company_id where id = lcl_user_id;
	select max(id) from address into lcl_address_id;
	call add_root_building(pv_area, pv_building_name, pv_building_category_id, 0, 0, lcl_address_id, pv_built, rv_company_id, lcl_user_id, lcl_building_id);
	select max(id) from company into max_company_id;
	update user set company_id = max_company_id where id = pv_user_id;
END//


CREATE OR REPLACE PROCEDURE get_companies_api(IN pv_user_id int(9)
						, IN pv_ssession_number int(9))
	BEGIN
    	declare lcl_auth int default 0;
    	declare lcl_company_id int default 0;
    	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    	IF lcl_auth = 1 THEN
		call get_companies(pv_user_id);
		END IF;
	END//

#78
CREATE OR REPLACE PROCEDURE get_companies(pv_user_id int(9))
	BEGIN
		declare lcl_rrole_id int default 0;
		declare lcl_my_company_id int default 0;
		select user.rrole_id INTO lcl_rrole_id FROM user where id = pv_user_id;
		select user.company_id into lcl_my_company_id from user where id = pv_user_id;
		IF lcl_rrole_id = 3 THEN
		SELECT company.id, company.name, company.timezone
		, company.flag, company.e_time
		, company.log_user_id
		FROM company;
		END IF;
		IF lcl_rrole_id < 3 THEN
		SELECT company.id, company.name, company.timezone
		, company.flag, company.e_time
		, company.log_user_id
		from company where id = lcl_my_company_id;
		END IF;
	END//


CREATE OR REPLACE PROCEDURE update_company_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_company_id int(9)
                                            , IN pv_new_name varchar(100)
                                            , IN pv_new_timezone int(2)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call update_company(pv_company_id, pv_new_name, pv_new_timezone);
    END IF;
END//


CREATE OR REPLACE PROCEDURe update_company(IN pv_company_id int(9)
                                            , IN pv_new_name varchar(100)
											, IN pv_new_timezone int(2)
                                            )
BEGIN
    update company SET 
    name = pv_new_name,
	timezone = pv_new_timezone
    WHERE id = pv_company_id;
END//