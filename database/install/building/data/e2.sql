insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("18/04/24","18/05/22",1098675,80,1,29,1098675,3.44365141598696,3.98,1269181,170506,13.4,11682.1291243343,11682.1291243343, 0, 217, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("18/05/23","18/06/21",1264200,87,1,30,1264200,3.83038537276358,4.29,1415551,151351,10.7,10369.711702176,22051.8408265104, 0, 218, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("18/06/22","18/07/24",1484100,90,1,33,1484100,4.08787082863646,4.42,1605477,121377,7.6,8316.05522391927,30367.8960504296, 0, 219, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("18/07/25","18/08/23",1287300,87,1,30,1287300,3.90037580316291,4.29,1415551,128251,9.1,8787.02935719186,39154.9254076215, 0, 220, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("18/08/24","18/09/24",1347075,80,1,32,1347075,3.82639444116005,3.98,1400476,53401,3.8,3658.725010459,42813.6504180805, 0, 221, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("18/09/25","18/10/23",1096425,68,1,29,1096425,3.43659908869639,3.45,1099150,2725,0.2,186.73245676137,43000.3828748419, 0, 222, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("18/10/24","18/11/20",1014300,54,1,28,1014300,3.29273161196869,3.86,1187581,173281,14.6,11872.2445369177,54872.6274117596, 0, 223, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("18/11/21","18/12/20",1095900,50,1,30,1095900,3.32045509413986,4.02,1325278,229378,17.3,15715.6750266556,70588.3024384152, 0, 224, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("18/12/21","19/01/22",1238100,47,1,33,1238100,3.41027752370783,4.14,1501423,263323,17.5,18041.4020862071,88629.7045246223, 0, 225, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("19/01/23","19/02/21",1116150,52,1,30,1116150,3.38181034156784,3.94,1298843,182693,14.1,12517.0988522943,101146.803376917, 0, 226, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("19/02/22","19/03/24",1081411,54,1,31,1081411,3.17085971213132,3.86,1314822,233411,17.8,15992.0027280776,117138.806104994, 0, 227, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);

insert into electricity (start, end, uusage,oat, building_id, service_days, adjusted_usage, norm_usage, model_usage, base_usage,  saved_usage, percent_savings, monthly_savings, cumulative_savings, adjustment, service_month_id, service_id, e_time, process_e_time, flag, company_id, log_user_id)
values("19/03/25","19/04/23",1000580,64.77,1,30,1000580,3.03164609735784,3.42,1130057,129477,11.5,8871.05105074944,126009.857155744, 0, 228, 1, UNIX_TIMESTAMP(NOW()), UNIX_TIMESTAMP(NOW()), 0, 1, 2);