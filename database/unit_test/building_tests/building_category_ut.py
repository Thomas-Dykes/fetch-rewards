import time, decimal, sys, unittest


sys.path.append("/home/ec2-user/pbc_portal/pbc")

import login
import connector as co
import pandas as pd


def add_building_category(cookie, name, description):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_building_category_api"
    params = [name, description, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    building_category_id = co.machine_add(jdat)
    return building_category_id


def get_building_categories(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_building_categories_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    building_categories = co.machine(jdat)
    for building_category in building_categories:
        print(f"building_category: {building_category}")
    return building_categories


def get_building_category(cookie, building_category_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_building_category_api"
    params = [building_category_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    building_category = co.machine(jdat)[0]
    print(f"building_category: {building_category}")
    return building_category


class buildingCategoryTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.building_category_id_one = None
        cls.building_category_id_two = None

    def test_add_1(self):
        entered_data = {
            "name": "commercial",
            "description": "Commercial building for commerce",
        }
        try:
            building_category_id = add_building_category(
                self.cookie, entered_data["name"], entered_data["description"]
            )
            time.sleep(0.1)

            building_category_data = get_building_category(
                self.cookie, building_category_id
            )
            for key in entered_data:
                self.assertEqual(
                    entered_data[key],
                    building_category_data[key],
                    f"{key} should be same",
                )

            self.test_name.append("test_add_1")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_building_category_api", "get_building_category_api"]
            )
            self.__class__.building_category_id_one = building_category_id

        except Exception as e:
            self.test_name.append("test_add_1")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_building_category_api", "get_building_category_api"]
            )
            raise Exception(repr(e))

    def test_add_2(self):
        entered_data = {
            "name": "personal",
            "description": "personal building for persoal use",
        }
        try:
            building_category_id = add_building_category(
                self.cookie, entered_data["name"], entered_data["description"]
            )
            time.sleep(0.1)
            building_category_data = get_building_category(
                self.cookie, building_category_id
            )
            for key in entered_data:
                self.assertEqual(
                    entered_data[key],
                    building_category_data[key],
                    f"{key} should be same",
                )
            self.test_name.append("test_add_2")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["add_building_category_api", "get_building_category_api"]
            )
            self.__class__.building_category_id_two = building_category_id

        except Exception as e:
            self.test_name.append("test_add_2")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["add_building_category_api", "get_building_category_api"]
            )
            raise Exception(e)

    def test_gets(self):

        building_category_one = {
            "id": None,
            "name": None,
            "description": None,
        }

        building_category_two = {
            "id": None,
            "name": None,
            "description": None,
        }
        try:
            for item in get_building_categories(self.cookie):
                if item["id"] == self.building_category_id_one:
                    building_category_one = item
                if item["id"] == self.building_category_id_two:
                    building_category_two = item

            building_1_by_get = get_building_category(
                self.cookie, self.building_category_id_one
            )
            building_2_by_get = get_building_category(
                self.cookie, self.building_category_id_two
            )

            for key in building_category_one:
                self.assertEqual(
                    building_category_one[key],
                    building_1_by_get[key],
                    f"{key} should be same",
                )
                self.assertEqual(
                    building_category_two[key],
                    building_2_by_get[key],
                    f"{key} should be same",
                )
            self.test_name.append("test_gets")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(
                ["get_building_categories_api", "get_building_category_api"]
            )
        except Exception as e:
            self.test_name.append("test_gets")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(
                ["get_building_categories_api", "get_building_category_api"]
            )
            raise Exception(repr(e))


if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = buildingCategoryTest()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())
