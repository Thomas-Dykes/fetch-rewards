#Iterate through the Json and load it.  Create one loop function for each table that reads data from a json and loads it into a database.  Use SQL to write the database and the tables
import json

import connector as co
import config


def add_receipt(id
            , bonusPointsEarned
            , bonusPointsEarnedReason
            , createDate
            , dateScanned
            , finishedDate
            , modifyDate
            , pointsAwardedDate
            , pointsEarned
            , purchaseDate
            , purchasedItemCount
            , rewardsReceiptStatus
            , totalSpent
            , userId
            , brandCode):
    proc_name = "add_user"
    params = [id, bonusPointsEarned, bonusPointsEarnedReason, createDate, dateScanned, finishedDate, modifyDate, pointsAwardedDate, pointsEarned, purchaseDate, purchasedItemCount, rewardsReceiptStatus, totalSpent, userId, brandCode]
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

receipts = []
with open("./receipts.json/receipts.json", "r") as f:
    for line in f.readlines():
        receipts.append(json.loads(line))
        print(json.loads(line))
        id = json.loads(line)["_id"]["$oid"]
        bonusPointsEarned = json.loads(line)["bonusPointsEarned"]
        createDate = json.loads(line)["createDate"]["$date"]
        dateScanned = json.loads(line)["dateScanned"]["$date"]
        finishedDate = json.loads(line)["finishedDate"]["$date"]
        modifyDate = json.loads(line)["modifyDate"]["$date"]
        pointsAwardedDate = json.loads(line)["pointsAwardedDate"]["$date"]
        pointsEarned = json.loads(line)["pointsEarned"]
        purchaseDate = json.loads(line)["purchaseDate"]
        purchasedItemCount = json.loads(line)["purchasedItemCount"]
        rewardsReceiptStatus = json.loads(line)["rewardsReceiptStatus"]
        totalSpent = json.loads(line)["totalSpent"]
        userId = json.loads(line)["userId"]
        # print(json.loads(line)["rewardsReceiptItemList"][0])
        try:
            brandCode = json.loads(line)["rewardsReceiptItemList"][0]["brandCode"]
        except KeyError:
            brandCode = 0
        mysql -u root fetch_rewards_request < 
        add_receipt(id, bonusPointsEarned, bonusPointsEarnedReason, createDate, dateScanned, finishedDate, modifyDate, pointsAwardedDate, pointsEarned, purchaseDate, purchasedItemCount, rewardsReceiptStatus, totalSpent, userId, brandCode)


# for r in receipts:
#     print(r.keys())
#     break
# receipt_items = set()
# for r in receipts:
#     try:
#         for item in r['rewardsReceiptItemList']:
#             for k in item.keys():
#                 receipt_items.add(k)
#     except KeyError:
#         pass

# print(f"receipt_items: {sorted(receipt_items)}")

print(receipts)
        