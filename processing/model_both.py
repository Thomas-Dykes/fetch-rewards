import csv
from scipy import optimize
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
import regressor as piecewise

import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config


def get_oat_between_e(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_oats_electricity_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine(jdat)
    for oat in oats:
        print(f"oat: {oat}")
    return oats


def get_equation(pv_x, pv_y):
    return process_model(piece_wise(pv_x, pv_y))


def piece_wise(pv_x, pv_y):
    print(f'pv_x: {pv_x}\n\npv_y: {pv_y})')
    model = piecewise.piecewise(pv_x, pv_y)
    return model


def process_model(model):
    segments = model.segments
    count = 0
    rv_model = {}
    for segment in segments:
        count += 1
        start = segment[0]
        end = segment[1]
        coef = segment[2]
        y_int = coef[0]
        slope = coef[1]
        rv_model[count] = {
            "id": count,
            "start": start,
            "end": end,
            "y_int": y_int,
            "slope": slope,
        }
    return rv_model

def get_norm_usage_between_e(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_norm_usages_electricity_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine(jdat)
    for norm_usage in norm_usages:
        print(f"norm_usage: {norm_usage}")
    return norm_usages

def calculate_baseline_electricity_price(building_id, service_month_id_start, service_month_id_end):
    proc_name = "calculate_baseline_electricity_price"
    params = [building_id, service_month_id_start, service_month_id_end, 0]
    jdat = {"name": proc_name, "params": params}
    price = co.machine_add(jdat)
    return price

def calculate_baseline_gas_price(building_id, service_month_id_start, service_month_id_end):
    proc_name = "calculate_baseline_gas_price"
    params = [building_id, service_month_id_start, service_month_id_end, 0]
    jdat = {"name": proc_name, "params": params}
    price = co.machine_add(jdat)
    return price

def get_oat_between_g(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_oats_gas_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine(jdat)
    for oat in oats:
        print(f"oat: {oat}")
    return oats

def get_norm_usage_between_g(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_norm_usages_gas_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine(jdat)
    for norm_usage in norm_usages:
        print(f"norm_usage: {norm_usage}")
    return norm_usages

def add_model(
    building_id, name, build_start, apply_start, apply_end, slope_h_e, slope_c_e, y_int_h_e, y_int_c_e, threshold_e, slope_h_g, slope_c_g, y_int_h_g, y_int_c_g, threshold_g, electricity_price, gas_price, log_user_id
):
    proc_name = "add_model"
    slope_h_e = float(round(slope_h_e, 5))
    slope_c_e = float(round(slope_c_e, 5))
    y_int_h_e = float(round(y_int_h_e, 5))
    y_int_c_e = float(round(y_int_c_e, 5))
    threshold_e = float(round(threshold_e, 5))
    slope_h_g = float(round(slope_h_g, 5))
    slope_c_g = float(round(slope_c_g, 5))
    y_int_h_g = float(round(y_int_h_g, 5))
    y_int_c_g = float(round(y_int_c_g, 5))
    threshold_g = float(round(threshold_g, 5))
    

    params = [
        building_id,
        name,
        build_start,
        apply_start,
        apply_end,
        slope_h_e,
        slope_c_e,
        y_int_h_e,
        y_int_c_e,
        threshold_e,
        electricity_price,
        slope_h_g,
        slope_c_g,
        y_int_h_g,
        y_int_c_g,
        threshold_g,
        gas_price,
        log_user_id,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    model_id = co.machine_add(jdat)
    print(f"model_id: {model_id}")
    return model_id


def line(equation):
    """
    seg 1
        x=0, y=xint
        x=end, y=mx+b
    """
    start = equation["start"]
    stop = equation["end"]
    y_int = equation["y_int"]
    slope = equation["slope"]

    x0 = start
    y0 = (slope * x0) + y_int

    x1 = stop
    y1 = (slope * x1) + y_int

    rv_x = [x0, x1]
    rv_y = [y0, y1]
    return rv_x, rv_y

def predict(pv_x, cooling, heating):
    # mid = cooling["end"]
    init = cooling["start"]
    end = heating["end"]

    mid = (cooling["y_int"] - heating["y_int"]) / (heating["slope"] - cooling["slope"])

    if pv_x > init:
        if pv_x < end:
            if pv_x > mid:
                # cooling
                start = 99999
                stop = cooling["end"]
                y_int = cooling["y_int"]
                slope = cooling["slope"]
                y = slope * pv_x + y_int

            if pv_x <= mid:
                start = heating["start"]
                stop = 99999
                y_int = heating["y_int"]
                slope = heating["slope"]
                y = slope * pv_x + y_int

            return y
    return 777

def build_model(pv_x_list_e, pv_y_list_e, pv_x_list_g, pv_y_list_g, name, service_month_id_build_start, service_month_id_build_end, service_month_id_apply_start, service_month_id_apply_end, electricity_price, gas_price, building_id, user_id):

    x_list_e = np.array(pv_x_list_e, dtype=float)
    y_list_e = np.array(pv_y_list_e, dtype=float)
    print("LENGTH OF X:")
    print(len(x_list_e))
    print("LENGTH OF Y:")
    print(len(y_list_e))
    path = "./graph.png"

    model = get_equation(x_list_e, y_list_e)

    first = model[1]
    second = model[2]

    midpoint = (first["y_int"] - second["y_int"]) / (second["slope"] - first["slope"])
    print(first["y_int"])
    print(second["y_int"])
    print(second["slope"])
    print(first["slope"])
    print(f"THRESHOLD: {midpoint}")

    print(f"first: {first}")
    print(f"second: {second}")

    # if(midpoint > second["end"]):
    #     second["start"] = first["start"]
    #     second["end"] = midpoint
    #     second["y_int"] = first["y_int"]
    #     second["slope"] = first["slope"]


    print(f"first: {first}")
    print(f"second: {second}")

    if (midpoint < second["end"]):
        first["end"] = midpoint
        second["start"] = midpoint
        print(f"midpoint: {midpoint}")

    print(f"first: {first}")
    print(f"second: {second}")

    first_line = line(first)
    second_line = line(second)

    plt.scatter(x_list_e, y_list_e)

    x_list_g = np.array(pv_x_list_g, dtype=float)
    y_list_g = np.array(pv_y_list_g, dtype=float)
    print("LENGTH OF X:")
    print(len(x_list_g))
    print("LENGTH OF Y:")
    print(len(y_list_g))
    # path = "./graph.png"

    m = (len(x_list_g) * np.sum(x_list_g*y_list_g) - np.sum(x_list_g) * np.sum(y_list_g)) / (len(x_list_g)*np.sum(x_list_g*x_list_g) - np.sum(x_list_g) ** 2)
    print(f"m: {m}")
    b = (np.sum(y_list_g) - m *np.sum(x_list_g))/len(x_list_g)

    # print(f"M: {m}")
    # plt.savefig('baylor_electric.png')

    model_id = add_model(
        building_id,
        name,
        service_month_id_build_end, 
        service_month_id_apply_start, 
        service_month_id_apply_end,
        first["slope"],
        second["slope"],
        first["y_int"],
        second["y_int"],
        first["end"],
        m,
        0,
        b,
        0,
        0,
        electricity_price,
        gas_price,
        user_id
    )

    return model_id

def main(user_id, ssession_number, building_id, electricity_price, gas_price, name, model_build_end, model_apply_start, model_apply_end):

    count = 0
    # print(f"model_build_end: {model_build_end}")
    # print(f"building_id: {building_id}")
    # print(f"user_id: {user_id}")
    # print(f"name: {name}")
    # print(f"model_apply_start: {model_apply_start}")
    # e_ids = get_electricity_ids(building_id, int(model_build_end)-12, int(model_build_end))
    #add data
    # for row in data:
    #validate data
    lcl_x_e = get_oat_between_e(user_id, building_id, model_build_end-12, model_build_end)
    lcl_y_e = get_norm_usage_between_e(user_id, building_id, model_build_end-12, model_build_end)

    # for e_id in e_ids:
    #     lcl_x = get_oat_between(user_id, building_id, model_build_end-12, model_build_end)
    #     print(f"lcl_x: {lcl_x}")
    lcl_x_return_e = []
    for row in lcl_x_e:
        lcl_x_return_e.append(float(row["oat"]))

    print(f"lcl_x_return: {lcl_x_return_e}")
        # lcl_y = get_norm_usage_between(user_id, building_id, model_build_end-12, model_build_end)
        # print(f"lcl_y: {lcl_y}")
    lcl_y_return_e = []
    for row2 in lcl_y_e:
        lcl_y_return_e.append(float(row2["norm_usage"]))
    print(f"lcl_y_return: {lcl_y_return_e}")

    lcl_x_g = get_oat_between_g(user_id, building_id, model_build_end-12, model_build_end)
    lcl_y_g = get_norm_usage_between_g(user_id, building_id, model_build_end-12, model_build_end)

    # for e_id in e_ids:
    #     lcl_x = get_oat_between(user_id, building_id, model_build_end-12, model_build_end)
    #     print(f"lcl_x: {lcl_x}")
    lcl_x_return_g = []
    for row in lcl_x_g:
        lcl_x_return_g.append(float(row["oat"]))

    print(f"lcl_x_return: {lcl_x_return_g}")
        # lcl_y = get_norm_usage_between(user_id, building_id, model_build_end-12, model_build_end)
        # print(f"lcl_y: {lcl_y}")
    lcl_y_return_g = []
    for row2 in lcl_y_g:
        lcl_y_return_g.append(float(row2["norm_usage"]))
    print(f"lcl_y_return: {lcl_y_return_g}")

    model_id = build_model(lcl_x_return_e, lcl_y_return_e, lcl_x_return_g, lcl_y_return_g, name, model_build_end-12, model_build_end, model_apply_start, model_apply_end, electricity_price, gas_price, building_id, user_id)

    return model_id


if __name__ == "__main__":
    data = []
    user_id = 6
    building_id = 23
    ssession_number = 6
    model_build_end = 590
    model_apply_start = 591
    model_apply_end = 605
    name = 'test_name'
    electricity_price = .072
    gas_price = 4.2

    # with open('electricity.csv')as f:
    #     readCSV = csv.reader(f, delimiter=',')
    #     next(readCSV, None)
    #     for row in readCSV:
    #         data.append(row)

    main(user_id, ssession_number, building_id, electricity_price, gas_price, name, model_build_end, model_apply_start, model_apply_end)