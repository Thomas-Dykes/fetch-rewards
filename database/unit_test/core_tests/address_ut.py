import time, decimal, sys
import user_log_ut as pl
import outbound_ut as o
import state_ut
import user_ut

sys.path.append("/home/ec2-user/pbc_portal/pbc")
# sys.path.append("../../../magyk")
import login
import connector as co
import config


def delete_address(cookie, address_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "delete_address_api"
    params = [address_id, 0, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    outbound_id = co.machine_add(jdat)
    print(f"outbound_id: {outbound_id}")
    return outbound_id


def add_address(cookie, city, latitude, longitude, state_id, street_address, zip_code):
    user_id = cookie["user_id"]
    print("THE user_ID IS")
    print(user_id)
    ssession_number = cookie["ssession_number"]
    print("THE SESSION NUMBER IS:")
    print(ssession_number)
    proc_name = "add_address_api"
    params = [
        city,
        "0",
        latitude,
        user_id,
        longitude,
        ssession_number,
        state_id,
        street_address,
        zip_code,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    print(f"add address jdat: {jdat} \n \n")
    address_id = co.machine_add(jdat)
    print(f"address id: {address_id}")
    return address_id


def get_addresses(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_addresses_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    addresses = co.machine(jdat)
    for address in addresses:
        print(f"address: {address}")
    return addresses


def get_addresses_states(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_addresses_states_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    addresses = co.machine(jdat)
    for address in addresses:
        print(f"address: {address}")
    return addresses


def get_addresses_by_state(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_addresses_by_state_api"
    params = [user_id, ssession_number, state_id]
    jdat = {"name": proc_name, "params": params}
    addresses = co.machine(jdat)
    for address in addresses:
        print(f"address: {address}")
    return addresses


def get_address(cookie, address_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_address_api"
    params = [address_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    try:
        address = co.machine(jdat)[0]
        print(f"address: {address}")
    except Exception as e:
        print(f"exception {e}")
        print(f"params: {params}")
        # time.sleep(5)
        return {
            "id": -6,
            "city": "null",
            "zip_code": "null",
            "state_id": "null",
            "street_address": "null",
            "latitude": "null",
            "longitude": "null",
        }
    return address


def get_address_count():
    proc_name = "get_address_count"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    count = co.machine_add(jdat)
    print(f"local count: {count}")
    return count


def get_coworker_address_count(user_id):
    proc_name = "get_coworker_address_count"
    params = [user_id, 0]
    jdat = {"name": proc_name, "params": params}
    count = co.machine_add(jdat)
    print(f"local coworker count: {count}")
    return count

def get_root_address_id(company_id):
    proc_name = "get_root_address_id"
    params = [company_id, 0]
    jdat = {"name": proc_name, "params": params}
    address_id = co.machine_add(jdat)
    print(f"root address id: {address_id}")
    return address_id

def test_delete_foreign_key_duplicate_error(cookie, pv_address_id):
    print("test delete address needs to have proper return")
    status_code = delete_address(cookie, pv_address_id)
    if status_code == -1:
        return {
            "test": "test_foreign_key_duplicate_error",
            "result": True,
            "table": "address",
            "proc_index_id": [4, 6],
        }
    return {
        "test": "test_foreign_key_duplicate_error",
        "result": False,
        "table": "address",
        "proc_index_id": [4, 6],
    }


def test_add_1(cookie):
    city = "tijuana"
    latitude = 12.235000
    longitude = 23.555000
    state_id = 1
    street_address = "1235 Whatever Street"
    zip_code = 12345
    print("add test 1")
    address_id_one = add_address(
        cookie, city, latitude, longitude, state_id, street_address, zip_code
    )
    time.sleep(0.1)
    print(" \n \n ")
    print("get test 1")
    address = get_address(cookie, address_id_one)
    # print(f"address: {address}")
    lcl_city = address["city"]
    lcl_latitude = address["latitude"]
    lcl_longitude = address["longitude"]
    lcl_state_id = address["state_id"]
    lcl_street_address = address["street_address"]
    lcl_zip_code = address["zip_code"]
    print(
        f"lcl_city: {lcl_city}, lcl_latitude: {lcl_latitude}, lcl_longitude: {lcl_longitude}, lcl_state_id: {lcl_state_id}, lcl_street_address: {lcl_street_address}, lcl_zip_code: {lcl_zip_code}"
    )
    if lcl_city == city:
        print("city match")
        if str(lcl_latitude) == (str(latitude) + "000"):
            print("latitude match")
            if str(lcl_longitude) == (str(longitude) + "000"):
                print("longitude match")
                if lcl_state_id == state_id:
                    print("state_id match")
                    if lcl_street_address == street_address:
                        print("street_address match")
                        if str(lcl_zip_code) == str(zip_code):
                            print("zip_code match")
                            return (
                                address_id_one,
                                {
                                    "test": "test_add_1",
                                    "result": True,
                                    "table": "address",
                                    "proc_index_id": [10, 11, 8, 9],
                                },
                            )
    return (
        address_id_one,
        {
            "test": "test_add_1",
            "result": False,
            "table": "address",
            "proc_index_id": [10, 11, 8, 9],
        },
    )


def test_add_2(cookie):
    city = "djibouti"
    latitude = 98.7870000
    longitude = 45.5550000
    state_id = 2
    street_address = "9887 Fairy Lane"
    zip_code = 98787
    print("add test 2")
    address_id_two = add_address(
        cookie, city, latitude, longitude, state_id, street_address, zip_code
    )
    time.sleep(0.1)
    print(f"id: {id}")
    print(" \n \n ")
    print("get test 2")
    address = get_address(cookie, address_id_two)
    lcl_city = address["city"]
    lcl_latitude = address["latitude"]
    lcl_longitude = address["longitude"]
    lcl_state_id = address["state_id"]
    lcl_street_address = address["street_address"]
    lcl_zip_code = address["zip_code"]
    print(
        f"lcl_city: {lcl_city}, lcl_latitude: {lcl_latitude}, lcl_longitude: {lcl_longitude}, lcl_state_id: {lcl_state_id}, lcl_street_address: {lcl_street_address}, lcl_zip_code: {lcl_zip_code}"
    )
    if lcl_city == city:
        print("city match")
        if str(lcl_latitude) == (str(latitude) + "000"):
            print("latitude match")
            if str(lcl_longitude) == (str(longitude) + "000"):
                print("longitude match")
                if lcl_state_id == state_id:
                    print("state_id match")
                    if lcl_street_address == street_address:
                        print("street_address match")
                        if str(lcl_zip_code) == str(zip_code):
                            print("zip_code match")
                            return (
                                address_id_two,
                                {
                                    "test": "test_add_2",
                                    "result": True,
                                    "table": "address",
                                    "proc_index_id": [10, 11, 8, 9],
                                },
                            )
    return (
        address_id_two,
        {
            "test": "test_add_2",
            "result": False,
            "table": "address",
            "proc_index_id": [10, 11, 8, 9],
        },
    )


def test_gets(cookie, a, b):
    # a, b are valid address ids

    address_one = {
        "id": -6,
        "city": "null",
        "zip_code": "null",
        "state_id": "null",
        "street_address": "null",
        "latitude": "null",
        "longitude": "null",
    }
    address_two = {
        "id": -6,
        "city": "null",
        "zip_code": "null",
        "state_id": "null",
        "street_address": "null",
        "latitude": "null",
        "longitude": "null",
    }
    for i in get_addresses(cookie):
        if i["id"] == a:
            address_one = i
        if i["id"] == b:
            address_two = i

    add_1 = get_address(cookie, a)
    add_2 = get_address(cookie, b)

    print(f"\n add 1{add_1} \n\n address_one: {address_one}")
    if add_1["city"] == address_one["city"]:
        print("city 1 match")
        if (str(add_1["latitude"])) == str(address_one["latitude"]):
            if (str(add_1["longitude"])) == str(address_one["longitude"]):
                print("long one match")
                if add_1["state_id"] == address_one["state_id"]:
                    print("state id match")
                    if add_1["street_address"] == address_one["street_address"]:
                        if str(add_1["zip_code"]) == str(address_one["zip_code"]):
                            print("zip 1 match")
                            if add_2["city"] == address_two["city"]:
                                if (str(add_2["latitude"])) == str(
                                    address_two["latitude"]
                                ):
                                    if (str(add_2["longitude"])) == str(
                                        address_two["longitude"]
                                    ):
                                        print("state id 2 match")
                                        if add_2["state_id"] == address_two["state_id"]:
                                            if (
                                                add_2["street_address"]
                                                == address_two["street_address"]
                                            ):
                                                if str(add_2["zip_code"]) == str(
                                                    address_two["zip_code"]
                                                ):

                                                    return {
                                                        "test": "test_gets",
                                                        "result": True,
                                                        "table": "address",
                                                        "proc_index_id": [3, 2],
                                                    }

    return {
        "test": "test_gets",
        "result": False,
        "table": "address",
        "proc_index_id": [3, 2],
    }


def test_get_addresses_states(cookie, a, b):
    # a, b are valid address ids
    address_states_one = {
        "id": -6,
        "city": "null",
        "zip_code": "null",
        "state_id": "null",
        "state_name": "null",
        "street_address": "null",
        "latitude": "null",
        "longitude": "null",
    }
    address_states_two = {
        "id": -6,
        "city": "null",
        "zip_code": "null",
        "state_id": "null",
        "state_name": "null",
        "street_address": "null",
        "latitude": "null",
        "longitude": "null",
    }
    for i in get_addresses_states(cookie):
        if i["id"] == a:
            address_states_one = i
        if i["id"] == b:
            address_states_two = i

    add_1 = get_address(cookie, a)
    state_name = state_ut.get_state(add_1["state_id"])["name"]
    add_2 = get_address(cookie, b)
    state_name_2 = state_ut.get_state(add_2["state_id"])["name"]
    print(f"add_1: {add_1}")
    if add_1["city"] == address_states_one["city"]:
        if (str(add_1["latitude"])) == str(address_states_one["latitude"]):
            if (str(add_1["longitude"])) == str(address_states_one["longitude"]):
                print("long lat match")
                if add_1["street_address"] == address_states_one["street_address"]:
                    if str(add_1["zip_code"]) == str(address_states_one["zip_code"]):
                        print(f"state name: {state_name}")
                        if state_name == address_states_one["state_name"]:
                            print("state match")
                            if add_2["city"] == address_states_two["city"]:
                                if (str(add_2["latitude"])) == str(
                                    address_states_two["latitude"]
                                ):
                                    if (str(add_2["longitude"])) == str(
                                        address_states_two["longitude"]
                                    ):
                                        if (
                                            add_2["street_address"]
                                            == address_states_two["street_address"]
                                        ):
                                            if str(add_2["zip_code"]) == str(
                                                address_states_two["zip_code"]
                                            ):
                                                print("zip two match")
                                                if (
                                                    state_name_2
                                                    == address_states_two["state_name"]
                                                ):
                                                    return {
                                                        "test": "test_addresses_states",
                                                        "result": True,
                                                        "table": "address",
                                                        "proc_index_id": [4, 5],
                                                    }
    return {
        "test": "test_addresses_states",
        "result": False,
        "table": "address",
        "proc_index_id": [4, 5],
    }


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        #   print(f"test: {test}")
        t_table = test["table"]
        t_test = test["test"]
        t_result = test["result"]
        t_proc_index_id = test["proc_index_id"]
        print(
            f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
        )


def machine(cookie):
    tests = []
    print(" \n \n ")
    lcl_id, result = test_add_1(cookie)
    tests.append(result)
    lcl_id_2, result_2 = test_add_2(cookie)
    tests.append(result_2)
    print(" \n \n ")
    tests.append(test_gets(cookie, lcl_id, lcl_id_2))
    tests.append(test_get_addresses_states(cookie, lcl_id, lcl_id_2))

    try:
        address_id = user_ut.get_my_info(cookie)["address_id"]
    except TypeError:
        address_id = 0
    try:
        tests.append(test_delete_foreign_key_duplicate_error(cookie, address_id))
    except pymysql.Error as e:
        print(f"address ut error: [489] {e}")

    delete_address(cookie, lcl_id)
    delete_address(cookie, lcl_id_2)
    return tests


def main():
    cookie = login.machine()
    print(cookie)
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()


