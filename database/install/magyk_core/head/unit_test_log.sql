#unit_test_log_count

call add_proc_index(
73
, 'return the number of rows in the unit_test_log table'
, 0
, '{}'
, '{"rv_out":"int(9)"}'
, '["unit_test_log"]'
, 'get_unit_test_log_count'
, 0
);

#get_unit_test_logs

call add_proc_index(
74
, 'returns all values from the unit_test_log table'
, 0
, '{"pv_user_id":"int(9)"}'
, '{}'
, '["unit_test_log"]'
, 'get_unit_test_logs'
, 0
);

# add_unit_test_log

call add_proc_index(
75
, 'adds a new row to the unit_test_log table'
, 0
, '{"pv_flag": "int(3)", "pv_user_id": "int(9)", "pv_passed": "int(1)", "pv_proc_index_id": "int(9)", "pv_test_group": "int(9)", "pv_ttable":"varchar(20)"}'
, '{}'
, '["unit_test_log"]'
, 'add_unit_test_log'
, 0
);