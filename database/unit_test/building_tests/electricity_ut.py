from datetime import date
from logging import ERROR
import pandas as pd
import time, decimal, sys, unittest

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import login
import connector as co
import config


# SQL Procedure : It doesn't have any "OUT" variable
def find_electricity_costs(cookie, building_id, start, end):
    user_id = cookie["user_id"]
    ssession_number = ["ssession_number"]
    proc_name = "find_electricity_costs_api"
    params = [user_id, ssession_number, building_id, start, end, 0]
    jdat = {"name": proc_name, "params": params}
    electricity_costs = co.machine_add(jdat)
    return electricity_costs


def print_electricity_cost_one_variable(cookie, building_id, service_month_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "print_electricity_cost_one_variable_api"
    params = [building_id, service_month_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    cost = co.machine_add(jdat)
    return cost


def print_now_and_before_costs(cookie, building_id, service_month_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "print_now_and_before_costs_api"
    params = [building_id, service_month_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    now_and_before_costs = co.machine_add(jdat)
    return now_and_before_costs


def print_now_and_before_electricity_cost_one_variable_monthly(
    cookie,
    building_id,
    service_month_id,
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "print_now_and_before_electricity_cost_one_variable_monthly_api"
    params = [building_id, service_month_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    now_and_before_costs = co.machine_add(jdat)
    return now_and_before_costs


def calculate_model_usage_electricity(cookie, electricity_id, model_log_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_model_usage_electricity_api"
    params = [electricity_id, model_log_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    model_usage = co.machine_add(jdat)
    return model_usage


def edit_base_usage_electricity(cookie, electricity_id, model_log_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "edit_base_usage_electricity_api"
    params = [electricity_id, model_log_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage


def calculate_base_usage(cookie, electricity_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_base_usage_electricity_api"
    params = [electricity_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage


def calculate_saved_usage(cookie, electricity_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_saved_usage_electricity_api"
    params = [electricity_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    saved_usage = co.machine_add(jdat)
    return saved_usage


def calculate_percent_savings(cookie, electricity_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_percent_savings_electricity_api"
    params = [electricity_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    percent_savings = co.machine_add(jdat)
    return percent_savings


def calculate_norm_usage(cookie, electricity_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_norm_usage_electricity_api"
    params = [electricity_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    norm_usage = co.machine_add(jdat)
    return norm_usage


# SQL Procedure : It doesn't have any "OUT" variable
def get_electricity_ids(cookie, start, end, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_electricity_ids_api"
    params = [start, end, user_id, building_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    ids = co.machine(jdat)
    for id in ids:
        print(f"id: {id}")
    return ids


# SQL Procedure : The procedure is commented in the sql file
def calculate_model_average_electricity(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_model_average_electricity"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    average = co.machine_add(jdat)
    return average


# SQL Procedure : The procedure is commented in the sql file
def calculate_electricity_cost(cookie, electricity_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_electricity_cost_api"
    params = [electricity_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    electricity_cost = co.machine_add(jdat)
    return electricity_cost


def calculate_monthly_savings(
    cookie, electricity_id, building_id, service_month_id_start, service_month_id_end
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_monthly_savings_electricity_api"
    params = [
        electricity_id,
        building_id,
        service_month_id_start,
        service_month_id_end,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    monthly_savings = co.machine_add(jdat)
    return monthly_savings


# SQL Procedure : It doesn't have any "OUT" variable
def add_cumulative_savings(cookie, electricity_id, cumulative_savings):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_cumulative_savings_electricity_api"
    params = [electricity_id, cumulative_savings, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    monthly_savings = co.machine_add(jdat)
    return monthly_savings


# SQL Procedure : The sql procedure used here is commented in the sql file
def add_electricity_pre_period(
    cookie,
    usage,
    model_log_id,
    adjustment,
    building_id,
    oat,
    start,
    end,
    dollar_kwh,
    service_id,
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_electricity_pre_period_api"
    params = [
        usage,
        model_log_id,
        adjustment,
        building_id,
        oat,
        start,
        end,
        dollar_kwh,
        service_id,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    electricity_id = co.machine_add(jdat)
    return electricity_id


# SQL Procedure : It doesn't have any "OUT" variable
def get_electricity_graph_data(
    cookie, building_id, service_month_start_id, service_month_end_id
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_electricity_graph_data_api"
    params = [
        user_id,
        ssession_number,
        building_id,
        service_month_start_id,
        service_month_end_id,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    graph_data = co.machine(jdat)
    for graph_point in graph_data:
        print(f"graph_point: {graph_point}")
    return graph_data


# SQL Procedure : It doesn't have any "OUT" variable
def get_electricity_price_graph_data(
    cookie, building_id, service_month_start_id, service_month_end_id
):
    user_id = cookie["user_id"]
    ssesion_number = cookie["ssession_number"]
    proc_name = "get_electricity_price_graph_data_api"
    params = [
        user_id,
        ssesion_number,
        building_id,
        service_month_start_id,
        service_month_end_id,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    price_graph_data = co.machine_add(jdat)
    return price_graph_data


# SQL Procedure : It doesn't have any "OUT" variable
def get_electricity_usage_cost_graph_data(
    cookie, building_id, service_month_start_id, service_month_end_id
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_electricity_usage_cost_graph_data_api"
    params = [
        user_id,
        ssession_number,
        building_id,
        service_month_start_id,
        service_month_end_id,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    usage_cost_graph_data = co.machine_add(jdat)
    return usage_cost_graph_data


# SQL Procedure : It doesn't have any "OUT" variable
def get_electricity_norm_usage_oat_graph_data(
    cookie, building_id, service_month_start_id, service_month_end_id
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_electricity_norm_usage_oat_graph_data_api"
    params = [
        user_id,
        ssession_number,
        building_id,
        service_month_start_id,
        service_month_end_id,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    norm_usage_oat_graph_data = co.machine_add(jdat)
    return norm_usage_oat_graph_data


# SQL Procedure : It doesn't have any "OUT" variable
def get_oats_electricity_between(
    cookie, building_id, service_month_id_begin, service_month_id_end
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_oats_electricity_between_api"
    params = [
        building_id,
        service_month_id_begin,
        service_month_id_end,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    oats_electricity_between = co.machine_add(jdat)
    return oats_electricity_between


# SQL Procedure : It doesn't have any "OUT" variable
def get_norm_usages_between_electricity(
    cookie, building_id, service_month_id_begin, service_month_id_end
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_norm_usages_between_electricity_api"
    params = [
        building_id,
        service_month_id_begin,
        service_month_id_end,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    norm_usage_between_electricity = co.machine_add(jdat)
    return norm_usage_between_electricity


# SQL Procedure : It doesn't have any "OUT" variable
def get_savings_electricity_between(
    cookie, building_id, service_month_id_start, service_month_end
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_savings_electricity_between_api"
    params = [
        service_month_id_start,
        service_month_end,
        building_id,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    savings_electricity_between = co.machine_add(jdat)
    return savings_electricity_between


# SQL Procedure : It doesn't have any "OUT" variable
def get_service_month_year_electricity_between(
    cookie, building_id, service_month_id_begin, service_month_end
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_service_month_year_electricity_between_api"
    params = [
        service_month_id_begin,
        service_month_end,
        building_id,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    service_month_year_electricity_between = co.machine_add(jdat)
    return service_month_year_electricity_between


# SQL Procdure : The procedure is commented in the sql file
def calculate_electricity_cost(cookie, electricity_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "calculate_electricity_cost_api"
    params = [electricity_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    electricity_cost = co.machine_add(jdat)
    return electricity_cost


# SQL Procedure : It doesn't have any "OUT" variable
def get_model_usages(cookie, service_month_id, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_model_usages_electricity_api"
    params = [service_month_id, building_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    model_usages = co.machine_add(jdat)
    return model_usages


# SQL Procedure : No sql procedure with the following proc_name
def get_norm_usages(cookie, period_date, period_length):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_norm_usages_electricity_api"
    params = [period_date, period_length, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine_add(jdat)
    return norm_usages


# SQL Procedure : No sql procedure with the following proc_name
def get_oats(cookie, period_date, period_length):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_oats_electricity_api"
    params = [period_date, period_length, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine_add(jdat)
    return oats


# SQL Procedure : It doesn't have any "OUT" variable
def get_electricity_usage_data(cookie, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_electricity_usage_data_api"
    params = [user_id, ssession_number, building_id, 0]
    jdat = {"name": proc_name, "params": params}
    usage_data = co.machine(jdat)
    for usage_point in usage_data:
        print(f"usage_point: {usage_point}")
    return usage_data


class electricityTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.electricity_id_one = None
        cls.electricity_id_two = None
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.model_log_id = None

    def test_add_1_electricity_period(self):
        entered_data = {
            "usage": 10,
            "model_log_id": 20,
            "adjustment": 10,
            "building_id": 100,
            "oat": 10,
            "start": date(2021, 1, 1),
            "end": date(2021, 6, 2),
            "dollar_kwh": 10.0,
            "service_id": 100,
        }
        electricity_id = add_electricity_pre_period(
            self.cookie,
            entered_data["usage"],
            entered_data["model_log_id"],
            entered_data["adjustment"],
            entered_data["building_id"],
            entered_data["oat"],
            entered_data["start"],
            entered_data["end"],
            entered_data["dollar_kwh"],
            entered_data["service_id"],
        )
        # ele_list = get_electricity_ids(
        #     self.cookie, entered_data["start"], entered_data["end"]
        # )
        # pymysql.err.OperationalError: (1318, 'Incorrect number of arguments for PROCEDURE pbc.get_electricity_ids; expected 3, got 5')

        self.__class__.electricity_id_one = electricity_id

    def test_add_2_electricity_period(self):
        entered_data = {
            "usage": 20,
            "model_log_id": 20,
            "adjustment": 20,
            "building_id": 100,
            "oat": 20,
            "start": date(2021, 2, 2),
            "end": date(2021, 7, 3),
            "dollar_kwh": 20.0,
            "service_id": 100,
        }
        electricity_id = add_electricity_pre_period(
            self.cookie,
            entered_data["usage"],
            entered_data["model_log_id"],
            entered_data["adjustment"],
            entered_data["building_id"],
            entered_data["oat"],
            entered_data["start"],
            entered_data["end"],
            entered_data["dollar_kwh"],
            entered_data["service_id"],
        )
        self.__class__.electricity_id_two = electricity_id

    def test_model_usage_1(self):
        self.model_log_id = 100  # Setting up model log id
        service_start = "2012/02/01"
        service_end = "2021/02/21"

        # Saved Usage
        saved_usage = calculate_saved_usage(self.cookie, self.electricity_id_one)
        self.assertEqual(saved_usage, -10, "Should be 10")

        # Norm Usage
        norm_usage = calculate_norm_usage(self.cookie, self.electricity_id_one)
        self.assertEqual(norm_usage, None, "It's coming out None")
        # Output is coming None

        # Monthly Savings
        # mothly_savings = calculate_monthly_savings(
        #     self.cookie, self.electricity_id_one, 100, service_start, service_end
        # )
        # ERROR : pymysql.err.DataError: (1265, "Data truncated for column 'pv_service_month_id_start' at row 1")

        # Percent Savings
        # percent_savings = calculate_percent_savings(
        #     self.cookie, self.electricity_id_one
        # )
        # ERROR : 1414, 'OUT or INOUT argument 2 for routine pbc.calculate_percent_savings_electricity is not a variable or NEW pseudo-variable in BEFORE trigger

        # Electricity Graph Data
        # graph_data = get_electricity_graph_data(
        #     self.cookie, self.electricity_id_one, service_start, service_end
        # )
        # print(graph_data)
        # ERROR : pymysql.err.DataError: (1265, "Data truncated for column 'pv_service_month_id_start' at row 1")

    def test_model_usage_2(self):
        self.model_log_id = 100  # Setting up model log id

        # Saved Usage
        saved_usage = calculate_saved_usage(self.cookie, self.electricity_id_two)
        self.assertEqual(saved_usage, -20, "Should be -20")

        # Norm Usage
        norm_usage = calculate_norm_usage(self.cookie, self.electricity_id_two)
        self.assertEqual(norm_usage, None, "It's coming out to be None")
        # Output is coming None

        # Monthly Savings
        # mothly_savings = calculate_monthly_savings(
        #     self.cookie, self.electricity_id_two, 100, service_start, service_end
        # )
        # ERROR : pymysql.err.DataError: (1265, "Data truncated for column 'pv_service_month_id_start' at row 1")

        # Percent Savings
        # percent_savings = calculate_percent_savings(
        #     self.cookie, self.electricity_id_two
        # )
        # ERROR : 1414, 'OUT or INOUT argument 2 for routine pbc.calculate_percent_savings_electricity is not a variable or NEW pseudo-variable in BEFORE trigger


if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = electricityTests()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())
