set -exou pipefail

echo "begin body deploy"

echo "set variables"
DB_USER=$2
HOST=$1
DB=$3
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/proc_index.sql || echo "proc index install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/unit_test_log.sql || echo "unit test log install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/country.sql || echo "country install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/state.sql || echo "state install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/address.sql || echo "address install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/company.sql || echo "company install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/user_log.sql || echo "user_log install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/user.sql || echo "user install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/ssession.sql || echo "ssession install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/jsonify.sql || echo "jsonify failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/outbound.sql || echo "outbound failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/body/api_test.sql || echo "api test"

echo "end body deploy"