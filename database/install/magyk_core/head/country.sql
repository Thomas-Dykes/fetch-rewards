#get_countrys_api

call add_proc_index(
14
, 'checks authentication, then runs get_countries'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["country"]'
, 'get_countrys_api'
, 1
);

#add_country_api

call add_proc_index(
14
, 'checks authentication, then runs add_country'
, 0
, '{"pv_flag": "int(3)", "pv_name": "varchar(100)", "pv_abbrev": "varchar(10)"}'
, '{"rv_company_id": "int(5)"}'
, '["country"]'
, 'add_country_api'
, 1
);