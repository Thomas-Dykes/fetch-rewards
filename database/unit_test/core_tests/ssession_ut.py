import time, decimal, sys
import company_ut as company
import user_log_ut as pl
import user_ut

sys.path.append("/home/ec2-user/pbc_portal/pbc")
import login as ll
import connector as co
import config


def delete_ssession(cookie, ssession_id):
    ssession_number = cookie["ssession_number"]
    user_id = cookie["user_id"]
    proc_name = "delete_ssession"
    params = [0, user_id, ssession_id, 0]
    jdat = {"name": proc_name, "params": params}
    status_code = co.machine_add(jdat)
    print(f"status: {status_code}")
    return status_code


def get_company_by_name(name):
    return company.get_company_by_name(name)


def get_company_ssession_count(company_id):
    proc_name = "get_company_ssession_count"
    params = [company_id, 0]
    jdat = {"name": proc_name, "params": params}
    count = co.machine_add(jdat)
    print(f"company session count: {count}")
    return count


def get_ssessions(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_ssessions_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    ssessions = co.machine(jdat)
    for ssession in ssessions:
        print(f"ssession: {ssession}")
    return ssessions


def get_ssession(cookie, ssession_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_ssession_api"
    params = [user_id, ssession_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    ssession = co.machine(jdat)[0]
    print(f"ssession: {ssession}")
    return ssession


def add_ssession(
    cookie, session_user_id, ssession_number, ssesion_length, start_time
):
    user_id = cookie["user_id"]
    proc_name = "add_ssession"
    params = [
        0,
        session_user_id,
        ssesion_length,
        ssession_number,
        start_time,
        user_id,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    ssession_id = co.machine_add(jdat)
    print(f"ssession id: {ssession_id}")
    return ssession_id


def login(username, password):
    proc_name = "login"
    params = [username, password, 0]
    jdat = {"name": proc_name, "params": params}
    cookie = ll.inbound_login(username, password)
    print(f"cookie: {cookie}")
    return cookie


def logout(cookie):
    proc_name = "logout"
    params = [cookie["user_id"], cookie["ssession_number"], 0]
    jdat = {"name": proc_name, "params": params}
    status = co.machine_add(jdat)
    print(f"logout status: {status}")
    return status


def test_add_1(cookie, user_id):
    ssession_number = 99494894
    ssession_length = 4949
    start_time = int(time.time())
    end_time = start_time + ssession_length
    print("add test 1")
    ssession_id = add_ssession(cookie, user_id, ssession_number, 1800, start_time)

    time.sleep(0.1)
    print(" \n \n ")
    print("get test 1")
    ssession = get_ssession(cookie, ssession_id)
    lcl_start_time = ssession["start_time"]
    lcl_end_time = ssession["end_time"]
    if int(ssession["start_time"]) == start_time:
        if user_id == ssession["user_id"]:
            print("end time")
            return (
                ssession_id,
                {
                    "test": "test_add_1",
                    "result": True,
                    "table": "ssession",
                    "proc_index_id": [146, 149],
                },
            )
    return (
        ssession_id,
        {
            "test": "test_add_1",
            "result": False,
            "table": "ssession",
            "proc_index_id": [146, 149],
        },
    )


def test_add_2(cookie, user_id):
    ssession_number = 994948789
    ssession_length = 4949
    start_time = int(time.time())
    end_time = start_time + ssession_length
    print("add test 2")
    ssession_id = add_ssession(cookie, user_id, ssession_number, 1800, start_time)

    time.sleep(0.1)
    print(" \n \n ")
    print("get test 2")
    ssession = get_ssession(cookie, ssession_id)
    lcl_start_time = ssession["start_time"]
    lcl_end_time = ssession["end_time"]
    if int(ssession["start_time"]) == start_time:
        if user_id == ssession["user_id"]:
            return (
                ssession_id,
                {
                    "test": "test_add_2",
                    "result": True,
                    "table": "ssession",
                    "proc_index_id": [146, 149],
                },
            )
    return (
        ssession_id,
        {
            "test": "test_add_2",
            "result": False,
            "table": "ssession",
            "proc_index_id": [146, 149],
        },
    )


def test_delete(cookie, ssession_id):
    status_code = delete_ssession(cookie, ssession_id)
    print(f"delete status: {status_code}")
    if status_code > 0:
        return {
            "test": "test_delete",
            "result": True,
            "table": "ssession",
            "proc_index_id": [402, 420],
        }
    return {
        "test": "test_delete",
        "result": False,
        "table": "ssession",
        "proc_index_id": [402, 420],
    }


def test_gets(cookie, ssession_id, ssession_id2):
    lcl_ssession = {}
    lcl_ssession2 = {}
    for i in get_ssessions(cookie):
        # print(f"i: {i}\n, session_id: {ssession_id} \n")
        if i["id"] == ssession_id:
            lcl_ssession = i
        if i["id"] == ssession_id2:
            lcl_ssession2 = i
    ssession = get_ssession(cookie, ssession_id)
    ssession2 = get_ssession(cookie, ssession_id2)
    print(f"lcl ssession: {lcl_ssession},\n\n\n session: {ssession} \n\n")

    if lcl_ssession["start_time"] == ssession["start_time"]:
        if lcl_ssession["end_time"] == ssession["end_time"]:
            if lcl_ssession2["start_time"] == ssession2["start_time"]:
                if lcl_ssession2["end_time"] == ssession2["end_time"]:
                    return {
                        "test": "test_gets",
                        "result": True,
                        "table": "ssession",
                        "proc_index_id": [144, 145],
                    }
    return {
        "test": "test_gets",
        "result": False,
        "table": "ssession",
        "proc_index_id": [144, 145],
    }


def test_login(pv_cookie):
    username = config.login_test_user
    password = config.login_test_password
    lcl_user_id = user_ut.get_user_id(username)

    cookie = login(username, password)
    if cookie["user_id"] > 0:
        if cookie["ssession_number"] > 0:
            return (
                cookie,
                {
                    "test": "test_login",
                    "result": True,
                    "table": "timelog",
                    "proc_index_id": [152],
                },
            )
    return (
        cookie,
        {
            "test": "test_login",
            "result": False,
            "table": "timelog",
            "proc_index_id": [152],
        },
    )


def test_logout(cookie):
    status = logout(cookie)
    if status == 1:
        return {
            "test": "test_logout",
            "result": True,
            "table": "timelog",
            "proc_index_id": [425],
        }
    return {
        "test": "test_logout",
        "result": False,
        "table": "timelog",
        "proc_index_id": [425],
    }


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        try:
            t_table = test["table"]
            t_test = test["test"]
            t_result = test["result"]
            t_proc_index_id = test["proc_index_id"]
            print(
                f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
            )
        except Exception as e:
            print(f"\n\n Exception: {e} \n\n Occured")
            print(f"\n\n data: {test} \n\n")
            time.sleep(3)


def hire_yoda(cookie):
    username = config.user_username
    password = config.login_test_password
    lcl_user_id = user_ut.get_user_id(username)
    if lcl_user_id == -7 or lcl_user_id == 0:
        return setup_ut.hire_yoda(cookie)
    return lcl_user_id


def fire_yoda(cookie, yoda_id):
    return user_ut.fire(cookie, yoda_id)


def machine(cookie):
    tests = []
    print(" \n \n ")
    ssession_length = 1800
    start_time = int(time.time())
    yoda_id = hire_yoda(cookie)
    ssession_number = 99494894
    sid, jdat1 = test_add_1(cookie, yoda_id)
    tests.append(jdat1)

    sid2, jdat2 = test_add_2(cookie, yoda_id)
    tests.append(jdat2)
    print(" \n \n ")
    tests.append(test_gets(cookie, sid, sid2))
    # tests.append(test_delete(cookie, sid))
    tmp_cookie, log_dat = test_login(cookie)
    tests.append(log_dat)
    tests.append(test_logout(cookie))

    fire_yoda(cookie, yoda_id)
    # delete_ssession(cookie, sid2)

    return tests


def main():
    cookie = ll.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
