DELIMITER //

CREATE OR REPLACE PROCEDURE get_max_rrole_id(OUT rv_id INT(10))
	BEGIN
		SELECT MAX(id) INTO rv_id FROM rrole;
	END//



CREATE OR REPLACE PROCEDURE delete_rroles()
        BEGIN
        DELETE FROM rrole;
		ALTER TABLE rrole auto_increment = 1;
        END//

#334
CREATE OR REPLACE PROCEDURE delete_rrole(IN pv_rrole_id int(9)
                                        ,OUT rv_success int(2))
BEGIN
	DECLARE lcl_count INT DEFAULT 0;
	DECLARE lcl_after INT DEFAULT 0;
	SET rv_success = -99;
	SELECT COUNT(1) INTO lcl_count FROM rrole;
	DELETE FROM rrole where rrole.id = pv_rrole_id;
	SELECT COUNT(1) INTO lcl_after from rrole;
	IF lcl_count > lcl_after THEN
	SELECT 1 INTO rv_success;
	END IF;
END//

#125
CREATE OR REPLACE PROCEDURE get_rrole_count(OUT rv_out INT)
	BEGIN
		SELECT COUNT(1) into rv_out from rrole;
	END//
#126
CREATE OR REPLACE PROCEDURE get_rrole_api(IN pv_rrole_id int(10)
                                     )
        BEGIN
	SELECT rrole.id, rrole.rrole
	, rrole.company_id
    , rrole.flag, rrole.e_time
        FROM rrole
        where rrole.id = pv_rrole_id;
        END//
#127

CREATE OR REPLACE PROCEDURE get_rroles_api(IN pv_log_employee_id int(9)
					, IN pv_ssession_number int(9)
		)
	BEGIN
		call get_rroles();
	END//
#128
CREATE OR REPLACE PROCEDURE get_rroles()
	BEGIN
		SELECT rrole.id, rrole.rrole, rrole.company_id, rrole.flag, rrole.e_time
		FROM rrole;
	END//

#348
CREATE OR REPLACE PROCEDURE get_rrole(IN pv_rrole_id int(9))
        BEGIN
             	SELECT rrole.id, rrole.rrole
                , rrole.company_id, rrole.flag, rrole.e_time
                FROM rrole WHERE rrole.id = pv_rrole_id;
        END//



#129
CREATE OR REPLACE PROCEDURE add_rrole(IN pv_flag INT(3)
                            , IN pv_id INT(10)
							, IN pv_rrole VARCHAR(100)
							, IN pv_company_id int(9)
							, OUT rv_rrole_id INT(10))
	BEGIN
	    DECLARE lcl_count INT default -5;
		SELECT COUNT(1) INTO lcl_count FROM rrole WHERE rrole = pv_rrole;
		IF lcl_count = 0 THEN
		INSERT INTO rrole (id, e_time, flag, company_id,rrole)
		VALUES (pv_id, UNIX_TIMESTAMP(NOW()), pv_flag,pv_company_id, pv_rrole);
		SELECT MAX(id) INTO rv_rrole_id FROM rrole;
		END IF;
		IF lcl_count = 1 THEN
		SELECT id INTO rv_rrole_id FROM rrole where rrole = pv_rrole;
		END IF;
	END//

CREATE OR REPLACE PROCEDURE get_role_id(IN pv_rrole VARCHAR(100)
										, IN pv_company_id int(9)
										, OUT rv_id INT(10))
	BEGIN
	DECLARE lcl_count INT DEFAULT 0;
	SET rv_id=-99;
	
	SELECT COUNT(1) INTO lcl_count FROM rrole WHERE rrole = pv_rrole AND company_id = pv_company_id;
	IF lcl_count = 1 THEN
	SELECT id INTO rv_id FROM rrole WHERE rrole = pv_rrole AND ttable = pv_company_id;
	END IF;
	END//
