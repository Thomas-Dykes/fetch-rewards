DELIMITER //
#NOT IN PROC INDEX
CREATE OR REPLACE PROCEDURE get_user_address_id(IN pv_user_id INT(10)
											,OUT rv_address_id INT(10))
	BEGIN
	SELECT c.address_id into rv_address_id
	FROM user a, user_log b
	WHERE a.user_log_id = b.id
	and a.id = pv_user_id;
	END//


#NOT IN PROC_INDEX
CREATE OR REPLACE PROCEDURE get_coworker_address_count(IN pv_user_id INT(10)
														, OUT rv_count INT(10))
	BEGIN
	DECLARE lcl_company_id INT(10);
	CALL get_user_company_id(pv_user_id, lcl_company_id);
	SELECT COUNT(1) INTO rv_count FROM address where company_id = lcl_company_id;
	END//

CREATE OR REPLACE PROCEDURE delete_addresses()
        BEGIN
        DELETE FROM address;
		ALTER TABLE address auto_increment = 1;
        END//

CREATE OR REPLACE PROCEDURE get_min_address_id(OUT rv_min_id int(9))
BEGIN
	select min(id) into rv_min_id from address;
END//

CREATE OR REPLACE PROCEDURE delete_address_api(IN pv_address_id int(9)
				, IN pv_flag int(3)
				, IN pv_user_id int(9)
                , IN pv_ssession_number int(9)
				, OUT rv_status_code int(9))
        BEGIN
			declare lcl_auth int default 0;
			CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
			IF lcl_auth = 1 THEN
				CALL delete_address(pv_address_id, pv_flag, pv_user_id, rv_status_code);
			END IF;
        END//

CREATE OR REPLACE PROCEDURE delete_address(IN pv_address_id int(10)
					, IN pv_flag int(3)
					, IN pv_user_id int(9)
					, OUT rv_status_code int(9)				
				)
BEGIN	
	declare lcl_outbound_id int default 0;
	declare rv_address_json varchar(500);
	declare lcl_company_id int default 0;
	declare lcl_city varchar(50) default '';
	declare lcl_e_time int(13) default 0;
	declare lcl_flag int(3) default 0;
	declare lcl_latitude varchar(40) default '0.0';
	declare lcl_log_user_id int default 0;
	declare lcl_longitude varchar(40) default '0.0';
	declare lcl_state_id int default 0;
	declare lcl_street_address varchar(100);
	declare lcl_zip_code int(5) default 0;
	declare lcl_address_string varchar(40) default '';
	declare lcl_city_string varchar(40) default '';
	declare lcl_company_id_string varchar(40) default '';
	declare lcl_e_time_string varchar(40) default '';
	declare lcl_flag_string varchar(40) default '';
	declare lcl_latitude_string varchar(40) default '';
	declare lcl_log_user_id_string varchar(40) default '';
	declare lcl_longitude_string varchar(40) default '';
	declare lcl_state_id_string varchar(40) default '';
	declare lcl_street_address_string varchar(100) default '';
	declare lcl_zip_code_string varchar(40) default '';
	declare lcl_almost_finished_string varchar(500) default '';

	DECLARE EXIT HANDLER FOR 1451
	-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails 
		BEGIN
			select -1 into rv_status_code;
		END;
	DECLARE EXIT HANDLER FOR 1318
	--  ERROR 1318 (42000): Incorrect number of arguments
		BEGIN
			select -2 into rv_status_code;
		END;

	select company_id into lcl_company_id from address where address.id = pv_address_id;
	select city into lcl_city from address where address.id = pv_address_id;
	select e_time into lcl_e_time from address where address.id = pv_address_id;
	select flag into lcl_flag from address where address.id = pv_address_id;
	select CAST(address.latitude AS CHAR(50)) into lcl_latitude from address where address.id = pv_address_id;
	select log_user_id into lcl_log_user_id from address where address.id = pv_address_id;
	select CAST(address.longitude AS CHAR(50)) into lcl_longitude from address where address.id = pv_address_id;
	select street_address into lcl_street_address FROM address where address.id = pv_address_id;
	select state_id into lcl_state_id from address where address.id = pv_address_id;
	select zip_code INTO lcl_zip_code from address where address.id = pv_address_id;
	call jsonify_int(pv_address_id, 'address_id', lcl_address_string);
	call jsonify_string('city', lcl_city, lcl_city_string);
	call jsonify_int(lcl_company_id, 'company_id', lcl_company_id_string);
	call jsonify_int(lcl_e_time, 'e_time', lcl_e_time_string);
	call jsonify_int(lcl_flag, 'flag', lcl_flag_string);
	call jsonify_string('latitude', lcl_latitude, lcl_latitude_string);
	call jsonify_int(lcl_log_user_id, 'log_user_id', lcl_log_user_id_string);
	call jsonify_string('longitude', lcl_longitude, lcl_longitude_string);
	call jsonify_int(lcl_state_id, 'state_id', lcl_state_id_string);
	call jsonify_string('street_address', lcl_street_address, lcl_street_address_string);
	call jsonify_int(lcl_zip_code, 'zip_code', lcl_zip_code_string);
	select concat("{", lcl_address_string, ",", lcl_city_string, ",", lcl_company_id_string, ","
	,  lcl_e_time_string, ",", lcl_flag_string, ",", lcl_latitude_string, ",", lcl_log_user_id_string, ","
	, lcl_longitude_string, ",", lcl_state_id_string, ",", lcl_street_address_string, ",", lcl_zip_code_string, "}") 
	into rv_address_json;
	-- into lcl_almost_finished_string;
	-- select concat(lcl_almost_finished_string, "'") into rv_address_json;
	call add_outbound(pv_flag, pv_user_id, rv_address_json, 'address', lcl_outbound_id);
	DELETE FROM address where address.id = pv_address_id and address.company_id = lcl_company_id;
	select lcl_outbound_id into rv_status_code;
END//

CREATE OR REPLACE PROCEDURE get_address_count(OUT rv_out int(10))
BEGIN
	SELECT COUNT(1) INTO rv_out FROM address;
END//



CREATE OR REPLACE PROCEDURE get_addresses_api(IN pv_user_id int(9)
				, IN pv_ssession_number int(9))
	BEGIN
		declare lcl_auth int default 0;
		CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
		IF lcl_auth = 1 THEN
			CALL get_addresses(pv_user_id);
		END IF;
	END//


CREATE OR REPLACE PROCEDURE get_addresses(IN pv_user_id int(9))
	BEGIN
		declare lcl_user_company_id int default 0;
		CALL get_user_company_id(pv_user_id, lcl_user_company_id);
		SELECT address.id, address.city, address.company_id
		, address.e_time,  address.flag, cast(address.latitude as char) as latitude
		, address.log_user_id, cast(address.longitude as char) as longitude
		, address.street_address,  address.state_id
		, address.zip_code
		FROM address
		where company_id = lcl_user_company_id;
	END//

CREATE OR REPLACE PROCEDURE get_root_address_id(IN pv_company_id int(9)
											, OUT rv_address_id int(9)
											)
BEGIN
	SELECT min(id) INTO rv_address_id FROM address 
	WHERE company_id = pv_company_id;
END//

CREATE OR REPLACE PROCEDURE get_addresses_states_api(IN pv_user_id int(9)
                                , IN pv_ssession_number int(9))
        BEGIN
		declare lcl_auth int default 0;
		CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
		IF lcl_auth = 1 THEN
			CALL get_addresses_states(pv_user_id);
		END IF;
        END//



CREATE OR REPLACE PROCEDURE get_addresses_states(IN pv_user_id int(9))
        BEGIN
                declare lcl_user_company_id int default 0;
                CALL get_user_company_id(pv_user_id, lcl_user_company_id);
                SELECT address.id, address.city, address.company_id
                , address.e_time,  address.flag, address.id, cast(address.latitude as char) as latitude
                , address.log_user_id, cast(address.longitude as char) as longitude
                , address.street_address, state.name state_name
                , address.zip_code
                FROM address INNER JOIN state ON address.state_id = state.id
                where company_id = lcl_user_company_id;
        END//




CREATE OR REPLACE PROCEDURE get_addresses_by_state_api(IN pv_user_id int(9)
                                 	, IN pv_ssession_number int(9)
					, IN pv_state_id int(9)
					)
         BEGIN
		declare lcl_auth int default 0;
		CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
		IF lcl_auth = 1 THEN
			CALL get_addresses_by_state(pv_user_id, pv_state_id);
		END IF;
         END//



CREATE OR REPLACE PROCEDURE get_addresses_by_state(IN pv_user_id int(9)
						, IN pv_state_id int(9))
         BEGIN
                 declare lcl_user_company_id int default 0;
                 CALL get_user_company_id(pv_user_id, lcl_user_company_id);
                 SELECT address.id, address.city, address.company_id
                 , address.e_time,  address.flag, address.id, cast(address.latitude as char) as latitude
                 , address.log_user_id, cast(address.longitude as char) as longitude
                 , address.street_address, state.name
                 , address.zip_code
                 FROM address INNER JOIN state ON address.state_id = state.id
                 where company_id = lcl_user_company_id AND address.state_id = pv_state_id;
         END//


CREATE OR REPLACE PROCEDURE get_address_api(IN pv_address_id int(9)
                                                , IN pv_user_id int(9)
                                                , IN pv_ssession_number int(9)
                                                )
        BEGIN
		declare lcl_auth int default 0;
		CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
		IF lcl_auth = 1 THEN
		CALL get_address(pv_address_id, pv_user_id);
		END IF;
        END//



CREATE OR REPLACE PROCEDURE get_address(IN pv_address_id int(10)
                                        , IN pv_user_id int(9)
                                                                      	)
        BEGIN
		declare lcl_user_company_id int default 0;
        call get_user_company_id(pv_user_id, lcl_user_company_id);

		SELECT address.city, 
			address.company_id, 
			address.e_time, 
			address.flag, 
			address.id, 
			Cast(address.latitude AS CHAR)  AS latitude, 
			address.log_user_id, 
			Cast(address.longitude AS CHAR) AS longitude, 
			address.street_address, 
			address.state_id, 
			address.zip_code 
		FROM   address 
		WHERE  address.company_id = lcl_user_company_id 
			AND address.id = pv_address_id; 
        END//




CREATE OR REPLACE PROCEDURE add_address_api(IN pv_city varchar(30)
				, IN pv_flag int(3)
				, IN pv_latitude decimal(9, 6)
				, IN pv_user_id int(9)
				, IN pv_longitude decimal(9, 6)
				, IN pv_ssession_number int(10)
				, IN pv_state_id int(10)
				, IN pv_street_address varchar(100)
				, IN pv_zip_code int(10)
				, OUT rv_address_id int(10)
							)
	BEGIN
	declare lcl_auth int default 0;
	CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
	IF lcl_auth = 1 THEN
		CALL add_address(pv_city, pv_flag, pv_latitude, pv_user_id, pv_longitude, pv_state_id, pv_street_address, pv_zip_code, rv_address_id);
	END IF;
	END//



CREATE OR REPLACE PROCEDURE add_address(IN pv_city varchar(30)
									, IN pv_flag int(3)
									, IN pv_latitude decimal(9,6)
									, IN pv_user_id int(9)
									, IN pv_longitude decimal(9,6)
									, IN pv_state_id int(10)
									, IN pv_street_address varchar(100)
									, IN pv_zip_code varchar(10)
									, OUT rv_address_id int(9))
	BEGIN
		declare lcl_user_company_id int default 0;
		CALL get_user_company_id(pv_user_id, lcl_user_company_id);
		INSERT INTO address(city, company_id, e_time, flag, latitude, log_user_id, longitude, state_id, street_address, zip_code)
		VALUES (pv_city, lcl_user_company_id, UNIX_TIMESTAMP(NOW()), pv_flag, pv_latitude, pv_user_id, pv_longitude, pv_state_id, pv_street_address, pv_zip_code);
		SELECT MAX(id) from address where address.log_user_id = pv_user_id INTO rv_address_id;
		IF rv_address_id IS NULL THEN
		SET rv_address_id = -4;
		END IF;
	END//
	

CREATE OR REPLACE PROCEDURE add_root_address(IN pv_city varchar(30)
									, IN pv_company_id int(9)
									, IN pv_flag int(3)
									, IN pv_latitude decimal(9,6)
									, IN pv_user_id int(9)
									, IN pv_longitude decimal(9,6)
									, IN pv_state_id int(10)
									, IN pv_street_address varchar(100)
									, IN pv_zip_code varchar(10)
									, OUT rv_address_id INT(10))
	BEGIN
		DECLARE lcl_duplicate INT default 0;
		SET rv_address_id = 0;
		INSERT INTO address(city, company_id, e_time, flag, latitude, log_user_id, longitude, state_id, street_address, zip_code)
		VALUES (pv_city, pv_company_id, UNIX_TIMESTAMP(NOW()), pv_flag, pv_latitude, pv_user_id, pv_longitude, pv_state_id, pv_street_address, pv_zip_code);
		SELECT COUNT(1) INTO lcl_duplicate FROM address WHERE company_id = pv_company_id AND flag = pv_flag AND latitude = pv_latitude AND longitude = pv_longitude
		AND state_id = pv_state_id AND street_address = pv_street_address AND zip_code = pv_zip_code;
		IF lcl_duplicate > 1 THEN
		SET rv_address_id = -5;
		END IF;
		IF lcl_duplicate = 1 THEN
		SELECT id INTO rv_address_id FROM address WHERE company_id = pv_company_id AND flag = pv_flag AND latitude = pv_latitude AND longitude = pv_longitude
		AND state_id = pv_state_id AND street_address = pv_street_address AND zip_code = pv_zip_code;
		END IF;
		IF rv_address_id IS NULL THEN
		SET rv_address_id = -3;
		END IF;
	END//
