DELIMITER //

CREATE OR REPLACE PROCEDURE calculate_EUI_electricity_api(IN pv_service_month_id int(9)
                                            , IN pv_building_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , OUT rv_EUI_electricity varchar(30)
)
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_EUI_electricity(pv_service_month_id, pv_building_id, pv_user_id, rv_EUI_electricity);
    END IF;
END//

CREATE OR REPLACE PROCEDURE is_this_EUI_existing(IN pv_service_month_id int(9)
                                                , IN pv_building_id int(9)
                                                , OUT rv_is_existing int(9)
                                                )
BEGIN
    declare lcl_eui_id int(9) default 0;
    select COUNT(1) into lcl_eui_id from eui where eui.service_month_id = pv_service_month_id AND eui.building_id = pv_building_id;
    select IF(lcl_eui_id>0, 1, 0) INTO rv_is_existing;
END//

CREATE OR REPLACE PROCEDURE calculate_EUI_electricity_no_store(IN pv_service_month_id int(9)
                                            , IN pv_building_id int(9)
                                            , OUT rv_EUI_electricity decimal(10,6)
                                            )
BEGIN
    declare lcl_building_area int default 0;
    declare lcl_electricity_usage int default 0;
    declare min_service_month_id int default 0;
    call get_building_area(pv_building_id, lcl_building_area);
    call get_min_building_service_month_id(pv_building_id, min_service_month_id);
    select IF(min_service_month_id > (pv_service_month_id-12), min_service_month_id, (pv_service_month_id-12)) INTO min_service_month_id;
    call get_previous_electricity_uusage(pv_building_id, min_service_month_id, pv_service_month_id, lcl_electricity_usage);
    set rv_EUI_electricity = lcl_electricity_usage*3.412/lcl_building_area;
END//


CREATE OR REPLACE PROCEDURE calculate_EUI_gas_no_store(IN pv_building_id int(9)
                                            , IN pv_service_month_id int(9)
                                            , OUT rv_EUI_gas decimal(10,6)
                                            )
BEGIN
    declare lcl_building_area int default 0;
    declare lcl_gas_usage int default 0;
    declare min_service_month_id int default 0;
    call get_building_area(pv_building_id, lcl_building_area);
    call get_min_building_service_month_id(pv_building_id, min_service_month_id);
    select IF(min_service_month_id > (pv_service_month_id-12), min_service_month_id, (pv_service_month_id-12)) INTO min_service_month_id;
    call get_previous_gas_uusage(pv_building_id, min_service_month_id, pv_service_month_id, lcl_gas_usage);
    set rv_EUI_gas = lcl_gas_usage*3.412/lcl_building_area;
END//

CREATE OR REPLACE PROCEDURE calculate_EUI_gas(IN pv_service_month_id int(9)
                                            , IN pv_building_id int(9)
                                            , OUT rv_EUI_gas decimal(12,9)
                                            )
BEGIN
    declare lcl_building_area int default 0;
    declare lcl_gas_usage int default 0;
    declare lcl_is_existing int default 0;
    declare lcl_baseline int default 0;
    declare lcl_post_period int default 0;
    declare min_service_month_id int default 0;
    call get_building_area(pv_building_id, lcl_building_area);
    call get_min_building_service_month_id(pv_building_id, min_service_month_id);
    select IF(min_service_month_id > (pv_service_month_id-12), min_service_month_id, (pv_service_month_id-12)) INTO min_service_month_id;
    call get_previous_gas_uusage(pv_building_id, min_service_month_id, pv_service_month_id, lcl_gas_usage);
    set rv_EUI_gas = lcl_gas_usage*1000/lcl_building_area;
END//

CREATE OR REPLACE PROCEDURE calculate_EUI_electricity(IN pv_service_month_id int(9)
                                            , IN pv_building_id int(9)
                                            , OUT rv_EUI_electricity decimal(10,6)
                                            )
BEGIN
    declare lcl_building_area int default 0;
    declare lcl_electricity_usage int default 0;
    declare lcl_is_existing int default 0;
    declare lcl_baseline int default 0;
    declare lcl_post_period int default 0;
    declare min_service_month_id int default 0;
    call get_building_area(pv_building_id, lcl_building_area);
    call get_min_building_service_month_id(pv_building_id, min_service_month_id);
    select IF(min_service_month_id > (pv_service_month_id-12), min_service_month_id, (pv_service_month_id-12)) INTO min_service_month_id;
    call get_previous_electricity_uusage(pv_building_id, min_service_month_id, pv_service_month_id, lcl_electricity_usage);
    set rv_EUI_electricity = lcl_electricity_usage*3.412/lcl_building_area;
    call is_this_EUI_existing(pv_service_month_id, pv_building_id, lcl_is_existing);
    select electricity.base_usage into lcl_baseline FROM electricity where building_id = pv_building_id and service_month_id = pv_service_month_id;
    select IF(lcl_baseline > 0, 1, 0) into lcl_post_period;
    IF lcl_is_existing = 0 THEN
        INSERT INTO eui(building_id, service_month_id, eui_electricity, eui_gas, eui_total, is_post_period)
        VALUES (pv_building_id, pv_service_month_id, 0, 0, 0, lcl_post_period);
    END IF;
    update eui set eui_electricity = rv_EUI_electricity where building_id = pv_building_id AND service_month_id = pv_service_month_id;
END//


CREATE OR REPLACE PROCEDURE calculate_EUI_gas_api(IN pv_service_month_id int(9)
                                            , IN pv_building_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , OUT rv_EUI_gas varchar(30)
)
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_EUI_gas(pv_service_month_id, pv_building_id, rv_EUI_gas);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_EUI_gas(IN pv_service_month_id int(9)
                                            , IN pv_building_id int(9)
                                            , OUT rv_EUI_gas decimal(12,9)
                                            )
BEGIN
    declare lcl_building_area int default 0;
    declare lcl_gas_usage int default 0;
    declare lcl_is_existing int default 0;
    declare lcl_baseline int default 0;
    declare lcl_post_period int default 0;
    declare min_service_month_id int default 0;
    call get_building_area(pv_building_id, lcl_building_area);
    call get_min_building_service_month_id(pv_building_id, min_service_month_id);
    select IF(min_service_month_id > (pv_service_month_id-12), min_service_month_id, (pv_service_month_id-12)) INTO min_service_month_id;
    call get_previous_gas_uusage(pv_building_id, min_service_month_id, pv_service_month_id, lcl_gas_usage);
    set rv_EUI_gas = lcl_gas_usage*1000/lcl_building_area;    
    call is_this_EUI_existing(pv_service_month_id, pv_building_id, lcl_is_existing);
    IF lcl_is_existing = 0 THEN
    select gas.base_usage into lcl_baseline FROM gas where building_id = pv_building_id and service_month_id = pv_service_month_id;
    select IF(lcl_baseline > 0, 1, 0) into lcl_post_period;
        INSERT INTO eui(building_id, service_month_id, eui_electricity, eui_gas, eui_total, is_post_period)
        VALUES (pv_building_id, pv_service_month_id, 0, 0, 0, lcl_post_period);
    END IF;
    update eui set eui_gas = rv_EUI_gas where building_id = pv_building_id AND service_month_id = pv_service_month_id;
END//

CREATE OR REPLACE PROCEDURE add_EUIs_api(IN pv_building_id int(9)
                                    , IN pv_user_id int(9)
                                    , IN pv_ssession_number int(9)
                                    , OUT rv_EUI_id int(9)
                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_EUIs(pv_building_id, rv_EUI_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE add_EUIs(IN pv_building_id int(9)
                                    , OUT rv_EUI_id int(9)
                                    )
BEGIN

    update eui set eui_total = eui_electricity + eui_gas where eui_electricity > 0 AND eui_gas > 0 AND building_id = pv_building_id;
    SELECT MAX(id) into rv_EUI_id
    from eui where eui.building_id = pv_building_id AND eui_electricity > 0 AND eui_gas > 0;
END//


CREATE OR REPLACE PROCEDURE get_EUIs_api(IN pv_building_id int(9)
                                    , IN pv_user_id int(9)
                                    , IN pv_ssession_number int(9)
                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_EUIs(pv_building_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_EUIs(IN pv_building_id int(9)
                                        )
BEGIN
    select eui.eui_total eui, building.name, service_month.month_date service_date
    FROM building INNER JOIN eui ON building.id = eui.building_id
    INNER JOIN service_month ON eui.service_month_id = service_month.id
    WHERE eui.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_EUI_api(IN pv_building_id int(9)
                                    , IN pv_service_month_id int(9)
                                    , IN pv_user_id int(9)
                                    , IN pv_ssession_number int(9)
                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_EUI(pv_building_id, pv_service_month_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_EUI(IN pv_building_id int(9)
                                        , IN pv_service_month_id int(9)
                                        )
BEGIN
    select cast(eui.eui_total as char) eui
    FROM building INNER JOIN eui ON building.id = eui.building_id
    INNER JOIN service_month ON eui.service_month_id = service_month.id
    WHERE eui.building_id = pv_building_id and service_month.id = pv_service_month_id;
END//

CREATE OR REPLACE PROCEDURE get_EUI_gas(IN pv_building_id int(9)
                                        , IN pv_service_month_id int(9)
                                        , OUT rv_EUI_gas decimal(10,6)
                                        )
BEGIN
    select cast(eui.eui_gas as char) eui INTO rv_EUI_gas
    FROM eui
    WHERE eui.building_id = pv_building_id and eui.service_month_id = pv_service_month_id;
END//

CREATE OR REPLACE PROCEDURE get_EUI_electricity(IN pv_building_id int(9)
                                        , IN pv_service_month_id int(9)
                                        , OUT rv_EUI_electricity decimal(10,6)
                                        )
BEGIN
    select cast(eui.eui_electricity as char) eui INTO rv_EUI_electricity
    FROM building INNER JOIN eui ON building.id = eui.building_id
    INNER JOIN service_month ON eui.service_month_id = service_month.id
    WHERE eui.building_id = pv_building_id and service_month.id = pv_service_month_id;
END//


CREATE OR REPLACE PROCEDURE get_post_period_EUIs_api(IN pv_building_id int(9)
                                    , IN pv_user_id int(9)
                                    , IN pv_ssession_number int(9)
                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_post_period_EUIs(pv_building_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_post_period_EUIs(IN pv_building_id int(9)
                                            , IN pv_user_id int(9)
                                            )
BEGIN
    declare max_service_month_id int default 0;
    call get_eligible_post_period_service_month_id(pv_building_id, pv_user_id, max_service_month_id);
    select cast(eui.eui_total as char) eui, building.name, service_month.month_date date
    FROM building INNER JOIN eui ON building.id = eui.building_id
    INNER JOIN service_month ON eui.service_month_id = service_month.id
    WHERE eui.building_id = pv_building_id AND eui.eui_electricity > 0 AND eui.eui_gas > 0;
END//

CREATE OR REPLACE PROCEDURE get_max_post_period_EUI_api(IN pv_building_id int(9)
                                            , IN pv_service_month_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , OUT rv_max_EUI varchar(30)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_max_post_period_EUI(pv_building_id, pv_service_month_id, rv_max_EUI);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_post_period_EUIs_between_api(IN pv_building_id int(9)
                                                    , IN pv_service_month_id_start int(9)
                                                    , IN pv_service_month_id_end int(9)
                                                    , IN pv_user_id int(9)
                                                    , IN pv_ssession_number int(9)
                                                    )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_post_period_EUIs_between(pv_building_id, pv_service_month_id_start, pv_service_month_id_end);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_post_period_EUIs_between(IN pv_building_id int(9)
                                            , IN pv_service_month_id_begin int(9)
                                            , IN pv_service_month_id_end int(9)
                                            )
BEGIN
    select cast(eui.eui_total as char) eui, building.name, service_month.month_date date
    FROM building INNER JOIN eui ON building.id = eui.building_id
    INNER JOIN service_month ON eui.service_month_id = service_month.id
    WHERE eui.building_id = pv_building_id 
    AND service_month.id BETWEEN pv_service_month_id_begin 
    AND pv_service_month_id_end 
    AND eui.eui_electricity > 0 
    AND eui.eui_gas > 0;
    -- AND eui.is_post_period = 1;
END//


CREATE OR REPLACE PROCEDURE get_max_post_period_EUI(IN pv_building_id int(9)
                                    , IN pv_post_period_id int(9)
                                    , OUT rv_max_EUI varchar(30)
                                    )
BEGIN
    select cast(MAX(eui.eui_total) as char) into rv_max_EUI from eui WHERE building_id = pv_building_id and service_month_id >= pv_post_period_id;
END//