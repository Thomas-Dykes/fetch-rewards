import json, time, os
import pymysql, config
from typing import List, Dict, Any
from dataclasses import dataclass


@dataclass
class LoginInfo:
    user_id: int
    session_number: int


def db_connect():
    print("--db_connect")
    conn = connect(
        db_name=config.db,
        name=config.db_user,
        password=config.db_password,
        rds_host="localhost",
    )
    if conn.open:
        print("Connected!")
        return conn

    print("Connection Failed")


def user_login(conn):
    print("--user_login")
    prompt = "Please enter your username and password\nusername: "
    username = config.user_username
    password = config.user_password
    debug = False
    return login(conn, username, password)


def login(connection: pymysql.Connection, username: str, password: str) -> LoginInfo:
    """
    Logs someone into the database
    Args:
        connection: Connection to the database
        username: Username of person logging in
        password: Password of person logging in

    Returns:
        A LoginInfo class with the user_id and session_number

    """
    cursor = connection.cursor(pymysql.cursors.DictCursor)
    params = [username, password, 0, 0]
    # print(f'params: {params}')
    cursor.callproc("login", params)
    cursor.fetchall()
    cursor.execute("SELECT @_login_2")
    user_id = cursor.fetchone()["@_login_2"]
    cursor.execute("SELECT @_login_3")
    session_number = cursor.fetchone()["@_login_3"]
    login_info = LoginInfo(user_id=user_id, session_number=session_number)
    return login_info


def connect(
    db_name: str, name: str, password: str, rds_host: str
) -> pymysql.Connection:
    """
    Connects to the database
    Args:
        db_name: database name
        name: username
        password: password
        rds_host: ip address of database server

    Returns:
        A connection to the database

    """
    try:
        connection = pymysql.connect(
            rds_host,
            user=name,
            passwd=password,
            db=db_name,
            connect_timeout=5,
            autocommit=True,
        )
        return connection

    except Exception as e:
        print("Unable to connect")
        exit(10)


def machine():
    conn = db_connect()
    login_info = user_login(conn)
    #print("login info: {}".format(login_info))
    conn.close()
    user_id = login_info.user_id
    session_number = login_info.session_number
    return {"user_id": user_id, "ssession_number": session_number}


def inbound_login(username, password):
    conn = db_connect()
    login_info = login(conn, username, password)
    conn.close()
    user_id = login_info.user_id
    session_number = login_info.session_number
    return {"user_id": user_id, "ssession_number": session_number}


def main():
    info = machine()
    print(f"info: {info}")


if __name__ == "__main__":
    main()
