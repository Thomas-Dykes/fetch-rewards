import csv
from scipy import optimize
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
import regressor as piecewise

import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config


def add_electricity_raw(usage, adjustment, building_id, oat, start, end, cost, user_id):
    proc_name = "add_electricity_pre_period_raw"
    params = [usage, adjustment, building_id, oat, start, end, cost, user_id, 0]
    jdat = {"name": proc_name, "params": params}
    electricity_id = co.machine_add(jdat)
    return electricity_id

def calculate_model_usage(electricity_id, model_log_id):
    proc_name = "calculate_model_usage_electricity"
    params = [electricity_id, model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_usage = co.machine_add(jdat)
    # print(f"model_id: {model_id}")
    return model_usage

def calculate_norm_usage(electricity_id):
    proc_name = "calculate_norm_usage_electricity"
    params = [electricity_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_usage = co.machine_add(jdat)
    # print(f"model_id: {model_id}")
    return model_usage

def get_oat(user_id, building_id, service_month_id):
    proc_name = "get_oats_electricity_pre"
    params = [user_id, building_id, service_month_id]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine(jdat)
    for oat in oats:
        print(f"oat: {oat}")
    return oats


def get_oat_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_oats_electricity_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine(jdat)
    for oat in oats:
        print(f"oat: {oat}")
    return oats


def get_norm_usage(user_id, building_id, service_month_id):
    proc_name = "get_norm_usages_electricity_pre"
    params = [user_id, building_id, service_month_id]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine(jdat)
    for norm_usage in norm_usages:
        print(f"norm_usage: {norm_usage}")
    return norm_usages

def get_norm_usage_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_norm_usages_electricity_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine(jdat)
    for norm_usage in norm_usages:
        print(f"norm_usage: {norm_usage}")
    return norm_usages


def get_model_log_id(model_id):
    proc_name = "get_model_log_id"
    params = [model_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_log_id = co.machine_add(jdat)
    return model_log_id

def piece_wise(pv_x, pv_y):
    print(f'pv_x: {pv_x}\n\npv_y: {pv_y})')
    model = piecewise.piecewise(pv_x, pv_y)
    return model


def process_model(model):
    segments = model.segments
    count = 0
    rv_model = {}
    for segment in segments:
        count += 1
        start = segment[0]
        end = segment[1]
        coef = segment[2]
        y_int = coef[0]
        slope = coef[1]
        rv_model[count] = {
            "id": count,
            "start": start,
            "end": end,
            "y_int": y_int,
            "slope": slope,
        }
    return rv_model

def calculate_baseline_electricity_price(building_id, service_month_id_start, service_month_id_end):
    proc_name = "calculate_baseline_electricity_price"
    params = [building_id, service_month_id_start, service_month_id_end, 0]
    jdat = {"name": proc_name, "params": params}
    price = co.machine_add(jdat)
    return price



def get_equation(pv_x, pv_y):
    return process_model(piece_wise(pv_x, pv_y))


def find_norm_average(building_id):
    proc_name = "find_norm_average_electricity"
    params = [building_id, 0]
    jdat = {"name": proc_name, "params": params}
    average = co.machine_add(jdat)
    return average

def get_electricity_ids(start, end, building_id):
    proc_name = "get_electricity_ids"
    params = [start, end, building_id]
    jdat = {"name": proc_name, "params": params}
    e_ids = co.machine(jdat)
    e_id_return = []
    for e_id in e_ids:
        print(f"e_id: {e_id}")
        e_id_return.append(float(e_id["id"]))
    return e_id_return

def add_r_squared(r_squared, model_log_id):
    proc_name = "add_r_squared"
    params = [r_squared, model_log_id]
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

def find_rmse(building_id, start, end, model_log_id):
    proc_name = "find_rmse_electricity"
    params = [building_id, start, end, model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    rmse = co.machine_add(jdat)
    return rmse

def find_covariance(building_id, start, end, model_log_id):
    proc_name = "find_covariance_electricity"
    params = [building_id, start, end, model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    cov = co.machine_add(jdat)
    return cov

def add_model(
    building_id, name, build_start, apply_start, apply_end, slope_h, slope_c, y_int_h, y_int_c, threshold, r_squared, rmse, percent_cov, log_user_id
):
    proc_name = "add_model"
    slope_h = float(round(slope_h, 5))
    slope_c = float(round(slope_c, 5))
    y_int_h = float(round(y_int_h, 5))
    y_int_c = float(round(y_int_c, 5))
    threshold = float(round(threshold, 5))
    r_squared = float(round(r_squared, 5))
    rmse = float(round(rmse, 5))
    percent_cov = float(round(percent_cov, 4))

    params = [
        building_id,
        name,
        build_start,
        apply_start,
        apply_end,
        slope_h,
        slope_c,
        y_int_h,
        y_int_c,
        threshold,
        r_squared,
        rmse,
        percent_cov,
        log_user_id,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    model_id = co.machine_add(jdat)
    print(f"model_id: {model_id}")
    return model_id

def line(equation):
    """
    seg 1
        x=0, y=xint
        x=end, y=mx+b
    """
    start = equation["start"]
    stop = equation["end"]
    y_int = equation["y_int"]
    slope = equation["slope"]

    x0 = start
    y0 = (slope * x0) + y_int

    x1 = stop
    y1 = (slope * x1) + y_int

    rv_x = [x0, x1]
    rv_y = [y0, y1]
    return rv_x, rv_y

def predict(pv_x, cooling, heating):
    # mid = cooling["end"]
    init = cooling["start"]
    end = heating["end"]

    mid = (cooling["y_int"] - heating["y_int"]) / (heating["slope"] - cooling["slope"])

    if pv_x > init:
        if pv_x < end:
            if pv_x > mid:
                # cooling
                start = 99999
                stop = cooling["end"]
                y_int = cooling["y_int"]
                slope = cooling["slope"]
                y = slope * pv_x + y_int

            if pv_x <= mid:
                start = heating["start"]
                stop = 99999
                y_int = heating["y_int"]
                slope = heating["slope"]
                y = slope * pv_x + y_int

            return y
    return 777

def build_model(pv_x_list, pv_y_list, name, service_month_id_build_start, service_month_id_build_end, service_month_id_apply_start, service_month_id_apply_end, building_id, user_id):

    x_list = np.array(pv_x_list, dtype=float)
    y_list = np.array(pv_y_list, dtype=float)
    print("LENGTH OF X:")
    print(len(x_list))
    print("LENGTH OF Y:")
    print(len(y_list))
    path = "./graph.png"

    model = get_equation(x_list, y_list)

    first = model[1]
    second = model[2]

    midpoint = (first["y_int"] - second["y_int"]) / (second["slope"] - first["slope"])
    print(first["y_int"])
    print(second["y_int"])
    print(second["slope"])
    print(first["slope"])
    print(f"THRESHOLD: {midpoint}")

    print(f"first: {first}")
    print(f"second: {second}")

    # if(midpoint > second["end"]):
    #     second["start"] = first["start"]
    #     second["end"] = midpoint
    #     second["y_int"] = first["y_int"]
    #     second["slope"] = first["slope"]


    print(f"first: {first}")
    print(f"second: {second}")

    if (midpoint < second["end"]):
        first["end"] = midpoint
        second["start"] = midpoint
        print(f"midpoint: {midpoint}")

    print(f"first: {first}")
    print(f"second: {second}")

    first_line = line(first)
    second_line = line(second)

    plt.scatter(x_list, y_list)
    plt.plot()
    # plt.savefig('baylor_electric.png')


    model_id = add_model(
        building_id,
        name,
        service_month_id_build_end, 
        service_month_id_apply_start, 
        service_month_id_apply_end,
        first["slope"],
        second["slope"],
        first["y_int"],
        second["y_int"],
        first["end"],
        2,
        0,
        0,
        user_id,
    )

    print(f"THE MODEL ID BUILD MODEL: {model_id}")

    model_log_id = get_model_log_id(model_id)

    print(f"THE MODEL ID BUILD MODEL LOG: {model_log_id}")

    average = find_norm_average(building_id)

    print(average)

    SST = 0

    SSR = 0

    e_ids = get_electricity_ids(building_id, service_month_id_build_start-1, service_month_id_build_end)


    for e_id in e_ids:
        model_usage = calculate_model_usage(e_id, model_log_id)
        print(f"MODEL USAGE: {model_usage}")
        norm_usage = calculate_norm_usage(e_id)
        print(f"NORM USAGE: {norm_usage}")
        SST = SST + (norm_usage - average) * (norm_usage - average)
        print(f"Current SST: {SST}")
        SSR = SSR + (model_usage - average) * (model_usage - average)
        print(f"Current SSR: {SSR}")

    print(SST)

    print(SSR)

    r_squared = SSR/SST

    add_r_squared(r_squared, model_log_id)

    find_rmse(building_id, service_month_id_apply_start, (service_month_id_apply_end-2), model_log_id)

    # find_covariance(building_id, service_month_id_apply_start, (service_month_id_apply_end-2), model_log_id)

    return model_id


def main(user_id, ssession_number, building_id, name, model_build_end, model_apply_start, model_apply_end):

    count = 0
    print(f"model_build_end: {model_build_end}")
    print(f"building_id: {building_id}")
    print(f"user_id: {user_id}")
    print(f"name: {name}")
    print(f"model_apply_start: {model_apply_start}")
    e_ids = get_electricity_ids(building_id, int(model_build_end)-12, int(model_build_end))
    #add data
    # for row in data:
    #validate data
    lcl_x = get_oat_between(user_id, building_id, model_build_end-12, model_build_end)
    lcl_y = get_norm_usage_between(user_id, building_id, model_build_end-12, model_build_end)

    # for e_id in e_ids:
    #     lcl_x = get_oat_between(user_id, building_id, model_build_end-12, model_build_end)
    #     print(f"lcl_x: {lcl_x}")
    lcl_x_return = []
    for row in lcl_x:
        lcl_x_return.append(float(row["oat"]))

    print(f"lcl_x_return: {lcl_x_return}")
        # lcl_y = get_norm_usage_between(user_id, building_id, model_build_end-12, model_build_end)
        # print(f"lcl_y: {lcl_y}")
    lcl_y_return = []
    for row2 in lcl_y:
        lcl_y_return.append(float(row2["norm_usage"]))
    print(f"lcl_y_return: {lcl_y_return}")



    model_id = build_model(lcl_x_return, lcl_y_return, name, model_build_end-12, model_build_end, model_apply_start, model_apply_end, building_id, user_id)
    calculate_baseline_electricity_price(building_id, model_build_end-12, model_build_end)

    print(f"THE MODEL ID: {model_id}")

    return(model_id)

if __name__ == "__main__":
    data = []
    user_id = 6
    building_id = 23
    ssession_number = 6
    model_build_end = 590
    model_apply_start = 591
    model_apply_end = 605
    name = 'test_name'

    # with open('electricity.csv')as f:
    #     readCSV = csv.reader(f, delimiter=',')
    #     next(readCSV, None)
    #     for row in readCSV:
    #         data.append(row)

    main(user_id, ssession_number, building_id, name, model_build_end, model_apply_start, model_apply_end)