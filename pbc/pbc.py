import os, time


def get_time():
    return str(time.time()).split(".")[0]


def main():
    # test functions here
    t = get_time()
    print(t)


if __name__ == "__main__":
    main()
