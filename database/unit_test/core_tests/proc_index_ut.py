import time, decimal, sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")
# sys.path.append("../../../magyk")
import login
import connector as co
import config


def get_proc_index_count():
    proc_name = "get_proc_index_count"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    count = co.machine_add(jdat)
    return count


def get_proc_index_max():
    proc_name = "get_proc_index_max"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    max = co.machine_add(jdat)
    return max


def add_proc_index(
    applied_id, description, j_inputs, j_outputs, j_tables, name, public
):
    proc_name = "add_proc_index"
    params = [applied_id, description, "0", j_inputs, j_outputs, j_tables, name, public]
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)
    return applied_id


def get_proc_indexes(nothing):
    proc_name = "get_proc_indexes_api"
    params = [nothing]
    jdat = {"name": proc_name, "params": params}
    proc_indexes = co.machine(jdat)
    for proc_index in proc_indexes:
        print(f"proc_index: {proc_index}")
    return proc_indexes


def get_proc_index(applied_id):
    proc_name = "get_proc_index_api"
    params = [applied_id]
    jdat = {"name": proc_name, "params": params}
    proc_index = co.machine(jdat)
    print(f"proc_index: {proc_index}")
    return proc_index


def delete_proc_index(cookie, proc_index_id):
    user_id = cookie["user_id"]
    proc_name = "delete_proc_index"
    params = [user_id, proc_index_id, 0]
    jdat = {"name": proc_name, "params": params}
    status_code = co.machine_add(jdat)
    print(f"status_code: {status_code}")
    return status_code


def test_add_proc_index_one():
    max = get_proc_index_max()
    applied_id_one = max + 1
    description = "test description 1"
    j_inputs = '{"name": "test_add_proc_index_api"}'
    j_outputs = "{}"
    j_tables = "[]"
    name = "test name 1"
    public = 1
    proc_id = add_proc_index(
        applied_id_one, description, j_inputs, j_outputs, j_tables, name, public
    )
    time.sleep(0.1)
    print(f"id: {proc_id}")
    print(" \n \n ")
    print("get test 1")
    proc_index = get_proc_index(proc_id)
    if applied_id_one == proc_index[0]["id"]:
        if description == proc_index[0]["description"]:
            if j_inputs == proc_index[0]["j_inputs"]:
                if j_outputs == proc_index[0]["j_outputs"]:
                    if j_tables == proc_index[0]["j_tables"]:
                        if name == proc_index[0]["name"]:
                            if public == proc_index[0]["public"]:
                                return (
                                    proc_id,
                                    {
                                        "test": "test_add_proc_index",
                                        "result": True,
                                        "table": "proc_index",
                                        "proc_index_id": [318, 408],
                                    },
                                )
    return (
        proc_id,
        {
            "test": "test_add_proc_index",
            "result": True,
            "table": "proc_index",
            "proc_index_id": [317, 408],
        },
    )


def test_add_proc_index_two():
    max = get_proc_index_max()
    applied_id_two = max + 1
    description = "test description 2"
    j_inputs = '{"name": "test_add_proc_index_api"}'
    j_outputs = "{}"
    j_tables = "[]"
    name = "test name 2"
    public = 1
    proc_id = add_proc_index(
        applied_id_two, description, j_inputs, j_outputs, j_tables, name, public
    )
    time.sleep(0.1)
    print(f"id: {id}")
    print(" \n \n ")
    print("get test 2")
    proc_index = get_proc_index(applied_id_two)
    if applied_id_two == proc_index[0]["id"]:
        if description == proc_index[0]["description"]:
            if j_inputs == proc_index[0]["j_inputs"]:
                if j_outputs == proc_index[0]["j_outputs"]:
                    if j_tables == proc_index[0]["j_tables"]:
                        if name == proc_index[0]["name"]:
                            if public == proc_index[0]["public"]:
                                return (
                                    proc_id,
                                    {
                                        "test": "test_add_proc_index",
                                        "result": True,
                                        "table": "proc_index",
                                        "proc_index_id": [318, 408],
                                    },
                                )
    return (
        proc_id,
        {
            "test": "test_add_proc_index",
            "result": True,
            "table": "proc_index",
            "proc_index_id": [318, 408],
        },
    )


def test_get_proc_indexes(lcl_id_one, lcl_id_two):
    nothing = 0
    get_1 = {}
    get_2 = {}
    for pi in get_proc_indexes(nothing):
        if pi["id"] == lcl_id_one:
            get_1 = pi
        if pi["id"] == lcl_id_two:
            get_2 = pi

    add_1 = get_proc_index(lcl_id_one)
    add_2 = get_proc_index(lcl_id_two)

    if add_1[0]["id"] == get_1["id"]:
        print("match applied_id 1")
        if add_1[0]["description"] == get_1["description"]:
            print("match desc 1")
            if add_1[0]["j_inputs"] == get_1["j_inputs"]:
                print("match j_inputs one")
                if add_1[0]["j_outputs"] == get_1["j_outputs"]:
                    print("match j_outputs one")
                    if add_1[0]["j_tables"] == get_1["j_tables"]:
                        print("match j_tables one")
                        if add_1[0]["name"] == get_1["name"]:
                            print("match name one")
                            if add_1[0]["public"] == get_1["public"]:
                                print("match public one")
                                if add_2[0]["id"] == get_2["id"]:
                                    print("match applied_id 2")
                                    if add_2[0]["description"] == get_2["description"]:
                                        print("match desc 2")
                                        if add_2[0]["j_inputs"] == get_2["j_inputs"]:
                                            print("match j_inputs two")
                                            if (
                                                add_2[0]["j_outputs"]
                                                == get_2["j_outputs"]
                                            ):
                                                print("match j_outputs two")
                                                if (
                                                    add_2[0]["j_tables"]
                                                    == get_2["j_tables"]
                                                ):
                                                    print("match j_tables two")
                                                    if (
                                                        add_2[0]["name"]
                                                        == get_2["name"]
                                                    ):
                                                        print("match name two")
                                                        if (
                                                            add_2[0]["public"]
                                                            == get_2["public"]
                                                        ):
                                                            print("match public two")
                                                            return {
                                                                "test": "test_get_proc_indexes",
                                                                "result": True,
                                                                "table": "proc_index",
                                                                "proc_index_id": [
                                                                    315,
                                                                    316,
                                                                    317,
                                                                ],
                                                            }
    return {
        "test": "test_get_proc_indexes",
        "result": False,
        "table": "proc_index",
        "proc_index_id": [315, 316, 317],
    }


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        t_table = test["table"]
        t_test = test["test"]
        t_result = test["result"]
        t_proc_index_id = test["proc_index_id"]
        print(
            f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
        )


def machine(cookie):
    tests = []
    print(" \n \n ")
    lcl_id, result = test_add_proc_index_one()
    lcl_id_2, result_2 = test_add_proc_index_two()
    tests.append(result)
    tests.append(result_2)
    tests.append(test_get_proc_indexes(lcl_id, lcl_id_2))
    # make delete function
    # delete the 3 files you made
    delete_proc_index(cookie, lcl_id)
    delete_proc_index(cookie, lcl_id_2)
    print(" \n \n ")
    return tests


def main():
    cookie = login.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
