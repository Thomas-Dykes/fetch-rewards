set -exou pipefail

echo "begin head deploy"

echo "set variables"
DB_USER=$2
HOST=$1
DB=$3
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/building.sql || echo "building install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/building_category.sql || echo "building_category install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/building_group.sql || echo "building_group install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/electricity.sql || echo "electricity install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/eui.sql || echo "eui install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/gas.sql || echo "gas install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/model.sql || echo "model install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/priority_status.sql || echo "priority_status install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/service_comment.sql || echo "service_comment install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/service_month.sql || echo "service_month install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/sservice.sql || echo "sservice install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/system.sql || echo "system install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/system_type.sql || echo "system_type install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/user_building.sql || echo "user_building install failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/head/work_order.sql || echo "work_order install failed"
