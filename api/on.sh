#!/bin/bash
set -exou pipefail
ps -ef | grep bash
pkill -f gunicorn || echo 'gunicorn is ready'
gunicorn --certfile=/etc/letsencrypt/live/api.facilitykpi.com/fullchain.pem --keyfile=/etc/letsencrypt/live/api.facilitykpi.com/privkey.pem --bind 0.0.0.0:8000 portal:app --daemon
ps -ef | grep python
date
