DELIMITER //

CREATE OR REPLACE PROCEDURE get_countries_api(
				)
BEGIN
	call get_countries();
END//


CREATE OR REPLACE PROCEDURE get_countries()
	BEGIN
		SELECT country.id 
			country.name, 
			country.abbrev, 
			country.e_time, 
			country.flag
		FROM   country; 
	END//

CREATE OR REPLACE PROCEDURE add_country_api(IN pv_flag INT(3)
							, IN pv_name VARCHAR(100)
							, IN pv_abbrev varchar(10)
							, OUT rv_country_id INT(5)
							)
	BEGIN
		SET rv_country_id = -6;
		CALL add_country(pv_flag, pv_name, pv_abbrev, rv_country_id);
		IF rv_country_id IS NULL THEN
		SET rv_country_id = -3;
		END IF;
	END//


#20
CREATE OR REPLACE PROCEDURE add_country(IN pv_flag INT(3)
							, IN pv_name VARCHAR(100)
							, IN pv_abbrev varchar(10)
							, OUT rv_country_id INT(5)
							)
	BEGIN
		INSERT INTO country(e_time, flag, name, abbrev)
		VALUES(UNIX_TIMESTAMP(NOW()), pv_flag, pv_name, pv_abbrev);
		select MAX(id) from country into rv_country_id;
		IF rv_country_id IS NULL THEN
		set rv_country_id = -4;
		END IF;
	END//