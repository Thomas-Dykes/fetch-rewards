echo "intialize private tests" $1
echo "test_address_table_install results:"
mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_address_table_install.sql
echo "test_company_table_install results:"
mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_company_table_install.sql
echo "test_user_table_install results:"
mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_user_table_install.sql
echo "test_user_ggroup_table_install results:"
mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_user_ggroup_table_install.sql
echo "test_ggroup_table_install results:"
mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_ggroup_table_install.sql
echo "test_ggroup_rrole_table_install results:"
mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_ggroup_rrole_table_install.sql
echo "test_rrole_table_install results:"
mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_rrole_table_install.sql
echo "test_user_log_table_install results:"
mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_user_log_table_install.sql
#echo "test_schedule_table_install results:"
#mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_schedule_table_install.sql
echo "test_ssession_table_install results:"
mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_ssession_table_install.sql
#echo "test_username_table_install results:"
#mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_username_table_install.sql
#echo "test_wage_table_install results:"
#mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_wage_table_install.sql
#echo "test_wage_log_table_install results:"
#mysql -h $2 -u $3 $1 < /home/ec2-user/pbc_portal/database/unit_test/table_install_tests/test_wage_log_table_install.sql
echo "initialize end tests"
