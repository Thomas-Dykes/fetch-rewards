
#add_priority_status_api
call add_proc_index(
130
, 'checks permission, then runs add_priority_status'
, 0
, '{"pv_name": "varchar(30)", "pv_description": "varchar(30)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_priority_id": "int(9)"}'
, '["priority_status"]'
, 'add_priority_status_api'
, 1
);

#get_priority_statuses_api
call add_proc_index(
131
, 'checks permissions, then runs get_priority_statuses'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["priority_status"]'
, 'get_priority_statuses_api'
, 1
);

#get_priority_status_api
call add_proc_index(
132
, 'checks permissions, then runs get_priority_status'
, 0
, '{"pv_priority_status_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["priority_status"]'
, 'get_priority_status_api'
, 1
);


#END