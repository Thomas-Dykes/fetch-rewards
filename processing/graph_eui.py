import csv
from scipy import optimize
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
import regressor as piecewise

import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config

def graph_EUIs(building_id, service_month_start, service_month_end):
    proc_name = "get_post_period_EUIs_between"
    params = [building_id, service_month_start, service_month_end]
    jdat = {"name": proc_name, "params": params}
    euis = co.machine(jdat)
    for eui in euis:
        print(f"eui: {eui}")
    return euis

def get_EUI(building_id, service_month_id):
    proc_name = "get_EUI"
    params = [building_id, service_month_id, 0]
    jdat = {"name": proc_name, "params": params}


