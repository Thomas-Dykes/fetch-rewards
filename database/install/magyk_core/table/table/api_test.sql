create or replace table api_test (
    id int(10) PRIMARY KEY AUTO_INCREMENT
	, name varchar(50)
    , test_time datetime
    , response int(10)
    , server_id int(10)
);