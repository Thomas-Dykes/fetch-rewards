CREATE TABLE IF NOT EXISTS gas (
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    uusage int(9),
    model_log_id int(9),
    norm_usage decimal(9,7),
    model_usage decimal(9,7),
    adjustment decimal(11,3),
    adjusted_usage decimal(10,2),
    building_id int(10),
    building_area int(9),
    oat decimal(7,3),
    base_usage int(9),
    start date,
    end date,
    service_days int(3),
    service_month_id int(10),
    cost decimal(11,3),
    dollar_per_mmbtu decimal(6,3),
    service_id int(9),
    saved_usage decimal(17,9),
    percent_savings decimal(6,3),
    monthly_savings decimal(16,10),
    cumulative_savings decimal(16,10),
    e_time int(13),
    process_e_time int(13),
    flag int(3),
    company_id int(9),
    log_user_id int(9)
);