
DELIMITER //

/* get_user_count(OUT rv_out INT) */

set @rv_passed = 0;

CREATE OR REPLACE PROCEDURE test_user_table_install(IN pv_username varchar(30)
														, OUT rv_passed INT(3)
								)
BEGIN
	DECLARE lcl_user INT default 0;
	DECLARE lcl_start INT default 0;
	DECLARE lcl_address_id int default 0;
	DECLARE lcl_company_id int default 0;
	DECLARE lcl_middle INT default 0;
	DECLARE lcl_end INT default 0;
	DECLARE lcl_response INT default 0;
	DECLARE lcl_user_id int default 0;
	SET rv_passed = -6;
	CALL get_user_id(pv_username, lcl_user_id);
	CALL get_user_count(lcl_start);
	CALL get_user_company_id(lcl_user_id, lcl_company_id);
	CALL get_root_address_id(lcl_company_id, lcl_address_id);
	CALL add_random_user(lcl_address_id, 1, 0, "1990/10/10", "ad@ad.ad", 0, "first", 0, "last", lcl_user_id,"pwd", "1122334455", "2000/01/01", lcl_user);
	CALL get_user_count(lcl_middle);
	CALL delete_user(0, lcl_user, lcl_response);
	CALL get_user_count(lcl_end);
	IF lcl_start < lcl_middle THEN
		SET rv_passed = -99;
		IF lcl_middle > lcl_end THEN
			SET rv_passed = 1;
		END IF;
	END IF;
END//

call test_user_table_install('leonardo',@rv_passed);

select @rv_passed;

call add_proc_index(
255
, 'tests the user_table_install procedure'
, 0
, '{}'
, '{"rv_passed": "int(9)"}'
, '["user"]'
, 'test_user_table_install'
, 0
);

call add_unit_test_log(
0
, 1
, @rv_passed
, 255
, 'user'
, 0
, @out
);
