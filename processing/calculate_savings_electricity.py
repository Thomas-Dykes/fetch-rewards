import csv
from scipy import optimize
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
# from piecewise import piecewise
#from piecewise import piecewise_plot
import regressor as piecewise
import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config

# def add_building_raw(area, name):
#     proc_name = "add_building"
#     params = [area, name, 0, 0, 0, 0, 0, 0]
#     jdat = {"name": proc_name, "params": params}
#     building_id = co.machine_add(jdat)
#     return building_id

def add_electricity_raw(usage, adjustment, building_id, oat, start, end, cost, user_id):
    proc_name = "add_electricity_pre_period_raw"
    params = [usage, adjustment, building_id, oat, start, end, cost, user_id, 0]
    jdat = {"name": proc_name, "params": params}
    electricity_id = co.machine_add(jdat)
    return electricity_id


def get_oat_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_oats_electricity_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine(jdat)
    for oat in oats:
        print(f"oat: {oat}")
    return oats



def get_norm_usage_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_norm_usages_electricity_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine(jdat)
    for norm_usage in norm_usages:
        print(f"norm_usage: {norm_usage}")
    return norm_usages

def calculate_model_usage(electricity_id, model_log_id):
    proc_name = "calculate_model_usage_electricity"
    params = [electricity_id, model_log_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_usage = co.machine_add(jdat)
    return model_usage

def get_electricity_ids(building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_electricity_ids"
    params = [building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    e_ids = co.machine(jdat)
    e_id_return = []
    for e_id in e_ids:
        print(f"e_id: {e_id}")
        e_id_return.append(float(e_id["id"]))
    return e_id_return

def get_service_month_id_electricity(e_id):
    proc_name = "get_service_month_id"
    params = [electricity_id, 0]
    jdat = {"name": proc_name, "params": params}
    service_month_id = co.machine_add(jdat)
    return service_month_id


def calculate_base_usage(electricity_id):
    proc_name = "calculate_base_usage_electricity"
    params = [electricity_id, 0]
    jdat = {"name": proc_name, "params": params}
    base_usage = co.machine_add(jdat)
    return base_usage

def calculate_saved_usage(electricity_id):
    proc_name = "calculate_saved_usage_electricity"
    params = [electricity_id, 0]
    jdat = {"name": proc_name, "params": params}    
    saved_usage = co.machine_add(jdat)
    return saved_usage

def calculate_percent_savings(electricity_id):
    proc_name = "calculate_percent_savings_electricity"
    params = [electricity_id, 0]
    jdat = {"name": proc_name, "params": params}
    percent_savings = co.machine_add(jdat)
    return percent_savings

def calculate_monthly_savings(electricity_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "calculate_monthly_savings_electricity"
    params = [electricity_id, building_id, service_month_id_start, service_month_id_end, 0]
    jdat = {"name": proc_name, "params": params}
    monthly_savings = co.machine_add(jdat)
    return monthly_savings

def calculate_baseline_electricity_price(building_id, service_month_id_start, service_month_id_end):
    proc_name = "calculate_baseline_electricity_price"
    params = [building_id, service_month_id_start, service_month_id_end, 0]
    jdat = {"name": proc_name, "params": params}
    price = co.machine_add(jdat)
    return price


def add_savings(electricity_id, cumulative_savings):
    proc_name = "add_cumulative_savings_electricity"
    params = [electricity_id, cumulative_savings]
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat) 

def get_model_log_id(model_id):
    proc_name = "get_model_log_id"
    params = [model_id, 0]
    jdat = {"name": proc_name, "params": params}
    model_log_id = co.machine_add(jdat)
    return model_log_id


def piece_wise(pv_x, pv_y):
    model = piecewise.piecewise(pv_x, pv_y)
    return model


def process_model(model):
    segments = model.segments
    count = 0
    rv_model = {}
    for segment in segments:
        count += 1
        start = segment[0]
        end = segment[1]
        coef = segment[2]
        y_int = coef[0]
        slope = coef[1]
        rv_model[count] = {
            "id": count,
            "start": start,
            "end": end,
            "y_int": y_int,
            "slope": slope,
        }
    return rv_model


def get_equation(pv_x, pv_y):
    return process_model(piece_wise(pv_x, pv_y))



def line(equation):
    """
    seg 1
        x=0, y=xint
        x=end, y=mx+b
    """
    start = equation["start"]
    stop = equation["end"]
    y_int = equation["y_int"]
    slope = equation["slope"]

    x0 = start
    y0 = (slope * x0) + y_int

    x1 = stop
    y1 = (slope * x1) + y_int

    rv_x = [x0, x1]
    rv_y = [y0, y1]
    return rv_x, rv_y


def build_plot(x_list_1, y_list_1, x_list_2, y_list_2, first_line, second_line):
    print(
        f"\n  first: {first_line[0]} {first_line[1]} \n second: {second_line[0]}, {second_line[1]}"
    )
    plt.plot(x_list_1, y_list_1, "o", label="Pre")
    # print(f"x list: {x_list}, y1: {y1} ")
    print(f"x_list_2: {x_list_2}")
    print(f"y_list_2: {y_list_2}")
    plt.plot(x_list_2, y_list_2, "x", label="Post", color="orange")
    plt.title("Magyk Office")
    plt.xlabel("Monthly Avg. Mean OAT (deg F)")
    plt.ylabel("Watt/sqft")
    plt.ylim(2.5, 4.5)
    print(f"first_linee: {first_line}")
    plt.plot(first_line[0], first_line[1], color="green", label="Estimate")
    #  plt.plot('x series', 'yseries', label='yoour label')
    print(f"second_linee: {second_line}")
    plt.plot(second_line[0], second_line[1], color="green")
    plt.legend()
    
    return plt


def build_model_post_period(pv_x_list_1, pv_y_list_1, pv_x_list_2, pv_y_list_2):
    lcl_x1_return = []
    for row in pv_x_list_1:
        lcl_x1_return.append(float(row["oat"]))
    x1_list = np.array(lcl_x1_return, dtype=float)
    lcl_y1_return = []
    for row2 in pv_y_list_1:
        lcl_y1_return.append(float(row2["norm_usage"]))
    y1_list = np.array(lcl_y1_return, dtype=float)
    lcl_x2_return = []
    for row3 in pv_x_list_2:
        lcl_x2_return.append(float(row3["oat"]))
    x2_list = np.array(lcl_x2_return, dtype=float)
    lcl_y2_return = []
    for row4 in pv_y_list_2:
        lcl_y2_return.append(float(row4["norm_usage"]))
    y2_list = np.array(lcl_y2_return, dtype=float)

    path = "./graph.png"

    model = get_equation(x1_list, y1_list)

    first = model[1]
    second = model[2]

    midpoint = (first["y_int"] - second["y_int"]) / (second["slope"] - first["slope"])

    first["end"] = midpoint

    second["start"] = midpoint
    print(f"midpoint: {midpoint}")

    print(f"first: {first}")
    print(f"second: {second}")

    first_line = line(first)
    second_line = line(second)

    plot = build_plot(x1_list, y1_list, x2_list, y2_list, first_line, second_line)
    plot.savefig(path)

def predict(pv_x, cooling, heating):
    # mid = cooling["end"]
    init = cooling["start"]
    end = heating["end"]

    mid = (cooling["y_int"] - heating["y_int"]) / (heating["slope"] - cooling["slope"])

    if pv_x > init:
        if pv_x < end:
            if pv_x > mid:
                # cooling
                start = 99999
                stop = cooling["end"]
                y_int = cooling["y_int"]
                slope = cooling["slope"]
                y = slope * pv_x + y_int

            if pv_x <= mid:
                start = heating["start"]
                stop = 99999
                y_int = heating["y_int"]
                slope = heating["slope"]
                y = slope * pv_x + y_int

            return y
    return 777


def main(data, user_id, building_id, model_id, model_build_end, model_apply_begin, model_apply_end):

    # count = 0
    # for row in data:
    #     row = row.split(', ')
    #     if (count > 1):
    #         print(f"ROWROW: {row}")
    #         print(f"LENGTHROW: {len(row)}")
    #         if len(row)== 6 :
    #             print("ROW PROCESSED")
    #             electricity_id = add_electricity_raw(row[2], row[3], building_id, row[5], row[0], row[1], 0, user_id)
    #     count = count + 1

    model_log_id = get_model_log_id(model_id)

    x_list_1 = get_oat_between(user_id, building_id, model_build_end-12, model_build_end)
    y_list_1 = get_norm_usage_between(user_id, building_id, model_build_end-12, model_build_end)
    x_list_2 = get_oat_between(user_id, building_id, model_apply_begin, model_apply_end)
    y_list_2 = get_norm_usage_between(user_id, building_id, model_apply_begin, model_apply_end)

    build_model_post_period(x_list_1, y_list_1, x_list_2, y_list_2)
    
    e_ids = get_electricity_ids(building_id, model_apply_begin, (model_apply_end+1))
    print(f"e_ids: {e_ids}")
    cumulative_savings = 0
    for e_id in e_ids:
        model_usage = calculate_model_usage(e_id, model_log_id)
        base_usage = calculate_base_usage(e_id)
        saved_usage = calculate_saved_usage(e_id)
        percent_savings = calculate_percent_savings(e_id)

    calculate_baseline_electricity_price(building_id, model_build_end-12, model_build_end)
    for e_id in e_ids:
        monthly_savings = calculate_monthly_savings(e_id, building_id, model_build_start, model_build_end)
        print(f"MONTHLY SAVINGS: {monthly_savings}")
        cumulative_savings = float(round(cumulative_savings,2)) + float(round(monthly_savings,2))
        add_savings(e_id, cumulative_savings)




if __name__ == "__main__":
    
    data = []
    user_id = 2
    building_id = 23
    ssession_number = 6
    model_build_end = 590
    model_apply_start = 591
    model_apply_end = 605
    model_id = 147
    # with open('electricity_post_period.csv')as f:
    #     readCSV = csv.reader(f, delimiter=',')
    #     next(readCSV, None)
    #     for row in readCSV:
    #         data.append(row)

    main(data, user_id, building_id, model_id, model_build_end, model_apply_start, model_apply_end)