CREATE TABLE IF NOT EXISTS brand (
	_id varchar(30)
    , name varchar(100)
	, barcode varchar(30)
	, brandCode varchar(100)
	, category varchar(30)
    , categoryCode varchar(30)
	, cpg varchar(300)
	, topBrand boolean
);

CREATE TABLE IF NOT EXISTS user (
    _id varchar(30)
    , state varchar(10)
    , createdDate int(13)
    , lastLogin int(13)
    , role varchar(30)
    , signUpSource varchar(30)
    , active boolean
);

CREATE TABLE IF NOT EXISTS receipt(
    _id varchar(30)
    , bonusPointsEarned int(9)
    , bonusPointsEarnedReason varchar(100)
    , createDate int(13)
    , dateScanned int(13)
    , finishedDate int(13)
    , modifyDate int(13)
    , pointsAwardedDate int(13)
    , pointsEarned varchar(30)
    , purchaseDate int(13)
    , purchasedItemCount int(9)
    , receipt_item_id int(9)
    , rewardsReceiptStatus varchar(30)
    , totalSpent varchar(30)
    , userId varchar(30)
    , brandCode varchar(30)
);

-- CREATE TABLE IF NOT EXISTS receipt_item(
--     id int(9) PRIMARY KEY auto_increment
--     , barcode varchar(30)
--     , brandCode varchar(30)
--     , competitiveProduct boolean
--     , competitorRewardsGroup varchar(100)
--     , deleted boolean
--     , description varchar(100)
--     , discountedItemPrice varchar(30)
--     , finalPrice varchar(30)
--     , itemNumber varchar(30)
--     , itemPrice varchar(30)
--     , metabriteCampaignId varchar(100)
--     , needsFetchReview boolean
--     , needsFetchReviewReason varchar(100)
--     , originalFinalPrice varchar(30)
--     , originalMetaBriteBarcode varchar(30)
--     , originalMetaBriteDescription varchar(100)
--     , originalMetaBriteItemPrice varchar(30)
--     , originalMetaBriteQuantityPurchased int(9)
--     , originalReceiptItemText varchar(100)
--     , partnerItemId varchar(30)
--     , pointsEarned varchar(30)
--     , pointsNotAwardedReason varchar(100)
--     , pointsPayerId varchar(100)
--     , preventTargetGapPoints boolean
--     , priceAfterCoupon varchar(30)
--     , quantityPurchased int(9)
--     , rewardsGroup varchar(100)
--     , rewardsProductPartnerId varchar(30)
--     , targetPrice varchar(30)
--     , userFlaggedBarcode varchar(30)
--     , userFlaggedDescription varchar(100)
--     , userFlaggedNewItem boolean
--     , userFlaggedPrice varchar(30)
--     , userFlaggedQuantity int(9)
-- );