CREATE TABLE IF NOT EXISTS model_log(
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    slope_heating decimal(14,12),
    slope_cooling decimal(14,12),
    y_int_cooling decimal(14,12),
    y_int_heating decimal(14,12),
    r_squared decimal(6,5),
    rmse decimal(6,4),
    percent_covariance decimal(11,8),
    threshold decimal(9,6),
    building_id int(9),
    elec_or_gas varchar(10),
    model_price decimal(7,4)
    e_time int(13),
    flag int(3),
    company_id int(9),
    log_user_id int(9)
)