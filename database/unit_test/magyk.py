import sys, os

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config


def create_address(street, city, state_id, zip_code, user_id):
    proc_name = "add_address"
    params = [city, 0, 0, user_id, 0, state_id, street, state_id, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine(jdat)


def create_company(name, timezone):
    proc_name = "add_company"
    params = [0, name, timezone, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine(jdat)

def create_root_user(
    address_id,
    admin,
    company_id,
    customer,
    dob,
    email,
    employee,
    first,
    last,
    log_user_id,
    password,
    phone,
    start_date,
    username
):
    proc_name = "add_root_user"
    params = [
        address_id,
        admin,
        company_id,
        customer,
        dob,
        email,
        employee,
        first,
        0,
        last,
        log_user_id,
        password,
        phone,
        start_date,
        username,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    return co.machine(jdat)


company_id = create_company("magyk", 4)
address_id = create_address("111 ranger st", "gotham", 1, 20000, 1)

# create_root_user(
#     address_id,
#     0,
#     1,
#     1,
#     "1990/11/11",
#     "fake@fake.fake",
#     0,
#     "Rodney",
#     "Dangerfield",
#     2,
#     config.user_password,
#     "1122334455",
#     "2020/10/10",
#     config.user_username,
# )
