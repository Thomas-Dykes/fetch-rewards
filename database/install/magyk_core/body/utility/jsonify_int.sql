DELIMITER //
#308
CREATE OR REPLACE PROCEDURE jsonify_int(IN pv_int int(9)
					, IN pv_name varchar(100)
					, OUT rv_string varchar(300)
					)
BEGIN
	SELECT CONCAT("\"", pv_name,"\"",":","\"",pv_int, "\"") INTO rv_string;
END//
