DELIMITER //
#309
CREATE OR REPLACE PROCEDURE jsonify_string(IN pv_name varchar(100)
					, IN pv_string varchar(100)
					, OUT rv_string varchar(300)
					)
BEGIN
	SELECT CONCAT("\"", pv_name,"\"",":","\"",pv_string, "\"") INTO rv_string;
END//
