import time, decimal, sys, unittest

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import login
import pandas as pd
import connector as co


def add_sservice_a(
    cookie,
    system_id,
    deficiency_description,
    building_id,
    recommended_action,
    actor,
    priority_status_id,
    comment,
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_sservice_a_api"
    params = [
        system_id,
        deficiency_description,
        building_id,
        recommended_action,
        actor,
        priority_status_id,
        comment,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    sservice_id = co.machine_add(jdat)
    return sservice_id


def add_sservice(
    cookie,
    system_id,
    deficiency_description,
    building_id,
    record_date,
    complete_date,
    recommended_action,
    actor,
    priority_status_id,
):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_sservice_api"
    params = [
        system_id,
        deficiency_description,
        building_id,
        record_date,
        complete_date,
        recommended_action,
        actor,
        priority_status_id,
        user_id,
        ssession_number,
        0,
    ]
    jdat = {"name": proc_name, "params": params}
    sservice_id = co.machine_add(jdat)
    return sservice_id


def get_building_sservices(cookie, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_building_sservices_api"
    params = [user_id, ssession_number, building_id, 0]
    jdat = {"name": proc_name, "params": params}
    building_services = co.machine_add(jdat)
    return building_services


def get_service_complete_date(cookie, sservice_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_service_complete_date_api"
    params = [sservice_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    sservice_date = co.machine_add(jdat)
    return sservice_date


def get_building_data(cookie, building_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_building_data_api"
    params = [building_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    data = co.machine(jdat)
    for point in data:
        print(f"point: {point}")
    return data


def get_sservices(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_sservices_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    sservices = co.machine(jdat)
    for sservice in sservices:
        print(f"sservice: {sservice}")
    return sservices


def get_sservice(cookie, sservice_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_sservice_api"
    params = [sservice_id, user_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    sservice = co.machine(jdat)[0]
    print(f"sservice: {sservice}")
    return sservice


class serviceTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cookie = login.machine()
        cls.test_name = []
        cls.test_result = []
        cls.procedures_in_tests = []
        cls.service_id_one = None
        cls.service_id_two = None

    def test_add_a_1(self):
        entered_data = {
            "system_id": 1,
            "deficiency_description": "test desc",
            "building_id": 100,
            "recommended_action": "Test",
            "actor": "Tester",
            "priority_status_id": 1,
            "comment": "Testing add_scervice_a",
        }
        try:
            service_id = add_sservice_a(
                self.cookie,
                entered_data["system_id"],
                entered_data["deficiency_description"],
                entered_data["building_id"],
                entered_data["recommended_action"],
                entered_data["actor"],
                entered_data["priority_status_id"],
                entered_data["comment"],
            )
            service_a_data = get_sservice(self.cookie, service_id)
            for key in entered_data:
                if key == "comment":
                    continue
                self.assertEqual(
                    entered_data[key], service_a_data[key], f"{key} should be same"
                )

            self.__class__.service_id_one = service_id
            self.test_name.append("test_add_1")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(["add_sservice_a_api", "get_sservice_api"])
        except Exception as e:
            self.test_name.append("test_add_1")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(["add_sservice_a_api", "get_sservice_api"])
            raise Exception(repr(e))

    def test_add_a_2(self):
        entered_data = {
            "system_id": 2,
            "deficiency_description": "test2 desc",
            "building_id": 101,
            "recommended_action": "Test 2",
            "actor": "Tester2",
            "priority_status_id": 2,
            "comment": "Testing2 add_scervice_a",
        }
        try:
            service_id = add_sservice_a(
                self.cookie,
                entered_data["system_id"],
                entered_data["deficiency_description"],
                entered_data["building_id"],
                entered_data["recommended_action"],
                entered_data["actor"],
                entered_data["priority_status_id"],
                entered_data["comment"],
            )
            service_a_data = get_sservice(self.cookie, service_id)
            for key in entered_data:
                if key == "comment":
                    continue
                self.assertEqual(
                    entered_data[key], service_a_data[key], f"{key} should be same"
                )
            self.__class__.service_id_two = service_id
            self.test_name.append("test_add_2")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(["add_sservice_a_api", "get_sservice_api"])
        except Exception as e:
            self.test_name.append("test_add_2")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(["add_sservice_a_api", "get_sservice_api"])
            raise Exception(repr(e))

    def test_gets(self):
        service_one = {
            "system_id": None,
            "deficiency_description": None,
            "building_id": None,
            "recommended_action": None,
            "actor": None,
            "priority_status_id": None,
            "comment": None,
        }
        service_two = {
            "system_id": None,
            "deficiency_description": None,
            "building_id": None,
            "recommended_action": None,
            "actor": None,
            "priority_status_id": None,
            "comment": None,
        }
        try:
            for item in get_sservices(self.cookie):
                if item["id"] == self.service_id_one:
                    service_one = item
                if item["id"] == self.service_id_two:
                    service_two = item

            service_1_by_get = get_sservice(self.cookie, self.service_id_one)
            service_2_by_get = get_sservice(self.cookie, self.service_id_two)

            for key in service_two:
                self.assertEqual(
                    service_two[key], service_2_by_get[key], f"{key} should be same"
                )
                self.assertEqual(
                    service_one[key], service_1_by_get[key], f"{key} should be same"
                )
            self.test_name.append("test_gets")
            self.test_result.append("PASS")
            self.procedures_in_tests.append(["get_sservices_api", "get_sservice_api"])
        except Exception as e:
            self.test_name.append("test_gets")
            self.test_result.append("FAIL")
            self.procedures_in_tests.append(["get_sservices_api", "get_sservice_api"])


if __name__ == "__main__":
    unittest.main(verbosity=3, exit=False)
    test = serviceTest()
    data = {
        "test_name": test.test_name,
        "test_result": test.test_result,
        "procedures_in_test": test.procedures_in_tests,
    }
    # print(data)
    df = pd.DataFrame(data)
    print("\n", df.to_string())
