	CREATE TABLE IF NOT EXISTS country(
	id int(10) PRIMARY KEY AUTO_INCREMENT
	, name varchar(100)
	, abbrev varchar(5)
	, e_time int(13)
	, flag int(3)
	);
