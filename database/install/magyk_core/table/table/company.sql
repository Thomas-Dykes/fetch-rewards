
	CREATE TABLE IF NOT EXISTS company(
	id int(10) PRIMARY KEY AUTO_INCREMENT
	, name varchar(100)
	, e_time int(13)
	, flag int(3)
	, log_user_id int(10)
	, timezone int(2)
	, INDEX(id)
	);
