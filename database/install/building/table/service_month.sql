CREATE TABLE IF NOT EXISTS service_month (
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    month varchar(10),
    month_date date ,
    year varchar(10),
    log_user_id int(9),
    e_time int(9)
);