CREATE TABLE IF NOT EXISTS model (
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    name varchar(40),
    build_service_month_id_end int(9),
    apply_service_month_id_start int(9),
    apply_service_month_id_end int(9),
    electricity_model_log_id int(10),
    gas_model_log_id int(10),
    building_id int(10),
    e_time int(13),
    flag int(3),
    company_id int(10),
    log_user_id int(10)
)