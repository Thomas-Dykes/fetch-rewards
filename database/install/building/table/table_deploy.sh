set -exou pipefail

echo "begin building table deploy"

echo "set variables"
DB_USER=$2
HOST=$1
DB=$3
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

echo "begin tables"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/electricity.sql || echo "electricity table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/gas.sql || echo "gas table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/building.sql || echo "building table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/building_category.sql || echo "building_category table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/model.sql || echo "model table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/model_log.sql || echo "model log table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/priority_status.sql || echo "priority status failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/sservice.sql || echo "sservice failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/service_comment.sql || echo "service comments failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/service_month.sql || echo "service_month failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/system_type.sql || echo "system type failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/system.sql || echo "system failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/eui.sql || echo "eui failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/building/table/table/rrole.sql || echo "rrole table failed"

