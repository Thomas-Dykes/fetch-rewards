import csv
from scipy import optimize
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
# from piecewise import piecewise
# from piecewise import piecewise_plot
import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config


def add_gas_raw(usage, adjustment, building_id, oat, start, end, service_month_id, cost, user_id):
    proc_name = "add_gas_pre_period_raw"
    params = [usage, adjustment, building_id, 0, oat, start, end, service_month_id, cost, user_id, 0]
    jdat = {"name": proc_name, "params": params}
    gas_id = co.machine_add(jdat)
    return gas_id

def interpret_service_month(month, year):
    proc_name = "interpret_service_month"
    params = [month, year, 0]
    jdat = {"name": proc_name, "params": params}
    service_month_id = co.machine_add(jdat)
    return service_month_id

def main(data, building_id, user_id):
    count = 0
    for row in data:
        if(isinstance(row, str)):
            row = row.split(', ')
        space = row[4][1:len(row[4])].find(" ")
        month = row[4][1:space+1]
        year = row[4][(space+2):len(row[4])]
        service_month_id = interpret_service_month(month, year)
        print(month)
        print(year)
        if len(row)== 7 :
            gas_id = add_gas_raw(int(row[2]), int(row[3]), building_id, round(float(row[5])), row[0], row[1], service_month_id, int(row[6]), user_id)
        if len(row)== 6:
            gas_id = add_gas_raw(int(row[2]), int(row[3]), building_id, round(float(row[5])), row[0], row[1], service_month_id, 0, user_id)
        count = count + 1
if __name__ == "__main__":

    data = []
    user_id = 1
    building_id = 15
    main(data, building_id, user_id)
