CREATE TABLE IF NOT EXISTS address (
  id int(10) PRIMARY KEY AUTO_INCREMENT 
, street_address varchar(100)
, latitude decimal(9,6)
, longitude decimal(9,6)
, city varchar(30)
, state_id int(9)
, zip_code int(5)
, flag int(3)
, e_time int(13)
, company_id int(5)
, log_user_id int(6)
, INDEX(id)
);
