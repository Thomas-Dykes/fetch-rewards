DELIMITER //

CREATE OR REPLACE PROCEDURE add_model_api(IN pv_building_id int(9),
                                    IN pv_name varchar(100),
                                    IN pv_build_service_month_id_end int(9),
                                    IN pv_apply_service_month_id_start int(9),
                                    IN pv_apply_service_month_id_end int(9),
                                    IN pv_slope_heating_e decimal(14,12),
                                    IN pv_slope_cooling_e decimal(14,12),
                                    IN pv_y_int_heating_e decimal(14,12),
                                    IN pv_y_int_cooling_e decimal(14,12),
                                    IN pv_threshold_e decimal(9,6),
                                    IN pv_model_price_e decimal(7,4),
                                    IN pv_slope_heating_g decimal(14,12),
                                    IN pv_slope_cooling_g decimal(14,12),
                                    IN pv_y_int_heating_g decimal(14,12),
                                    IN pv_y_int_cooling_g decimal(14,12),
                                    IN pv_threshold_g decimal(9,6),
                                    IN pv_model_price_g decimal(7,4),
                                    IN pv_user_id int(9),
                                    IN pv_ssession_number int(9),
                                    OUT rv_model_id int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_model(pv_building_id, pv_name, pv_build_service_month_id_end, pv_apply_service_month_id_start
        , pv_apply_service_month_id_end, pv_slope_heating_e, pv_slope_cooling_e, pv_y_int_heating_e, pv_y_int_cooling_e
        , pv_threshold_e, pv_model_price_e, pv_slope_heating_g, pv_slope_cooling_g, pv_y_int_heating_g, pv_y_int_cooling_g, pv_threshold_g
        , pv_model_price_g, pv_user_id, rv_model_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE add_model(IN pv_building_id int(9),
                                    IN pv_name varchar(100),
                                    IN pv_build_service_month_id_end int(9),
                                    IN pv_apply_service_month_id_start int(9),
                                    IN pv_apply_service_month_id_end int(9),
                                    IN pv_slope_heating_e decimal(14,12),
                                    IN pv_slope_cooling_e decimal(14,12),
                                    IN pv_y_int_heating_e decimal(14,12),
                                    IN pv_y_int_cooling_e decimal(14,12),
                                    IN pv_threshold_e decimal(9,6),
                                    IN pv_model_price_e decimal(7,4),
                                    IN pv_slope_heating_g decimal(14,12),
                                    IN pv_slope_cooling_g decimal(14,12),
                                    IN pv_y_int_heating_g decimal(14,12),
                                    IN pv_y_int_cooling_g decimal(14,12),
                                    IN pv_threshold_g decimal(9,6),
                                    IN pv_model_price_g decimal(7,4),
                                    IN pv_user_id int(9),
                                    OUT rv_model_id int(9)
                                    )
BEGIN
    call add_model_electric(pv_building_id, pv_name, pv_build_service_month_id_end, pv_apply_service_month_id_start, pv_apply_service_month_id_end, pv_slope_heating_e, pv_slope_cooling_e, pv_y_int_heating_e, pv_y_int_cooling_e, pv_threshold_e, pv_model_price_e, pv_user_id, rv_model_id);
    call add_model_gas(pv_building_id, pv_name, pv_build_service_month_id_end, pv_apply_service_month_id_start, pv_apply_service_month_id_end, pv_slope_heating_g, pv_slope_cooling_g, pv_y_int_heating_g, pv_y_int_cooling_g, pv_threshold_g, pv_model_price_g, pv_user_id, rv_model_id);
END//

CREATE OR REPLACE PROCEDURE add_model_electric(IN pv_building_id int(9),
                                    IN pv_name varchar(100),
                                    IN pv_build_service_month_id_end int(9),
                                    IN pv_apply_service_month_id_start int(9),
                                    IN pv_apply_service_month_id_end int(9),
                                    IN pv_slope_heating decimal(14,12),
                                    IN pv_slope_cooling decimal(14,12),
                                    IN pv_y_int_heating decimal(14,12),
                                    IN pv_y_int_cooling decimal(14,12),
                                    IN pv_threshold decimal(9,6),
                                    IN pv_model_price decimal(7,4),
                                    IN pv_user_id int(9),
                                    OUT rv_model_id int(9))
BEGIN
    
    DECLARE lcl_model_log_id int default 0;
    DECLARE lcl_company_id int default 0;
    declare lcl_model_count int default 0;
    select count(1) INTO lcl_model_count FROM model where building_id = pv_building_id
    AND name = pv_name
    AND build_service_month_id_end = pv_build_service_month_id_end
    AND apply_service_month_id_start = pv_apply_service_month_id_start
    AND apply_service_month_id_end = pv_apply_service_month_id_end;
    IF lcl_model_count = 0 THEN
    CALL get_user_company_id(pv_user_id, lcl_company_id);
    INSERT INTO model_log(slope_heating, slope_cooling, y_int_heating, y_int_cooling, threshold, elec_or_gas, model_price, building_id, e_time, flag, company_id, log_user_id)
    VALUES(pv_slope_heating, pv_slope_cooling, pv_y_int_heating, pv_y_int_cooling, pv_threshold, 'elec', pv_model_price, pv_building_id, UNIX_TIMESTAMP(NOW()), 0, lcl_company_id, pv_user_id);
    select MAX(id) from model_log where model_log.log_user_id = pv_user_id INTO lcl_model_log_id;
    INSERT INTO model(electricity_model_log_id, name, build_service_month_id_end, apply_service_month_id_start, apply_service_month_id_end, building_id, e_time, flag, company_id, log_user_id)
    VALUES(lcl_model_log_id, pv_name, pv_build_service_month_id_end, pv_apply_service_month_id_start, pv_apply_service_month_id_end, pv_building_id, UNIX_TIMESTAMP(NOW()), 0, lcl_company_id, pv_user_id);
    SELECT MAX(id) into rv_model_id
    from model
    where log_user_id = pv_user_id;
    END IF;
    IF lcl_model_count = 1 THEN
    select id INTO rv_model_id FROM model where building_id = pv_building_id
    AND name = pv_name
    AND build_service_month_id_end = pv_build_service_month_id_end
    AND apply_service_month_id_start = pv_apply_service_month_id_start
    AND apply_service_month_id_end = pv_apply_service_month_id_end;
    END IF;
END//

CREATE OR REPLACE PROCEDURE add_model_gas(IN pv_building_id int(9),
                                    IN pv_name varchar(100),
                                    IN pv_build_service_month_id_end int(9),
                                    IN pv_apply_service_month_id_start int(9),
                                    IN pv_apply_service_month_id_end int(9),
                                    IN pv_slope_heating decimal(14,12),
                                    IN pv_slope_cooling decimal(14,12),
                                    IN pv_y_int_heating decimal(14,12),
                                    IN pv_y_int_cooling decimal(14,12),
                                    IN pv_threshold decimal(9,6),
                                    IN pv_model_price decimal(7,4),
                                    IN pv_user_id int(9),
                                    OUT rv_model_id int(9))
BEGIN
    
    DECLARE lcl_model_log_id int default 0;
    DECLARE lcl_company_id int default 0;
    CALL get_user_company_id(pv_user_id, lcl_company_id);
    INSERT INTO model_log(slope_heating, slope_cooling, y_int_heating, y_int_cooling, threshold, elec_or_gas, model_price, building_id, e_time, flag, company_id, log_user_id)
    VALUES(pv_slope_heating, pv_slope_cooling, pv_y_int_heating, pv_y_int_cooling, pv_threshold, 'gas', pv_model_price, pv_building_id, UNIX_TIMESTAMP(NOW()), 0, lcl_company_id, pv_user_id);
    select MAX(id) from model_log where model_log.log_user_id = pv_user_id INTO lcl_model_log_id;
    SELECT MAX(id) into rv_model_id
    from model
    where log_user_id = pv_user_id;
    UPDATE model set gas_model_log_id = lcl_model_log_id where id = rv_model_id;
END//



CREATE OR REPLACE PROCEDURE get_electric_models_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_electric_models(pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_electric_models(IN pv_user_id int(9)
                                    )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select model.id, model.name, model.build_service_month_id_end, model.apply_service_month_id_start
    , model.apply_service_month_id_end, CAST(model_log.slope_heating AS CHAR) slope_heating
    , CAST(model_log.slope_cooling AS CHAR) slope_cooling, model_log.building_id
    , CAST(model_log.y_int_heating AS CHAR) y_int_heating, CAST(model_log.y_int_cooling AS CHAR) y_int_cooling
    , CAST(model_log.threshold AS CHAR) threshold, percent_covariance, CAST(model_log.model_price AS CHAR) dollar_per_kwh
    , model_log.elec_or_gas, model_log.e_time, model_log.flag, model_log.log_user_id
    FROM model INNER JOIN model_log ON model.electricity_model_log_id = model_log.id
    where model.company_id = lcl_company_id
    and model_log.elec_or_gas = 'elec';
END//


CREATE OR REPLACE PROCEDURE get_models_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_models(pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_models(IN pv_user_id int(9)
                                    )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select model.id, model.name, model.build_service_month_id_end, model.apply_service_month_id_start
    , model.apply_service_month_id_end, CAST(model_log_e.model_price AS CHAR) electricity_model_price
    , CAST(model_log_g.model_price AS CHAR) gas_model_price
    FROM model_log model_log_e INNER JOIN model ON model_log_e.id = model.electricity_model_log_id
    INNER JOIN model_log model_log_g ON model.gas_model_log_id = model_log_g.id
    where model.company_id = lcl_company_id;
END//


CREATE OR REPLACE PROCEDURE get_gas_models_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_gas_models(pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_gas_models(IN pv_user_id int(9)
                                    )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select model.id, model.name, model.build_service_month_id_end, model.apply_service_month_id_start
    , model.apply_service_month_id_end, CAST(model_log.slope_heating AS CHAR) slope_heating
    , CAST(model_log.slope_cooling AS CHAR) slope_cooling, model_log.building_id
    , CAST(model_log.y_int_heating AS CHAR) y_int_heating, CAST(model_log.y_int_cooling AS CHAR) y_int_cooling
    , CAST(model_log.threshold AS CHAR) threshold, CAST(model_log.r_squared AS CHAR)
    , model_log.elec_or_gas, CAST(model_log.model_price AS CHAR) dollar_per_mmbtu, model_log.e_time, model_log.flag, model_log.log_user_id
    FROM model INNER JOIN model_log ON model.gas_model_log_id = model_log.id
    where model.company_id = lcl_company_id
    and model_log.elec_or_gas = 'gas';
END//

CREATE OR REPLACE PROCEDURE get_electric_model_api(IN pv_model_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_electric_model(pv_model_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_electric_model(IN pv_model_id int(9)
                                    , IN pv_user_id int(9)
                                    )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select model.id, model.name, model.build_service_month_id_end, model.apply_service_month_id_start
    , model.apply_service_month_id_end, model_log.slope_heating, model_log.slope_cooling, model_log.building_id
    , model_log.y_int_heating, model_log.y_int_cooling, model_log.threshold
    , model_log.elec_or_gas, model_log.model_price dollar_per_kwh, model_log.e_time, model_log.flag, model_log.log_user_id
    FROM model INNER JOIN model_log ON model.electricity_model_log_id = model_log.id
    where model.company_id = lcl_company_id AND model.id = pv_model_id;
END//

CREATE OR REPLACE PROCEDURE get_gas_model_api(IN pv_model_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_gas_model(pv_model_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_gas_model(IN pv_model_id int(9)
                                    , IN pv_user_id int(9)
                                    )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    select model.id, model.name, model.build_service_month_id_end, model.apply_service_month_id_start
    , model.apply_service_month_id_end, model_log.slope_heating, model_log.slope_cooling, model_log.building_id
    , model_log.y_int_heating, model_log.y_int_cooling, model_log.threshold
    , model_log.elec_or_gas, model_log.model_price dollar_per_mmbtu, model_log.e_time, model_log.flag, model_log.log_user_id
    FROM model INNER JOIN model_log ON model.gas_model_log_id = model_log.id
    where model.company_id = lcl_company_id AND model.id = pv_model_id;
END//

CREATE OR REPLACE PROCEDURE get_model_log_id_elec(IN pv_model_id int(9)
                                            , OUT rv_e_model_log_id int(9)
                                            )
BEGIN
    SELECT model.electricity_model_log_id INTO rv_e_model_log_id FROM model WHERE id = pv_model_id;
END//

CREATE OR REPLACE PROCEDURE get_model_log_id_gas(IN pv_model_id int(9)
                                            , OUT rv_g_model_log_id int(9)
                                            )
BEGIN
    SELECT model.gas_model_log_id INTO rv_g_model_log_id FROM model WHERE id = pv_model_id;
END//

CREATE OR REPLACE PROCEDURE update_electricity_model_price(IN pv_model_price decimal(7,4)
                                                        , IN pv_model_id int(9)
                                                        )
BEGIN
    declare lcl_model_log_id int default 0;
    SELECT model.electricity_model_log_id INTO lcl_model_log_id FROM model where id = pv_model_id;
    UPDATE model_log set model_price = pv_model_price where id = lcl_model_log_id;
END//


CREATE OR REPLACE PROCEDURE update_gas_model_price(IN pv_model_price decimal(7,4)
                                                        , IN pv_model_id int(9)
                                                        )
BEGIN
    declare lcl_model_log_id int default 0;
    SELECT model.gas_model_log_id INTO lcl_model_log_id FROM model where id = pv_model_id;
    UPDATE model_log set model_price = pv_model_price where id = lcl_model_log_id;
END//

CREATE OR REPLACE PROCEDURE get_model_price(IN pv_model_log_id int(9)
                                                    , OUT rv_model_price decimal(7,4)
                                                    )
BEGIN 
    SELECT model_log.model_price INTO rv_model_price FROM model_log WHERE id = pv_model_log_id;
END//

CREATE OR REPLACE PROCEDURE delete_electricity_model(IN pv_model_id int(9)
                                                        )
BEGIN
    declare lcl_model_log_id int default 0;
    UPDATE model set electricity_model_log_id = 0 where id = pv_model_id;
    select model.gas_model_log_id INTO lcl_model_log_id  FROM model where id = pv_model_id;
    IF lcl_model_log_id = 0 THEN
    delete from model where id = pv_model_id;
    END IF;
END//

CREATE OR REPLACE PROCEDURE delete_gas_model(IN pv_model_id int(9)
                                                        )
BEGIN
    declare lcl_model_log_id int default 0;
    UPDATE model set gas_model_log_id = 0 where id = pv_model_id;
    select model.electricity_model_log_id INTO lcl_model_log_id FROM model where id = pv_model_id;
    IF lcl_model_log_id = 0 THEN
    delete from model where id = pv_model_id;
    END IF;
END//
