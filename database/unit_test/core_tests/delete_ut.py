import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")
import login
import connector as co

def delete_addresses():
    proc_name = "delete_addresses"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

def delete_companies():
    proc_name = "delete_companys"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

def delete_ggroup_rroles():
    proc_name = "delete_ggroup_rroles"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

def delete_ggroups():
    proc_name = "delete_ggroups"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)


def delete_roles():
    proc_name = "delete_rroles"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

def delete_ssessions():
    proc_name = "delete_ssessions"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)


def delete_states():
    proc_name = "delete_states"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

def delete_user_ggroups():
    proc_name = "delete_user_ggroups"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

def delete_user_logs():
    proc_name = "delete_user_logs"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

def delete_users():
    proc_name = "delete_users"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)


def main():
    delete_user_ggroups()
    delete_users()
    delete_user_logs()
    delete_addresses()
    delete_companies()
    delete_ggroup_rroles()
    delete_ggroups()
    delete_roles()
    delete_ssessions()
    delete_states()


#  delete_roles()

if __name__ == "__main__":
    main()
