set -exou pipefail

echo "begin core table deploy"

echo "set variables"
DB_USER=$2
HOST=$1
DB=$3
echo "variables: DB_USER: $DB_USER; HOST: $HOST; DB: $DB" 

echo "begin tables"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/proc_index.sql || echo "proc index table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/unit_test_log.sql || echo "unit test log table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/country.sql || echo "country table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/state.sql || echo "state table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/address.sql || echo "address table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/company.sql || echo "company table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/user_log.sql || echo "user_log table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/user.sql || echo "user table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/ssession.sql || echo "ssession table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/outbound.sql || echo "outbound table failed"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/api_test.sql || echo "api test failed"

echo "end tables"
echo "begin views"
mysql -h $HOST -u $DB_USER $DB < /home/ec2-user/pbc_portal/database/install/magyk_core/table/view/user_view.sql || echo "user view failed"






    #ssession has user depenency
#mysql -h $HOST -u $DB_USER < /home/ec2-user/pbc_portal/database/install/magyk_core/table/table/ssession.sql || echo "ssession table failed"

echo "end core table install"