import boto3
s3 = boto3.resource('s3')


# # Upload a new file
# data = open('graph.png', 'rb')
# s3.Bucket('pbc-public-pictures').put_object(Key='1/building/20/graph.png', Body=data)


def main(company_id, tab_name, photo_id, file_name):


    # for bucket in s3.buckets.all():
    #     print(bucket.name)

# Upload a new file
    filenamestring = file_name + ".png"
    data = open(filenamestring, 'rb')
    Keystring = str(company_id) + "/" + str(tab_name) + "/" + str(photo_id) + "/" + str(filenamestring)

    s3.Bucket('pbc-public-pictures').put_object(Key=Keystring, Body=data)


if __name__ == "__main__":
    company_id = 1
    tab_name = "summary"
    photo_id = 20
    file_name = "test_2"

    # with open('electricity.csv')as f:
    #     readCSV = csv.reader(f, delimiter=',')
    #     next(readCSV, None)
    #     for row in readCSV:
    #         data.append(row)

    main(company_id, tab_name, photo_id, file_name)