CREATE TABLE IF NOT EXISTS priority_status(
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    name varchar(30),
    description varchar(100),
    flag int(10),
    e_time int(13),
    log_user_id int(10)
);