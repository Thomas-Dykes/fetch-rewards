
DELIMITER //

/* get_rrole_count(OUT rv_out INT) */

set @rv_passed = 0;

CREATE OR REPLACE PROCEDURE test_rrole_table_install( IN pv_username VARCHAR(30)
													  ,OUT rv_passed INT(3)
													)
BEGIN
	DECLARE lcl_rrole_id INT default 0;
	DECLARE lcl_start INT default 0;
	DECLARE lcl_middle INT default 0;
	DECLARE lcl_end INT default 0;
	DECLARE lcl_response INT default 0;
	DECLARE lcl_add_response INT default 0;
	SET rv_passed = -6;
	CALL get_rrole_count(lcl_start);
	SELECT MAX(rrole.id) INTO lcl_rrole_id FROM rrole;
	SET lcl_rrole_id = lcl_rrole_id + 1;
	CALL add_rrole(0, lcl_rrole_id, 'q', 'question everything', 'everything', lcl_add_response);
	CALL get_rrole_count(lcl_middle);
	CALL delete_rrole(lcl_rrole_id, lcl_response);
	CALL get_rrole_count(lcl_end);
	IF lcl_start < lcl_middle THEN
		SET rv_passed = -99;
		IF lcl_middle > lcl_end THEN
			SET rv_passed = 1;
		END IF;
	END IF;
END//

call test_rrole_table_install('leonardo',@rv_passed);


select @rv_passed;

call add_proc_index(
263
, 'tests the rrole_table_install procedure'
, 0
, '{}'
, '{"rv_passed": "int(9)"}'
, '["rrole"]'
, 'test_rrole_table_install'
, 0
);

call add_unit_test_log(
0
, 1
, @rv_passed
, 263
, 'rrole'
, 0
, @out
);

