#add_system_api
call add_proc_index(
161
, 'checks permission, then runs add_system'
, 0
, '{"pv_name": "varchar(30)", "pv_system_type_id": "int(9)", "pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["system"]'
, 'add_system_api'
, 1
);

#get_systems_api
call add_proc_index(
162
, 'checks permissions, then runs get_systems'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_building_id": "int(9)"}'
, '["system"]'
, 'get_systems_api'
, 1
);

#get_system_api
call add_proc_index(
163
, 'checks permissions, then runs get_system'
, 0
, '{"pv_system_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_building_id": "int(9)"}'
, '["system"]'
, 'get_system_api'
, 1
);


#END