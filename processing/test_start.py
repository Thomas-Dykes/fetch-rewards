import csv
from scipy import optimize
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
import regressor as piecewise

import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config

sys.path.append("/home/ec2-user/pbc_portal/processing")

import build_model_and_upload_electricity
import build_model_and_upload_gas
import calculate_savings_electricity
import calculate_savings_gas


def add_company_full(company_name, timezone, admin, user_dob, email_address, employee, first_name, last_name, user_id, password, phone_number, start_date, username, city, state_id, street_address, zip_code, area, building_name, building_category_id, built):
    proc_name = "add_company_full"
    params = [company_name, timezone, admin, 0, user_dob, email_address, employee, first_name, 0, last_name, user_id, password, phone_number, start_date, username, city, state_id, street_address, zip_code, area, building_name, building_category_id, 0, built, 0]
    jdat = {"name": proc_name, "params": params}
    company_id = co.machine_add(jdat)
    return company_id


def main(data_electricity, data_gas, user_id, building_id, model_build_start, model_build_end, model_apply_start, model_apply_end):

    add_company_full('Magyk Test', 5, 1, '1990/01/01', 'test@test.test', 0, 'Firstest', 'Lastest', 1, 'test_password', '1122334455', '2020/01/01'
    , 'test_username_718', 'test_city', 1, 'test_address'
    , '54321', 100000, 'test_building_name', 1, '1980/01/01');

    with open('electricity_sample_hospital.csv')as f:
        readCSV = csv.reader(f, delimiter=',')
        next(readCSV, None)
        for row in readCSV:
            data_electricity.append(row)

    with open('gas_sample_hospital.csv')as f:
        readCSV = csv.reader(f, delimiter=',')
        next(readCSV, None)
        for row in readCSV:
            data_gas.append(row)

    model_id_1 = build_model_and_upload_electricity.main(data_electricity, building_id, user_id, 6, model_build_start, model_build_end, model_apply_start, model_apply_end)
    calculate_savings_electricity.main(data_electricity, user_id, building_id, model_id_1, model_build_start, model_build_end, model_apply_start, model_apply_end)
    model_id_2 = build_model_and_upload_gas.main(data_gas, building_id, user_id, model_build_start, model_build_end, model_apply_start, model_apply_end)
    calculate_savings_gas.main(data_gas, user_id, building_id, model_id_2, model_build_start, model_build_end, model_apply_start, model_apply_end)


if __name__ == "__main__":
    data_electricity = []
    data_gas = []
    user_id = 1
    building_id = 46
    model_build_start = 567
    model_build_end = 578
    model_apply_start = 579
    model_apply_end = 596
    main(data_electricity, data_gas, user_id, building_id, model_build_start, model_build_end, model_apply_start, model_apply_end)
