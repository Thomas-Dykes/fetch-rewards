DELIMITER //
CREATE OR REPLACE PROCEDURE calculate_model_usage_gas_api(IN pv_gas_id int(9)
                                                                , IN pv_model_log_id int(9)
                                                                , IN pv_user_id int(9)
                                                                , IN pv_ssession_number int(9)
                                                                , OUT rv_model_usage varchar(30)
                                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_model_usage_gas(pv_gas_id, pv_model_log_id, rv_model_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_model_usage_gas(IN pv_gas_id int(9)
                                            , IN pv_model_log_id int(9)
                                            , OUT rv_model_usage varchar(30)
                                            )
BEGIN
    declare lcl_oat int default 0;
    declare lcl_threshold int default 0;
    declare model_slope decimal(10,8) default 0;
    declare model_intercept decimal(9,8) default 0;
    declare lcl_area int default 0;
    declare lcl_service_days int default 0;
    UPDATE gas set model_log_id = pv_model_log_id;
    SELECT gas.OAT INTO lcl_oat FROM gas WHERE gas.id = pv_gas_id;
    SELECT model_log.threshold INTO lcl_threshold FROM gas INNER JOIN model_log ON gas.model_log_id = model_log.id WHERE gas.id = pv_gas_id;
    IF (lcl_oat <= lcl_threshold) THEN
        SELECT model_log.slope_heating 
            INTO   model_slope 
            FROM   gas INNER JOIN model_log ON gas.model_log_id = model_log.id WHERE gas.id = pv_gas_id;

            SELECT model_log.y_int_heating 
            INTO   model_intercept 
            FROM   gas INNER JOIN model_log ON gas.model_log_id = model_log.id WHERE gas.id = pv_gas_id; 
    END IF;
    IF (lcl_oat > lcl_threshold) THEN
        SELECT model_log.slope_cooling INTO model_slope FROM gas INNER JOIN model_log ON gas.model_log_id = model_log.id WHERE gas.id = pv_gas_id;
        SELECT model_log.y_int_cooling INTO model_intercept FROM gas INNER JOIN model_log ON gas.model_log_id = model_log.id WHERE gas.id = pv_gas_id;
    END IF;
    SELECT cast(lcl_oat*model_slope + model_intercept as char) INTO rv_model_usage;
    UPDATE gas set model_usage = rv_model_usage where id = pv_gas_id;
END//



CREATE OR REPLACE PROCEDURE calculate_model_usage_gas_one_slope_api(IN pv_gas_id int(9)
                                                                , IN pv_model_log_id int(9)
                                                                , IN pv_user_id int(9)
                                                                , IN pv_ssession_number int(9)
                                                                , OUT rv_model_usage varchar(30)
                                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_model_usage_gas_one_slope(pv_gas_id, pv_model_log_id, rv_model_usage);
    END IF;
END//


CREATE OR REPLACE PROCEDURE calculate_model_usage_gas_one_slope(IN pv_gas_id int(9)
                                            , IN pv_model_log_id int(9)
                                            , OUT rv_model_usage varchar(30)
                                            )
BEGIN
    declare lcl_oat decimal(10,5) default 0;
    declare lcl_threshold decimal(10,5) default 0;
    declare model_slope decimal(11,9) default 0;
    declare model_intercept decimal(11,9) default 0;
    declare lcl_area int default 0;
    declare lcl_service_days int default 0;
    UPDATE gas set model_log_id = pv_model_log_id;
    SELECT gas.OAT INTO lcl_oat FROM gas WHERE gas.id = pv_gas_id;
    SELECT model_log.slope_heating INTO model_slope 
    FROM gas INNER JOIN model_log ON gas.model_log_id = model_log.id WHERE gas.id = pv_gas_id;
    SELECT model_log.y_int_heating INTO model_intercept 
    FROM gas INNER JOIN model_log ON gas.model_log_id = model_log.id WHERE gas.id = pv_gas_id; 
    SELECT cast(lcl_oat*model_slope + model_intercept as char) INTO rv_model_usage;
    UPDATE gas set model_usage = rv_model_usage where id = pv_gas_id;
END//


CREATE OR REPLACE PROCEDURE calculate_baseline_gas_price_api(IN pv_building_id int(9)
                                                        , IN pv_service_month_id_start int(9)
                                                        , IN pv_service_month_id_end int(9)
                                                        , IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , OUT rv_price decimal(4,3)
                                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_baseline_gas_price(pv_building_id, pv_service_month_id_start, pv_service_month_id_end, rv_price);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_baseline_gas_price(IN pv_building_id int(9)
                                                , IN pv_service_month_id_start int(9)
                                                , IN pv_service_month_id_end int(9)
                                                , OUT rv_price decimal(4,3)
                                                )
BEGIN    
    declare lcl_start_date date;
    declare lcl_end_date date;
    declare sum_usage decimal(11,3) default 0;
    declare sum_cost decimal(11,3) default 0;
    SELECT SUM(gas.uusage) INTO sum_usage FROM gas where gas.service_month_id between pv_service_month_id_start and pv_service_month_id_end and building_id = pv_building_id and base_usage = 0;
    SELECT SUM(gas.cost) INTO sum_cost FROM gas where gas.service_month_id between pv_service_month_id_start and pv_service_month_id_end and building_id = pv_building_id and base_usage = 0;
    UPDATE gas set dollar_per_mmbtu = sum_cost/sum_usage where building_id = pv_building_id;
    SELECT sum_cost/sum_usage INTO rv_price;
END//

CREATE OR REPLACE PROCEDURE calculate_base_usage_gas_api(IN pv_gas_id int(9)
                                                            , IN pv_model_log_id int(9)
                                                            , IN pv_user_id int(9)
                                                            , IN pv_ssession_number int(9)
                                                            , OUT rv_base_usage decimal(12,9)
                                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_base_usage_gas(pv_gas_id, pv_model_log_id, rv_base_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_base_usage_gas(IN pv_gas_id int(9)
                                            , IN pv_model_log_id int(9)
                                            , OUT rv_base_usage decimal(12,9))
BEGIN
    declare lcl_oat int(9) default 0;
    declare lcl_area int(9) default 0;
    declare lcl_service_days int(9) default 0;
    declare lcl_threshold decimal(9,5) default 0;
    declare model_slope decimal(11,9) default 0;
    declare model_intercept decimal(10,9) default 0;
    SELECT gas.building_area INTO lcl_area FROM 
    gas INNER JOIN building ON gas.building_id = building.id
    where gas.id = pv_gas_id;
    SELECT (DATEDIFF(gas.end, gas.start)+1) INTO lcl_service_days FROM
    gas WHERE gas.id = pv_gas_id;
    select gas.oat into lcl_oat FROM gas where id = pv_gas_id;
    SELECT model_log.threshold INTO lcl_threshold FROM
    model_log
    WHERE model_log.id = pv_model_log_id;

    IF (lcl_oat <= lcl_threshold) THEN
        SELECT model_log.slope_heating 
        INTO model_slope 
        FROM model_log
        WHERE model_log.id = pv_model_log_id; 

        SELECT model_log.y_int_heating 
        INTO model_intercept 
        FROM model_log
        WHERE model_log.id = pv_model_log_id; 
    END IF;
    IF (lcl_oat > lcl_threshold) THEN
        SELECT model_log.slope_cooling INTO model_slope
        FROM model_log
        WHERE model_log.id = pv_model_log_id; 
        SELECT model_log.y_int_cooling INTO model_intercept
        FROM model_log
        WHERE model_log.id = pv_model_log_id; 
    END IF;
    SELECT (((lcl_oat*model_slope + model_intercept)*lcl_area*lcl_service_days*.001)+1) INTO rv_base_usage;
    UPDATE gas set base_usage = rv_base_usage where id = pv_gas_id;
END//

CREATE OR REPLACE PROCEDURE calculate_base_usage_one_slope_gas_api(IN pv_gas_id int(9)
                                                            , IN pv_model_log_id int(9)
                                                            , IN pv_user_id int(9)
                                                            , IN pv_ssession_number int(9)
                                                            , OUT rv_base_usage decimal(17,9)
                                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_base_usage_one_slope_gas(pv_gas_id,pv_model_log_id, rv_base_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_base_usage_gas_one_slope(IN pv_gas_id int(9)
                                            , IN pv_model_log_id int(9)
                                            , OUT rv_base_usage decimal(17,9))
BEGIN
    declare lcl_oat int(9) default 0;
    declare lcl_area int(9) default 0;
    declare lcl_service_days int(9) default 0;
    declare lcl_threshold decimal(9,5) default 0;
    declare model_slope decimal(11,9) default 0;
    declare model_intercept decimal(10,9) default 0;
    SELECT gas.building_area INTO lcl_area FROM 
    gas where gas.id = pv_gas_id;
    SELECT gas.service_days INTO lcl_service_days FROM
    gas WHERE gas.id = pv_gas_id;
    select gas.oat into lcl_oat FROM gas where id = pv_gas_id;
    SELECT model_log.slope_heating 
    INTO model_slope 
    FROM model_log
    WHERE model_log.id = pv_model_log_id;
    SELECT model_log.y_int_heating 
    INTO model_intercept 
    FROM model_log
    WHERE model_log.id = pv_model_log_id; 
    SELECT (((lcl_oat*model_slope + model_intercept)*lcl_area*lcl_service_days*.001)+1) INTO rv_base_usage;
    UPDATE gas set base_usage = rv_base_usage where id = pv_gas_id;
END//

CREATE OR REPLACE PROCEDURE calculate_saved_usage_gas_api(IN pv_gas_id int(9)
                                                        , IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , OUT rv_saved_usage decimal(17,9)
                                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_saved_usage_gas(pv_gas_id, rv_base_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_saved_usage_gas(IN pv_gas_id int(9)
                                                , OUT rv_saved_usage decimal(17,9)
                                                )
BEGIN
    SELECT gas.base_usage - gas.uusage  into rv_saved_usage FROM gas where id = pv_gas_id;
    UPDATE gas set saved_usage = rv_saved_usage where id = pv_gas_id;
END//

CREATE OR REPLACE PROCEDURE calculate_percent_savings_gas_api(IN pv_gas_id int(9)
                                                                , IN pv_user_id int(9)
                                                                , IN pv_ssession_number int(9)
                                                                , OUT rv_percent_savings decimal(6,3)
                                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_percent_savings_gas(pv_gas_id, rv_base_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_percent_savings_gas(IN pv_gas_id int(9)
                                                , OUT rv_percent_savings decimal(11,9)
                                                )
BEGIN
    set rv_percent_savings = 0;
    select gas.saved_usage/gas.base_usage INTO rv_percent_savings FROM gas where id = pv_gas_id;
    -- if rv_percent_savings > 0 THEN
    --     update gas set percent_savings = rv_percent_savings where id = pv_gas_id;
    -- END IF;
END//


CREATE OR REPLACE PROCEDURE calculate_norm_usage_gas_api(IN pv_gas_id int(9)
                                                            , IN pv_user_id int(9)
                                                            , IN pv_ssession_number int(9)
                                                            , OUT rv_norm_usage varchar(30)
                                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_norm_usage_gas(pv_gas_id, rv_norm_usage);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_norm_usage_gas(IN pv_gas_id int(9)
                                                , OUT rv_norm_usage varchar(30)
                                                )
BEGIN
    select cast(gas.uusage/gas.building_area/(DATEDIFF(gas.end,gas.start)+1)*1000 as char) into rv_norm_usage
    FROM building INNER JOIN gas ON building.id = gas.building_id
    WHERE gas.id = pv_gas_id;
    UPDATE gas set norm_usage = rv_norm_usage where id = pv_gas_id;
END//

CREATE OR REPLACE PROCEDURE get_gas_ids_api(IN pv_service_month_id_start int(9)
                                                , IN pv_service_month_id_end int(9)
                                                , IN pv_building_id int(9)
                                                , IN pv_user_id int(9)
                                                , IN pv_ssession_number int(9)
                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_gas_ids(pv_service_month_id_start, pv_service_month_id_end, pv_building_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_gas_ids(IN pv_building_id int(9)
                                                , IN pv_service_month_id_start int(9)
                                                , IN pv_service_month_id_end int(9)
                                                )
BEGIN
    declare lcl_start_date date;
    declare lcl_end_date date;
    select service_month.month_date into lcl_start_date from service_month where id = pv_service_month_id_start; 
    select service_month.month_date into lcl_end_date from service_month where id = pv_service_month_id_end;
    select gas.id
    FROM gas
    WHERE gas.start BETWEEN lcl_start_date AND lcl_end_date
    and gas.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_previous_gas_uusage(IN pv_building_id int(9)
                                            , IN pv_service_month_id_begin int(9)
                                            , IN pv_service_month_id_end int(9)
                                            , OUT rv_sum_usage int(9)
                                            )
BEGIN
    declare lcl_present date;
    declare lcl_past date;
    select service_month.month_date into lcl_past from service_month where id = pv_service_month_id_begin; 
    select service_month.month_date into lcl_present from service_month where id = pv_service_month_id_end; 
    SELECT SUM(gas.uusage)*12/(pv_service_month_id_end-pv_service_month_id_begin) INTO rv_sum_usage FROM gas where gas.start between lcl_past and lcl_present AND gas.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE calculate_monthly_savings_gas_api(IN pv_gas_id int(9)
                                                                , IN pv_model_log_id int(9)
                                                                , IN pv_building_id int(9)
                                                                , IN pv_user_id int(9)
                                                                , IN pv_ssession_number int(9)
                                                                , OUT rv_monthly_savings decimal(12,5)
                                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call calculate_monthly_savings_gas(pv_gas_id, pv_model_log_id, pv_building_id, rv_monthly_savings);
    END IF;
END//

CREATE OR REPLACE PROCEDURE calculate_monthly_savings_gas(IN pv_gas_id int(9)
                                                                , IN pv_model_log_id int(9)
                                                                , IN pv_building_id int(9)
                                                                , OUT rv_monthly_savings decimal(12,5)
                                                )
BEGIN
    declare lcl_monthly_mmbtu decimal(11,5) default 0;
    declare lcl_gas_cost decimal(5,4) default 0;

    select model_log.model_price INTO lcl_gas_cost FROM gas
    INNER JOIN model_log ON gas.model_log_id = model_log.id WHERE gas.id = pv_gas_id;

    select gas.saved_usage INTO lcl_monthly_mmbtu FROM gas WHERE id = pv_gas_id;
    set rv_monthly_savings = lcl_monthly_mmbtu*lcl_gas_cost;
    -- UPDATE gas SET monthly_savings = rv_monthly_savings where id = pv_gas_id;
END//

CREATE OR REPLACE PROCEDURE add_cumulative_savings_gas_api(IN pv_gas_id int(9)
                                                                , IN pv_cumulative_savings decimal(9,2)
                                                                , IN pv_user_id int(9)
                                                                , IN pv_ssession_number int(9)
                                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_cumulative_savings_gas(pv_gas_id, pv_cumulative_savings);
    END IF;
END//

CREATE OR REPLACE PROCEDURE add_cumulative_savings_gas(IN pv_gas_id int(9)
                                                , IN pv_cumulative_savings decimal(9,2)
                                            )
BEGIN
    UPDATE gas SET cumulative_savings = pv_cumulative_savings WHERE id = pv_gas_id;
END//



CREATE OR REPLACE PROCEDURE add_gas_pre_period_raw_api(IN pv_usage int(9)
                                            , IN pv_adjustment int(9)
                                            , IN pv_building_id int(9)
                                            , IN pv_building_area int(9)
                                            , IN pv_oat int(3)
                                            , IN pv_start date
                                            , IN pv_end date
                                            , IN pv_cost decimal(11,3)
                                            , IN pv_service_month_id int(9)
                                            , IN pv_log_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , OUT rv_gas_id int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_log_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call add_gas_pre_period_raw(pv_usage, pv_adjustment, pv_building_id, pv_building_area, pv_oat, pv_start, pv_end, pv_cost, pv_service_month_id, pv_log_user_id, rv_gas_id);
    END IF;  
END//

CREATE OR REPLACE PROCEDURE add_gas_pre_period_raw(IN pv_usage int(9)
                                            , IN pv_adjustment int(9)
                                            , IN pv_building_id int(9)
                                            , IN pv_building_area int(9)
                                            , IN pv_oat int(3)
                                            , IN pv_start date
                                            , IN pv_end date
                                            , IN pv_service_month_id int(9)
                                            , IN pv_cost decimal(11,3)
                                            , IN pv_log_user_id int(9)
                                            , OUT rv_gas_id int(9)
)
BEGIN

    DECLARE lcl_company_id INT DEFAULT 0; 

    DECLARE lcl_norm_usage DECIMAL(5, 3) DEFAULT 0; 

    DECLARE lcl_model_usage DECIMAL(5, 3) DEFAULT 0;

    DECLARE lcl_service_month_id int default 0;

    DECLARE lcl_gas_eui int default 0;

    DECLARE lcl_is_existing int default 0;

    declare lcl_output decimal(8,4) default 0;

    declare lcl_building_area int(9) default 0;

    call get_user_company_id(pv_log_user_id, lcl_company_id);
    set lcl_building_area = pv_building_area;
    IF lcl_building_area = 0 THEN
        select building.area into lcl_building_area FROM building where building.id = pv_building_id;
    END IF;
    INSERT INTO gas 
                (uusage, 
                model_log_id, 
                norm_usage, 
                model_usage, 
                adjustment, 
                adjusted_usage, 
                building_id, 
                building_area,
                oat, 
                base_usage, 
                start, 
                end, 
                service_days, 
                service_id,
                cost, 
                service_month_id,
                saved_usage, 
                percent_savings, 
                monthly_savings, 
                cumulative_savings, 
                e_time, 
                process_e_time, 
                flag, 
                company_id, 
                log_user_id) 
    VALUES      (pv_usage, 
                0,
                0, 
                0, 
                0, 
                0, 
                pv_building_id, 
                lcl_building_area,
                pv_oat, 
                0, 
                pv_start, 
                pv_end, 
                0, 
                0,
                pv_cost, 
                pv_service_month_id,
                0, 
                0, 
                0, 
                0, 
                Unix_timestamp(Now()), 
                Unix_timestamp(Now()), 
                0, 
                lcl_company_id, 
                pv_log_user_id); 

    SELECT Max(id) 
    INTO   rv_gas_id 
    FROM   gas; 

    CALL calculate_norm_usage_gas(rv_gas_id, lcl_norm_usage); 

    UPDATE gas 
    SET    norm_usage = lcl_norm_usage 
    WHERE  id = rv_gas_id; 

    UPDATE gas 
    SET    service_days = (Datediff(end, start) + 1 );

    -- call get_service_month_id_gas(rv_gas_id, lcl_service_month_id);

    -- UPDATE gas
    -- SET service_month_id = (lcl_service_month_id-1)
    -- WHERE id = rv_gas_id;

    UPDATE gas 
    SET    adjusted_usage = (pv_usage + pv_adjustment) 
    WHERE  id = rv_gas_id;
    select max(company.id) into lcl_company_id FROM company;
    UPDATE gas
    SET company_id = lcl_company_id;

    call calculate_eui_gas(pv_service_month_id, pv_building_id, lcl_gas_eui);

    update gas set dollar_per_mmbtu = pv_cost/pv_usage where id = rv_gas_id;

    call add_EUIs(pv_building_id, lcl_output);

END//

CREATE OR REPLACE PROCEDURE get_gas_graph_data_api(IN pv_user_id int(10)
                                                    , IN pv_ssession_number int(10)
                                                    , IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_gas_graph_data(pv_building_id, pv_service_month_start_id, pv_service_month_end_id);
    END IF; 
END//


CREATE OR REPLACE PROCEDURE get_gas_graph_data(IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_begin_date date;
    declare lcl_end_date date;
    SELECT cast(a.adjusted_usage as char) adjusted_usage
        , cast(a.base_usage as char) base_usage
        , a.company_id
        , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from gas a, service_month b
    where a.service_month_id = b.id
    and a.service_month_id between pv_service_month_start_id and pv_service_month_end_id
    and a.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_gas_usage_cost_graph_data_api(IN pv_user_id int(10)
                                                    , IN pv_ssession_number int(10)
                                                    , IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_gas_price_graph_data(pv_building_id, pv_service_month_start_id, pv_service_month_end_id);
    END IF; 
END//


CREATE OR REPLACE PROCEDURE get_gas_usage_cost_graph_data(IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_begin_date date;
    declare lcl_end_date date;
    SELECT a.uusage
        , CAST(a.cost AS CHAR) cost
        , a.company_id
        , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from gas a, service_month b
    where a.service_month_id = b.id
    and a.service_month_id between pv_service_month_start_id and pv_service_month_end_id
    and a.building_id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE get_gas_norm_usage_oat_graph_data_api(IN pv_user_id int(10)
                                                    , IN pv_ssession_number int(10)
                                                    , IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_gas_norm_usage_oat_graph_data(pv_building_id, pv_service_month_start_id, pv_service_month_end_id);
    END IF; 
END//


CREATE OR REPLACE PROCEDURE get_gas_norm_usage_oat_graph_data(IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_begin_date date;
    declare lcl_end_date date;
    SELECT CAST(a.norm_usage AS CHAR) norm_usage
        , CAST(a.oat AS CHAR) oat
        , a.company_id
        , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from gas a, service_month b
    where a.service_month_id = b.id
    and a.service_month_id between pv_service_month_start_id and pv_service_month_end_id
    and a.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_gas_price_graph_data_api(IN pv_user_id int(10)
                                                    , IN pv_ssession_number int(10)
                                                    , IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_gas_price_graph_data(pv_building_id, pv_service_month_start_id, pv_service_month_end_id);
    END IF; 
END//


CREATE OR REPLACE PROCEDURE get_gas_price_graph_data(IN pv_building_id int(9)
                                                    , IN pv_service_month_start_id int(9)
                                                    , IN pv_service_month_end_id int(9)
                                                    )
BEGIN
    declare lcl_begin_date date;
    declare lcl_end_date date;
    SELECT CAST(a.dollar_per_mmbtu AS CHAR) dolar_per_mmbtu
        , a.company_id
        , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from gas a, service_month b
    where a.service_month_id = b.id
    and a.service_month_id between pv_service_month_start_id and pv_service_month_end_id
    and a.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_gas_post_period_graph_data_api(IN pv_user_id int(10)
                                                    , IN pv_ssession_number int(10)
                                                    , IN pv_building_id int(9)
                                                    )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_gas_post_period_graph_data(pv_building_id);
    END IF; 
END//


CREATE OR REPLACE PROCEDURE get_gas_post_period_graph_data(IN pv_building_id int(9)
                                                    )
BEGIN
    declare lcl_begin_date date;
    declare lcl_end_date date;
    SELECT a.start
        , a.end
        , a.uusage
        , b.month_date
        , CAST(a.oat AS CHAR) oat
        , CAST(c.area AS CHAR) area
        , a.service_days
        , CAST(a.adjusted_usage AS CHAR) adjusted_usage
        , CAST(a.norm_usage AS CHAR) norm_usage
        , CAST(a.model_usage AS CHAR) model_usage
        , CAST(a.base_usage AS CHAR) base_usage
        , CAST(a.saved_usage AS CHAR) saved_usage
        , CAST(a.percent_savings AS CHAR) percent_savings
        , CAST(a.monthly_savings AS CHAR) monthly_savings
        , CAST(a.cumulative_savings AS CHAR) cumulative_savings
        , CAST(a.base_usage AS CHAR) base_usage
        , a.company_id
        , a.service_month_id
        , CONCAT(b.month,"-",b.year) service_month
    from gas a, service_month b, building c
    where a.service_month_id = b.id
    and a.building_id = c.id
    and a.building_id = pv_building_id
    and a.base_usage > 0;
END//

CREATE OR REPLACE PROCEDURE get_oats_gas_between_api(IN pv_building_id int(9)
                                        , IN pv_service_month_id_begin int(9)
                                        , IN pv_service_month_id_end int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_oats_gas_between(pv_user_id, pv_building_id, pv_service_month_id_begin, pv_service_month_id_end);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_oats_gas_between(IN pv_user_id int(9)
                                    , IN pv_building_id int(9)
                                    , IN pv_service_month_id_begin int(9)
                                    , IN pv_service_month_id_end int(9)
                                    )
BEGIN
    select gas.oat from gas
    WHERE gas.service_month_id between pv_service_month_id_begin AND pv_service_month_id_end
    AND building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_norm_usages_gas_between_api(IN pv_building_id int(9)
                                        , IN pv_service_month_id_begin int(9)
                                        , IN pv_service_month_id_end int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_norm_usages_gas_between(pv_user_id, pv_building_id, pv_service_month_id_begin, pv_service_month_id_end);
    END IF;
END//


CREATE OR REPLACE PROCEDURE get_norm_usages_gas_between(IN pv_user_id int(9)
                                            , IN pv_building_id int(9)
                                            , IN pv_service_month_id_begin int(9)
                                            , IN pv_service_month_id_end int(9)
                                            )
BEGIN
    select gas.norm_usage from gas
    where gas.service_month_id between pv_service_month_id_begin AND pv_service_month_id_end
    AND gas.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE get_savings_gas_between_api(IN pv_service_month_id_begin int(9)
                                        , IN pv_service_month_id_end int(9)
                                        , IN pv_building_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_savings_gas_between(pv_service_month_id_begin, pv_service_month_id_end, pv_user_id, pv_building_id);
    END IF;
END//


CREATE OR REPLACE PROCEDURE get_savings_gas_between(IN pv_service_month_id_begin int(9)
                                    , IN pv_service_month_id_end int(9)
                                    , IN pv_user_id int(9)
                                    , IN pv_building_id int(9)

                                    )
BEGIN
    select gas.monthly_savings from gas
    where gas.service_month_id between pv_service_month_id_begin AND pv_service_month_id_end
    AND gas.building_id = pv_building_id;
END//



CREATE OR REPLACE PROCEDURE get_service_month_year_gas_between_api(IN pv_service_month_id_begin int(9)
                                        , IN pv_service_month_end int(9)
                                        , IN pv_building_id int(9)
                                        , IN pv_user_id int(9)
                                        , IN pv_ssession_number int(9)
                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        CALL get_service_month_year_gas_between(pv_user_id, pv_building_id, pv_service_month_id_begin, pv_service_month_id_end);
    END IF;
END//


CREATE OR REPLACE PROCEDURE get_service_month_year_gas_between(IN pv_user_id int(9)
                                            , IN pv_building_id int(9)
                                            , IN pv_service_month_id_begin int(9)
                                            , IN pv_service_month_id_end int(9)
                                            )
BEGIN
    select service_month.month, service_month.year from service_month INNER JOIN gas ON service_month.id = gas.service_month_id
    where service_month.id between pv_service_month_id_begin and pv_service_month_id_end
    AND gas.building_id = pv_building_id;
END//

CREATE OR REPLACE PROCEDURE find_gas_costs_api(IN pv_user_id int(9)
                                                , IN pv_ssession_number int(9)
                                                , IN pv_building_id int(9)
                                                , IN pv_service_month_id_begin int(9)
                                                , IN pv_service_month_id_end int(9)
                                                )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call find_gas_costs(pv_building_id, pv_service_month_id_begin, pv_service_month_id_end);
    END IF;
END//

CREATE OR REPLACE PROCEDURE find_gas_costs(IN pv_building_id int(9)
                                                , IN pv_service_month_id_begin int(9)
                                                , IN pv_service_month_id_end int(9)
                                                )
BEGIN
    select CAST(cost AS CHAR) cost, id from gas where gas.service_month_id between (pv_service_month_id_begin) and pv_service_month_id_end and gas.building_id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE get_gas_usage_data_api(IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , IN pv_building_id int(9)
                                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_gas_usage_data(pv_user_id, pv_building_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_gas_usage_data(IN pv_user_id int(9)
                                                        , IN pv_building_id int(9)
                                                    )
BEGIN
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    
    SELECT CONCAT(service_month.month,'-', service_month.year) service_month, 
        gas.uusage, 
        Cast(gas.base_usage AS CHAR)  base_usage, 
        Cast(gas.norm_usage AS CHAR)  norm_usage, 
        Cast(gas.model_usage AS CHAR) model_usage, 
        Cast(gas.oat AS CHAR)         oat, 
        CAST(gas.building_area AS CHAR) area,
        gas.company_id,
        gas.start,
        gas.end,
        CAST(gas.adjustment AS CHAR) adjustment,
        gas.id
    FROM   gas 
        INNER JOIN service_month 
                ON gas.service_month_id = service_month.id 
    WHERE  gas.building_id = pv_building_id; 

END//

CREATE OR REPLACE PROCEDURE get_gas_savings_data_api(IN pv_user_id int(9)
                                                        , IN pv_ssession_number int(9)
                                                        , IN pv_building_id int(9)
                                                        )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_gas_savings_data(pv_user_id, pv_building_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_gas_savings_data(IN pv_user_id int(9)
                                                        , IN pv_building_id int(9)
                                                        )
BEGIN 
    declare lcl_company_id int default 0;
    call get_user_company_id(pv_user_id, lcl_company_id);
    
    SELECT CONCAT(service_month.month,'-', service_month.year) service_month, 
        Cast(gas.saved_usage AS CHAR)  saved_usage, 
        Cast(gas.percent_savings AS CHAR)  percent_savings, 
        Cast(gas.monthly_savings AS CHAR) monthly_savings, 
        Cast(gas.cumulative_savings AS CHAR) cumulative_savings, 
        gas.company_id 
    FROM   gas 
        INNER JOIN service_month 
                ON gas.service_month_id = service_month.id 
    WHERE  gas.building_id = pv_building_id; 
END//

CREATE OR REPLACE PROCEDURE get_cumulative_savings_gas_api(IN pv_building_id int(9)
                                            , IN pv_service_month_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_cumulative_savings_gas(pv_building_id, pv_service_month_id);
    END IF;
END//


CREATE OR REPLACE PROCEDURE get_cumulative_savings_gas(IN pv_building_id int(9)
                                                                , IN pv_service_month_id int(9)
                                                                , OUT rv_cumulative_savings varchar(10)
                                                                )
BEGIN
    SELECT CAST(ROUND(gas.cumulative_savings, 2) AS CHAR) gas_savings INTO rv_cumulative_savings FROM gas
    WHERE gas.service_month_id = pv_service_month_id and gas.building_id = pv_building_id;
    IF rv_cumulative_savings IS NULL THEN
    SET rv_cumulative_savings = 0;
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_cumulative_savings_post_period_gas_api(IN pv_building_id int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_cumulative_savings_post_period_gas(pv_building_id, pv_user_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_cumulative_savings_post_period_gas(IN pv_building_id int(9)
                                                                , IN pv_user_id int(9)
                                                                )
BEGIN
    select service_month.month_date savings_date, CAST(ROUND(gas.cumulative_savings, 2) AS CHAR) savings
    FROM gas INNER JOIN service_month ON gas.service_month_id = service_month.id
    WHERE gas.base_usage > 0 AND gas.building_id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE get_adjusted_usage_gas(IN pv_gas_id int(9)
                                                                , OUT rv_adjusted_usage int(9)
                                                                )
BEGIN
    SELECT gas.adjusted_usage INTO rv_adjusted_usage FROM gas
    WHERE gas.id = pv_gas_id;
END//

CREATE OR REPLACE PROCEDURE get_dollar_per_mmbtu_gas_api(IN pv_building_id int(9)
                                            , IN pv_service_month_id_begin int(9)
                                            , IN pv_service_month_id_end int(9)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            )
BEGIN
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call get_dollar_per_mmbtu_gas(pv_building_id, pv_service_month_id_begin, pv_service_month_id_end);
    END IF;
END//

CREATE OR REPLACE PROCEDURE get_dollar_per_mmbtu_gas(IN pv_building_id int(9)
                                                                , IN pv_service_month_id_begin int(9)
                                                                , IN pv_service_month_id_end int(9)
                                                                )
BEGIN
    select service_month.month_date savings_date, CAST(ROUND(gas.dollar_per_mmbtu, 2) AS CHAR) price, gas.id
    FROM gas INNER JOIN service_month ON gas.service_month_id = service_month.id
    WHERE gas.service_month_id BETWEEN pv_service_month_id_begin AND pv_service_month_id_end AND gas.building_id = pv_building_id;
END//



CREATE OR REPLACE PROCEDURE get_min_gas_id(IN pv_user_id int(9)
                                            , IN pv_building_id int(9)
                                            , OUT rv_gas_id int(9)
                                                    )
BEGIN
    select min(id) into rv_gas_id FROM gas WHERE log_user_id = pv_user_id and building_id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE update_gas_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_gas_id int(9)
                                            , IN pv_new_service_start varchar(30)
                                            , IN pv_new_service_end varchar(30)
                                            , IN pv_new_usage int(9)
                                            , IN pv_new_adjustment decimal(11,3)
                                            , IN pv_new_service_month_id int(9)
                                            , IN pv_new_oat int(9)
                                            , IN pv_new_cost decimal(11,3)
                                            , IN pv_new_building_area int(9)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call update_gas(pv_gas_id, pv_new_service_start, pv_new_service_end, pv_new_usage, pv_new_adjustment, pv_new_service_month_id, pv_new_oat, pv_new_cost, pv_new_building_area);
    END IF;
END//


CREATE OR REPLACE PROCEDURe update_gas(IN pv_gas_id int(9)
                                            , IN pv_new_service_start varchar(30)
                                            , IN pv_new_service_end varchar(30)
                                            , IN pv_new_usage int(9)
                                            , IN pv_new_adjustment decimal(11,3)
                                            , IN pv_new_service_month_id int(9)
                                            , IN pv_new_oat int(9)
                                            , IN pv_new_cost decimal(11,3)
                                            , IN pv_new_building_area int(9)
                                            )
BEGIN
    update gas SET 
    start = pv_new_service_start,
    end = pv_new_service_end,
    uusage = pv_new_usage,
    adjustment = pv_new_adjustment,
    service_month_id = pv_new_service_month_id,
    oat = pv_new_oat,
    cost = pv_new_cost,
    building_area = pv_new_building_area
    WHERE id = pv_gas_id;
END//

CREATE OR REPLACE PROCEDURE update_gas_model_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_gas_id int(9)
                                            , IN pv_norm_usage decimal(5,3)
                                            , IN pv_model_usage decimal(5,3)
                                            , IN pv_base_usage int(10)
                                            , IN pv_saved_usage int(9)
                                            , IN pv_percent_savings decimal(12,6)
                                            , IN pv_monthly_savings decimal(16,10)
                                            , IN pv_cumulative_savings decimal(12,2)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call update_gas_model(pv_gas_id, pv_norm_usage, pv_model_usage, pv_base_usage, pv_saved_usage, pv_percent_savings, pv_monthly_savings, pv_cumulative_savings);
    END IF;
END//


CREATE OR REPLACE PROCEDURe update_gas_model(IN pv_gas_id int(9)
                                            , IN pv_norm_usage decimal(5,3)
                                            , IN pv_model_usage decimal(5,3)
                                            , IN pv_base_usage int(10)
                                            , IN pv_saved_usage int(9)
                                            , IN pv_percent_savings decimal(12,6)
                                            , IN pv_monthly_savings decimal(16,10)
                                            , IN pv_cumulative_savings decimal(12,2)
                                            )
BEGIN
    update gas SET 
    norm_usage = pv_norm_usage,
    model_usage = pv_model_usage,
    base_usage = pv_base_usage,
    saved_usage = pv_saved_usage,
    percent_savings = pv_percent_savings,
    monthly_savings = pv_monthly_savings,
    cumulative_savings = pv_cumulative_savings
    WHERE id = pv_gas_id;
END//


CREATE OR REPLACE PROCEDURE delete_buildings_gas_data_api(IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_building_id int(9)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call delete_buildings_gas_data(pv_building_id);
    END IF;
END//

CREATE OR REPLACE PROCEDURE delete_buildings_gas_data(IN pv_building_id int(9))
BEGIN 
    delete from gas where building_id = pv_building_id;
END//


CREATE OR REPLACE PROCEDURE delete_gas_api(IN pv_flag int(3)
                                            , IN pv_user_id int(9)
                                            , IN pv_ssession_number int(9)
                                            , IN pv_gas_id int(9)
                                            , OUT rv_status_code int(9)
                                            )
BEGIN 
    declare lcl_auth int default 0;
    CALL authenticate(pv_user_id, pv_ssession_number, lcl_auth);
    IF lcl_auth = 1 THEN
        call delete_gas(pv_flag, pv_gas_id, rv_status_code);
    END IF;
END//

CREATE OR REPLACE PROCEDURE delete_gas(IN pv_flag int(3)
                                                , IN pv_gas_id int(9)
                                                , OUT rv_status_code int(9)
                                                )
BEGIN
    declare lcl_outbound_id int(9) default 0;
	declare rv_gas_json varchar(500) default '';
	declare lcl_uusage int(9) default 0;
	declare lcl_model_log_id int(9) default 0;
	declare lcl_norm_usage decimal(5,3) default 0;
	declare lcl_model_usage decimal(5,3) default 0;
	declare lcl_adjustment int(9) default 0;
	declare lcl_adjusted_usage int(10) default 0;
    declare lcl_building_id int(9) default 0;
    declare lcl_oat decimal(7,3) default 0;
    declare lcl_base_usage int(9) default 0;
    declare lcl_start varchar(30) default 0;
    declare lcl_end varchar(30) default 0;
    declare lcl_service_days int(9) default 0;
    declare lcl_service_month_id int(9) default 0;
    declare lcl_service_id int(9) default 0;
    declare lcl_cost decimal(11,3) default 0;
    declare lcl_dollar_per_mmbtu decimal(5,3) default 0;
    declare lcl_saved_usage int(9) default 0;
    declare lcl_percent_savings decimal(6,3) default 0;
    declare lcl_monthly_savings decimal(16,3) default 0;
    declare lcl_cumulative_savings decimal(15,6) default 0;
    declare lcl_e_time int(13) default 0;
    declare lcl_flag int(3) default 0;
    declare lcl_company_id int(9) default 0;
    declare lcl_log_user_id int(9) default 0;

	declare lcl_uusage_string varchar(40) default 0;
	declare lcl_model_log_id_string varchar(40) default 0;
	declare lcl_norm_usage_string varchar(40) default 0;
	declare lcl_model_usage_string varchar(40) default 0;
	declare lcl_adjustment_string varchar(40) default 0;
	declare lcl_adjusted_usage_string varchar(40) default 0;
    declare lcl_building_id_string varchar(40) default 0;
    declare lcl_oat_string varchar(40) default 0;
    declare lcl_base_usage_string varchar(40) default 0;
    declare lcl_start_string varchar(40) default 0;
    declare lcl_end_string varchar(40) default 0;
    declare lcl_service_days_string varchar(40) default 0;
    declare lcl_service_month_id_string varchar(40) default 0;
    declare lcl_service_id_string varchar(40) default 0;
    declare lcl_cost_string varchar(40) default 0;
    declare lcl_dollar_per_mmbtu_string varchar(40) default 0;
    declare lcl_saved_usage_string varchar(40) default 0;
    declare lcl_percent_savings_string varchar(40) default 0;
    declare lcl_monthly_savings_string varchar(40) default 0;
    declare lcl_cumulative_savings_string varchar(40) default 0;
    declare lcl_e_time_string varchar(40) default 0;
    declare lcl_flag_string varchar(40) default 0;
    declare lcl_company_id_string varchar(40) default 0;
    declare lcl_log_user_id_string varchar(40) default 0;
	DECLARE EXIT HANDLER FOR 1451
	-- ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails 
	BEGIN
		select -1 into rv_status_code;
	END;
	select gas.uusage INTO lcl_uusage from gas where gas.id = pv_gas_id;
	select gas.model_log_id into lcl_model_log_id from gas where gas.id = pv_gas_id;
	select gas.norm_usage into lcl_norm_usage from gas where gas.id = pv_gas_id;
	select gas.model_usage into lcl_model_usage from gas where gas.id = pv_gas_id;
    select gas.adjustment into lcl_adjustment from gas where gas.id = pv_gas_id;
    select gas.adjusted_usage into lcl_adjusted_usage from gas where gas.id = pv_gas_id;
    select gas.building_id into lcl_building_id from gas where gas.id = pv_gas_id;
	select gas.oat into lcl_oat from gas where gas.id = pv_gas_id;
    select gas.base_usage into lcl_base_usage from gas where gas.id = pv_gas_id;
    select gas.start into lcl_start from gas where gas.id = pv_gas_id;
    select gas.end into lcl_end from gas where gas.id = pv_gas_id;
    select gas.service_days from gas where gas.id = pv_gas_id;
    select gas.service_month_id into lcl_service_month_id from gas where gas.id = pv_gas_id;
    select gas.service_id into lcl_service_id from gas where gas.id = pv_gas_id;
    select gas.cost into lcl_cost from gas where gas.id = pv_gas_id;
    select gas.dollar_per_mmbtu into lcl_dollar_per_mmbtu from gas where gas.id = pv_gas_id;
    select gas.saved_usage into lcl_saved_usage from gas where gas.id = pv_gas_id;
    select gas.percent_savings into lcl_percent_savings from gas where gas.id = pv_gas_id;
    select gas.cumulative_savings into lcl_cumulative_savings from gas where gas.id = pv_gas_id;
    select gas.e_time into lcl_e_time from gas where gas.id = pv_gas_id;
    select gas.flag into lcl_flag from gas where gas.id = pv_gas_id;
    select gas.company_id into lcl_company_id from gas where gas.id = pv_gas_id;
    select gas.log_user_id INTO lcl_log_user_id from gas where gas.id = pv_gas_id;

	call jsonify_int(lcl_uusage, 'uusage', lcl_uusage_string);
	call jsonify_int(lcl_model_log_id, 'model_log_id', lcl_model_log_id_string);
	call jsonify_decimal(lcl_norm_usage, 'norm_usage', lcl_norm_usage_string);
	call jsonify_decimal(lcl_model_usage, 'model_usage', lcl_model_usage_string);
	call jsonify_int(lcl_adjustment, 'adjustment', lcl_adjustment_string);
	call jsonify_int(lcl_adjusted_usage, 'adjusted_usage', lcl_adjusted_usage_string);
	call jsonify_int(lcl_building_id, 'building_id', lcl_building_id_string);
	call jsonify_decimal(lcl_oat, 'oat', lcl_oat_string);
	call jsonify_int(lcl_base_usage, 'base_usage', lcl_base_usage_string);
	call jsonify_string('start', lcl_start, lcl_start_string);
	call jsonify_string('end', lcl_end, lcl_end_string);
	call jsonify_int(lcl_service_days, 'service_days', lcl_base_usage_string);
	call jsonify_int(lcl_service_month_id, 'service_month_id', lcl_service_month_id_string);
	call jsonify_int(lcl_service_id, 'service_id', lcl_service_id_string);
    call jsonify_decimal(lcl_cost, 'cost', lcl_cost_string);
    call jsonify_decimal(lcl_dollar_per_mmbtu, 'dollar_per_mmbtu', lcl_dollar_per_mmbtu_string);
	call jsonify_int(lcl_saved_usage, 'saved_usage', lcl_saved_usage_string);
	call jsonify_decimal(lcl_percent_savings, 'percent_savings', lcl_percent_savings_string);
	call jsonify_decimal(lcl_monthly_savings, 'monthly_savings', lcl_monthly_savings_string);
    call jsonify_decimal(lcl_cumulative_savings, 'cumulative_savings', lcl_cumulative_savings_string);
	call jsonify_int(lcl_e_time, 'e_time', lcl_e_time_string);
	call jsonify_int(lcl_flag, 'flag', lcl_flag_string);
	call jsonify_int(lcl_company_id, 'company_id', lcl_company_id_string);
	call jsonify_int(lcl_log_user_id, 'log_user_id', lcl_log_user_id_string);	
    select concat("{", lcl_uusage_string, ",", lcl_model_log_id_string, ",", lcl_norm_usage_string, ",", lcl_model_usage_string, ",", lcl_adjustment_string
    , ",", lcl_adjusted_usage_string, ",", lcl_building_id_string, ",", ",",lcl_oat_string, ",",lcl_base_usage_string, ",", lcl_start_string
    , ",",lcl_end_string, ",",lcl_service_days_string, ",", lcl_service_month_id_string, ",", lcl_service_id_string, ",", lcl_cost_string
    , ",", lcl_dollar_per_mmbtu_string, ",", lcl_saved_usage_string, ",", lcl_percent_savings_string, ","
    , lcl_monthly_savings_string, ",", lcl_cumulative_savings_string, lcl_e_time_string, ",", lcl_flag_string, ","
    , lcl_company_id_string, ",", lcl_log_user_id_string, "}'") into rv_gas_json;
    call add_outbound(pv_flag, pv_gas_id, rv_gas_json,  'gas', lcl_outbound_id);
	DELETE FROM gas where id = pv_gas_id AND company_id = lcl_company_id;
    select lcl_outbound_id into rv_status_code;
END//

CREATE OR REPLACE PROCEDURE get_service_month_id_gas(IN pv_gas_id int(9)
                                            , OUT rv_service_month_id int(9)
                                            )
BEGIN
    select gas.service_month_id INTO rv_service_month_id 
    FROM gas WHERE id = pv_gas_id;
END//