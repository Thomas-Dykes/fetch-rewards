import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")
import login
import connector as co
import config


def get_procs(employee_id):
    return call_proc("get_public_procs", [employee_id])


def grant_permission(proc):
    return call_proc("grant_execute_procedure", [config.db_user, config.db_password, proc])


def call_proc(name, params):
    return co.machine({"name": name, "params": params})


def get_employee():
    company_name = config.test_company
    print(f"company_name: {company_name}")
    company_id = get_company_id(company_name)
    print(f"company_id for employee: {company_id}")
    employee_id = get_root_employee(company_id)
    print(f"employee_id: {employee_id}")
    return employee_id


def get_root_employee(company_id):
    proc_name = "get_root_employee"
    params = [company_id, 0]
    jdat = {"name": proc_name, "params": params}
    employee_id = co.machine_add(jdat)
    print(f"employee id: {employee_id}")
    return employee_id


def get_company_id(name):
    proc_name = "get_company_by_name"
    params = [name, 0]
    jdat = {"name": proc_name, "params": params}
    id = co.machine_add(jdat)
    print(f"company id: {id}")
    return id


def main():
    #employee_id = get_employee()
    #procs = get_procs(employee_id)
    procs = get_procs(1)
    
    db_password = config.db_password
    print(f"procs: {procs}")
    with open("./permissions.sql", "w") as f:
        for i in procs:
            name = i["name"]
            statement = f"GRANT EXECUTE ON PROCEDURE pbc.{name} TO 'svc_user'@'localhost' IDENTIFIED by '{db_password}';"
            print(statement)
            f.write(f"{statement}\n")
        f.write(f"FlUSH PRIVILEGES;")
        # grant_permission(name)


if __name__ == "__main__":
    main()
