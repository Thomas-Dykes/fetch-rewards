from os import walk
import time


def walker(pv_path):
    files = []
    p = ""
    fp = []
    for (dirpath, dirnames, filenames) in walk(pv_path):
        files.extend(filenames)
        p = dirpath
        break
    for f in files:
        lcl_path = f"{p}/{f}"
        fp.append(lcl_path)
    return fp


def folder_walker(pv_path):
    p = ""
    dirs = []
    paths = []
    for (dirpath, dirnames, filenames) in walk(pv_path):
        dirs.extend(dirnames)
        p = dirpath
        break
    for d in dirs:
        lcl_path = f"{p}/{d}"
        paths.append(lcl_path)
    return paths


def builder(pv_path, files):
    full_files = []
    for f in files:
        p = f"{pv_path}/{f}"
        full_files.append(p)
    return p


def get_line_count(pv_path):
    f = open(pv_path)
    try:
        content = f.readlines()
    except UnicodeDecodeError:
        print("UnicodeDecode Error")
        content = []

    return len(content)


def main():
    print("start")
    lcl_path = "./"
    paths = [lcl_path]
    file_paths = []
    line_count = 0
    endings = set()
    uncounted = []
    for p in paths:
        lcl_paths = folder_walker(p)
        for l in lcl_paths:
            paths.append(l)
    for pp in paths:
        fps = walker(pp)
        for f in fps:
            file_paths.append(f)
    for f in file_paths:
        flag = 0
        print(f"path: {f}")
        ending = f[-4:]
        ending_three = f[-3:]
        print(f"ending: {ending}")

        endings.add(ending)

        #        if ending == ".txt":
        #            flag = 2
        if ending_three == ".sh":
            flag = 2
        if ending_three == ".py":
            flag = 2
        if ending ==".ts":
            flag = 2
        if ending == ".sql":
            flag = 2
        if ending == ".yml":
            flag = 2
        if ending == ".css":
            flag = 2
        if ending == "html":
            flag = 2
             
        if flag == 2:
            flag = 1
            print(f"ending: {ending}")
            temp_count = get_line_count(f)
            print(f"file path: {f}; count: {temp_count}")
            line_count = line_count + temp_count
        if flag == 0:
            uncounted.append(f)

    count = 0
    total = 0
    interval = 200
    endings = set()
    for i in uncounted:
        lcl_spit = i.split(".")
        if len(lcl_spit) == 2 and lcl_spit[1][0] != "g":
            try:
                end = lcl_spit[1]

                endings.add(end)
            except IndexError:
                pass

            except Exception as e:
                input(f"exception: {e}, occured. ENTER TO CONTINUE")

            print(f"uncounted: {i}")
            count += 1
            if count == interval:
                count = 0
                total += 1
                records = total * interval
                input(f"{records} UNCOUNTED RECORDS ABOVE; ENTER TO CONTINUE ")

    print("\n \nUNCOUNTED FILE TYPES:")
    for e in endings:
        print(f"    ENDING: {e}")
    print("\n \nCOUNTED FILE TYPES:")
    for i in [".txt", ".sh", ".py", ".sql", ".yml"]:
        print(f"    ENDING: {i}")

    print(f" \n Total Line Count: {line_count} \n")

main()