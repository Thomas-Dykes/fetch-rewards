CREATE TABLE IF NOT EXISTS project(
    id int(10) PRIMARY KEY AUTO_INCREMENT,
    name varchar(30),
    description varchar(100),
    pre_period_end_id int(9),
    post_period_start_id int(9),
    post_period_end_id int(9),
    flag int(10),
    e_time int(13),
    log_user_id int(10)
);