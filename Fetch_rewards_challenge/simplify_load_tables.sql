    INSERT INTO receipt(_id
            , bonusPointsEarned
            , bonusPointsEarnedReason
            , createDate
            , dateScanned
            , finishedDate
            , modifyDate
            , pointsAwardedDate
            , pointsEarned
            , purchaseDate
            , purchasedItemCount
            , rewardsReceiptStatus
            , totalSpent
            , userId
            , brandCode
            )
    VALUES(pv_id
            , pv_bonusPointsEarned
            , pv_bonusPointsEarnedReason
            , pv_createDate
            , pv_dateScanned
            , pv_finishedDate
            , pv_modifyDate
            , pv_pointsAwardedDate
            , pv_pointsEarned
            , pv_purchaseDate
            , pv_purchasedItemCount
            , pv_rewardsReceiptStatus
            , pv_totalSpent
            , pv_userId
            , pv_brandCode
    );

        INSERT INTO user
                (_id, 
                state, 
                createdDate, 
                lastLogin, 
                role,
                signUpSource,
                active) 
    VALUES      (pv_id, 
                pv_state, 
                pv_createdDate, 
                pv_lastLogin, 
                pv_role
                pv_signUpSource
                pv_active); 

    INSERT INTO brand(
        _id
        , name
        , barcode
        , brandCode
        , category
        , categoryCode
        , cpg
        , topbrand)
    VALUES(
        pv_id
        , pv_name
        , pv_barcode
        , pv_brandCode
        , pv_category
        , pv_categoryCode
        , pv_cpg
        , pv_topbrand
    );