
import json

# import connector as co
# import config

import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
#   password="yourpassword",
  database="fetch_rewards_request"
)

mycursor = mydb.cursor()

brands = []
def add_brand(id, name, barcode, brandCode, category, categoryCode, cpg, topBrand):
    sql = "INSERT INTO brand(_id, name, barcode, brandCode, category, categoryCode, cpg, topbrand) VALUES(%s, %s, %s, %s, %s, %s, %s, %s)"
    params = (id, name, barcode, brandCode, category, categoryCode, str(cpg), topBrand)
    mycursor.execute(sql, params)
    mydb.commit()

with open("./brands.json/brands.json", "r") as f:
    for line in f.readlines():
        brands.append(json.loads(line))
        id = json.loads(line)["_id"]["$oid"]
        name = json.loads(line)["name"]
        barcode = json.loads(line)["barcode"]
        try:
            brandCode = json.loads(line)["brandCode"]
        except KeyError:
            brandCode = -1
        try: 
            category = json.loads(line)["category"]
        except KeyError:
            category = -1
        try:
            categoryCode = json.loads(line)["categoryCode"]
        except KeyError:
            categoryCode = -1
        cpg = json.loads(line)["cpg"]
        try:
            topBrand = json.loads(line)["topBrand"]
        except KeyError:
            topBrand = -1
        add_brand(id, name, barcode, brandCode, category, categoryCode, cpg, topBrand)

# print(brands[0:5])
