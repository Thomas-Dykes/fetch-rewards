import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config
import numpy as np
# from piecewise import piecewise
#from piecewise import piecewise_plot
import sys

def get_oats_gas_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_oats_gas_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    oats = co.machine(jdat)
    for oat in oats:
        print(f"oat: {oat}")
    return oats


def get_norm_usage_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_norm_usages_gas_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    norm_usages = co.machine(jdat)
    for norm_usage in norm_usages:
        print(f"norm_usage: {norm_usage}")
    return norm_usages

def get_service_month_year_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_service_month_year_gas_between"
    params = [user_id, building_id, service_month_id_start, service_month_id_end]
    jdat = {"name": proc_name, "params": params}
    month_years = co.machine(jdat)
    for month_year in month_years:
        print(f"month_year: {month_year}")
    return month_years


def get_savings_between(user_id, building_id, service_month_id_start, service_month_id_end):
    proc_name = "get_savings_gas_between"
    params = [service_month_id_start, service_month_id_end, user_id, building_id]
    jdat = {"name": proc_name, "params": params}
    savings = co.machine(jdat)
    for saving in savings:
        print(f"savings: {savings}")
    return savings

def get_data(user_id, service_month_id_build_start, service_month_id_build_end, service_month_id_apply_start, service_month_id_apply_end, building_id):
    x_list_pre = get_oats_gas_between(user_id, building_id, service_month_id_build_start, service_month_id_build_end)
    lcl_x1_return = []
    for row in x_list_pre:
        lcl_x1_return.append(float(row["oat"]))

    x_list_post = get_oats_gas_between(user_id, building_id, service_month_id_apply_start, service_month_id_apply_end)
    lcl_x2_return = []
    for row2 in x_list_post:
        lcl_x2_return.append(float(row2["oat"]))

    y_list_pre = get_norm_usage_between(user_id, building_id, service_month_id_build_start, service_month_id_build_end)
    lcl_y1_return = []
    for row3 in y_list_pre:
        lcl_y1_return.append(float(row3["norm_usage"]))
    
    y_list_post = get_norm_usage_between(user_id, building_id, service_month_id_apply_start, service_month_id_apply_end)
    lcl_y2_return = []
    for row4 in y_list_post:
        lcl_y2_return.append(float(row4["norm_usage"]))

    service_month_year_pre = get_service_month_year_between(user_id, building_id, service_month_id_build_start, service_month_id_build_end)
    lcl_s1_return = []
    for row5 in service_month_year_pre:
        lcl_s1_return.append(row5["month"] + "-" + row5["year"])
    service_month_year_post = get_service_month_year_between(user_id, building_id, service_month_id_apply_start, service_month_id_apply_end)
    lcl_s2_return = []
    for row6 in service_month_year_post:
        lcl_s2_return.append(row6["month"] + "-" + row6["year"])

    savings_post = get_savings_between(user_id, building_id, service_month_id_apply_start, service_month_id_apply_end)
    lcl_save_return = []
    for row7 in savings_post:
        lcl_save_return.append(float(row7["monthly_savings"]))

    # print(lcl_x1_return)
    # print(len(lcl_x1_return))
    # print(lcl_x2_return)
    # print(lcl_y1_return)
    # print(len(lcl_y1_return))
    # print(lcl_y2_return)
    # print(lcl_s1_return)
    # print(len(lcl_s1_return))
    # print(lcl_s2_return)
    # print(len(lcl_save_return))
    # print(lcl_save_return)
    dot_a = []
    for i in range(len(lcl_x1_return)):
        pre_base_dict = {'x': 0, 'y': 0, 'm': ''}
        pre_base_dict['x'] = lcl_x1_return[i]
        pre_base_dict['y'] = lcl_y1_return[i]
        pre_base_dict['m'] = lcl_s1_return[i]
        dot_a.append(pre_base_dict)
    

    dot_b = []
    for j in range(len(lcl_x2_return)):
        post_base_dict = {'x': 0, 'y': 0, 'c': 0, 'm': ''}
        post_base_dict['x'] = lcl_x2_return[j]
        post_base_dict['y'] = lcl_y2_return[j]
        post_base_dict['m'] = lcl_s2_return[j]
        post_base_dict['c'] = lcl_save_return[j]
        dot_b.append(post_base_dict)
    x_list = np.array(lcl_x1_return, dtype=float)
    y_list = np.array(lcl_y1_return, dtype=float)
    print("LENGTH OF X:")
    print(len(x_list))
    print("LENGTH OF Y:")
    print(len(y_list))
    path = "./graph.png"

    m = (len(x_list) * np.sum(x_list*y_list) - np.sum(x_list) * np.sum(y_list)) / (len(x_list)*np.sum(x_list*x_list) - np.sum(x_list) ** 2)

    b = (np.sum(y_list) - m *np.sum(x_list))/len(x_list)

    min_x = min(x_list)

    max_x = max(x_list)

    print(f"M: {m}")

    line_a = []
    line_b = []
    min_dict = {'x': 0, 'y': 0}
    min_dict['x'] = min_x
    min_dict['y'] = m*min_x+b
    max_dict = {'x': 0, 'y': 0}
    max_dict['x'] = max_x
    max_dict['y'] = m*max_x+b
    line_a.append(min_dict)
    line_a.append(max_dict)
    line_b = line_a

    print(f"dot_a: {dot_a}")
    print(f"dot_b: {dot_b}")
    print(f"line_a: {line_a}")
    print(f"line_b: {line_b}")

    return {'dot_a': dot_a, 'dot_b': dot_b, 'line_a': line_a, 'line_b': line_b}


if __name__ == "__main__":
    data = []
    user_id = 1
    building_id = 15
    # ssession_number = 6
    # period_start = '2017/04/01'
    # period_end = '2019/10/01'
    # model_id = 55

    get_data(user_id, 567, 578, 579, 596, building_id)