#get_ssessions_api

call add_proc_index(
15
, 'checks authentication, then runs get_ssessions'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["ssession"]'
, 'get_ssessions_api'
, 1
);


#get_ssession_api

call add_proc_index(
16
, 'checks authentication, then runs get_ssession'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["ssession"]'
, 'get_ssession_api'
, 1
);

call add_proc_index(
17
, 'logs a user in'
, 0
, '{"pv_username": "varchar(30)","pv_pwd": "varchar(30)"}'
, '{}'
, '["user", "ssession"]'
, 'login_api'
, 1
);

#delete_ssession_api

call add_proc_index(
18
, 'chceks_authentication, then runs delete_ssession'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_status_code": "int(9)"}'
, '["ssession"]'
, 'delete_ssession_api'
, 1
);

#logout_api

call add_proc_index(
19
, 'checks permissions, then call logout'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["ssession"]'
, 'logout_api'
, 1
);
