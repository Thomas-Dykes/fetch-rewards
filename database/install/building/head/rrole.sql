#get_rrole_api

call add_proc_index(
137
, 'checks authentication, then runs get_rrole'
, 0
, '{"pv_rrole_id": "int(9)"}'
, '{}'
, '["rrole"]'
, 'get_rrole_api'
, 0
);

#get_rroles_api

call add_proc_index(
138
, 'checks authentication, then runs get_rroles'
, 0
, '{"pv_log_employee_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["rrole"]'
, 'get_rroles_api'
, 0
);
