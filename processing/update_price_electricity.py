import csv
import numpy as np
# from piecewise import piecewise
#from piecewise import piecewise_plot
import regressor as piecewise
import sys

sys.path.append("/home/ec2-user/pbc_portal/pbc")

import connector as co
import config


def update_price(price, model_id):
    proc_name = "update_electricity_model_price"
    params = [price, model_id]
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)

def main(price, model_id):

    update_price(price, model_id)
