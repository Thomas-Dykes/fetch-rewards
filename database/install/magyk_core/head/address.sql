#delete_address_api
call add_proc_index(
1
, 'deletes a row from address with the given address_id'
, 0
, '{"pv_address_id": "int(9)", "pv_flag": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_status_code": "int(9)"}'
, '["address"]'
, 'delete_address_api'
, 0
);

#get_addresses_api

call add_proc_index(
2
, 'checks authentication, then runs get_addresses'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["address"]'
, 'get_addresses_api'
, 1
);

#get_addresses_states_api

call add_proc_index(
3
, 'checks authentication, then runs get_addresses_states'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["address", "state"]'
, 'get_addresses_states_api'
, 1
);

#get_addresses_by_state_api

call add_proc_index(
4
, 'checks authentication, then runs get_addresses_by_state'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_state_id": "int(9)"}'
, '{}'
, '["address", "state"]'
, 'get_addresses_by_state_api'
, 1
);

#get_address_api
call add_proc_index(
5
, 'checks authentication, then runs get_address'
, 0
, '{"pv_address_id": "int(9)", "pv_user_id":"int(9)", "pv_ssession_number":"int(9)"}'
, '{}'
, '["address"]'
, 'get_address_api'
, 1
);

#add_address_api

call add_proc_index(
6
, 'checks authentication, then runs add_address'
, 0
, '{"pv_city": "varchar(30)", "pv_flag": "int(3)", "pv_latitude": "decimal(9,6)", "pv_user_id": "int(9)", "pv_longitude":"decimal(9,6)", "pv_ssession_number": "int(9)", "pv_state_id": "int(10)", "pv_street_address": "varchar(100)", "pv_zip_code": "int(10)"}'
, '{"rv_address_id": "int(10)"}'
, '["address"]'
, 'add_address_api'
, 1
);