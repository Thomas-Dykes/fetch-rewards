import time, decimal, sys

import user_log_ut as pl


sys.path.append("/home/ec2-user/pbc_portal/pbc")
# sys.path.append("../../../magyk")
import login
import connector as co
import config
import ggroup_ut as gg

def get_user_ggroup_count():
    proc_name = "get_user_ggroup_count"
    params = [0]
    jdat = {"name": proc_name, "params": params}
    count = co.machine_add(jdat)
    print(f"local count: {count}")
    return count

def get_user_ggroups(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_user_ggroups_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    user_ggroups = co.machine(jdat)
    for user_ggroup in user_ggroups:
        print(f"user_ggroup: {user_ggroup}")
    return user_ggroups

def get_user_ggroup(cookie, pick_user_id, ggroup_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_user_ggroup_api"
    params = [pick_user_id, ggroup_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    user_ggroup = co.machine(jdat)[0]
    print(f"user_ggroup: {user_ggroup}")
    return user_ggroup

def get_users_ggroups(cookie, pick_user_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_users_ggroups_api"
    params = [pick_user_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    users_ggroups = co.machine(jdat)
    for users_ggroup in users_ggroups:
        print(f"users_ggroup: {users_ggroup}")
    return users_ggroups

def get_users_ggroup(cookie, user_id):
    log_user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_users_ggroup_api"
    params = [user_id, log_user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    user_ggroups = co.machine(jdat)
    for user_ggroup in user_ggroups:
        print(f"user_ggroup: {user_ggroup}")
    return user_ggroups

def get_ggroups_users(cookie, ggroup_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_ggroups_users_api"
    params = [ggroup_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    ggroups_users = co.machine(jdat)
    for ggroups_user in ggroups_users:
        print(f"ggroups_user: {ggroups_user}")
    return ggroups_user    

def add_user_ggroup(cookie, pick_user_id, ggroup_id):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "add_user_ggroup_api"
    params = [pick_user_id, ggroup_id, user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    co.machine_add(jdat)

def get_user_ggroups_full(cookie):
    user_id = cookie["user_id"]
    ssession_number = cookie["ssession_number"]
    proc_name = "get_user_ggroups_full_api"
    params = [user_id, ssession_number]
    jdat = {"name": proc_name, "params": params}
    user_ggroups_full = co.machine(jdat)
    for user_ggroup_full in user_ggroups_full:
        print(f"user_ggroup_full: {user_ggroup_full}")
    return user_ggroups_full

def delete_user_ggroup(cookie, user_id, ggroup_id):
    ssession_number = cookie["ssession_number"]
    proc_name = "delete_user_ggroup_api"
    params = [user_id, 0, ggroup_id, ssession_number, 0]
    jdat = {"name": proc_name, "params": params}
    outbound_id = co.machine_add(jdat)
    print(f"outbound_id: {outbound_id}")
    return outbound_id

def delete_user_ggroups():
    ssession_number = cookie["ssession_number"]
    proc_name = "delete_user_ggroups"
    params = []
    jdat = {"name": proc_name, "params": params}
    co.machine(jdat)


def test_delete_user_ggroup_foreign_key_duplicate_error(
    pv_user_id, pv_ggroup_id
):
    print("test delete_user_ggroup needs to have proper return")
    proc_name = "delete_user_ggroup"
    params = [pv_user_id, 0, pv_ggroup_id, 1, 0]
    jdat = {"name": proc_name, "params": params}
    status_code = co.machine_add(jdat)
    print(f"status: {status_code}")
    if status_code == -1:
        return {
            "test": "test_foreign_key_duplicate_error",
            "result": True,
            "table": "user_ggroup",
            "proc_index_id": [196],
        }
    return {
        "test": "test_foreign_key_duplicate_error",
        "result": False,
        "table": "user_ggroup",
        "proc_index_id": [196],
    }


def test_add(cookie, user_id, ggroup_id):
    print("add test")
    add_user_ggroup(cookie, user_id, ggroup_id)
    time.sleep(0.1)
    print(" \n \n ")
    print("get test")
    user_ggroup = get_user_ggroup(cookie, user_id, ggroup_id)
    lcl_user_id = user_ggroup["user_id"]
    lcl_ggroup_id = user_ggroup["ggroup_id"]
    print(f"lcl_user_id: {lcl_user_id}, lcl_ggroup_id: {lcl_ggroup_id}")
    if lcl_user_id == user_id:
        if lcl_ggroup_id == ggroup_id:
            return {
                "test": "test_add_1",
                "result": True,
                "table": "user_ggroup",
                "proc_index_id": [27, 28, 31, 32],
            }

    return {
        "test": "test_add_1",
        "result": False,
        "table": "user_ggroup",
        "proc_index_id": [27, 28, 31, 32],
    }


def test_gets(cookie, user_id, g1, g2):
    user_ggroup_one = {"user_id": -9, "ggroup_id": -9}
    user_ggroup_two = {}

    for i in get_user_ggroups(cookie):
        if i["user_id"] == user_id:
            if i["ggroup_id"] == g1:
                user_ggroup_one = i
            if i["ggroup_id"] == g2:
                user_ggroup_two = i

    add_1 = get_user_ggroup(cookie, user_id, g1)
    add_2 = get_user_ggroup(cookie, user_id, g2)

    if add_1["user_id"] == user_ggroup_one["user_id"]:
        print("match user_id_1")
        if add_1["ggroup_id"] == user_ggroup_one["ggroup_id"]:
            print("match ggroup_id_1")
            if add_2["user_id"] == user_ggroup_two["user_id"]:
                print("match user_id_2")
                print(f"add 2{add_2} \n\n eg: {user_ggroup_two}")
                if add_2["ggroup_id"] == user_ggroup_two["ggroup_id"]:
                    print("match ggroup_id_2")
                    return {
                        "test": "test_gets",
                        "result": True,
                        "table": "user_ggroup",
                        "proc_index_id": [23, 26],
                    }
    return {
        "test": "test_gets",
        "result": False,
        "table": "user_ggroup",
        "proc_index_id": [23, 26],
    }


def get_user_id(username):
    proc_name = "get_user_id"
    params = [username, 0]
    jdat = {"name": proc_name, "params": params}
    return co.machine_add(jdat)


def test_printer(tests):
    print(" \n Test Results \n")
    for test in tests:
        try:
            t_table = test["table"]
            t_test = test["test"]
            t_result = test["result"]
            t_proc_index_id = test["proc_index_id"]
            print(
                f"Table: {t_table}, Proc ID: {t_proc_index_id} Test: {t_test}: Result: {t_result} \n"
            )
        except Exception as e:
            print(f"exception: {e} \n\n\n   Data: {test}")
            time.sleep(5)


def machine(cookie):
    tests = []
    print(" \n \n ")
    e1 = get_user_id("leonardo")
    g1 = gg.add_ggroup(cookie, "user group test 1", "desc 1")
    g2 = gg.add_ggroup(cookie, "user group test 2", "desc 2")

    result = test_add(cookie, e1, g1)
    result_2 = test_add(cookie, e1, g2)

    tests.append(result)
    tests.append(result_2)
    tests.append(test_gets(cookie, e1, g1, g2))
    print("end add tests")

    #    tests.append(test_delete_user_ggroup_foreign_key_duplicate_error(2,2))
    print(" \n \n ")

    # delete user_group
    delete_user_ggroup(cookie, e1, g1)
    delete_user_ggroup(cookie, e1, g2)
    # delete ggroup
    gg.delete_ggroup(cookie, g1)
    gg.delete_ggroup(cookie, g2)

    return tests


def main():
    cookie = login.machine()
    print(f"cookie: {cookie}")
    tests = machine(cookie)
    test_printer(tests)


if __name__ == "__main__":
    main()
