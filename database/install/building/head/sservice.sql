#add_sservice_api
call add_proc_index(
148
, 'checks permission, then runs add_sservice'
, 0
, '{"pv_system_id": "int(9)", "pv_deficiency_description": "varchar(100)", "pv_building_id": "int(9)"
, "pv_record_date": "date", "pv_complete_date": "date", "pv_recommended_action": "varchar(100)"
, "pv_actor": "varchar(20)", "pv_priority_status_id": "int(20)", "pv_user_id": "int(9)"
, "pv_ssession_number": "int(9)"}'
, '{"rv_service_id": "int(9)"}'
, '["sservice"]'
, 'add_sservice_api'
, 1
);

#get_sservice_complete_date_api
call add_proc_index(
149
, 'checks permission, then runs get_sservice_complete_date'
, 0
, '{"pv_sservice_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_sservice_date": "date"}'
, '["sservice"]'
, 'get_sservice_complete_date_api'
, 1
);

#get_building_data_api
call add_proc_index(
150
, 'checks permission, then runs get_building_data'
, 0
, '{"pv_building_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["sservice"]'
, 'get_building_data_api'
, 1
);

#get_sservices_api
call add_proc_index(
151
, 'checks permissions, then runs get_sservices'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["sservice"]'
, 'get_sservices_api'
, 1
);

#get_building_sservices_api
call add_proc_index(
152
, 'checks permissions, then runs get_building_sservices'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_building_id": "int(9)"}'
, '{}'
, '["sservice"]'
, 'get_building_sservices_api'
, 1
);

#get_sservice_api
call add_proc_index(
153
, 'checks permissions, then runs get_sservice'
, 0
, '{"pv_sservice_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["sservice"]'
, 'get_sservice_api'
, 1
);

#add_sservice_a_api
call add_proc_index(
154
, 'checks permission, then runs add_sservice_a'
, 0
, '{"pv_system_id": "int(9)", "pv_deficiency_description": "varchar(100)", "pv_building_id": "int(9)"
, "pv_recommended_action": "varchar(100)"
, "pv_actor": "varchar(20)", "pv_priority_status_id": "int(20)", "pv_user_id": "int(9)"
, "pv_ssession_number": "int(9)"}'
, '{"rv_service_id": "int(9)"}'
, '["sservice"]'
, 'add_sservice_a_api'
, 1
);

#get_sservice_priority_data_api
call add_proc_index(
155
, 'checks permissions, then runs get_sservice_priority_data'
, 0
, '{"pv_user_id": "int(9)"
, "pv_ssession_number": "int(9)"}'
, '{}'
, '["sservice", "priority_status"]'
, 'get_sservice_priority_data_api'
, 1
);

#update_status_api

call add_proc_index(
156
, 'checks permissions, then runs update_status'
, 0
, ''
, ''
, '["sservice", "priority_status"]'
, 'update_status_api'
, 1
);

#delete_sservice_api

call add_proc_index(
157
, 'checks permissions, then runs delete_sservice'
, 0
, '{"pv_flag": "int(3)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)", "pv_sservice_id": "int(9)"}'
, '{"rv_status_code": "int(9)"}'
, '["sservice", "priority_status"]'
, 'delete_sservice_api'
, 1
);


#END