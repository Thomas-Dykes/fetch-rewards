#add_system_type_api
call add_proc_index(
158
, 'checks permission, then runs add_system_type'
, 0
, '{"pv_name": "varchar(30)", "pv_abv": "varchar(10)", "pv_description": "varchar(100)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{}'
, '["system_type"]'
, 'add_system_type_api'
, 1
);

#get_system_types_api
call add_proc_index(
159
, 'checks permissions, then runs get_system_types'
, 0
, '{"pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_building_id": "int(9)"}'
, '["system_type"]'
, 'get_system_types_api'
, 1
);

#get_system_type_api
call add_proc_index(
160
, 'checks permissions, then runs get_system_type'
, 0
, '{"pv_system_type_id": "int(9)", "pv_user_id": "int(9)", "pv_ssession_number": "int(9)"}'
, '{"rv_building_id": "int(9)"}'
, '["system_type"]'
, 'get_system_type_api'
, 1
);


#END