	create TABLE IF NOT EXISTS user(
	id int(9) PRIMARY KEY AUTO_INCREMENT
	, user_log_id int references user_log(id)
    , username varchar(30)
	, e_time int(13)
	, flag int(3)
	, company_id int(9)
    , start_date date
	, log_user_id int(9)
	, rrole_id int(9)
	, INDEX(id)
	, FOREIGN KEY (user_log_id) references user_log(id)
	, UNIQUE KEY(username)
	);
