CREATE TABLE IF NOT EXISTS proc_index (
id int(10) primary key
, name varchar(100)
, description varchar(100)
, j_tables varchar(200)
, j_inputs varchar(2000)
, j_outputs varchar(200)
, public int(1)
, flag int(3)
, e_time int(13)
, database_name varchar(100)
, CHECK(JSON_VALID(j_tables))
, CHECK(JSON_VALID(j_inputs))
, CHECK(JSON_VALID(j_outputs))
);